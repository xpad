/**
* \file calibration.cpp
* \brief 
* \author Patrick BREUGNON
* \version 1.0
* \date April 20 2009
 */

#include "calibration.h"
#include "defines.h"
#include "usbwrap.h"
#include "utilities.h"
#include "barrette.h"
#include "cyclone.h"

#ifdef USE_ROOT_API
#include "RootAPI.h"
#endif

#include "time.h"


#ifdef CALIB_MSG
#define CALIB_MSG(fmt,args...) printf("\033[22;33mcalib_msg :  " fmt,##args)  
#else
#define CALIB_MSG(fmt,args...)
#endif


#define SAVE_ITH_FILE 	0
#define SAVE_DACL_FILE 	0

#define ITH			0
#define DACL			1
#define COUNT_MATRIX		2
#define DACLVAL_MATRIX		3

#define NBMODULES		1
#define READJUST		0

#define DECREASE		0	
#define INCREASE		1

#define NPRINT_DATA	0
#define PRINT_DATA	1

using namespace std;


string file ;
string fileSelected ;
char ith_mess[500] ;

int data_matrix[120*8][80*7] ; 
int noisy_matrix[120*8][80*7] ; 
int cmatrix[64][120*8][80*7] ;
int dacl_val[120*8][80*7] ;

int count_matrix[64][120*8][80*7] ;
int daclval_matrix[64][120*8][80*7] ;

int noisy_level[2] ;
int noisy_step[64] ;
int max_ith ;

struct timeval start ; 

extern int MaxModuleReady;

extern int cmos_dis[8][7] ;
extern int ithh_val[8][7] ;
extern int vadj_val[8][7] ;
extern int vref_val[8][7] ;
extern int imfp_val[8][7] ; 
extern int iota_val[8][7] ;
extern int ipre_val[8][7] ;
extern int ithl_val[8][7] ;
extern int itune_val[8][7]  ;
extern int ibuffer_val[8][7] ;
extern int ith_return[8][7] ;

struct struct_imageUS
	{
	int integration_time ;
	int index_img ;
	int nb_image ; 
	int Gate_Mode;
	int FastReadout ;
	} imageUS = {500,0,1,0,0} ; 

	
struct struct_readImg
	{
	int IndexImg ;
	int DataDelay ;
	int FastReadout ;
	}  readImg = {0,8000,0} ;
	
	
struct struct_ith
	{
	int val[7] ;
	int found[7] ;
	int cnt[7] ;
	}  ;

struct struct_ith ith[8] ;

#ifdef USE_ROOT_API	
int Local_ITHL_Calibration(CQuickUsb *wqusb,int *module_ready,int ith_min,int ith_max,int gate_time,string PathAndfileName,TCanvas *mClone,int fast_readout)
#else 
int Local_ITHL_Calibration(CQuickUsb *wqusb,int *module_ready,int ith_min,int ith_max,int gate_time,string PathAndfileName,int fast_readout)
#endif 
{
int i ;
char cindex[10] ;
string sindex ;
string title ;
int pmodule ;
int result = 0 ;

#ifdef USE_ROOT_API	
	CALIB_MSG("Local_ITHL_Calibration ith_min = %d ith_max = %d  with ROOT_API\n",ith_min,ith_max) ;
#else 
	CALIB_MSG("Local_ITHL_Calibration ith_min = %d ith_max = %d \n",ith_min,ith_max) ;
#endif
	gettimeofday(&start,NULL) ;
	reset_cmatrix( ) ;
	reset_matrix(data_matrix) ;
	//for (j=0;j<8;j++)	status[j] = 0 ;
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			MaxModuleReady = pmodule+1 ;
		}
	//WriteFlatConfig(wqusb,wmodule,0x7f,ALL_CHIP,0x0101) ;
	//SendConfigG_DefaultValue(wqusb, wmodule) ;
	ExtractFileName(PathAndfileName.c_str(),&file) ;
	max_ith = ith_max ;
	imageUS.integration_time = gate_time ;
	imageUS.FastReadout = fast_readout ;
	readImg.FastReadout =  fast_readout ;
	for (i=0;i<(ith_max- ith_min);i++)
		{
		CALIB_MSG("Configure modules with Ith val = %d  \n",(ith_max-i)) ;
		SendWriteConfigGToModuleReady(wqusb,module_ready,ALL_CHIPS,ITHL,(ith_max-i),fast_readout) ;
		CALIB_MSG("Image us integration time = %d Index img = %d Nb_Image = %d\033[22;30m\n",imageUS.integration_time,imageUS.index_img,imageUS.nb_image) ;
		ExposeModuleReady(wqusb,module_ready,imageUS) ;
		CALIB_MSG("Download image corresponding to ITH = %d \033[22;30m\n",(ith_max-i)) ;
		reset_matrix(data_matrix) ;
		result = ReadImageFromModuleReady(wqusb,module_ready,readImg) ;
		if (result < 0)
		   {
		   // Si failed, recharge ith à la valeur précédente puis prise de l'image
		   CALIB_MSG("Reload image for ITH = %d \n",(ith_max-i)) ;
		   if (i > 0)
		   	i = i - 1 ;
		   }
		else
		   {
		   //correct_12b_matrix(data_matrix) ;
		   sprintf(cindex,"%d",(ith_max-i)) ;
		   sindex.clear() ;
		   sindex.insert(0,cindex) ;
		   fileSelected = PathAndfileName + "/" + file  + "_ITH_" + sindex + ".dat" ;
		   //sprintf(fileSelected,"%s/%s_DACL_%d.dat",PathAndfileName,file, i) ;
		   #ifdef USE_ROOT_API 
		   	TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
		   	title = "Ith = " + sindex ;
		   	RootHistoHitConfigFillPlotMatrix(mClone,h2,data_matrix,title) ;
		   	delete(h2) ;
		   #endif
		   if (SAVE_ITH_FILE)
			{
			CALIB_MSG("Save image to %s \033[22;30m\n",fileSelected.c_str()) ;
			SaveMatrixToFile(fileSelected,data_matrix) ;
			}
		   CopyMatrix(data_matrix,cmatrix[i]) ;
		   }

		}
	SaveCalibScanValueToOutputFile(ITH,(ith_max- ith_min),cmatrix,PathAndfileName) ;
	CALIB_MSG("Searching for ITH values\033[22;30m\n") ;
	Search_ITHL(ith_min,ith_max,module_ready,cmatrix) ;

	Load_Ith_Adjusted(wqusb,module_ready,ith,fast_readout) ;
	CALIB_MSG("*************************\033[22;30m\n") ;
	CALIB_MSG("Reload ith ajusted ended \033[22;30m\n") ;
	CALIB_MSG("*************************\033[22;30m\n") ;

	
return 0 ;
}




void Search_ITHL(int ith_min, int ith_max,int *module_ready,int cmatrix[][120*8][80*7])
{
int pchip ;
int pith_val ;
int chip_cnt[7] ;
int i,j ;
int prow ;
int pcol ;
int pmodule ;
int x ,y ;
int noisy_pix[30] ;


	
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		for (pchip = 0;pchip < 7; pchip++)
			{
			ith[pmodule].val[pchip] = ith_max ;
			ith[pmodule].found[pchip] = 0 ;
			ith[pmodule].cnt[pchip] = 0 ;
			chip_cnt[pchip] = 0 ;
			}
		}
	
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			for (pchip = 0;pchip < 7; pchip++)
				{
				for (i=0;i<(ith_max- ith_min);i++)
					{
					//pith_val = (ith_max- ith_min) - i ;	
					pith_val =  i ;	
					noisy_pix[i] = 0 ;
					for (prow = 0;prow < 120; prow++)
						{
						for (pcol = 0;pcol < 80; pcol++)
							{
							x = pcol+(pchip*80) ; 
							y = prow+(pmodule*120) ;
							//if (cmatrix[pith_val][y][x] > 10) ith[pmodule].cnt[pchip]++ ;
							if (cmatrix[pith_val][y][x] > 0) noisy_pix[i]++ ;
							
							}
						}
					//CALIB_MSG("Module = %d pchip =  %d : ith = %d  : cnt = %d \033[22;30m\n",(pmodule+1),(pchip+1),(ith_max-i),noisy_pix[i]) ;	
					//if ((ith[pmodule].cnt[pchip] > 4800) & (ith[pmodule].found[pchip] != 1))
					if ((noisy_pix[i] > 4800) & (ith[pmodule].found[pchip] != 1))
						{
						ith[pmodule].cnt[pchip] = noisy_pix[i] ;
						ith[pmodule].val[pchip] = ith_max-i ;
						ithl_val[pmodule][pchip] = ith[pmodule].val[pchip] ;
						ith[pmodule].found[pchip] = 1 ;
						/*
						CALIB_MSG("Module = %d pchip =  %d : ith = ",(pmodule+1),(pchip+1)) ;
						for (j=0;j<(i+1);j++)
							printf(" %d ",(ith_max-j)) ;
						printf("\033[22;30m\n") ;
						
						CALIB_MSG("Module = %d pchip =  %d : cnt = ",(pmodule+1),(pchip+1)) ;
						for (j=0;j<(i+1);j++)
							printf(" %d ",noisy_pix[j]) ;
						printf("\033[22;30m\n") ;
						*/
						//CALIB_MSG("Module = %d pchip =  %d : cnt = %d \033[22;30m\n",(pmodule+1),(pchip+1),noisy_pix[j] ) ;
						i = (ith_max- ith_min) ;
						}
					
					}
				}
			}
		}
		
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			CALIB_MSG("Module_%d chip_cnt %d %d %d %d %d %d %d  \033[22;30m\n", (pmodule+1),ith[pmodule].cnt[0], ith[pmodule].cnt[1], ith[pmodule].cnt[2], ith[pmodule].cnt[3], ith[pmodule].cnt[4],ith[pmodule].cnt[5], ith[pmodule].cnt[6]) ;
			CALIB_MSG("Module_%d ith.val %d %d %d %d %d %d %d  \033[22;30m\n", (pmodule+1),ith[pmodule].val[0],ith[pmodule].val[1],ith[pmodule].val[2],ith[pmodule].val[3],ith[pmodule].val[4],ith[pmodule].val[5],ith[pmodule].val[6]) ;
			}
		}
	
		
return ;
}

int Load_Ith_Adjusted(CQuickUsb *wqusb,int *module_ready,struct struct_ith *ithl,int fastReadout) 
{
int i ;
int pmodule ;
int wmodule ;
int module_msk = 0 ;

	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1; 
			module_msk = 1 << pmodule ;
			for (i=0;i<MAXCHIP;i++)
			   {
			   if (fastReadout)
			       WriteConfigG(wqusb,module_msk,i,ITHL,ithl[pmodule].val[i]) ;
			   else
			       WriteConfigG(wqusb,wmodule,i,ITHL,ithl[pmodule].val[i]) ;
			   }
			}
		}

return 0 ;
}


#ifdef USE_ROOT_API
int Local_DACL_Calibration(CQuickUsb *wqusb,int *module_ready,int walgo,int gate_time,string PathAndfileName,TCanvas *mClone,int noise_accepted,int fast_readout,int dec_dacl_val)
#else
int Local_DACL_Calibration(CQuickUsb *wqusb,int *module_ready,int walgo,int gate_time,string PathAndfileName,int noise_accepted,int fast_readout,int dec_dacl_val)
#endif
{
// [15..9] => Not used \033[22;33m\n
// [8..3]   => DACL 6bits \033[22;33m\n
// [2]       => Disable Analog Preamp \033[22;33m\n
// [1]       => Enable analog or Digital. \033[22;33m\n ‏
// [0]       => Enable compter \033[22;33m\n

int config_val = 0x1 ;
int Flat_val  ;
char cindex[10] ;
string sindex ;
string title ;
int pmodule ;
int wmodule ;
int pdacl ;
int result = 0 ;


/***************************************************************************************/
/***************************************************************************************/

	reset_cmatrix( ) ; 	
	ExtractFileName(PathAndfileName.c_str(),&file) ;
	//for (j=0;j<8;j++)	status[j] = 0 ;

	imageUS.integration_time = gate_time ;
	imageUS.FastReadout = fast_readout ;
	readImg.FastReadout =  fast_readout ;
		for (pdacl=0;pdacl<64;pdacl++)
			{
			Flat_val = config_val | (pdacl << 3)  ;
			CALIB_MSG("Send FlatConfig val = 0x%x corresponding to DACL = %d -- status ",Flat_val,pdacl) ;
			//for (j=0;j<8;j++) printf("%d  ",status[j]) ;
			printf("\033[22;30m\n") ;
			SendFlatConfigValToModuleReady(wqusb,module_ready,Flat_val,fast_readout) ;
   			CALIB_MSG("Image us integration time = %d Index img = %d Nb_Image = %d\033[22;30m\n",imageUS.integration_time,imageUS.index_img,imageUS.nb_image) ;
			ExposeModuleReady(wqusb,module_ready,imageUS) ;
			CALIB_MSG("Download image corresponding to DACL = %d \033[22;30m\n",pdacl) ;
			reset_matrix(data_matrix) ;
			result = ReadImageFromModuleReady(wqusb,module_ready,readImg) ;
			if (result < 0)
			   {
			    CALIB_MSG("Reload image for DACL = %d \n",pdacl) ;
			    if (pdacl > 0)
			    	pdacl = pdacl - 1 ;
			    }
			else
			   {
			    //correct_12b_matrix(data_matrix) ;
			   sprintf(cindex,"%d", pdacl) ;
			   sindex.clear() ;
			   sindex.insert(0,cindex) ;
			   fileSelected = PathAndfileName + "/" + file  + "_DACL_" + sindex + ".dat" ;
			   //sprintf(fileSelected,"%s/%s_DACL_%d.dat",PathAndfileName,file, i) ;
			   #ifdef USE_ROOT_API
			 	TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
			   	title = "DACL = " + sindex ;
			   	RootHistoHitConfigFillPlotMatrix(mClone,h2,data_matrix,title) ;
			   	delete(h2) ;
			   #endif
			   CopyMatrix(data_matrix,cmatrix[pdacl]) ;
			
			   if (SAVE_DACL_FILE)
				{
				CALIB_MSG("Save image to %s \033[22;30m\n",fileSelected.c_str()) ;
				SaveMatrixToFile(fileSelected,data_matrix) ;
				}
			  /*
			   if (pdacl == 0)  
				{
				fileSelected = PathAndfileName + "/" + file  + "_noisy_level0.dat" ;
				noisy_level[0] = EstimateNoisyPixel(module_ready,data_matrix,noisy_matrix) ;
				SaveMatrixToFile(fileSelected,noisy_matrix) ;
				}
			  */
			   }
			//CALIB_MSG("fin de boucle pdacl = %d\n",pdacl) ;
			}
			
	SaveCalibScanValueToOutputFile(DACL,(pdacl+1),cmatrix,PathAndfileName) ;
	CALIB_MSG("Searching for DACL value\033[22;30m\n") ;
	Search_DACL(cmatrix,module_ready,walgo,noise_accepted,dec_dacl_val) ;
	
	#ifdef USE_ROOT_API
		TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
		title = "DACL result " ;
		RootHistoHitConfigFillPlotMatrix(mClone,h2,dacl_val,title) ;	
		delete(h2) ;
	#endif

	
	if (dec_dacl_val == 0)
		{
		if ((walgo == LOCAL_NOISE_CALIB_FROM_MAXCNT) || (walgo == LOCAL_NOISE_CALIB_FROM_FIRST_NOISY)) 
			{
			#ifdef USE_ROOT_API
		  	noisy_level[1] = ReAdjustDACL(wqusb,module_ready,gate_time,dacl_val,PathAndfileName,walgo,mClone) ;
			#else
		  	noisy_level[1] = ReAdjustDACL(wqusb,module_ready,gate_time,dacl_val,PathAndfileName,walgo) ;
			#endif	
			}
		}

	CALIB_MSG("DACL calibration ended\033[22;30m\n") ;

	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1 ;
			SaveDACLToConfigFile(PathAndfileName,pmodule,dacl_val) ;
			GenerateMasterFile(PathAndfileName,pmodule) ;
			} 
		}
		
	CALIB_MSG("Create DACL files and master file\033[22;30m\n") ;
	SaveDACLToDataFile(PathAndfileName,dacl_val) ;
	CALIB_MSG("Calibration ended, elapsed_time to make calibration = %dmn\033[22;30m\n",elapsed_time_s(start)/60) ;
	CALIB_MSG("Calibration data are stored in folder %s\033[22;30m\n",PathAndfileName.c_str()) ;

	
return 0 ;
}



void Search_DACL(int cmatrix[][120*8][80*7],int *module_ready,int walgo,int noise_accepted,int dec_dacl_val)
{
#define MAX_SCURVE	64
int pchip ;
int i ;
int prow ;
int pcol ;
int s_diff[64] ;
int noise = 20;
int pmodule ;
int wmodule ;
int x ,y ;
int max_val = 0 ;
int max_val_index = 0 ;

	
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1; 	
			for (pchip = 0;pchip < MAXCHIP; pchip++)
				{
				for (prow = 0;prow < ROW_NB; prow++)
					{
					for (pcol = 0;pcol < MAXCOL; pcol++)
						{
						x = pcol+(pchip*MAXCOL) ;
						y = prow+(pmodule*ROW_NB) ;
						dacl_val[y][x] = 63 ;	
						/**********************************************************************/
						/*			LOCAL_NOISE_CALIB_FROM_ZERO				*/
						/*********************************************************************/
						if (walgo == LOCAL_NOISE_CALIB_FROM_ZERO)
							{
				 			for (i=0;i<(MAX_SCURVE-1);i++)
								{
								dacl_val[y][x] = i ;
								//if((cmatrix[i][y][x]  == 0)  && (cmatrix[i+1][y][x] > 0) && (cmatrix[i+2][y][x] > cmatrix[i+1][y][x] ))  
								if((cmatrix[i][y][x]  == 0)  && (cmatrix[i+1][y][x] > 0))  
									{
									i = MAX_SCURVE ;
									}
								}
							}

						/**********************************************************************/
						/*			LOCAL_NOISE_CALIB_FROM_MAXCNT			*/
						/*********************************************************************/
						if (walgo == LOCAL_NOISE_CALIB_FROM_MAXCNT)
							{
							max_val = 0 ;
							max_val_index = 63 ;
				 			for (i=0;i<MAX_SCURVE;i++)
								{
								if(cmatrix[i][y][x] > max_val)
									{  
									max_val = cmatrix[i][y][x] ;
									max_val_index = i ;
									}
								}
							dacl_val[y][x] = max_val_index - dec_dacl_val ;
							//dacl_val[y][x] = max_val_index  ; // dec_dacl_val forced to 0
							}

						/********************************************************************************/
						/*			LOCAL_NOISE_CALIB_FROM_FIRST_NOISY				*/
						/********************************************************************************/
						if (walgo == LOCAL_NOISE_CALIB_FROM_FIRST_NOISY)
							{
				 			for (i=0;i<MAX_SCURVE;i++)
								{
								dacl_val[y][x] = i ;
								if (i < MAX_SCURVE-1)
									{
								
									//if((cmatrix[i][y][x]  == 0)  && (cmatrix[i+1][y][x] > 0)  &&  (cmatrix[i+2][y][x] > 0))  
									if((cmatrix[i][y][x]  > 100)  && (cmatrix[i+1][y][x] > 100))
										i = MAX_SCURVE ;
									}
								}
							}
							
						/******************************************************/
						/*			LOCAL_BEAM_CALIB			   */
						/*****************************************************/
						if (walgo == LOCAL_BEAM_CALIB)
							{
							for (i=0;i<(MAX_SCURVE-1);i++)
								{
								//s_diff[i] = s_curve[i+1] - s_curve[i];
								s_diff[i] = cmatrix[i+1][y][x] - cmatrix[i][y][x]  ;
								}
				 			// find dacl
            						for(i = 0; i<(MAX_SCURVE-1); i++)
            							{
               				 			if( (i>0) && (i<(MAX_SCURVE-2) ))
                							{
                   				 			if( (s_diff[i]>s_diff[i+1]) && (s_diff[i]>s_diff[i-1]) && (s_diff[i]>noise_accepted) )
                        						break;
                							}
                						else if(i == 0)
               						 		{
                   				 			if( (s_diff[0]>s_diff[1]) && (s_diff[0]>noise) )
                       						 	break;
                							}   
               				 			else
               						 		{
                   				 			if(s_diff[i]>noise)
                    								{
                        							i++;
                        							break;
                    								}
                							}
            							}
							dacl_val[y][x] = i;	
							}	
						}
					}
				}
			}
		}

return ;
}


#ifdef USE_ROOT_API
int ReAdjustDACL(CQuickUsb *wqusb,int *module_ready,int gate_time,int dacl_val[][80*7],string PathAndfileName,int walgo,TCanvas *mClone)
#else
int ReAdjustDACL(CQuickUsb *wqusb,int *module_ready,int gate_time,int dacl_val[][80*7],string PathAndfileName,int walgo)
#endif
{
#define DACL_0	0
#define DACL_63	63
int pmodule ;
char cindex[10] ;
string sindex ;
string title_1 ;
string title_2 ;
int step = 0 ;
int pnoisy = (120*8*80*7) - 1   ;
int previous_pnoisy = (120*8*80*7)  ;
int nb_pixel_DACL_0[64] ;
int nb_pixel_DACL_63[64] ;
int nb_pixel_noisy[64] ;
int nb_pixel_corrected[64] ;
int dacl_all_0[120*8][80*7] ;
int i = 0 ;
int gate_modified = 0 ;


	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			MaxModuleReady = pmodule+1 ;
		}

	
	memset(dacl_all_0,0,sizeof(dacl_all_0)) ;
	CALIB_MSG("Load DACL = 0  and Estimate NoisyPix \033[22;30m\n") ;
	//nb_pixel_DACL_0[0] = CalculatePixelEqualToSpecificValue(module_ready,dacl_val,DACL_0) ;
	nb_pixel_DACL_0[0] = CalculatePixelEqualToSpecificDACLValue(module_ready,dacl_val,data_matrix,DACL_0,NPRINT_DATA) ;
	//nb_pixel_DACL_63[0] = CalculatePixelEqualToSpecificValue(module_ready,dacl_val,DACL_63) ;
	nb_pixel_DACL_63[0] = CalculatePixelEqualToSpecificDACLValue(module_ready,dacl_val,data_matrix,DACL_63,NPRINT_DATA) ;
	nb_pixel_noisy[0] = LoadDACLTabAndEstimateNoisyPix(wqusb,module_ready,gate_time,dacl_all_0) ;
	nb_pixel_corrected[0] = 0 ;
	CALIB_MSG("Initial ajustment \033[22;30m\n") ;
	CALIB_MSG("Nb_DACL_0 = %d ; Nb_DACL_63 = %d ; nb of noisy pixel with DACL_0 => %d \033[22;30m\n",nb_pixel_DACL_0[0],nb_pixel_DACL_63[0],nb_pixel_noisy[0]) ;
		

	while ((pnoisy > nb_pixel_noisy[0]) & (step<35))
		{
		step++ ;
		CALIB_MSG("Step_%d : Load DACL Tab and Estimate NoisyPix \033[22;30m\n",step) ;
		nb_pixel_noisy[step] = LoadDACLTabAndEstimateNoisyPix(wqusb,module_ready,gate_time,dacl_val) ;
		pnoisy = nb_pixel_noisy[step] ; 
		//sprintf(fileSelected,"%s/%s_DACL_%d.dat",PathAndfileName,file, i) ;
		#ifdef USE_ROOT_API
			TH2F *h2_1 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
			TH2F *h2_2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
			sprintf(cindex,"%d", step) ;
			sindex.clear() ;
			sindex.insert(0,cindex) ;
			title_1 = "ReAdjust step_" + sindex ;
			sprintf(cindex,"%d", nb_pixel_noisy[step] ) ;
			sindex.clear() ;
			sindex.insert(0,cindex) ;
			title_1 = title_1 + " : noisy_pixel = " + sindex ;
			Root2HistoHitConfigFillPlotMatrix(mClone,h2_1,data_matrix,title_1,h2_2,dacl_val,"DACL ") ;
		#endif
		sprintf(cindex,"%d", step ) ;
		sindex.clear() ;
		sindex.insert(0,cindex) ;
		//RootHistoHitConfigFillPlotMatrix(mClone,h2,data_matrix,title) ;
		CALIB_MSG("Step_%d : Save data_matrix and dacl_val to file \n",step) ;
		fileSelected = PathAndfileName + "/" + file  + "_data_readj_Step_" + sindex + ".dat" ;
		SaveMatrixToFile(fileSelected,data_matrix) ;
		fileSelected = PathAndfileName + "/" + file  + "_dacl_readj_Step_" + sindex + ".dat" ;
		SaveMatrixToFile(fileSelected,dacl_val) ;
		if (walgo == LOCAL_NOISE_CALIB_FROM_FIRST_NOISY)
			{
			if ((step > 0) && (step < 5))
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,DECREASE) ;
			if ((step >= 5) && (step < 10))
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,INCREASE) ;
			if ((step >= 10) && (step < 15))
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,DECREASE) ;
			if ((step >= 15) && (step < 20))
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,INCREASE) ;
			if ((step >= 20) && (step < 25))
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,DECREASE) ;
			if ((step >= 25) && (step < 30))
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,INCREASE) ;
			if (step >= 30) 
				nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,DECREASE) ;
			}
		else
			nb_pixel_corrected[step] = ModifyDACLStepByStep(module_ready,data_matrix,dacl_val,DECREASE) ;
			
		printf_intermediate_result(step,nb_pixel_noisy,nb_pixel_corrected) ;
		CopyMatrix(data_matrix,count_matrix[step]) ;
		CopyMatrix(dacl_val,daclval_matrix[step]) ;
		SaveCalibScanValueToOutputFile(COUNT_MATRIX,step,count_matrix,PathAndfileName) ;
		SaveCalibScanValueToOutputFile(DACLVAL_MATRIX,step,daclval_matrix,PathAndfileName) ;
		#ifdef USE_ROOT_API
			delete(h2_1) ;
			delete(h2_2) ;
		#endif
		if (walgo != LOCAL_NOISE_CALIB_FROM_FIRST_NOISY)
			{
			if (step > 20) step = 35 ;
			}
		}

#undef DACL_0
#undef DACL_63
return pnoisy ;
}

int printf_intermediate_result(int step,int *nb_pixel_noisy,int *nb_pixel_corrected)
{
int i ;
	CALIB_MSG("Step_%d : nb_pixel_noisy          => ",step) ;
	for (i=0;i<(step+1);i++)
		printf("%3d ",nb_pixel_noisy[i]) ;
	printf("\n") ;
	CALIB_MSG("Step_%d : nb_pixel_corrected   => ",step ) ;
	for (i=0;i<(step+1);i++)
		printf("%3d ",nb_pixel_corrected[i]) ;
	printf("\n") ;
}

int LoadDACLTabAndEstimateNoisyPix(CQuickUsb *wqusb,int *module_ready,int gate_time,int dacl_val[][80*7])
{
int nb_pixel_noisy = 0 ;

		CALIB_MSG("=> Reload DACL values \033[22;30m\n") ;
		LoadDACLFromTabToModuleReady(wqusb,module_ready,dacl_val) ;
		imageUS.integration_time = gate_time ;
		imageUS.nb_image = 1 ;
   		CALIB_MSG("=> Image us integration time = %d Index img = %d Nb_Image = %d\033[22;30m\n",imageUS.integration_time,imageUS.index_img,imageUS.nb_image) ;
		ExposeModuleReady(wqusb,module_ready,imageUS) ;
		reset_matrix(data_matrix) ;
		CALIB_MSG("=> Read Image From Module Ready \n") ;
		ReadImageFromModuleReady(wqusb,module_ready,readImg) ;
		nb_pixel_noisy = CalculateNoisyPixel(module_ready,data_matrix) ;
return nb_pixel_noisy ;
}

int ModifyDACLStepByStep(int *module_ready,int datamatrix[][80*7],int daclmatrix[][80*7], int dec_or_inc)
{
#define NOISE_ACCEPTED		0
int nb_pixel_corrected = 0 ;
int pmodule ;
int x, y;
int pchip ;
int prow ;
int pcol ;
int nb_pixel_dacl_0 = 0 ;
int nb_pixel_dacl_63 = 0 ;

	
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)
			{
			for (pchip = 0;pchip < MAXCHIP; pchip++)
				{
				for (prow = 0;prow < ROW_NB; prow++)
					{
					for (pcol = 0;pcol < MAXCOL; pcol++)
						{
						x = pcol+(pchip*MAXCOL) ;
						y = prow+(pmodule*ROW_NB) ;		
						if (daclmatrix[y][x] == 0) nb_pixel_dacl_0++ ;
						if (daclmatrix[y][x] == 63) nb_pixel_dacl_63++ ;
						if (dec_or_inc == DECREASE)
							{
							if ((datamatrix[y][x] > NOISE_ACCEPTED) && (daclmatrix[y][x] > 0))
								{
								nb_pixel_corrected++ ;
								daclmatrix[y][x] = daclmatrix[y][x] - 1 ;
								}
							}
						else if (dec_or_inc == INCREASE)
							{
							if ((datamatrix[y][x] == 0)  && (daclmatrix[y][x] < 63))
								{
								nb_pixel_corrected++ ;
								daclmatrix[y][x] = daclmatrix[y][x] + 1 ;
								}
							}
						}
					}
				}
			}
		}
		
	if (dec_or_inc == DECREASE)
		CALIB_MSG("Decrease DACL StepByStep => nb_pixel_dacl_0 = %d nb_pixel_dacl_63 = %d nb_pixel_corrected = %d\n",nb_pixel_dacl_0,nb_pixel_dacl_63,nb_pixel_corrected) ;
	else if (dec_or_inc == INCREASE)
		CALIB_MSG("Increase DACL StepByStep => nb_pixel_dacl_0 = %d nb_pixel_dacl_63 = %d nb_pixel_corrected = %d\n",nb_pixel_dacl_0,nb_pixel_dacl_63,nb_pixel_corrected) ;

return nb_pixel_corrected ;
} 
				
		
int SaveCalibScanValueToOutputFile(int wcalib,int max_tab,int cmatrix[][(120*8)][80*7],string PathAndfileName)
{
FILE *pFile = NULL ;
int pcol ;
int prow ;
int i ;
int write2file = 0 ;

	
	if (wcalib == ITH)
		{
		CALIB_MSG("Writing ITH scan file\033[22;30m\n") ;
		fileSelected = PathAndfileName + "/" + file  + "_ITH_SCAN" + ".dat" ;
		ExtractFileName(PathAndfileName.c_str(),&file) ;
		pFile = fopen(fileSelected.c_str(),"w") ; 
		if (pFile == NULL )  return -1 ;
		for (prow = 0;prow < 120*8; prow++)
			{
			for (pcol = 0;pcol < (80*7); pcol++)
				{
				fprintf(pFile,"%d %d ",(prow+1),(pcol+1)) ;
				for (i=0;i<max_tab;i++)
					fprintf(pFile,"%d ",cmatrix[i][prow][pcol])  ;
				fprintf(pFile,"\n") ;
				}
			}
		}
	if (wcalib == DACL)
		{
		CALIB_MSG("Writing DACL scan file\033[22;30m\n") ;
		fileSelected = PathAndfileName + "/" + file  + "_DACL_SCAN" + ".dat" ;
		ExtractFileName(PathAndfileName.c_str(),&file) ;
		pFile = fopen(fileSelected.c_str(),"w") ; 
		if (pFile == NULL )  return -1 ;
		for (prow = 0;prow < 120*8; prow++)
			{
			for (pcol = 0;pcol < (80*7); pcol++)
				{
				fprintf(pFile,"%d %d ",(prow+1),(pcol+1)) ;
				for (i=0;i<max_tab;i++)
					fprintf(pFile,"%d ",cmatrix[i][prow][pcol])  ;
				fprintf(pFile,"\n") ;
				}
			}
		}
	if (wcalib == COUNT_MATRIX)
		{
		CALIB_MSG("Writing COUNT_MATRIX file\033[22;30m\n") ;
		fileSelected = PathAndfileName + "/" + file  + "_COUNT_MATRIX" + ".dat" ;
		ExtractFileName(PathAndfileName.c_str(),&file) ;
		pFile = fopen(fileSelected.c_str(),"w") ; 
		if (pFile == NULL )  return -1 ;
		write2file = 0 ; 
		for (prow = 0;prow < 120*8; prow++)
			{
			for (pcol = 0;pcol < (80*7); pcol++)
				{
				/*
				for (i=0;i<max_tab;i++) 
					{
					if (cmatrix[i][prow][pcol] > 0) 
						write2file = 1; 
					}
				if (write2file == 1)
					{
				*/	
				fprintf(pFile,"%d %d ",(prow+1),(pcol+1)) ;
				for (i=0;i<max_tab;i++)
					fprintf(pFile,"%d ",cmatrix[i][prow][pcol])  ;
				fprintf(pFile,"\n") ;
				write2file = 0 ;
				//	}
				}
			}
		}
	if (wcalib == DACLVAL_MATRIX)
		{
		CALIB_MSG("Writing DACL_MATRIX file\033[22;30m\n") ;
		fileSelected = PathAndfileName + "/" + file  + "_DACLVAL_MATRIX" + ".dat" ;
		ExtractFileName(PathAndfileName.c_str(),&file) ;
		pFile = fopen(fileSelected.c_str(),"w") ; 
		if (pFile == NULL )  return -1 ;
		write2file = 0 ; 
		for (prow = 0;prow < 120*8; prow++)
			{
			for (pcol = 0;pcol < (80*7); pcol++)
				{
				/*
				for (i=0;i<max_tab;i++) 
					{
					if (cmatrix[i][prow][pcol] > 0) 
						write2file = 1; 
					}
				if (write2file == 1)
					{	
				*/
				fprintf(pFile,"%d %d ",(120 - (prow+1)),(pcol+1)) ;
				for (i=0;i<max_tab;i++)
					fprintf(pFile,"%d ",cmatrix[i][prow][pcol])  ;
				fprintf(pFile,"\n") ;
				write2file = 0 ;
				//	}
				}
			}
		}
		
	fclose(pFile) ;
	
return 0 ;
}


int SaveDACLToDataFile(string PathAndfileName,int dacl_val[][80*7])
{

	ExtractFileName(PathAndfileName.c_str(),&file) ;
	fileSelected=  PathAndfileName + "/" + file   + "_DACL_MAP.dat"  ;
	SaveMatrixToFile(fileSelected,dacl_val) ;

return 0 ;
}

int SaveDACLToConfigFile(string PathAndfileName,int wmodule,int dacl_val[][80*7])
{
FILE *pFile ;
int pchip ;
int prow ;
int pcol ;
char pmodule[10] ;
int x,y ;

	std::sprintf(pmodule,"%d",(wmodule+1)) ;
	ExtractFileName(PathAndfileName.c_str(),&file) ;
	fileSelected = PathAndfileName + "/" + file  + "_"+ pmodule +".dacs" ;
	pFile = fopen(fileSelected.c_str(),"a") ; 
		//printf(" ReadFromModule NbWord = %d\n",datar_length) ;
	//for (pchip = 0;pchip < 7; pchip++)
	//	ithl_val[wmodule][pchip] = ith[wmodule].val[pchip] ;
		
	SaveDACToFile(fileSelected,wmodule,cmos_dis,ithh_val,vadj_val,vref_val,imfp_val,iota_val,ipre_val,ithl_val,itune_val,ibuffer_val) ;
	for (pchip = 0;pchip < 7; pchip++)
		{
		for (prow = 0;prow < 120; prow++)
			{
			fprintf(pFile,"%d %d ",(pchip+1),(prow+1)) ;
			for (pcol = 0;pcol < 80; pcol++)
				{
				y = prow+(wmodule*120) ;
				x = pcol+(pchip*80) ;
				// fprintf(pFile,"%d ",dacl_val[prow+(wmodule*120)][pcol+(pchip*80)] )  ;	
				fprintf(pFile,"%d ",dacl_val[y][x] )  ;	
				}
			fprintf(pFile,"\n") ;
			}
		}
		
	fclose(pFile) ;
return 0 ;
}


int GenerateMasterFile(string PathAndfileName,int wmodule)
{
FILE *pFile = NULL ;
char pmodule[10] ;

	std::sprintf(pmodule,"%d",(wmodule+1)) ;
	ExtractFileName(PathAndfileName.c_str(),&file) ;
	fileSelected = PathAndfileName + "/" + file  + ".master" ;
	//printf("%s \n",fileSelected.c_str()) ;
	pFile = fopen(fileSelected.c_str(),"a") ; 
	if (pFile == NULL) 
	   {
	   CALIB_MSG("\033[22;31mERROR: file not avaliable => %s\033[22;30m\n",fileSelected.c_str()) ;
	   return -1 ;
	   }
	fileSelected = PathAndfileName + "/" + file  + "_"+ pmodule +".dacs" ;
	fprintf(pFile,"mod_%d %s\n",(wmodule+1),fileSelected.c_str()) ;
	fclose(pFile) ;
return 0 ;
}



int reset_matrix(int wmatrix[][80*7] )
{
int prow ;
int pcol ;

	for (prow = 0;prow < (120*8); prow++)
		{
		for (pcol = 0;pcol < (80*7); pcol++)
			wmatrix[prow][pcol] = 0  ;
		}

return 0 ;
}


int move_matrix_value_to(int wmatrix[][80*7] ,int dec_or_inc,int value)
{
int prow ;
int pcol ;

	for (prow = 0;prow < (120*8); prow++)
		{
		for (pcol = 0;pcol < (80*7); pcol++)
			if (dec_or_inc == DECREASE)
				wmatrix[prow][pcol] = wmatrix[prow][pcol] - value ;
			else if (dec_or_inc == INCREASE)
				wmatrix[prow][pcol] = wmatrix[prow][pcol] + value ;
		}

return 0 ;
}


int correct_12b_matrix(int wmatrix[][80*7] )
{
int prow ;
int pcol ;

	for (prow = 0;prow < (120*8); prow++)
		{
		for (pcol = 0;pcol < (80*7); pcol++)
			{
		 	if ((wmatrix[prow][pcol] > 4095) | (wmatrix[prow][pcol] < 0)) 
				{
				CALIB_MSG("\033[22;31mWARNING Pixel=%d Row_%d Col_%d corrected to 0\033[22;30m\n",wmatrix[prow][pcol],prow,pcol) ;
		 		wmatrix[prow][pcol] = 0 ;
				}
			}
		}

return 0 ;
}

int reset_cmatrix(void )
{
int i ;
int prow ;
int pcol ;

	for (i=0;i<64;i++)
		{
		for (prow = 0;prow < (120*8); prow++)
			{
			for (pcol = 0;pcol < (80*7); pcol++)
				{
				cmatrix[64][120*8][80*7] = 0  ;
				count_matrix[64][120*8][80*7] = -1 ;
				daclval_matrix[64][120*8][80*7] = -1 ;
				}
			}
		}
return 0 ;
}


int ExposeModuleReady(CQuickUsb *wqusb,int *module_ready,struct struct_imageUS imageUS)
{
int pmodule ;
int wmodule ;
int module_answer = 0 ;
clock_t start_time ;
int status = 0 ; 
int progress_acq ;
int module_msk = 0 ;

for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)  
			{
			if (module_answer == 0) 
				module_answer = pmodule +1 ;
			module_msk += 1 <<  pmodule ;
			}
		}
		
  if (imageUS.FastReadout  == 1)
  	ImageUs(wqusb,module_msk,imageUS.Gate_Mode,imageUS.integration_time,imageUS.index_img,imageUS.nb_image,WITH_ANSWER) ;
  else
  	{
   	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)
			{
			wmodule = pmodule + 1 ; 
			if (module_answer == wmodule)
				//ImageUs(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
				ImageUs(wqusb,wmodule,imageUS.Gate_Mode,imageUS.integration_time,imageUS.index_img,imageUS.nb_image,WITH_ANSWER) ;
			else
				//ImageUs(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITHOUT_ANSWER) ;
				ImageUs(wqusb,wmodule,imageUS.Gate_Mode,imageUS.integration_time,imageUS.index_img,imageUS.nb_image,WITHOUT_ANSWER) ;
			}
		}
	}
 
	start_time = clock() ; 
 	while ((status !=  IMAGE_US_ENDED) & (status !=  TIMOUT_DETECTED))
    	{
	// receive  (IMAGE_ADDED	0x1140) and  (IMAGE_US_ENDED 0x1141) at the end
    	status = WaitingForHandShakeImageUSEnded(wqusb,(long) (imageUS.integration_time),imageUS.nb_image,&progress_acq) ;
	//if (status ==  IMAGE_US_ENDED) progress_acq = nb_image ;
	//if(status == TIMOUT_DETECTED) return ;
	 //Image_ProgressBar->setProgress(++image_index, nb_image ) ; QApplication::flush() ; 
	//Image_ProgressBar->setProgress(progress_acq, nb_image ) ; QApplication::flush() ; 
	 }
	 

return 0 ;
}


int ReadImageFromModuleReady(CQuickUsb *wqusb,int *module_ready,struct struct_readImg readImg)
{
int pmodule ;
int module_msk = 0 ;
int status ;
	
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)  
			{
			module_msk += 1 <<  pmodule ;
			}
		}
		
	 if (imageUS.FastReadout  == 1)
	 	{
	 	status = FastReadImageAndFillMatrix(wqusb,MODE_32b,module_msk,data_matrix,INDEX_0) ;
		if (status < 0)
		   {	
		   //ResetModule(wqusb,module_msk) ;
		   //ResetHUBFifo(wqusb) ;
		   //usleep(100000) ;
		   //printf("\033[22;31mERROR: Not enougth data in FIFO, Reset HUB and modules and try again\033[22;30m\n") ;
  		   printf("\033[22;31mERROR: Not enougth data in FIFO\033[22;30m\n") ;	
		   return -1 ;
		   }
		}
	 else
		{
		for (pmodule=0;pmodule<MAXMODULE;pmodule++)
			{	
			if (module_ready[pmodule] == 1)
				{

				ReadImageIndexAndFillMatrix(wqusb,READ_IMAGE_COUNTER,(pmodule+1),readImg.IndexImg,data_matrix) ;
				}
			}
		}
return 0 ;
}


int LoadDACLFromTabToModuleReady(CQuickUsb *wqusb,int *module_ready,int dacl_tab[][80*7]) 
{
int pmodule ;
int wmodule ;
int module_msk ;

	
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)
			{
			LoadDACLFromTab(wqusb,pmodule,dacl_tab) ;
			}
		}
	
return 0 ;
}


int CalculateNoisyPixel(int *module_ready,int data[][80*7])
{
int pmodule ;
int x,y ;
int pnoisy = 0 ;


	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			for (int pchip = 0;pchip < MAXCHIP; pchip++)
				{
				for (int prow = 0;prow < ROW_NB; prow++)
					{
					for (int pcol = 0;pcol < MAXCOL; pcol++)
						{
						x = pcol+(pchip*MAXCOL) ;
						y = prow+(pmodule*ROW_NB) ;		
						if (data[y][x] > 0) 
							{
							pnoisy++ ;
							}
						}
					}
				}
			}
		}
return pnoisy ;
}

int CalculatePixelEqualToSpecificDACLValue(int *module_ready,int dacl_data[][80*7],int cnt_data[][80*7],int value,int print_data)
{
int pmodule ;
int x,y ;
int pnoisy = 0 ;


	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			for (int pchip = 0;pchip < MAXCHIP; pchip++)
				{
				for (int prow = 0;prow < ROW_NB; prow++)
					{
					for (int pcol = 0;pcol < MAXCOL; pcol++)
						{
						x = pcol+(pchip*MAXCOL) ;
						y = prow+(pmodule*ROW_NB) ;		
						if (dacl_data[y][x] == value) 
							{
							pnoisy++ ;
							if (print_data == PRINT_DATA)
							   printf("%d ",cnt_data[y][x]) ;
							}
						}
					}
				}
			}
		}
	printf("\n") ;
return pnoisy ;
}


int EstimateNoisyPixel(int *module_ready,int data[][80*7], int noisy[][80*7])
{
int pmodule ;
int x,y ;
int pnoisy = 0 ;
int mnoisy = 0 ;

	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			mnoisy = 0 ;
			for (int pchip = 0;pchip < MAXCHIP; pchip++)
				{
				for (int prow = 0;prow < ROW_NB; prow++)
					{
					for (int pcol = 0;pcol < MAXCOL; pcol++)
						{
						x = pcol+(pchip*MAXCOL) ;
						y = prow+(pmodule*ROW_NB) ;		
						noisy[y][x] = 0 ;
						if (data[y][x] > 0) 
							{
							mnoisy++ ;
							pnoisy++ ;
							noisy[y][x] = 1 ;
							//CALIB_MSG(" %d Noisy Pixel Y=%d X=%d Module_%d Chip_%d Row_%d Col_%d Value = %d  \033[22;30m\n",pnoisy,y,x,(pmodule+1),(pchip+1),(pchip+1),(prow+1),(pcol+1),noisy[y][x]) ;
							}
						}
					}
				}
			CALIB_MSG("Module_%d Noisy Pixel = %d  \033[22;30m\n",(pmodule+1),mnoisy) ;	
			}
		}
return pnoisy ;
}

/*
int lgetline(char s[], int lim)
{
  int c, i, j;

  strlen(
  for(i = 0, j = 0; (c = getchar())!=EOF && c != '\n'; ++i)
  {
    if(i < lim - 1)
    {
      s[j++] = c;
    }
  }
  if(c == '\n')
  {
    if(i <= lim - 1)
    {
      s[j++] = c;
    }
    ++i;
  }
  s[j] = '\0';
  return i;
} 
*/
/*
int line2int(char *line, int *pchar, int *value)
{
int line_length ;
char loc_buffer[10] ;

	line_length = strlen(line) ;
	for (i=*pchar;i<line_length;i++)
		{
		if (loc_buffer[i] = gets(== 


int FillcmatrixFromFile(char *wfile,int cmatrix[][(120*8)][80*7])
{
#define MAX_LINE 100
FILE *file ;
int pline;
char *line ;
char buff_line[100]  ;
size_t len = 0 ;
ssize_t read ;
int i ;
int tmp_buff[10] ;
int data_buff[64] ;
int x,y ;
int max_val ;
int status = 1 ;

// (120*8) (80*7) data_scan
	
	file = fopen(wfile,"r") ;
	while ((read = getline(&line,&len,file)) > 0)
		{
		i = 0 ;
		sprintf(buff_line,"%s",line) ;
		printf("length = %d line = %s",len,buff_line) ;
		printf("******************************\n") ;
		pline = 0 ;
		while (c=getc)
			{
			status = sscanf(buff_line,"%d %s",&data_buff[i],buff_line) ; 
			pline += status ;
			printf("%d:%d ",status,data_buff[i]) ;
			i++ ;
			}
		max_val = i ;
		i = 0 ;
		y = data_buff[0] ;
		x = data_buff[1] ;
		while (i < max_val) cmatrix[2+i][y][x];
		}
	fclose(file); 
	CALIB_MSG("cmatrix loaded \n") ;
	
return 0 ;
}
*/


int FillcmatrixFromFile(char *wfile,int cmatrix[][(120*8)][80*7])
{
FILE *file = NULL ;
int i = 0 ;
int j = 0 ;
int buff[100] ;
int x,y ;
int max_i ;
int stop_line = 0 ;


// (120*8) (80*7) data_scan
	
	file = fopen(wfile,"r") ;
	
	while (fscanf(file,"%d",&buff[i]) != EOF) 
		{
		printf("0x%x ",buff[i]) ;
		if ((i > 64) | (buff[i] == 0x0a) | (buff[i] == 0xa) | (buff[i] == 0x0d))
			{
			printf("\n") ;
			max_i = i - 1  ;
			i = 0 ;
			y = buff[0] - 1 ;
			x = buff[1] - 1 ;
			printf("%d : LineFeed or ReturnCarriage detected\n",stop_line++) ;
			for (j=0;j<(max_i+1);j++)
				printf("%d ",buff[j]) ;
			printf("\n") ;
			for (j=0;j<(max_i-2);j++)
				cmatrix[j+2][y][x] = buff[j+2];
			
			}
		else
			i++ ;
		}
	fclose(file); 
	CALIB_MSG("cmatrix loaded \n") ;
	
return 0 ;
}


/*
int FillDACLmatrixFromFile(char *wfile,int matrix[(120*8)][80*7])
{
FILE *file ;
int pline;
char *line ;
size_t len = 0 ;
ssize_t read ;
int i = 0 ;
int j = 0 ;
int buff[100] ;
int x,y ;
int max_i ;
int status = 1 ;
int stop_line = 0 ;


// (120*8) (80*7) data_scan
	
	file = fopen(wfile,"r") ;
	
	if (file == NULL) 
		{
		printf("Error => file %s doesn't exist\n",wfile) ;
		return -1 ;
		}
	
	while (fscanf(file,"%d",&buff[i]) != EOF) 
		{
		printf("0x%x ",buff[i]) ;
		if ((i > 64) | (buff[i] == 0x0a) | (buff[i] == 0xa) | (buff[i] == 0x0d))
			{
			printf("\n") ;
			max_i = i - 1  ;
			i = 0 ;
			y = buff[0] - 1 ;
			x = buff[1] - 1 ;
			printf("%d : LineFeed or ReturnCarriage detected\n",stop_line++) ;
			for (j=0;j<(max_i+1);j++)
				printf("%d ",buff[j]) ;
			printf("\n") ;
			for (j=0;j<(max_i-2);j++)
				cmatrix[j+2][y][x] = buff[j+2];
			
			}
		else
			i++ ;
		}
	fclose(file); 
	CALIB_MSG("cmatrix loaded \n") ;
	
return 0 ;
}
*/
/*****************************************************************************************/

#ifdef toto
int dacl_scurve[MAX_SCURVE][MAX_COL][MAX_ROW];  // 64 value of scurve for every pixel
int dacl[MAX_COL][MAX_ROW];                     			// final dacl
int s_curve[MAX_SCURVE];                       				 // actual s-curve
int s_diff[MAX_SCURVE-1];

void dacl_search_sc(void)
{
    int row, col = 0;
    int i, j = 0;
    int ovf_idx = 0;
    int ovf = 0;
    int max_diff = 0;
    int max_diff_idx = 0;
    int noise = 20;
    
    for(col = 0; col < MAX_COL; col++)
    {
        for(row = 0; row < MAX_ROW; row++)
        {
            /* init s-curve */
            for(i = 0; i < MAX_SCURVE; i++)
                s_curve[i] = dacl_scurve[i][col][row];
            
            /* create differences */
            for(i = 0; i < MAX_SCURVE-1; i++)
                s_diff[i] = s_curve[i+1] - s_curve[i];

            
            // find dacl
            for(i = 0; i<MAX_SCURVE-1; i++)
            {
                if( (i>0) && (i<MAX_SCURVE-2) )
                {
                    if( (s_diff[i]>s_diff[i+1]) && (s_diff[i]>s_diff[i-1]) && (s_diff[i]>noise) )
                        break;
                }
                else if(i == 0)
                {
                    if( (s_diff[0]>s_diff[1]) && (s_diff[0]>noise) )
                        break;
                }   
                else
                {
                    if(s_diff[i]>noise)
                    {
                        i++;
                        break;
                    }
                }
            }
            
            /* create array of final dacl */
            dacl[col][row] = i;
        }
    }
}


void Search_ITHL(alt_u8 mask, int max_ith, int min_ith, alt_u16 *val, alt_u32 gatesno, alt_u8 clkdiv)
{
	volatile alt_u16 val_IthL, nb_pixels_actifs, valeur_RD = 0;
	volatile alt_u16 chip, row, col = 0;
	
	for(val_IthL=max_ith ; val_IthL>min_ith ; val_IthL--)
	{
		// load ITHL value
		alt_printf("\nLoad ITHL = 0x%x\n", val_IthL);
        CONFIGURE_G(0x3C, val_IthL, mask);
		
		VALID_COLONNE(0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,mask);
		
		// enable caounters
		open_gate(mask, 0, gatesno,  clkdiv);
    	// read counters
		DATA12_to_ImagesArr(0, mask, ITHL, 0);
        
		for (chip=0 ; chip<7 ; chip++)
		{
			// if chip not masked and ITHL not found ( ==0)
            alt_printf("\t\tchip = 0x%x\n", chip);
            alt_printf("\t\tmask>>chip)&0x01 = 0x%x\n", ((mask>>chip)&0x01));
            alt_printf("\t\t*(val+chip) = 0x%x\n",*(val+chip));
			if(  (((mask>>chip)&0x01) !=0 ) &&  (*(val+chip)==0) ) 
			{
                alt_printf("\t\tchip = 0x%x\n", chip);
				nb_pixels_actifs = 0;
				for (row=0 ; row<120 ; row++)
				{
					for (col=0; col<80 ; col++)
					{
						valeur_RD = images_arr[ITHL][col + 80*chip + 560*row];
						if(valeur_RD>=1) nb_pixels_actifs ++;
					}
				}
				alt_printf("\tNumber of counting pixels for chip 0x%x = 0x%x\n", chip, nb_pixels_actifs);
                
				if(nb_pixels_actifs>=4800)
					*(val+chip) = val_IthL; 
			}
		}
	}
}

#endif
