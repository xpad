/*
TCanvas *gEditCanvas;
[...]
   tabId = fMyTab->GetCurrent(); // get tab id
   gEditCanvas = new TCanvas("gEditCanvas","Graphic Editor");
   gEditCanvas->ToggleEventStatus(); // show event status bar
   gEditCanvas->ToggleToolBar(); // show toolbar
   gEditCanvas->ToggleEditor(); // show editor
   gEditCanvas->cd();
   if (tabId == 0)
      fCanvasA->DrawClonePad(); // clone the canvas A into gEditCanvas
   if (tabId == 1)
      fCanvasB->DrawClonePad(); // clone the canvas B into gEditCanvas
[...] 
*/

/*

TGraph *gr = Graph; // from $ROOTSYS/tutorials/graph.C
TCanvas c2("c2");
 gr->Draw("ap");
    TPad p1("p1", "", 0.3, 0.6, 0.65, 0.9);
    p1.SetFillStyle(4000);
    p1.Draw();
    p1.cd();
    Printf("%s", gROOT->GetSelectedPad()->GetName());
    gPad->Update(); // important !!!
    Printf("%s", gROOT->GetSelectedPad()->GetName());
    TGraph *clone = (TGraph *) gr->DrawClone("ap");
    clone->GetXaxis()->SetRangeUser(0.3, 0.6); 

clone->SetName("clone_gr");
((TGraph*)p1.FindObject("clone_gr"))->GetYaxis()->SetNdivisions(505);

*/

/*
TCanvas CloneID ;
(TCanvas*) CloneID.FindObject("mClone") ; 
cID = CloneID.GetCanvasID() ;
cout << "Before CloneCanvas ID = " << cID << endl ;
cID = (mClone->GetCanvas())->GetCanvasID() ;
*/


#include <iostream>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <istream>
#include <fstream>
#include <strstream>
#include <vector>

#include <signal.h>
#include <sys/time.h>


#include <qfile.h>
#include <q3textstream.h>

#include "defines.h"
#include "pthread.h"

#include "TQRootCanvas.h" 
#include "TCanvas.h" 
#include "TH2F.h" 
#include "TStyle.h" 
#include "TSystem.h" 
#include "TAttImage.h"
#include "TColor.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TFrame.h"
#include "defines.h"
#include "TRint.h"
#include "TVirtualPad.h"
#include "TObject.h"

#include "utilities.h"

#include "cyclone.h"

 
TH1F *hn1 = new TH1F("hn1","",64,0,64) ;
TH1F *hn2 = new TH1F("hn2","",64,0,64) ;
TH1F *hn3 = new TH1F("hn3","",64,0,64) ;
TH1F *hn4 = new TH1F("hn4","",64,0,64) ;
TH1F *hn5 = new TH1F("hn5","",64,0,64) ;
TH1F *hn6 = new TH1F("hn6","",64,0,64) ;
TH1F *hn7 = new TH1F("hn7","",64,0,64) ;

using namespace std;


int RootConfigHistoDACL(TCanvas *myCanvas,TH1F *Myhisto1d,char *Title,int ChipID)
{
float kMargin = 0.175 ; 
char message[100] ;

	myCanvas->Clear();	 
        gStyle->SetPalette(1) ;		
	myCanvas->SetRightMargin((kMargin*4)) ;
	myCanvas->SetLeftMargin(kMargin) ;
	myCanvas->GetFrame()->SetBorderSize(10) ;
 	myCanvas->SetGridx(0); 
	myCanvas->SetGridy(0) ;
	//myCanvas->ToggleEventStatus(); // show event status bar
   	//myCanvas->ToggleToolBar(); // show toolbar
   	//myCanvas->ToggleEditor(); // show editor
   	//(MyCanvas->GetCanvas())->cd();
	Myhisto1d->Reset() ;
	std::sprintf(message,"DACs file chip_%d => %s",ChipID,Title) ;
	Myhisto1d->SetTitle(message) ;	

return 0 ;
}

//int RootConfigHistoDACLAllChips(TCanvas *myCanvas,TH1F *Myhisto1d,char *Title)
int RootConfigHistoDACLAllChips(TCanvas *myCanvas)
{
float kMargin = 0.175 ; 
	     
	myCanvas->Clear();	 
        gStyle->SetPalette(1) ;		
	myCanvas->SetRightMargin((kMargin*4)) ;
	myCanvas->SetLeftMargin(kMargin) ;
	myCanvas->GetFrame()->SetBorderSize(10) ;
 	myCanvas->SetGridx(0); 
	myCanvas->SetGridy(0) ;
	//myCanvas->ToggleEventStatus(); // show event status bar
   	//myCanvas->ToggleToolBar(); // show toolbar
   	//myCanvas->ToggleEditor(); // show editor
   	//(MyCanvas->GetCanvas())->cd();
	hn1->Reset() ;
	hn2->Reset() ;
	hn3->Reset() ;
	hn4->Reset() ;
	hn5->Reset() ;
	hn6->Reset() ;
	hn7->Reset() ;
	//std::sprintf(message,"DACs file chip_%d => %s",ChipID,Title) ;
	//Myhisto1d->SetTitle(message) ;	

return 0 ;
}


int RootFillHistoDACL(TH1F *Myhisto1d,TH2F *Myhisto2d,const char *fileName,int ModuleID,int ChipID)
{
#define FIRST_DWORD 	2
#define MAX_DATA		82
string line ;
int dataline[100] ;
Int_t gRow ;
Int_t gCol ;
double gHit ;
int start_read = -1 ;

	ifstream datafile(fileName) ; // open file in read mode
	if (!datafile) { 
	cout << " file not found => " << fileName << endl ;
	return -1 ; }
	//cout << "RootFillHistoDACL => " << fileName << endl ;
	//assure(datafile,WhichFile) ;  // verify open
	
	while (getline(datafile,line))
	 {
	   istrstream linestream(line.c_str()) ;
	   if (start_read < 0) 
	   	{
	   	if (strcmp(line.c_str(),"DACL") == 0) start_read = 0 ;
		if (strcmp(line.c_str(),"DACL ") == 0) start_read = 0 ;
		if (strcmp(line.c_str(),"DACL  ") == 0) start_read = 0 ;
	   	}
	 else
	 	{
		start_read++ ;
		}
	 //printf("%d : %s \n",start_read,line.c_str()) ;
	  if (start_read > 0) 
	   	{		
	 	 for (Int_t i=0;i<MAXDATALINE;i++)
	     		linestream >> dataline[i] ;
		
	   	// *******************************************************//    
	  	// 					Histo 1D  					   //
	  	// *******************************************************//
	   	for (Int_t i=FIRST_DWORD;i<MAX_DATA;i++)
	  		{
	  		if (dataline[0] == ChipID)
				{
	  			gHit =(double) dataline[i] ;	
				Myhisto1d->Fill(gHit) ;
				}
			}
		
		   // *******************************************************//    
	  	  // 					Histo 2D  					   //
	  	 // *******************************************************//
	  
	 	  //gRow = (dataline[2]-1) + (ModuleID*120) ;
	   	gRow = dataline[1] - 1 ;
	   	for (Int_t i=FIRST_DWORD;i<MAXDATALINE;i++)
	  		{
	  		//gCol = (i-FIRST_DWORD) + ((dataline[1]-1)*80) ; 
	  		gCol = (i-FIRST_DWORD)  ; 
			gHit =(double) dataline[i] ;	
			if (dataline[0] == ChipID)
				Myhisto2d->Fill(gCol,gRow,gHit) ;
			//cout << "gCol = " << gCol  << " gRow = " << gRow << " gHit = " << gHit << endl ;
			}
	  	}
	  	}
	//cout << endl ;
	datafile.close() ;
	
#undef FIRST_DWORD 	
#undef MAX_DATA	
return 0 ;
}


//int RootFillHistoDACLAllChips(TH1F *Myhisto1d,TH2F *Myhisto2d,const char *fileName,int ModuleID)
int RootFillHistoDACLAllChips(TH2F *Myhisto2d,const char *fileName)
{
#define FIRST_DWORD 	2
#define MAX_DATA		82
string line ;
int dataline[100] ;
Int_t gRow ;
Int_t gCol ;
double gHit ;
int start_read = -1 ;
int ChipID ;

	ifstream datafile(fileName) ; // open file in read mode
	if (!datafile) { 
	cout << " file not found => " << fileName << endl ;
	return -1 ; }
	//cout << "RootFillHistoDACL => " << fileName << endl ;
	//assure(datafile,WhichFile) ;  // verify open
	
	while (getline(datafile,line))
	 {
	   istrstream linestream(line.c_str()) ;
	   if (start_read < 0) 
	   	{
	   	if (strcmp(line.c_str(),"DACL") == 0) start_read = 0 ;
		if (strcmp(line.c_str(),"DACL ") == 0) start_read = 0 ;
		if (strcmp(line.c_str(),"DACL  ") == 0) start_read = 0 ;
	   	}
	 else
	 	{
		start_read++ ;
		}
	 //printf("%d : %s \n",start_read,line.c_str()) ;
	  if (start_read > 0) 
	   	{		
	 	 for (Int_t i=0;i<MAXDATALINE;i++)
	     		linestream >> dataline[i] ;
		
	   	// *******************************************************//    
	  	// 					Histo 1D  					   //
	  	// *******************************************************//
	   	for (Int_t i=FIRST_DWORD;i<MAX_DATA;i++)
	  		{
	  		if (dataline[0] == 1)
				{
	  			gHit =(double) dataline[i] ;	
				hn1->Fill(gHit) ;
				}
			if (dataline[0] == 2)
				{
	  			gHit =(double) dataline[i] ;	
				hn2->Fill(gHit) ;
				}
			if (dataline[0] == 3)
				{
	  			gHit =(double) dataline[i] ;	
				hn3->Fill(gHit) ;
				}
			if (dataline[0] == 4)
				{
	  			gHit =(double) dataline[i] ;	
				hn4->Fill(gHit) ;
				}
			if (dataline[0] == 5)
				{
	  			gHit =(double) dataline[i] ;	
				hn5->Fill(gHit) ;
				}
			if (dataline[0] == 6)
				{
	  			gHit =(double) dataline[i] ;	
				hn6->Fill(gHit) ;
				}
			if (dataline[0] == 7)
				{
	  			gHit =(double) dataline[i] ;	
				hn7->Fill(gHit) ;
				}
			}
		
		   // *******************************************************//    
	  	  // 					Histo 2D  					   //
	  	 // *******************************************************//
	  
	 	  //gRow = (dataline[2]-1) + (ModuleID*120) ;
	   	gRow = dataline[1] - 1 ;
	   	for (Int_t i=FIRST_DWORD;i<MAXDATALINE;i++)
	  		{
	  		//gCol = (i-FIRST_DWORD) + ((dataline[1]-1)*80) ; 
			ChipID = dataline[0] ;
	  		gCol = (i-FIRST_DWORD)  ; 
			gHit =(double) dataline[i] ;	
			//if (dataline[0] == ChipID)

			Myhisto2d->Fill((gCol+(80*ChipID)),gRow,gHit) ;
			//cout << "gCol = " << gCol  << " gRow = " << gRow << " gHit = " << gHit << endl ;
			}
	  	}
	  	}
	//cout << endl ;
	datafile.close() ;

#undef  FIRST_DWORD 	
#undef  MAX_DATA		
return 0 ;
}

int RootPlotHistoDACL(TCanvas *myCanvas,TH1F *Myhisto1d,TH2F *Myhisto2d)
{


	 Myhisto1d->SetLineColor(4) ;
	gStyle->SetOptFit(1110) ;
	myCanvas->Divide(1,2) ;
	 myCanvas->cd(1) ;
	 Myhisto1d->Draw("") ;
	 myCanvas->Modified(); myCanvas->Update();
	 myCanvas->cd(2) ;
	 //Myhisto2d->SetTitle("DAC Config\nIth") ;	
	 gStyle->SetOptStat("ei") ;
	 Myhisto2d->Draw("COLZ")  ;
	 myCanvas->Modified(); myCanvas->Update();
	 myCanvas->cd() ;
return 0 ;
}

//int RootPlotHistoDACLAllChips(TCanvas *myCanvas,TH1F *Myhisto1d,TH2F *Myhisto2d)
int RootPlotHistoDACLAllChips(TCanvas *myCanvas,TH2F *Myhisto2d)
{
	gStyle->SetOptFit(1110) ;
	myCanvas->Divide(1,2) ;
	 myCanvas->cd(1) ;
 	hn1->Draw("") ;
	hn1->SetLineColor(4) ;
	hn2->Draw("") ;
	hn2->SetLineColor(4) ;
	hn3->Draw("") ;
	hn3->SetLineColor(4) ;
	hn4->Draw("") ;
	hn4->SetLineColor(4) ;
	hn5->Draw("") ;
	hn5->SetLineColor(4) ;
	hn6->Draw("") ;
	hn6->SetLineColor(4) ;
	hn7->Draw("") ;
	hn7->SetLineColor(4) ;
	 myCanvas->Modified(); myCanvas->Update();
	 myCanvas->cd(2) ;
	 //Myhisto2d->SetTitle("DAC Config\nIth") ;	
	 gStyle->SetOptStat("ei") ;
	 Myhisto2d->Draw("COLZ")  ;
	 myCanvas->Modified(); myCanvas->Update();
	 myCanvas->cd() ;
	 
return 0 ;
}





//int RootConfigHistoHit(TCanvas *myCanvas,TH2F *Myhisto2d,char *Title)
int RootConfigHistoHit(TH2F *Myhisto2d,char *Title)
{
//float kRMargin = 0.170 ; 
//float kLMargin = 0.100 ; 
	
	/*
        gStyle->SetPalette(1) ;
	gStyle->SetOptStat("ei") ; 
	gStyle->SetOptFit(0001) ;
	*/
	//gROOT->GetListOfCanvases()->Write("",TObject::kOverwrite); 
	//myCanvas->resize(800,400) ; 
	/*
	myCanvas->Clear() ; 
	myCanvas->SetRightMargin((kRMargin)) ;
	myCanvas->SetLeftMargin(kLMargin) ;
	*/
	//myCanvas->GetFrame()->SetBorderSize(10) ;
 	/*
	myCanvas->SetGridx(0); 
	myCanvas->SetGridy(0);
	*/
	Myhisto2d->SetTitle(Title) ;
        Myhisto2d->GetXaxis()->SetTitle("Column") ;
	Myhisto2d->GetXaxis()->SetTitleSize(0.03) ; 
	Myhisto2d->GetXaxis()->CenterTitle() ; 
	Myhisto2d->GetXaxis()->SetLabelSize(0.03) ; 

	Myhisto2d->GetYaxis()->SetTitle("Row") ;
	Myhisto2d->GetYaxis()->SetTitleSize(0.03) ; 
	Myhisto2d->GetYaxis()->CenterTitle() ;
	Myhisto2d->GetYaxis()->SetLabelSize(0.03) ;  
	
	//myCanvas->ToggleEventStatus(); // show event status bar
   	//myCanvas->ToggleToolBar(); // show toolbar
   	//myCanvas->ToggleEditor(); // show editor
	
	//Myhisto2d->GetXaxis()->SetLabelOffset(80) ;
	
return 0 ;
}



int RootFillHistoHit(TH2F *Myhisto2d,const char *fileName,int ModuleID)
{
#define FIRST_DWORD 	3
string line ;
int dataline[100] ;
Int_t gRow ;
Int_t gCol ;
double gHit ;


	//cout << "RootFillHistoHit => " << fileName << endl ;
	ifstream datafile(fileName) ; // open file in read mode
	if (!datafile) { 
	cout << " file not found => " << fileName << endl ;
	return -1 ; }

	//assure(datafile,WhichFile) ;  // verify open
	while (getline(datafile,line))
	 {
	 istrstream linestream(line.c_str()) ;
	  for (Int_t i=0;i<MAXDATALINE;i++)
	     linestream >> dataline[i] ;     
	     
	  gRow = (dataline[2]-1) + (ModuleID*120) ;
	   for (Int_t i=FIRST_DWORD;i<MAXDATALINE;i++)
	  	{
	  	gCol = (i-FIRST_DWORD) + ((dataline[1]-1)*80) ; 
	  	gHit =(double) dataline[i] ;	
		Myhisto2d->Fill(gCol,gRow,gHit) ;
		//cout << "gCol = " << gCol  << " gRow = " << gRow << " gHit = " << gHit << endl ;
		if (dataline[i+1] == 0xf0f0) i =  MAXDATALINE ;
		}
	  }
	datafile.close() ;

#undef FIRST_DWORD 	
return 0 ;
}


int RootFillHistoHitFromMatrix(TH2F *Myhisto2d,int matrix[][(80*7)])
{

Int_t gRow ;
Int_t gCol ;
double gHit ;

	 for (int py=0;py<(120*8);py++)
	  {
	   for (int px=0;px<(80*7);px++)
	      {
		gCol= px ;
		gRow = py ;
		gHit = (double) matrix[py][px] ;	
	  	Myhisto2d->Fill(gCol,gRow,gHit) ;
		}
	  }
	 
return 0 ;
}

//int RootAPI::Plot(TQRootCanvas *myCanvas,TH2F *Myhisto2d)
int RootPlotHistoHit(TCanvas *myCanvas,TH2F *Myhisto2d)
{
	// Myhisto2d->Refresh() ;
	 Myhisto2d->Draw("COLZ")  ;
	 myCanvas->Modified(); 
	 myCanvas->Update();

return 0 ;
}

int RootHistoHitConfigFillPlot(TCanvas *myCanvas,TH2F *Myhisto2d,const char *QStrFileSelected,int ModuleID)
{
char fileName[500] ;
string pfileName ;
	ExtractFileName(QStrFileSelected,&pfileName) ;
	std::sprintf(fileName,"%s",pfileName.c_str()) ;
	myCanvas->Divide(1,1) ;
	RootConfigHistoHit(Myhisto2d,fileName) ;
	RootFillHistoHit(Myhisto2d,QStrFileSelected,ModuleID) ;
	RootPlotHistoHit(myCanvas,Myhisto2d) ;
return 0 ;
}


int RootHistoHitConfigFillPlotMatrix(TCanvas *myCanvas,TH2F *Myhisto2d,int matrix[][(80*7)],std::string fileName)
{
char locfileName[500] ;
float kRMargin = 0.170 ; 
float kLMargin = 0.100 ; 
	
        gStyle->SetPalette(1) ;
	gStyle->SetOptStat("ei") ; 
	gStyle->SetOptFit(0001) ;
	
	//gROOT->GetListOfCanvases()->Write("",TObject::kOverwrite); 
	//myCanvas->resize(800,400) ; 
	myCanvas->Clear() ; 
	myCanvas->SetRightMargin((kRMargin)) ;
	myCanvas->SetLeftMargin(kLMargin) ;
	//myCanvas->GetFrame()->SetBorderSize(10) ;
 	myCanvas->SetGridx(0); 
	myCanvas->SetGridy(0);

	std::sprintf(locfileName,"%s",fileName.c_str()) ;
	RootConfigHistoHit(Myhisto2d,locfileName) ;
	RootFillHistoHitFromMatrix(Myhisto2d,matrix) ;
	RootPlotHistoHit(myCanvas,Myhisto2d) ;

return 0 ;
}


int Root2HistoHitConfigFillPlotMatrix(TCanvas *myCanvas,TH2F *histo2d_1,int matrix_1[][(80*7)],std::string fileName_1,TH2F *histo2d_2,int matrix_2[][(80*7)],std::string fileName_2)
{
char locfileName[500] ;
float kRMargin = 0.170 ; 
float kLMargin = 0.100 ; 
	
        gStyle->SetPalette(1) ;
	gStyle->SetOptStat("ei") ; 
	gStyle->SetOptFit(0001) ;
	
	//gROOT->GetListOfCanvases()->Write("",TObject::kOverwrite); 
	//myCanvas->resize(800,400) ; 
	myCanvas->Clear() ; 
	myCanvas->SetRightMargin((kRMargin)) ;
	myCanvas->SetLeftMargin(kLMargin) ;
	//myCanvas->GetFrame()->SetBorderSize(10) ;
 	myCanvas->SetGridx(0); 
	myCanvas->SetGridy(0);


	myCanvas->Divide(1,2) ;
	
	myCanvas->cd(1) ;
	std::sprintf(locfileName,"%s",fileName_1.c_str()) ;
	RootConfigHistoHit(histo2d_1,locfileName) ;
	RootFillHistoHitFromMatrix(histo2d_1,matrix_1) ;
	RootPlotHistoHit(myCanvas,histo2d_1) ;
	
	myCanvas->cd(2) ;
	std::sprintf(locfileName,"%s",fileName_2.c_str()) ;
	RootConfigHistoHit(histo2d_2,locfileName) ;
	RootFillHistoHitFromMatrix(histo2d_2,matrix_2) ;
	RootPlotHistoHit(myCanvas,histo2d_2) ;


return 0 ;
}


