/**
* \file synchro.cpp
* \brief 
* \author Patrick BREUGNON
* \version 1.0
* \date June 05 2009
 */


#include <stdio.h>
#include "synchro.h"
#include "defines.h"
#include "usbwrap.h"

#include <sys/ioctl.h>
#include <termios.h>
#include <fcntl.h>

#define D2AM	0


#ifdef SYNCHRO_MSG
#define SYNCHRO_MSG(fmt,args...) printf("\033[22;37msynchro_msg :  " fmt,##args)  
#define SYNCHRO_NDEF(fmt,args...)
#else
#define SYNCHRO_NDEF(fmt,args...) printf("\033[22;37m " fmt,##args)  
#define SYNCHRO_MSG(fmt,args...)
#endif



int init_file_synchro_struct(struct file_synchro *f_synchro,int wsynchro)
{
	if (wsynchro == D2AM_SYNCHRO)
		{
		f_synchro->printWait = 0 ;
		f_synchro->path = "/users/blissadm/local/spec/controlXpad/data/"  ;
		f_synchro->cmdname = "cmdFromSpec.cmd" ;
		f_synchro->attente = 0 ;
		f_synchro->line = (char *) malloc(200) ;
		f_synchro->len = 100 ;
		f_synchro->time_param = 0 ;
		}
	if (wsynchro == PIXSCAN_SYNCHRO)
		{
		f_synchro->printWait = 0 ;
		//f_synchro->path = "/home/marinella/DAQ_OPTO_DMULTI/soft/kdev_daq/scr/data/"  ;
		f_synchro->path = "/data/dataPix/"  ;
		f_synchro->cmdname = "cmdFromSpec.cmd" ;
		f_synchro->attente = 0 ;
		f_synchro->line = (char *) malloc(200) ;
		f_synchro->len = 100 ;
		f_synchro->time_param = 0 ;
		f_synchro->index_param = 0 ;
		}
return 0 ;
}


/*
int d2am_read_file_synchro(struct file_synchro *f_synchro)
{
	init_file_synchro_struct(f_synchro,D2AM) ;
	sprintf(f_synchro->fileselected,"%s/%s", f_synchro->path, f_synchro->cmdname)  ;
	if (f_synchro->printWait==0) { SYNCHRO_MSG("WAIT %s ", f_synchro->fileselected);  fflush(stdout);   }
	else if (!(f_synchro->printWait % 10))  {printf("."); fflush(stdout);  }
	f_synchro->printWait++;
	f_synchro->attente =0;
//	f_synchro_line=(char *) malloc(200);
	while (f_synchro->attente !=1)
	{ 	
		f_synchro->cmdfile = fopen(f_synchro->fileselected,"r") ;
		if (f_synchro->cmdfile==NULL) { free(f_synchro->line); return -1; }
		while (getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) >0)
			if (strstr(f_synchro->line,"ATTENTE") !=NULL) f_synchro->attente=1;
		fclose(f_synchro->cmdfile) ;
	}
	
	printf("\n");
	SYNCHRO_MSG("in FILE %s ",f_synchro->fileselected);
	f_synchro->cmdfile = fopen(f_synchro->fileselected, "r") ;
	getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
		{
		if (strstr(f_synchro->line,"ACQUI") !=NULL) 
			{
			getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
			sscanf(f_synchro->line,"%d",&f_synchro->time_param) ;
			getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
			sscanf(f_synchro->line,"%s", &f_synchro->fileName_param) ;
			SYNCHRO_MSG("params: countTime:%d  image:%s\n", f_synchro->time_param, f_synchro->fileName_param);
			}
		else	{ SYNCHRO_MSG("CMD %s not allowed \n", f_synchro->line); fclose(f_synchro->cmdfile); return -2; }
		}
	free(f_synchro->line);
	fclose(f_synchro->cmdfile) ;
 	f_synchro->printWait=0;
	
return 0 ;
}
*/

// Format du fichier détecté
// ACQUI
// time_param
// fileName_param
// Index_param
// ATTENTE

int read_file_synchro(struct file_synchro *f_synchro,int wsynchro)
{
char sys_command[500]  ;
char fileSelected[200] ;


	init_file_synchro_struct(f_synchro,wsynchro) ;
	sprintf(fileSelected,"%s%s", f_synchro->path, f_synchro->cmdname)  ;
	
	while (f_synchro->attente !=1)
		{	 
		if (f_synchro->printWait==0) { SYNCHRO_MSG("WAIT %s \033[22;30m\n",fileSelected);  fflush(stdout);   }
		else if (!(f_synchro->printWait % 1000))  { printf("."); fflush(stdout); usleep(100) ; }   /* JFB revoir synchro ??? */
		f_synchro->printWait++;
		f_synchro->cmdfile = fopen(fileSelected,"r") ;
		if (f_synchro->cmdfile!=NULL)
			{
			while (getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) >0)
				if (strstr(f_synchro->line,"ATTENTE") !=NULL) f_synchro->attente=1;
			fclose(f_synchro->cmdfile) ;
			}
		}
		
	printf("\n");
	SYNCHRO_MSG("File detected => %s \n",fileSelected);
	f_synchro->cmdfile = fopen(fileSelected, "r") ;
	getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
		{
		if (strstr(f_synchro->line,"ACQUI") != NULL) 
			{
			getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
			sscanf(f_synchro->line,"%d ",&f_synchro->time_param) ;
			getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
			sscanf(f_synchro->line,"%s ", &f_synchro->fileName_param) ;
			getline(&f_synchro->line, &f_synchro->len, f_synchro->cmdfile) ;
			sscanf(f_synchro->line,"%d ", &f_synchro->index_param) ;
			SYNCHRO_MSG("params: countTime:%d  image:%s\033[22;30m\n", f_synchro->time_param, f_synchro->fileName_param);
			}
		else	{ SYNCHRO_MSG("CMD %s not allowed \n", f_synchro->line); fclose(f_synchro->cmdfile); return -2; }
		}

	free(f_synchro->line);
	fclose(f_synchro->cmdfile) ;

 	f_synchro->printWait=0;
	sprintf(sys_command,"rm %s%s",f_synchro->path,f_synchro->cmdname) ;
	SYNCHRO_MSG("Delete synchro file => %s%s\033[22;30m\n",f_synchro->path,f_synchro->cmdname) ;
	system(sys_command) ;
	
return 0 ;
}

int write_file_synchro(struct file_synchro *f_synchro,int* roi)
{
FILE *file;
char fileSelected[200] ;
//char sys_command[500]  ;


		SYNCHRO_MSG("Write synchro file => %s.res\033[22;30m\n",f_synchro->cmdname) ;
		//sprintf(sys_command,"%s/%s.res",f_synchro->path, f_synchro->cmdname)  ;
		sprintf(fileSelected,"%s%s.res",f_synchro->path, f_synchro->cmdname)  ;
		file=fopen(fileSelected,"w");
		if (file != NULL)
			{
			//fprintf(d2am_result," ");
			//if (d2am_result == NULL)
			// printf("ERROR d2am_result file %s not open\n",sys_command) ;
			//printf("roi[]= %d %d %d %d\n", roi[0], roi[1], roi[2], roi[3]);
			fprintf(file,"%d %d %d %d\n", roi[0], roi[1], roi[2], roi[3]);
			fclose(file);
		    //sprintf(sys_command,"rm %s",f_synchro->fileselected) ;
		    //system(sys_command) ;
			}
		else
			SYNCHRO_MSG("unable to open the file %s.res\033[22;30m\n",f_synchro->cmdname) ;
return 0 ;
}

/*
int config_serial_synchro(int *fd,struct termios oldtio,struct termios newtio)
{
int lfd ;

	lfd = open("/dev/ttyS0",O_RDWR | O_NOCTTY | O_NDELAY ) ;
	if (lfd < 0)
		{ 
		printf("\033[22;31mERROR Serial Port not ready fd = %d\033[22;30m\n",lfd ) ;
		return -1 ;
		}
	else
		SYNCHRO_MSG("Open serial port fd = %d\033[22;30m\n",lfd) ;
	tcgetattr(lfd,&oldtio); // sauvegarde de la configuration courante 
	bzero(&newtio, sizeof(newtio)); // on initialise la structure à zéro 
 	// BAUDRATE: Affecte la vitesse. vous pouvez également utiliser cfsetispeed
 	// et cfsetospeed.
 	// CRTSCTS : contrôle de flux matériel (uniquement utilisé si le câble a
 	// les lignes nécessaires. Voir la section 7 du Serial-HOWTO).
 	// CS8     : 8n1 (8 bits,sans parité, 1 bit d'arrêt)
 	// CLOCAL  : connexion locale, pas de contrôle par le modem
 	// CREAD   : permet la réception des caractères
 	// newtio.c_cflag = B115200 | CRTSCTS | CS8 | CLOCAL | CREAD  ;
	// newtio.c_cflag = B115200 | CS8 | CLOCAL | CREAD ;
	newtio.c_cflag = 0 ;
	newtio.c_cflag = B115200  | CS8| CLOCAL | CREAD;
	newtio.c_oflag = 0;
        newtio.c_lflag = 0;  
	newtio.c_lflag = ICANON;
 	// IGNPAR  : ignore les octets ayant une erreur de parité.
 	// ICRNL   : transforme CR en NL (sinon un CR de l'autre côté de la ligne
 	// ne terminera pas l'entrée).
 	// sinon, utiliser l'entrée sans traitement (device en mode raw).
	// newtio.c_iflag = IGNPAR | ICRNL;
 	// Sortie sans traitement (raw).
	// newtio.c_oflag = 0;
	// newtio.c_oflag &= ~OPOST;
 	// ICANON  : active l'entrée en mode canonique
 	// désactive toute fonctionnalité d'echo, et n'envoit pas de signal au
	// programme appelant.
 	// à présent, on vide la ligne du modem, et on active la configuration
 	// pour le port
	newtio.c_cc[VMIN] = 60 ;
 	newtio.c_cc[VTIME] =1 ;
	// cfsetospeed(&newtio, B115200);
	tcflush(lfd, TCIFLUSH);
 	tcsetattr(lfd,TCSANOW,&newtio);
	*fd = lfd ;
return 0 ;
}
*/

/*
int config_serial_synchro(int *fd,struct termios oldtio,struct termios newtio)
{
int lfd ;

	lfd = open("/dev/ttyS0",O_RDWR | O_NOCTTY | O_NDELAY ) ;
	if (lfd < 0)
		{ 
		printf("\033[22;31mERROR Serial Port not ready fd = %d\033[22;30m\n",lfd ) ;
		return -1 ;
		}
	else
		SYNCHRO_MSG("Open serial port fd = %d\033[22;30m\n",lfd) ;
	tcgetattr(lfd,&oldtio); //sauvegarde de la configuration courante 
	bzero(&newtio, sizeof(newtio)); // on initialise la structure à zéro 
 	// BAUDRATE: Affecte la vitesse. vous pouvez également utiliser cfsetispeed
 	// et cfsetospeed.
 	// CRTSCTS : contrôle de flux matériel (uniquement utilisé si le câble a
 	// les lignes nécessaires. Voir la section 7 du Serial-HOWTO).
 	// CS8     : 8n1 (8 bits,sans parité, 1 bit d'arrêt)
 	// CLOCAL  : connexion locale, pas de contrôle par le modem
 	// CREAD   : permet la réception des caractères
 	// newtio.c_cflag = B115200 | CRTSCTS | CS8 | CLOCAL | CREAD  ;
	// newtio.c_cflag = B115200 | CS8 | CLOCAL | CREAD ;
	newtio.c_cflag = 0 ;
	newtio.c_cflag = B115200  | CS8| CLOCAL | CREAD  ;
        newtio.c_lflag = 0;  
	newtio.c_lflag = ICANON;
 	// IGNPAR  : ignore les octets ayant une erreur de parité.
 	// ICRNL   : transforme CR en NL (sinon un CR de l'autre côté de la ligne
 	// ne terminera pas l'entrée).
 	// sinon, utiliser l'entrée sans traitement (device en mode raw).
	// newtio.c_iflag = IGNPAR | ICRNL;
 	// Sortie sans traitement (raw).
	// newtio.c_oflag = 0;
	// newtio.c_oflag &= ~OPOST;
 	// ICANON  : active l'entrée en mode canonique
 	// désactive toute fonctionnalité d'echo, et n'envoit pas de signal au
	// programme appelant.
 	// à présent, on vide la ligne du modem, et on active la configuration
 	// pour le port
	newtio.c_cc[VMIN] = 10 ;
 	newtio.c_cc[VTIME] = 5 ;
	cfsetospeed(&newtio, B115200);
	tcflush(lfd, TCIFLUSH);
 	tcsetattr(lfd,TCSANOW,&newtio);
	*fd = lfd ;
return 0 ;
}
*/

int config_serial_synchro(int *fd,struct termios oldtio,struct termios newtio,int rw)
{
int lfd = -1 ;

	lfd = open("/dev/ttyS0",O_RDWR | O_NOCTTY | O_NDELAY ) ;
	if (lfd < 0)
		{ 
		printf("\033[22;31mERROR Serial Port not ready fd = %d\033[22;30m\n",lfd ) ;
		return -1 ;
		}
	else
		if (rw == 0)
			SYNCHRO_MSG("open wserial port fd = %d\033[22;30m\n",lfd) ;
		if (rw == 1)
			SYNCHRO_MSG("open rserial port fd = %d\033[22;30m\n",lfd) ;
	tcgetattr(lfd,&oldtio); //sauvegarde de la configuration courante 
	bzero(&newtio, sizeof(newtio)); // on initialise la structure à zéro 
 	// BAUDRATE: Affecte la vitesse. vous pouvez également utiliser cfsetispeed
 	// et cfsetospeed.
 	// CRTSCTS : contrôle de flux matériel (uniquement utilisé si le câble a
 	// les lignes nécessaires. Voir la section 7 du Serial-HOWTO).
 	// CS8     : 8n1 (8 bits,sans parité, 1 bit d'arrêt)
 	// CLOCAL  : connexion locale, pas de contrôle par le modem
 	// CREAD   : permet la réception des caractères
 	// newtio.c_cflag = B115200 | CRTSCTS | CS8 | CLOCAL | CREAD  ;
	// newtio.c_cflag = B115200 | CS8 | CLOCAL | CREAD ;

	//newtio.c_cflag = B115200  | CS8| CLOCAL | CREAD  | PARENB | PARODD ;
	newtio.c_cflag = B9600  | CS8| CLOCAL | CREAD  ;
	newtio.c_oflag = 0 ;
	newtio.c_lflag = 0 ;
        //newtio.c_oflag = OPOST | OFILL ;
	newtio.c_lflag = ICANON;
        //newtio.c_oflag = OPOST | OFILL ;
	
 	// IGNPAR  : ignore les octets ayant une erreur de parité.
 	// ICRNL   : transforme CR en NL (sinon un CR de l'autre côté de la ligne
 	// ne terminera pas l'entrée).
 	// sinon, utiliser l'entrée sans traitement (device en mode raw).
	// newtio.c_iflag = IGNPAR | ICRNL;
 	// Sortie sans traitement (raw).
	// newtio.c_oflag = 0;
	// newtio.c_oflag &= ~OPOST;
 	// ICANON  : active l'entrée en mode canonique
 	// désactive toute fonctionnalité d'echo, et n'envoit pas de signal au
	// programme appelant.
 	// à présent, on vide la ligne du modem, et on active la configuration
 	// pour le port
	
	newtio.c_cc[VMIN] = 10 ;
 	newtio.c_cc[VTIME] = 5 ;
	//cfsetospeed(&newtio, B115200);
	cfsetospeed(&newtio, B9600);
	tcflush(lfd, TCIFLUSH);
 	tcsetattr(lfd,TCSANOW,&newtio);

	*fd = lfd ;
return 0 ;
}


int read_serial(int fd,struct termios oldtio,char *buf)
{
volatile int STOP=FALSE;
int res = 0 ;

	/*
	Additionally, keep in mind that c_cc[VMIN] will control how long to
	wait, too...
	*/
	memset(buf,0,sizeof(buf)) ;
	while (STOP==FALSE) {     /* boucle jusqu'à condition de terminaison */
 	/* read bloque l'exécution du programme jusqu'à ce qu'un caractère de
 	fin de ligne soit lu, même si plus de 255 caractères sont saisis.
 	Si le nombre de caractères lus est inférieur au nombre de caractères
 	disponibles, des read suivant retourneront les caractères restants.
 	res sera positionné au nombre de caractères effectivement lus */
 	res = read(fd,buf,255);
 	buf[res]=0;       /* on termine la ligne, pour pouvoir l'afficher */
 	//if ((buf[0]=='z') & (buf[(res-1)]=='z')) STOP=TRUE;
	if (buf[0]=='z')  STOP=TRUE;
	//printf("%x ",buf[res] ) ;
	}
	SYNCHRO_MSG("Message detected on serial port, length = %d fd = %d\033[22;30m\n",res,fd) ;
	SYNCHRO_MSG("%s\033[22;30m\n", buf);
 	/* restaure les anciens paramètres du port */
 	tcsetattr(fd,TCSANOW,&oldtio);
	if (fd >0) close(fd) ;
return 0 ;
}


int ExtractField(char *buf,struct file_synchro *f_synchro)
{
char first_field[255] ;

	//if (strstr(buf,"zACQUI") != NULL) 
	//		{
			sscanf(buf,"%s %d %s",&first_field,&f_synchro->nb_image,&f_synchro-> fileName_param) ;
			SYNCHRO_MSG("params: %s nb_image:%d  filename:%s \033[22;30m\n", first_field,f_synchro->nb_image, f_synchro-> fileName_param) ;
	//		}
	//	else	{ SYNCHRO_MSG("CMD %s not allowed \n", f_synchro->line); fclose(f_synchro->cmdfile); return -2; }

return 0 ;
}

int write_serial(int fd,struct termios oldtio,char *buf)
{
int status ;

	usleep(200000) ;
	SYNCHRO_MSG("write_serial, length = %d\033[22;30m\n",strlen(buf)) ;
	SYNCHRO_MSG("Message = %s \n",buf) ;
	status = write(fd,buf,(size_t) strlen(buf)) ;
	//SYNCHRO_MSG("write_serial status = %d \n",status) ;
	usleep(200000) ;
	tcsetattr(fd,TCSANOW,&oldtio);
	if (fd > 0)
		close(fd) ;
	
return 0 ;
}


int socket_synchro(void)
{
return 0 ;
}

