/**
* \file synchro.h
* \brief The header file of synchro.cpp
* \author Patrick BREUGNON
* \version 1.0
* \date June 05 2009
 */


#ifndef _SYNCHRO_H_
#define _SYNCHRO_H_

struct file_synchro
{
	int printWait;
	char *path;
	char *cmdname;
	//char fileselected[300] ;
	int  attente;	
	FILE *cmdfile ;
	char *line ;
	size_t len  ;
	int  time_param ;
	char fileName_param[500] ;
	int  index_param ;
	int serial_fd ;
	int nb_image ;
	
}  ;

int init_file_synchro_struct(struct file_synchro *f_synchro,int wsynchro) ;
int read_file_synchro(struct file_synchro *f_synchro,int wsynchro) ;
int write_file_synchro(struct file_synchro *f_synchro,int* roi) ;

int config_serial_synchro(int *fd,struct termios oldtio,struct termios newtio,int rw) ;
int read_serial(int fd,struct termios oldtio,char *buf) ;
int write_serial(int fd,struct termios oldtio,char *buf) ;
int ExtractField(char *buf,struct file_synchro *f_synchro) ;

int socket_synchro(void) ;


#endif

