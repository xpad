TEMPLATE	= app
LANGUAGE	= C++

CONFIG	+= qt warn_on thread

LIBS	+= -pthread -lm -ldl -lusb -lquickusb -L/usr/lib/root -lGui -L/usr/lib/root/5.18 -lQtGSI -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -rdynamic

DEFINES	+= USE_ROOT_API SYNCHRO_MSG CALIB_MSG USB_ERR DAQ_MSG BARRETTE_MSG _REENTRANT

INCLUDEPATH	+= ../xpad ../QuickUSB /usr/include/root /usr/include/root/gui/gui/inc /usr/include/root/gui/qtgsi/inc

SOURCES	+= main.cpp \
	RootAPI.cpp \
	synchro.cpp \
        calibration.cpp \
	../xpad/barrette.cpp \
	../xpad/cyclone.cpp \
	../xpad/usbwrap.cpp \
	../xpad/utilities.cpp \
	../QuickUSB/CQuickUsb.cpp

#The following line was changed from FORMS to FORMS3 by qt3to4
FORMS3	= DAQ_XPAD.ui

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}


#The following line was inserted by qt3to4
QT +=  qt3support 
#The following line was inserted by qt3to4
CONFIG += uic3

