/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

/*****************************************************************************
//
// Modifs du soft
// le 08-09-2009
//  1 - Masquage du bit 15 de la valeur OVF, dans fichier utilities.cpp
//       AddLSBMatrixandMSBMatrix(int lsb_source_matrix[][(80*7)],int msb_source_matrix[][(80*7)],int dest_matrix[][(80*7)])
//    2 - Modif de la recherche de la valeur de DACL, on sort de la boucle si if((cmatrix[i][y][x]  == 0)  & (cmatrix[i+1][y][x] > 0) )  
// 	     Search_DACL(int cmatrix[][120*8][80*7],int *module_ready,int walgo,int noise_accepted)
*****************************************************************************/

//CalibModID_ComboBox
#include <iostream>
#include <cstdlib>
#include <cstring>
//#include <stdlib>
#include <fstream>
#include <strstream>
#include <istream>
#include <vector>

#include <unistd.h>


#include <q3textedit.h> 
#include <qtimer.h> 
#include <qcombobox.h>
#include <qstatusbar.h>
#include <q3filedialog.h>
#include <q3buttongroup.h> 
#include <qmessagebox.h>
#include <qapplication.h> 
#include <qwidget.h> 
#include <qevent.h> 
#include <qsizepolicy.h>
#include <qtooltip.h> 
#include <qsettings.h> 
#include <qlayout.h> 
//Added by qt3to4:
#include <QTextOStream>
#include <math.h>
#include <qstring.h>
#include <qlineedit.h>
#include <q3textstream.h>
#include <q3canvas.h>



/*****************************/
/*    Include ROOT                           */
/*****************************/
#include "TQRootCanvas.h"
#include "TCanvas.h" 
#include "TH2F.h" 
#include "TStyle.h" 
#include "TSystem.h" 
#include "TAttImage.h"
#include "TColor.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TFrame.h"
#include "defines.h"
#include "TGClient.h"
#include "TROOT.h"
#include "TBrowser.h"

//#include "TQtWidget.h"

#include "RootAPI.h"
#include "synchro.h"

#include "time.h"

/*
#include <sys/types.h>
#include <sys/stat.h>
*/
#include <sys/ioctl.h>
#include <termios.h>
#include <fcntl.h>

//#include "serial.h"


//#include "ThreadTimerAPI.h"


//#include <qwt_color_map.h>
//#include <qwt_plot_spectrogram.h>
//#include <qwt_scale_widget.h>
//#include <qwt_scale_draw.h>

//#include <qwt_plot_zoomer.h>
//#include <qwt_plot_panner.h>
//#include <qwt_plot_layout.h>



/**
* \file DAQ_XPAD.cpp
* \brief The top file of the project, initially named DAQ_XPAD.ui.h \n
*  NOTA : The file DAQ_XPAD.ui.h has been rename especially to generate documentation with doxygen \n
*            - All functions have also been rename :: replace be __ \n
*            - For exemple, void MainGUI::init()  become void MainGUI__init() ;
*
* \author Patrick BREUGNON
* \version 1.0
* \date April 08 2008
 */



#include "defines.h"
#include "CQuickUsb.h"
#include "usbwrap.h"
#include "cyclone.h"
#include "utilities.h"
#include "barrette.h"
#include "pthread.h"

#include "DAQ_XPAD.h"

#include "common.h"
#include "calibration.h"

#ifdef USE_XPADDET_LIB
#include "XPadDetector.h"
#endif


// QuickUsb pointer

#ifdef DAQ_MSG
#define DAQ_MSG(fmt,args...) printf("\033[22;34mdaq_msg :  " fmt,##args)  
#else
#define DAQ_MSG(fmt,args...)
#endif

#ifdef USE_XPADDET_LIB
#define INTERFACE_ON	0
#else
#define INTERFACE_ON	1
#endif

#define DELETE_H2	0

#define with_imageUS_X_samples  	1 
#define SERIAL_SYNCHRO_ACTIVATED	1

#define TEST_SERIAL_COM			0


class QSpinBox;

int config_dac[7][8] ;
string current ;
string wconfig [10] = {"CMOS_Dis","ITHH","VADj","VRef","IMFP","IOTA","IPRE","ITHL","ITUNE","IBuffer" } ;
string ConfigFile[10] ;
CQuickUsb *qusb = NULL ;
QString QStrFileSelected ;
int kindoffile ;
int Prev_FlatConfig[7] = {0,0,0,0,0,0,0} ;
pthread_t plot_thread ; 
int ret ;
int timer_progress ;
int timer_totalsteps ;
int timer_steps ;
QTimer *gen_timer = new QTimer() ;
QTimer *ctb_timer = new QTimer() ;
char message[1000] ;
unsigned int toggle = 0 ;
unsigned int inc = 0 ;

char PathAndfileName[1000] ;

int timer_type ;

int thread_valid = 0 ;
//ClassImp(MainGUI) ;

TCanvas *mClone  ;
TH1F *h1 = new TH1F("h1","",64,0,64) ;

TH1F *sourceHist = new TH1F("source","source hist",100,-3,3);
TH1F *finalHist = new TH1F("final","final hist",100,-3,3);

int dmatrix[(120*8)][80*7] ;
int dlsbmatrix[(120*8)][80*7] ;
int dmsbmatrix[(120*8)][80*7] ;

int refmatrix[(120*8)][80*7] ;
int error_matrix[(120*8)][80*7] ;

int summatrix[(120*8)][80*7] ;
int index_img ; 

int WMODULE = 0 ;

int ConfigG_tab[8][7] ;
QString FlatConfig_tab[8][7] ;

int ModuleReady_tab[9] ;
int MaxModuleReady ;
int module_msk ;

int handShake_val = 0 ;
int addr_return = 0 ; 

int MSB_LSB ;
int FAST_READOUT ;

int roi[4] ;

int debug_matrix[64][120*8][80*7] ;
int dacl_matrix[120*8][80*7] ;
int down_dacl_matrix[120*8][80*7] ;

clock_t start_time ;
int enable_loop = 0 ;

int total_error = 0 ;

extern int cmos_dis[8][7] ;
extern int ithh_val[8][7] ;
extern int vadj_val[8][7] ;
extern int vref_val[8][7] ;
extern int imfp_val[8][7] ; 
extern int iota_val[8][7] ;
extern int ipre_val[8][7] ;
extern int ithl_val[8][7] ;
extern int itune_val[8][7]  ;
extern int ibuffer_val[8][7] ;
extern int ith_return[8][7] ;


struct file_synchro synchro ; 


/*
struct EdfConfigFile_
	{
	int ImageNb ;
	char *HeaderID ;
	char *DetectorIdent ;
	char *ByteOrder ;
	char *DataType ;
	int Dim_X ;
	int Dim_Y ;
	char *ConfigFileNameModule1 ; 
	char *ConfigFileNameModule2 ;
	char *ConfigFileNameModule3 ;
	char *ConfigFileNameModule4 ;
	char *ConfigFileNameModule5 ;
	char *ConfigFileNameModule6 ;
	char *ConfigFileNameModule7 ;
	char *ConfigFileNameModule8 ;
	float Exposure_sec ;
	char *Experiment_date ;
	char *Experiment_time ;
	} ;


*/

struct EdfConfigFile_ EdfConfigFile ;
int integration_time  ; 

int Module_Answer = 0 ;
int Max_Answer = 0 ;
 
int loop_img ;
int loop_synchro_img ;

#ifdef USE_XPADDET_LIB
extern XPadDetConfig 			t_DetConfig  ;
extern XPadDetRegConfig 		t_RegConfig ;
extern XPadDetFileConfig 		t_FileConfig[MAX_MODULE] ;
extern XPadDetDACConfig  		t_DACConfig[MAX_MODULE] ;
extern XPadDetCalibConfig 		t_CalibConfig  ;
extern XPadDetAcqParameters		t_AcqParameters ;
extern XPadDetImageParameters  		t_ImageParameters ;
extern XPadDetFlatConfig  		t_FlatConfig[MAX_MODULE] ;
#endif

using namespace std;


/**
* \fn void MainGUI::init()
* \brief Function excecuted when start the software \brief
*
* THE SEQUENCE OF INITIALIZATION \n
* Find the module connected to the USB interface => qusb->FindModules(nameList, length) \n
* Initialise the pointer to the interface => qusb = new CQuickUsb(currentDevice.c_str()) \n
* Reset the two FIFOS implemented in the FPGA "OPTO_RFIFO_ACLR" and "OPTO_TFIFO_ACLR" => ResetSignal(qusb,OPTO_RFIFO_ACLR) and ResetSignal(qusb,OPTO_TFIFO_ACLR) \n
* Read the version of the FPGA project 	=> qusb->ReadCommand((unsigned short) FPGA_VERSION,data,&length_data) \n
* Make a connection between Qt Widget and function => MainGUI::Connect_Signals_Main_Config_Panel_Slot() \n
* Enable and disable widget on the GUI => MainGUI::EnableDisableControls_Slot() \n
* Make a test communication for the first module => MainGUI::AskReadyLoop_Slot() \n
* Initialize the Main Config Panel with default values => MainGUI::ConfigG_SetDefaultValue_Slot() \n
* Finally if not failed, the sofware is ready to communicate with the detector
*
* \param 
* \return 
*/
void MainGUI::exec(void)
{
int i ;
char *str = NULL ;
char nameList[256];
unsigned long length = sizeof(nameList);
int result;
string currentDevice ;
unsigned char data[3] ;        
unsigned short length_data = sizeof(data)  ;

	
	if (INTERFACE_ON)
	{
	result = qusb->FindModules(nameList, length);
   	 if (result == 0)  
     		{
		//IdentifyQuickUSBError(qusb) ;
		// DisplayString( "Cannot open Device:"+currentDevice);
		printf("\n") ;
		printf("\033[22;31m*******************************************************************************\033[22;30m\n") ;
		printf("\033[22;31m                      ERROR : Unable to find the QuickUSB module\033[22;30m\n") ;
		//return  ;
		}
	        
    	moduleComboBox->clear() ;
    	str = nameList;
	if (strlen(nameList) == 0) 
		{
		printf("\033[22;31m               ERROR : USB_OPTO card not connected, program abort\033[22;30m\n") ;
		printf("\033[22;31m*******************************************************************************\033[22;30m\n") ;
		printf("\n") ;
		return ;
		}
	else
        	DAQ_MSG("\033[22;31mQuickUSB Module list => %s\033[22;30m\n",nameList) ;    	

	if (strlen(nameList) != 0)
		{
		while (*str != '\0') 
			{
			moduleComboBox->insertItem(str, -1);
			str = str + strlen(str) + 1;
			}
	
		currentDevice = (moduleComboBox->text(0)).ascii();
		qusb = new CQuickUsb(currentDevice.c_str());
	
		result = qusb->Open() ;
		if (result == 0)  
			{
			IdentifyQuickUSBError(qusb) ;
			printf("\033[22;31mERROR : Unable to open the QuickUSB module\033[22;30m\n") ;
			//return  ;
			}
    		//unsigned short  MajorVersion ;
    		//unsigned short  MinorVersion ;
    		// unsigned short  BuildVersion ;
    		// qusb->GetDriverVersion(&MajorVersion, &MinorVersion, &BuildVersion);
    		// printf("DriverVersion => MajorVersion = %x MinorVersion = %x BuildVersion = %x\n",&MajorVersion,&MinorVersion, &BuildVersion);
    		// Read FPGA programm version
   		length_data = 2 ;
   
		ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
		ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
		
     		qusb->ReadCommand((unsigned short) FPGA_VERSION,data,&length_data);
    		qusb->ReadCommand((unsigned short) FPGA_VERSION,data,&length_data);
    		DAQ_MSG("\033[22;31mFPGA Program version => 0x%x%x\033[22;30m\n",data[1],data[0]) ;
    		//USB_MSG("FPGA Program version => 0x%x%x\n",data[1],data[0]) ;
    		if ((data[1] == 0x2b) & (data[0] == 0x3c))
			{
			USBOPTOVersion->setText("V2.0") ;
			ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
			ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
			WriteWordToModule(qusb,0,0,DUMMY_MSG) ; 
			WriteWordToModule(qusb,0,0,DUMMY_MSG) ; 
    			ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
			ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
			}
		else
			{	
	//		std::sprintf(message,"<font color=\"red\">Interface not avaliable\n\n ") ;
	//		std::strcat(message,"<font color=\"blue\">You must follow this procedure\n1: Disconnect and reconnect the USB cable on the USB_OPTO card\n2: Program the FPGA on excecuting the command\n    nios2-configure-sof USB_OPTO_V2.sof\n") ;
			std::sprintf(message,"Interface not avaliable\n") ;
			std::strcat(message,"You must follow this procedure\n1: Disconnect and reconnect the USB cable on the USB_OPTO card\n2: Program the FPGA on excecuting the command\n    nios2-configure-sof USB_OPTO_V2.sof\n") ;
			QMessageBox::critical(this, tr("**********  INTERFACE ERROR  **********"), tr(message));
			 MainGUI::close() ;
			}
		}

		mClone = (TCanvas *) (tQRootCanvas2->GetCanvas())->DrawClone("MENU") ;
		mClone->ToggleEventStatus(); // show event status bar
		connect( gen_timer, SIGNAL(timeout()), this, SLOT(myTimerEvent_Slot()) );
	//	connect( ctb_timer, SIGNAL(timeout()), this, SLOT(CTB_TimerSlot()) );
	
		MainGUI::Connect_Signals_Main_Config_Panel_Slot() ;
		MainGUI::FastReadout_Slot() ;
	
		if (INTERFACE_ON)
			MainGUI::AskReadyLoop_Slot() ;
		else 
	    		{
	    		for (i=0;i<MAXMODULE;i++)	
	    			ModuleReady_tab[i] = 1 ;
	    		MaxModuleReady = 8 ;
	    		printf("\n\n") ;
	    		DAQ_MSG("\033[22;31m******************************************************************\033[22;30m\n") ;
	    		DAQ_MSG("\033[22;31m*           WARNING: INTERFACE USB NOT CONNECTED                 *\033[22;30m\n") ;
	    		DAQ_MSG("\033[22;31m*                                                                *\033[22;30m\n") ;
	    		DAQ_MSG("\033[22;31m*           This mode is used only for debug purpose             *\033[22;30m\n") ;
	    		DAQ_MSG("\033[22;31m*                                                                *\033[22;30m\n") ;
	    		DAQ_MSG("\033[22;31m******************************************************************\033[22;30m\n") ;
	    		printf("\n\n") ;
	    		}

		MainGUI::EnableDisableControls_Slot() ;    
		MainGUI::ConfigG_SetDefaultValue_Slot() ;
	
		MSB_LSB = 0 ;
		FAST_READOUT = 1 ;
		
		}
	else
		MainGUI::close() ; // Close the main window	
}


/**
* \fn void MainGUI::Connect_Signals_Main_Config_Panel_Slot()
* \brief Make a connection between Qt Widgets, buttons,  and functions
*
* \param
* \return
*/
void MainGUI::Connect_Signals_Main_Config_Panel_Slot()
{
	/*
	connect( Value_SpinBox_1_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_WriteValue_Slot()) );
	connect( Value_SpinBox_1_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_1_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_1_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_1_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_1_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_1_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	
	connect( Value_SpinBox_2_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_2_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_2_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_2_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_2_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_2_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_2_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	
	connect( Value_SpinBox_3_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_3_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_3_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_3_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_3_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_3_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_3_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	
	connect( Value_SpinBox_4_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_4_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_4_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_4_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_4_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_4_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_4_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	
	connect( Value_SpinBox_5_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_5_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_5_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_5_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_5_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_5_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_5_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	
	connect( Value_SpinBox_6_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_6_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_6_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_6_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_6_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_6_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_6_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	
	connect( Value_SpinBox_7_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_7_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_7_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_7_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_7_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_7_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_7_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	
	connect( Value_SpinBox_8_1, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_8_2, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	connect( Value_SpinBox_8_3, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_8_4, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );	
	connect( Value_SpinBox_8_5, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_8_6, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) ); 
	connect( Value_SpinBox_8_7, SIGNAL(valueChanged(int)), this, SLOT(ConfigG_Valid_Slot()) );
	*/

	connect( Value_SpinBox_1_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_1_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_1_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_1_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_1_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_1_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_1_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	
	connect( Value_SpinBox_2_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_2_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_2_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_3_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_3_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_3_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_3_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_3_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_3_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_3_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_4_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_4_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_4_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_5_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_5_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_5_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_6_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_6_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_6_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_7_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_7_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_7_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_8_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_8_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_8_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_8_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_8_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_8_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_8_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );

	
	connect( ConfigFile_Mod_1, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_1_Slot()) );
	connect( ConfigFile_Mod_2, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_2_Slot()) );
	connect( ConfigFile_Mod_3, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_3_Slot()) );
	connect( ConfigFile_Mod_4, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_4_Slot()) );
	connect( ConfigFile_Mod_5, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_5_Slot()) );
	connect( ConfigFile_Mod_6, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_6_Slot()) );
	connect( ConfigFile_Mod_7, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_7_Slot()) );
	connect( ConfigFile_Mod_8, SIGNAL(clicked()), this, SLOT(ConfigFile_Mod_8_Slot()) );
	
	connect( SendConfig_Mod_1, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_1_Slot()) );
	connect( SendConfig_Mod_2, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_2_Slot()) );
	connect( SendConfig_Mod_3, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_3_Slot()) );
	connect( SendConfig_Mod_4, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_4_Slot()) );
	connect( SendConfig_Mod_5, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_5_Slot()) );	
	connect( SendConfig_Mod_6, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_6_Slot()) );
	connect( SendConfig_Mod_7, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_7_Slot()) );
	connect( SendConfig_Mod_8, SIGNAL(clicked()), this, SLOT(SendConfig_Mod_8_Slot()) );
	
	//connect( FlatConfiglineEdit_1_1, SIGNAL(textChanged(const QString&)), this, SLOT(FlatConfig_Valid_Slot()) );
	
	connect( SendFlatConfig_Mod_1, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_1_Slot()) );
	connect( SendFlatConfig_Mod_2, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_2_Slot()) );
	connect( SendFlatConfig_Mod_3, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_3_Slot()) );
	connect( SendFlatConfig_Mod_4, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_4_Slot()) );
	connect( SendFlatConfig_Mod_5, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_5_Slot()) );	
	connect( SendFlatConfig_Mod_6, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_6_Slot()) );
	connect( SendFlatConfig_Mod_7, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_7_Slot()) );
	connect( SendFlatConfig_Mod_8, SIGNAL(clicked()), this, SLOT(SendFlatConfig_Mod_8_Slot()) );
	connect(FlatConfig_SendAll_Button, SIGNAL(clicked()), this, SLOT(FlatConfig_SendAll_Slot()) );
	
	connect( Calib_File_Mod_1, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_1_Slot()) );
	connect( Calib_File_Mod_2, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_2_Slot()) );
	connect( Calib_File_Mod_3, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_3_Slot()) );
	connect( Calib_File_Mod_4, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_4_Slot()) );
	connect( Calib_File_Mod_5, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_5_Slot()) );
	connect( Calib_File_Mod_6, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_6_Slot()) );
	connect( Calib_File_Mod_7, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_7_Slot()) );
	connect( Calib_File_Mod_8, SIGNAL(clicked()), this, SLOT(Calib_File_Mod_8_Slot()) );
	
	connect( ROI_Button, SIGNAL(clicked()), this, SLOT(ROI_Slot()) );
	connect(DownloadImageIndex_Button, SIGNAL(clicked()), this, SLOT(DownloadImageIndex_Slot()) );	
	
	connect(M1_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_1_Slot()) ) ;
	connect(M2_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_2_Slot()) ) ;
	connect(M3_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_3_Slot()) ) ;
	connect(M4_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_4_Slot()) ) ;
	connect(M5_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_5_Slot()) ) ;
	connect(M6_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_6_Slot()) ) ;
	connect(M7_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_7_Slot()) ) ;
	connect(M8_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_8_Slot()) ) ;
	connect(MALL_pushButton,SIGNAL(clicked()),this,SLOT(ValidateModule_ALL_Slot()) ) ;
	
	//connect(ResetModuleButton,SIGNAL(clicked()),this,SLOT(ResetModule_Slot()) ) ;
	
	connect(MFunction_ComboBox,SIGNAL(activated(const QString&)),this,SLOT(MFunction_Slot()) ) ;
	
	connect(Increment_PushButton,SIGNAL(clicked()),this,SLOT(IncrementValue_Slot()) ) ;
	connect(Decrement_PushButton,SIGNAL(clicked()),this,SLOT(DecrementValue_Slot()) ) ;
	
	//connect(MSB_LSB_ComboBox,SIGNAL(textChanged(const QString&)),this,SLOT(MSBandLSB_Slot()))  ;
	connect(MSB_LSB_ComboBox,SIGNAL(activated(const QString&)),this,SLOT(MSBandLSB_Slot()))  ;
	connect(FastReadout_ComboBox,SIGNAL(activated(const QString&)),this,SLOT(FastReadout_Slot()))  ;	
	
	connect( ConfigFile_MasterMode, SIGNAL(clicked()), this, SLOT(ConfigFileMasterMode_Slot()) );
}


void MainGUI::Connect_ConfiGControl_Slot()
{
	/*
	connect(Value_SpinBox_1_1,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	connect(Value_SpinBox_1_2,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	connect(Value_SpinBox_1_3,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	connect(Value_SpinBox_1_4,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	connect(Value_SpinBox_1_5,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	connect(Value_SpinBox_1_6,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	connect(Value_SpinBox_1_7,SIGNAL(valueChanged(int)),this,SLOT(ConfigGValueModified_Slot()) ) ;
	*/
	connect( Value_SpinBox_1_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_1_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_1_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_1_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_1_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_1_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_1_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	
	connect( Value_SpinBox_2_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_2_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_2_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_2_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_3_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_3_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_3_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_3_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_3_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_3_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_3_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_4_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_4_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_4_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_4_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_5_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_5_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_5_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_5_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_6_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_6_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_6_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_6_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_7_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_7_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_7_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_7_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	
	connect( Value_SpinBox_8_1, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_8_2, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );
	connect( Value_SpinBox_8_3, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_8_4, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );	
	connect( Value_SpinBox_8_5, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_8_6, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) ); 
	connect( Value_SpinBox_8_7, SIGNAL(valueChanged(int)), this, SLOT(ModifCurrentControl_Slot()) );


}


void MainGUI::Disconnect_ConfiGControl_Slot()
{
	disconnect(Value_SpinBox_1_1,0,0,0) ;
	disconnect(Value_SpinBox_1_2,0,0,0) ;
	disconnect(Value_SpinBox_1_3,0,0,0) ;
	disconnect(Value_SpinBox_1_4,0,0,0) ;
	disconnect(Value_SpinBox_1_5,0,0,0) ;
	disconnect(Value_SpinBox_1_6,0,0,0) ;
	disconnect(Value_SpinBox_1_7,0,0,0) ;
	
	disconnect(Value_SpinBox_2_1,0,0,0) ;
	disconnect(Value_SpinBox_2_2,0,0,0) ;
	disconnect(Value_SpinBox_2_3,0,0,0) ;
	disconnect(Value_SpinBox_2_4,0,0,0) ;
	disconnect(Value_SpinBox_2_5,0,0,0) ;
	disconnect(Value_SpinBox_2_6,0,0,0) ;
	disconnect(Value_SpinBox_2_7,0,0,0) ;
	
	disconnect(Value_SpinBox_3_1,0,0,0) ;
	disconnect(Value_SpinBox_3_2,0,0,0) ;
	disconnect(Value_SpinBox_3_3,0,0,0) ;
	disconnect(Value_SpinBox_3_4,0,0,0) ;
	disconnect(Value_SpinBox_3_5,0,0,0) ;
	disconnect(Value_SpinBox_3_6,0,0,0) ;
	disconnect(Value_SpinBox_3_7,0,0,0) ;
	
	disconnect(Value_SpinBox_4_1,0,0,0) ;
	disconnect(Value_SpinBox_4_2,0,0,0) ;
	disconnect(Value_SpinBox_4_3,0,0,0) ;
	disconnect(Value_SpinBox_4_4,0,0,0) ;
	disconnect(Value_SpinBox_4_5,0,0,0) ;
	disconnect(Value_SpinBox_4_6,0,0,0) ;
	disconnect(Value_SpinBox_4_7,0,0,0) ;
	
	disconnect(Value_SpinBox_5_1,0,0,0) ;
	disconnect(Value_SpinBox_5_2,0,0,0) ;
	disconnect(Value_SpinBox_5_3,0,0,0) ;
	disconnect(Value_SpinBox_5_4,0,0,0) ;
	disconnect(Value_SpinBox_5_5,0,0,0) ;
	disconnect(Value_SpinBox_5_6,0,0,0) ;
	disconnect(Value_SpinBox_5_7,0,0,0) ;
	
	disconnect(Value_SpinBox_6_1,0,0,0) ;
	disconnect(Value_SpinBox_6_2,0,0,0) ;
	disconnect(Value_SpinBox_6_3,0,0,0) ;
	disconnect(Value_SpinBox_6_4,0,0,0) ;
	disconnect(Value_SpinBox_6_5,0,0,0) ;
	disconnect(Value_SpinBox_6_6,0,0,0) ;
	disconnect(Value_SpinBox_6_7,0,0,0) ;
	
	disconnect(Value_SpinBox_7_1,0,0,0) ;
	disconnect(Value_SpinBox_7_2,0,0,0) ;
	disconnect(Value_SpinBox_7_3,0,0,0) ;
	disconnect(Value_SpinBox_7_4,0,0,0) ;
	disconnect(Value_SpinBox_7_5,0,0,0) ;
	disconnect(Value_SpinBox_7_6,0,0,0) ;
	disconnect(Value_SpinBox_7_7,0,0,0) ;
	
	disconnect(Value_SpinBox_8_1,0,0,0) ;
	disconnect(Value_SpinBox_8_2,0,0,0) ;
	disconnect(Value_SpinBox_8_3,0,0,0) ;
	disconnect(Value_SpinBox_8_4,0,0,0) ;
	disconnect(Value_SpinBox_8_5,0,0,0) ;
	disconnect(Value_SpinBox_8_6,0,0,0) ;
	disconnect(Value_SpinBox_8_7,0,0,0) ;

}


/************************************************
*
*		Generals Functions
*
************************************************/


/**
* \fn void MainGUI::ResetModule_Slot()
* \brief Reset Command of all modules connected
*
* \param
* \return
*/
void MainGUI::ResetModule_Slot()
{
//ModuleID_ComboBox->currentItem(),ConfigGWChip_ComboBox->currentItem()
 
	//for (i=0;i<NbofModules_spinBox->value();i++)
	//	{
	if(FAST_READOUT)
		ResetModule(qusb,0xff)  ;
	else
		ResetModule(qusb,ALL_MODULES)  ;
		
 	DAQ_MSG("Reset all modules\033[22;30m\n") ;
	
	/*
	for(i=0;i<MaxModuleReady;i++)
		{
		if (ModuleReady_tab[i] == 0)
			{
			MODULE = i+1 ;
			ResetModule(qusb,MODULE)  ;
 		//ResetModule(qusb,ALL_MODULES)  ;
 			DAQ_MSG("%d : Reset module_%d\033[22;30m\n",i,MODULE) ;
			datar_length = ReadFromModule(qusb,datar,(TIMEOUT),4) ;
			}
		}
	*/
		/*
		if (datar_length > 128)  datar_length = 128 ;
		if (datar_length == -1) printf("ERROR fifo empty \n") ;
		for (ii=0;ii<datar_length;ii++)
			printf("0x%x ",datar[ii]) ;
		printf("\n") ;
		*/
	//	}
}


/**
* \fn void MainGUI::ResetHUBFifo_Slot()
* \brief Reset the fifo contains of the HUB card
*
* \param
* \return
*/
void MainGUI::ResetHUBFifo_Slot()
{
int datar[MAX_RFIFO_SIZE] ;
int datar_length ;
//int i ;

	ResetHUBFifo(qusb) ;
	DAQ_MSG("Reset HUB Fifo\033[22;30m\n" ) ;
	datar_length = ReadFromModule(qusb,datar,(TIMEOUT),4) ;
	/*
	if (datar_length > 128)  datar_length = 128 ;
	if (datar_length == -1) printf("ERROR fifo empty \n") ;
	for (i=0;i<datar_length;i++)
			printf("0x%x ",datar[i]) ;
	printf("\n") ;
	*/
}


/**
* \fn void MainGUI::SetWriteFile()
* \brief Display a Write FileDialog widget
*
* \param
* \return
*/
void MainGUI::SetWriteFile()
{
 if (kindoffile == DATA_FILE) {
  QString currentDirData = QString("%1%2").arg(QDir::currentDirPath(),"/data") ;
//  QString sFileName = QFileDialog::getOpenFileName(currentDir, tr("Image Files (*.dat"),this);
  QStrFileSelected = Q3FileDialog::getSaveFileName(currentDirData,"data file (*.dat)",this,"save file dialog","Choose a data file to save under" );
  }
 if (kindoffile == CFG_FILE) {
  QString currentDirCfg = QString("%1%2").arg(QDir::currentDirPath(),"/configs") ;
//  QString sFileName = QFileDialog::getOpenFileName(currentDir, tr("Image Files (*.cfg"),this);
  QStrFileSelected = Q3FileDialog::getSaveFileName(currentDirCfg,"cfg file (*.dacs)",this,"save file dialog","Choose a DACs file to save under" );
  }
}


/**
* \fn void MainGUI::SetReadFile()
* \brief Display a Read FileDialog widget
*
* \param
* \return
*/
void MainGUI::SetReadFile()
{
 if (kindoffile == DATA_FILE) {
  QString currentDirCfg = QString("%1%2").arg(QDir::currentDirPath(),"/data");
//  QString sFileName = QFileDialog::getOpenFileName(currentDir, tr("Image Files (*.cfg"),this);
  QStrFileSelected = Q3FileDialog::getOpenFileName(currentDirCfg,"data file (*.dat)",this,"open file dialog","Choose a data file" );
  }
 if (kindoffile == CFG_FILE) {
  QString currentDirCfg = QString("%1%2").arg(QDir::currentDirPath(),"/configs") ;
//  QString sFileName = QFileDialog::getOpenFileName(currentDir, tr("Image Files (*.cfg"),this);
  QStrFileSelected = Q3FileDialog::getOpenFileName(currentDirCfg,"config file (*.dacs)",this,"open file dialog","Choose a config file" );
  }
 if (kindoffile == SCAN_FILE) {
  QString currentDirCfg = QString("%1%2").arg(QDir::currentDirPath(),"/configs") ;
//  QString sFileName = QFileDialog::getOpenFileName(currentDir, tr("Image Files (*.cfg"),this);
  QStrFileSelected = Q3FileDialog::getOpenFileName(currentDirCfg,"config file (*.dat)",this,"open file dialog","Choose a scan file" );
  } 
   if (kindoffile == MST_FILE) {
  QString currentDirCfg = QString("%1%2").arg(QDir::currentDirPath(),"/configs") ;
//  QString sFileName = QFileDialog::getOpenFileName(currentDir, tr("Image Files (*.cfg"),this);
  QStrFileSelected = Q3FileDialog::getOpenFileName(currentDirCfg,"master file (*.master)",this,"open file dialog","Choose a master file" );
  }
 
}



/************************************************
*
*		TOP GUI PART
*
************************************************/

/**
* \fn void MainGUI::AskReadyLoop_Slot()
* \brief Make a loop on the AskReady function for all modules connected and test if modules answer 
*
*
* \param
* \return
*/
/*
void MainGUI::AskReadyLoop_Slot()
{
int i ;
int status ;
int addr_return ;

 
 	for (i=0;i<MAXMODULE;i++) ModuleReady_tab[i] = 0 ;
	
	M1_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M2_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M3_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M4_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M5_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M6_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M7_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 	M8_pushButton->setPaletteBackgroundColor( QColor( 230,230,230 ) );
 
 	MainGUI::EnableDisableControls_Slot() ;	
	
	DAQ_MSG("*********************************\n") ;
	for(i=0;i<NbofModules_spinBox->value();i++)
	//for(i=0;i<8;i++)
		{
		status = -1 ;
		status = AskReadyModule(qusb,(i+1),&addr_return) ;	
		//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
		if (status >0 ) 
			{
			if (addr_return == 1) M1_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 2) M2_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 3) M3_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 4) M4_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 5) M5_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 6) M6_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 7) M7_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (addr_return == 8) M8_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );	
			DAQ_MSG("Module_%d ready\n",(i+1)) ;
			ModuleReady_tab[i] = 1 ;
			}
		else 
			{
			if (i == 0) M1_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 1) M2_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 2) M3_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 3) M4_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 4) M5_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 5) M6_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 6) M7_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			if (i == 7) M8_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			DAQ_MSG("ERROR => Module_%d not avaliable\n",(i+1)) ;
			//return ;
			}
		if (status > 0) 
			{
			//NbofModules_spinBox->setValue(i) ;
			 MainGUI::EnableDisableControls_Slot() ;	
			}
		}	

}
*/
void MainGUI::AskReadyLoop_Slot()
{
	
	MainGUI::ValidateModule_1_Slot() ;
	MainGUI::ValidateModule_2_Slot() ;
	MainGUI::ValidateModule_3_Slot() ;
	MainGUI::ValidateModule_4_Slot() ;
	MainGUI::ValidateModule_5_Slot() ;
	MainGUI::ValidateModule_6_Slot() ;
	MainGUI::ValidateModule_7_Slot() ;
	MainGUI::ValidateModule_8_Slot() ;
}


void MainGUI::EnableDisableControls_Slot()
{

	ConfigFile_Mod_1->setEnabled( FALSE );
	ConfigFile_Mod_2->setEnabled( FALSE );
	ConfigFile_Mod_3->setEnabled( FALSE );
	ConfigFile_Mod_4->setEnabled( FALSE );
	ConfigFile_Mod_5->setEnabled( FALSE );
	ConfigFile_Mod_6->setEnabled( FALSE );
	ConfigFile_Mod_7->setEnabled( FALSE );
	ConfigFile_Mod_8->setEnabled( FALSE );

	Value_SpinBox_1_1->setEnabled( FALSE );
	Value_SpinBox_1_2->setEnabled( FALSE );
	Value_SpinBox_1_3->setEnabled( FALSE );
	Value_SpinBox_1_4->setEnabled( FALSE );
	Value_SpinBox_1_5->setEnabled( FALSE );
	Value_SpinBox_1_6->setEnabled( FALSE );
	Value_SpinBox_1_7->setEnabled( FALSE );
	
	Value_SpinBox_2_1->setEnabled( FALSE );
	Value_SpinBox_2_2->setEnabled( FALSE );
	Value_SpinBox_2_3->setEnabled( FALSE );
	Value_SpinBox_2_4->setEnabled( FALSE );
	Value_SpinBox_2_5->setEnabled( FALSE );
	Value_SpinBox_2_6->setEnabled( FALSE );
	Value_SpinBox_2_7->setEnabled( FALSE );
	
	Value_SpinBox_3_1->setEnabled( FALSE );
	Value_SpinBox_3_2->setEnabled( FALSE );
	Value_SpinBox_3_3->setEnabled( FALSE );
	Value_SpinBox_3_4->setEnabled( FALSE );
	Value_SpinBox_3_5->setEnabled( FALSE );
	Value_SpinBox_3_6->setEnabled( FALSE );
	Value_SpinBox_3_7->setEnabled( FALSE );
	
	Value_SpinBox_4_1->setEnabled( FALSE );
	Value_SpinBox_4_2->setEnabled( FALSE );
	Value_SpinBox_4_3->setEnabled( FALSE );
	Value_SpinBox_4_4->setEnabled( FALSE );
	Value_SpinBox_4_5->setEnabled( FALSE );
	Value_SpinBox_4_6->setEnabled( FALSE );
	Value_SpinBox_4_7->setEnabled( FALSE );
	
	Value_SpinBox_5_1->setEnabled( FALSE );
	Value_SpinBox_5_2->setEnabled( FALSE );
	Value_SpinBox_5_3->setEnabled( FALSE );
	Value_SpinBox_5_4->setEnabled( FALSE );
	Value_SpinBox_5_5->setEnabled( FALSE );
	Value_SpinBox_5_6->setEnabled( FALSE );
	Value_SpinBox_5_7->setEnabled( FALSE );
	
	Value_SpinBox_6_1->setEnabled( FALSE );
	Value_SpinBox_6_2->setEnabled( FALSE );
	Value_SpinBox_6_3->setEnabled( FALSE );
	Value_SpinBox_6_4->setEnabled( FALSE );
	Value_SpinBox_6_5->setEnabled( FALSE );
	Value_SpinBox_6_6->setEnabled( FALSE );
	Value_SpinBox_6_7->setEnabled( FALSE );
	
	Value_SpinBox_7_1->setEnabled( FALSE );
	Value_SpinBox_7_2->setEnabled( FALSE );
	Value_SpinBox_7_3->setEnabled( FALSE );
	Value_SpinBox_7_4->setEnabled( FALSE );
	Value_SpinBox_7_5->setEnabled( FALSE );
	Value_SpinBox_7_6->setEnabled( FALSE );
	Value_SpinBox_7_7->setEnabled( FALSE );
	
	Value_SpinBox_8_1->setEnabled( FALSE );
	Value_SpinBox_8_2->setEnabled( FALSE );
	Value_SpinBox_8_3->setEnabled( FALSE );
	Value_SpinBox_8_4->setEnabled( FALSE );
	Value_SpinBox_8_5->setEnabled( FALSE );
	Value_SpinBox_8_6->setEnabled( FALSE );
	Value_SpinBox_8_7->setEnabled( FALSE );
	
	SendConfig_Mod_1->setEnabled( FALSE );
	SendConfig_Mod_2->setEnabled( FALSE );
	SendConfig_Mod_3->setEnabled( FALSE );
	SendConfig_Mod_4->setEnabled( FALSE );
	SendConfig_Mod_5->setEnabled( FALSE );
	SendConfig_Mod_6->setEnabled( FALSE );
	SendConfig_Mod_7->setEnabled( FALSE );
	SendConfig_Mod_8->setEnabled( FALSE );
	
	Ack_SendConfig_Mod_1->setEnabled( FALSE );
	Ack_SendConfig_Mod_2->setEnabled( FALSE );
	Ack_SendConfig_Mod_3->setEnabled( FALSE );
	Ack_SendConfig_Mod_4->setEnabled( FALSE );
	Ack_SendConfig_Mod_5->setEnabled( FALSE );
	Ack_SendConfig_Mod_6->setEnabled( FALSE );
	Ack_SendConfig_Mod_7->setEnabled( FALSE );
	Ack_SendConfig_Mod_8->setEnabled( FALSE );

	FlatConfiglineEdit_1_1->setEnabled( FALSE );	
	FlatConfiglineEdit_1_2->setEnabled( FALSE );
	FlatConfiglineEdit_1_3->setEnabled( FALSE );
	FlatConfiglineEdit_1_4->setEnabled( FALSE );
	FlatConfiglineEdit_1_5->setEnabled( FALSE );
	FlatConfiglineEdit_1_6->setEnabled( FALSE );
	FlatConfiglineEdit_1_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_2_1->setEnabled( FALSE );	
	FlatConfiglineEdit_2_2->setEnabled( FALSE );
	FlatConfiglineEdit_2_3->setEnabled( FALSE );
	FlatConfiglineEdit_2_4->setEnabled( FALSE );
	FlatConfiglineEdit_2_5->setEnabled( FALSE );
	FlatConfiglineEdit_2_6->setEnabled( FALSE );
	FlatConfiglineEdit_2_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_3_1->setEnabled( FALSE );	
	FlatConfiglineEdit_3_2->setEnabled( FALSE );
	FlatConfiglineEdit_3_3->setEnabled( FALSE );
	FlatConfiglineEdit_3_4->setEnabled( FALSE );
	FlatConfiglineEdit_3_5->setEnabled( FALSE );
	FlatConfiglineEdit_3_6->setEnabled( FALSE );
	FlatConfiglineEdit_3_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_4_1->setEnabled( FALSE );	
	FlatConfiglineEdit_4_2->setEnabled( FALSE );
	FlatConfiglineEdit_4_3->setEnabled( FALSE );
	FlatConfiglineEdit_4_4->setEnabled( FALSE );
	FlatConfiglineEdit_4_5->setEnabled( FALSE );
	FlatConfiglineEdit_4_6->setEnabled( FALSE );
	FlatConfiglineEdit_4_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_5_1->setEnabled( FALSE );	
	FlatConfiglineEdit_5_2->setEnabled( FALSE );
	FlatConfiglineEdit_5_3->setEnabled( FALSE );
	FlatConfiglineEdit_5_4->setEnabled( FALSE );
	FlatConfiglineEdit_5_5->setEnabled( FALSE );
	FlatConfiglineEdit_5_6->setEnabled( FALSE );
	FlatConfiglineEdit_5_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_6_1->setEnabled( FALSE );	
	FlatConfiglineEdit_6_2->setEnabled( FALSE );
	FlatConfiglineEdit_6_3->setEnabled( FALSE );
	FlatConfiglineEdit_6_4->setEnabled( FALSE );
	FlatConfiglineEdit_6_5->setEnabled( FALSE );
	FlatConfiglineEdit_6_6->setEnabled( FALSE );
	FlatConfiglineEdit_6_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_7_1->setEnabled( FALSE );	
	FlatConfiglineEdit_7_2->setEnabled( FALSE );
	FlatConfiglineEdit_7_3->setEnabled( FALSE );
	FlatConfiglineEdit_7_4->setEnabled( FALSE );
	FlatConfiglineEdit_7_5->setEnabled( FALSE );
	FlatConfiglineEdit_7_6->setEnabled( FALSE );
	FlatConfiglineEdit_7_7->setEnabled( FALSE );
	
	FlatConfiglineEdit_8_1->setEnabled( FALSE );	
	FlatConfiglineEdit_8_2->setEnabled( FALSE );
	FlatConfiglineEdit_8_3->setEnabled( FALSE );
	FlatConfiglineEdit_8_4->setEnabled( FALSE );
	FlatConfiglineEdit_8_5->setEnabled( FALSE );
	FlatConfiglineEdit_8_6->setEnabled( FALSE );
	FlatConfiglineEdit_8_7->setEnabled( FALSE );
	
	SendFlatConfig_Mod_1->setEnabled( FALSE );
	SendFlatConfig_Mod_2->setEnabled( FALSE );
	SendFlatConfig_Mod_3->setEnabled( FALSE );
	SendFlatConfig_Mod_4->setEnabled( FALSE );
	SendFlatConfig_Mod_5->setEnabled( FALSE );
	SendFlatConfig_Mod_6->setEnabled( FALSE );
	SendFlatConfig_Mod_7->setEnabled( FALSE );
	SendFlatConfig_Mod_8->setEnabled( FALSE );
	
	Ack_SendFlatConfig_Mod_1->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_2->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_3->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_4->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_5->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_6->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_7->setEnabled( FALSE );
	Ack_SendFlatConfig_Mod_8->setEnabled( FALSE );
	
	
	ITHL_1_1->setEnabled( FALSE ) ;
	ITHL_1_2->setEnabled( FALSE ) ;
	ITHL_1_3->setEnabled( FALSE ) ;
	ITHL_1_4->setEnabled( FALSE ) ;
	ITHL_1_5->setEnabled( FALSE ) ;
	ITHL_1_6->setEnabled( FALSE ) ;
	ITHL_1_7->setEnabled( FALSE ) ;
	
	ITHL_2_1->setEnabled( FALSE ) ;
	ITHL_2_2->setEnabled( FALSE ) ;
	ITHL_2_3->setEnabled( FALSE ) ;
	ITHL_2_4->setEnabled( FALSE ) ;
	ITHL_2_5->setEnabled( FALSE ) ;
	ITHL_2_6->setEnabled( FALSE ) ;
	ITHL_2_7->setEnabled( FALSE ) ;

	ITHL_3_1->setEnabled( FALSE ) ;
	ITHL_3_2->setEnabled( FALSE ) ;
	ITHL_3_3->setEnabled( FALSE ) ;
	ITHL_3_4->setEnabled( FALSE ) ;
	ITHL_3_5->setEnabled( FALSE ) ;
	ITHL_3_6->setEnabled( FALSE ) ;
	ITHL_3_7->setEnabled( FALSE ) ;

	ITHL_4_1->setEnabled( FALSE ) ;
	ITHL_4_2->setEnabled( FALSE ) ;
	ITHL_4_3->setEnabled( FALSE ) ;
	ITHL_4_4->setEnabled( FALSE ) ;
	ITHL_4_5->setEnabled( FALSE ) ;
	ITHL_4_6->setEnabled( FALSE ) ;
	ITHL_4_7->setEnabled( FALSE ) ;
	
	ITHL_5_1->setEnabled( FALSE ) ;
	ITHL_5_2->setEnabled( FALSE ) ;
	ITHL_5_3->setEnabled( FALSE ) ;
	ITHL_5_4->setEnabled( FALSE ) ;
	ITHL_5_5->setEnabled( FALSE ) ;
	ITHL_5_6->setEnabled( FALSE ) ;
	ITHL_5_7->setEnabled( FALSE ) ;
	
	ITHL_6_1->setEnabled( FALSE ) ;
	ITHL_6_2->setEnabled( FALSE ) ;
	ITHL_6_3->setEnabled( FALSE ) ;
	ITHL_6_4->setEnabled( FALSE ) ;
	ITHL_6_5->setEnabled( FALSE ) ;
	ITHL_6_6->setEnabled( FALSE ) ;
	ITHL_6_7->setEnabled( FALSE ) ;
	
	ITHL_7_1->setEnabled( FALSE ) ;
	ITHL_7_2->setEnabled( FALSE ) ;
	ITHL_7_3->setEnabled( FALSE ) ;
	ITHL_7_4->setEnabled( FALSE ) ;
	ITHL_7_5->setEnabled( FALSE ) ;
	ITHL_7_6->setEnabled( FALSE ) ;
	ITHL_7_7->setEnabled( FALSE ) ;
	
	ITHL_8_1->setEnabled( FALSE ) ;
	ITHL_8_2->setEnabled( FALSE ) ;
	ITHL_8_3->setEnabled( FALSE ) ;
	ITHL_8_4->setEnabled( FALSE ) ;
	ITHL_8_5->setEnabled( FALSE ) ;
	ITHL_8_6->setEnabled( FALSE ) ;
	ITHL_8_7->setEnabled( FALSE ) ;

	Calib_File_Mod_1->setEnabled( FALSE ) ;
	Calib_File_Mod_2->setEnabled( FALSE ) ;
	Calib_File_Mod_3->setEnabled( FALSE ) ;
	Calib_File_Mod_4->setEnabled( FALSE ) ;
	Calib_File_Mod_5->setEnabled( FALSE ) ;
	Calib_File_Mod_6->setEnabled( FALSE ) ;
	Calib_File_Mod_7->setEnabled( FALSE ) ;
	Calib_File_Mod_8->setEnabled( FALSE ) ;
	
	
	Calib_ProgressBar_1->setEnabled( FALSE ) ;
	Calib_ProgressBar_2->setEnabled( FALSE ) ;
	Calib_ProgressBar_3->setEnabled( FALSE ) ;
	Calib_ProgressBar_4->setEnabled( FALSE ) ;
	Calib_ProgressBar_5->setEnabled( FALSE ) ;
	Calib_ProgressBar_6->setEnabled( FALSE ) ;
	Calib_ProgressBar_7->setEnabled( FALSE ) ;
	Calib_ProgressBar_8->setEnabled( FALSE ) ;
	
	
		if (ModuleReady_tab[0] == 1) 
			{
			 ConfigFile_Mod_1->setEnabled( TRUE );
			 Value_SpinBox_1_1->setEnabled( TRUE );
			Value_SpinBox_1_2->setEnabled( TRUE );
			Value_SpinBox_1_3->setEnabled( TRUE );
			Value_SpinBox_1_4->setEnabled( TRUE );
			Value_SpinBox_1_5->setEnabled( TRUE );
			Value_SpinBox_1_6->setEnabled( TRUE );
			Value_SpinBox_1_7->setEnabled( TRUE );
			SendConfig_Mod_1->setEnabled( TRUE );
			Ack_SendConfig_Mod_1->setEnabled( TRUE );
			
			FlatConfiglineEdit_1_1->setEnabled( TRUE );	
			FlatConfiglineEdit_1_2->setEnabled( TRUE );
			FlatConfiglineEdit_1_3->setEnabled( TRUE );
			FlatConfiglineEdit_1_4->setEnabled( TRUE );
			FlatConfiglineEdit_1_5->setEnabled( TRUE );
			FlatConfiglineEdit_1_6->setEnabled( TRUE );
			FlatConfiglineEdit_1_7->setEnabled( TRUE );
			
			SendFlatConfig_Mod_1->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_1->setEnabled( TRUE);
	
			ITHL_1_1->setEnabled( TRUE ) ;
			ITHL_1_2->setEnabled( TRUE ) ;
			ITHL_1_3->setEnabled( TRUE ) ;
			ITHL_1_4->setEnabled( TRUE ) ;
			ITHL_1_5->setEnabled( TRUE ) ;
			ITHL_1_6->setEnabled( TRUE ) ;
			ITHL_1_7->setEnabled( TRUE ) ;
			
			Calib_File_Mod_1->setEnabled( TRUE ) ;
			Calib_ProgressBar_1->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[1] == 1) 
			{
			ConfigFile_Mod_2->setEnabled( TRUE );
			Value_SpinBox_2_1->setEnabled( TRUE );
			Value_SpinBox_2_2->setEnabled( TRUE );
			Value_SpinBox_2_3->setEnabled( TRUE );
			Value_SpinBox_2_4->setEnabled( TRUE );
			Value_SpinBox_2_5->setEnabled( TRUE );
			Value_SpinBox_2_6->setEnabled( TRUE );
			Value_SpinBox_2_7->setEnabled( TRUE );
			SendConfig_Mod_2->setEnabled( TRUE );
			Ack_SendConfig_Mod_2->setEnabled( TRUE );
			
			FlatConfiglineEdit_2_1->setEnabled( TRUE );	
			FlatConfiglineEdit_2_2->setEnabled( TRUE );
			FlatConfiglineEdit_2_3->setEnabled( TRUE );
			FlatConfiglineEdit_2_4->setEnabled( TRUE );
			FlatConfiglineEdit_2_5->setEnabled( TRUE );
			FlatConfiglineEdit_2_6->setEnabled( TRUE );
			FlatConfiglineEdit_2_7->setEnabled( TRUE );
		
			SendFlatConfig_Mod_2->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_2->setEnabled( TRUE);
			
			ITHL_2_1->setEnabled( TRUE ) ;
			ITHL_2_2->setEnabled( TRUE ) ;
			ITHL_2_3->setEnabled( TRUE ) ;
			ITHL_2_4->setEnabled( TRUE ) ;
			ITHL_2_5->setEnabled( TRUE ) ;
			ITHL_2_6->setEnabled( TRUE ) ;
			ITHL_2_7->setEnabled( TRUE ) ;
			 
			Calib_File_Mod_2->setEnabled( TRUE ) ;
			Calib_ProgressBar_2->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[2] == 1) 
			{
			ConfigFile_Mod_3->setEnabled( TRUE );
			Value_SpinBox_3_1->setEnabled( TRUE );
			Value_SpinBox_3_2->setEnabled( TRUE );
			Value_SpinBox_3_3->setEnabled( TRUE );
			Value_SpinBox_3_4->setEnabled( TRUE );
			Value_SpinBox_3_5->setEnabled( TRUE );
			Value_SpinBox_3_6->setEnabled( TRUE );
			Value_SpinBox_3_7->setEnabled( TRUE );
			SendConfig_Mod_3->setEnabled( TRUE );
			Ack_SendConfig_Mod_3->setEnabled( TRUE );
		
			FlatConfiglineEdit_3_1->setEnabled( TRUE );	
			FlatConfiglineEdit_3_2->setEnabled( TRUE );
			FlatConfiglineEdit_3_3->setEnabled( TRUE );
			FlatConfiglineEdit_3_4->setEnabled( TRUE );
			FlatConfiglineEdit_3_5->setEnabled( TRUE );
			FlatConfiglineEdit_3_6->setEnabled( TRUE );
			FlatConfiglineEdit_3_7->setEnabled( TRUE );
			
			SendFlatConfig_Mod_3->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_3->setEnabled( TRUE);
			
			ITHL_3_1->setEnabled( TRUE ) ;
			ITHL_3_2->setEnabled( TRUE ) ;
			ITHL_3_3->setEnabled( TRUE ) ;
			ITHL_3_4->setEnabled( TRUE ) ;
			ITHL_3_5->setEnabled( TRUE ) ;
			ITHL_3_6->setEnabled( TRUE ) ;
			ITHL_3_7->setEnabled( TRUE ) ;
			
			Calib_File_Mod_3->setEnabled( TRUE ) ;
			Calib_ProgressBar_3->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[3] == 1) 
			{
			ConfigFile_Mod_4->setEnabled( TRUE );
			Value_SpinBox_4_1->setEnabled( TRUE );
			Value_SpinBox_4_2->setEnabled( TRUE );
			Value_SpinBox_4_3->setEnabled( TRUE );
			Value_SpinBox_4_4->setEnabled( TRUE );
			Value_SpinBox_4_5->setEnabled( TRUE );
			Value_SpinBox_4_6->setEnabled( TRUE );
			Value_SpinBox_4_7->setEnabled( TRUE );
			SendConfig_Mod_4->setEnabled( TRUE );
			Ack_SendConfig_Mod_4->setEnabled( TRUE );
			
			FlatConfiglineEdit_4_1->setEnabled( TRUE );	
			FlatConfiglineEdit_4_2->setEnabled( TRUE );
			FlatConfiglineEdit_4_3->setEnabled( TRUE );
			FlatConfiglineEdit_4_4->setEnabled( TRUE );
			FlatConfiglineEdit_4_5->setEnabled( TRUE );
			FlatConfiglineEdit_4_6->setEnabled( TRUE );
			FlatConfiglineEdit_4_7->setEnabled( TRUE );
			
			SendFlatConfig_Mod_4->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_4->setEnabled( TRUE);
			
			ITHL_4_1->setEnabled( TRUE ) ;
			ITHL_4_2->setEnabled( TRUE ) ;
			ITHL_4_3->setEnabled( TRUE ) ;
			ITHL_4_4->setEnabled( TRUE ) ;
			ITHL_4_5->setEnabled( TRUE ) ;
			ITHL_4_6->setEnabled( TRUE ) ;
			ITHL_4_7->setEnabled( TRUE ) ;
		
			Calib_File_Mod_4->setEnabled( TRUE ) ;
			Calib_ProgressBar_4->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[4] == 1) 
			{
			ConfigFile_Mod_5->setEnabled(TRUE);
			Value_SpinBox_5_1->setEnabled( TRUE );
			Value_SpinBox_5_2->setEnabled( TRUE );
			Value_SpinBox_5_3->setEnabled( TRUE );
			Value_SpinBox_5_4->setEnabled( TRUE );
			Value_SpinBox_5_5->setEnabled( TRUE );
			Value_SpinBox_5_6->setEnabled( TRUE );
			Value_SpinBox_5_7->setEnabled( TRUE );
			SendConfig_Mod_5->setEnabled( TRUE );
			Ack_SendConfig_Mod_5->setEnabled( TRUE );
			
			FlatConfiglineEdit_5_1->setEnabled( TRUE );	
			FlatConfiglineEdit_5_2->setEnabled( TRUE );
			FlatConfiglineEdit_5_3->setEnabled( TRUE );
			FlatConfiglineEdit_5_4->setEnabled( TRUE );
			FlatConfiglineEdit_5_5->setEnabled( TRUE );
			FlatConfiglineEdit_5_6->setEnabled( TRUE );
			FlatConfiglineEdit_5_7->setEnabled( TRUE );
			
			SendFlatConfig_Mod_5->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_5->setEnabled( TRUE);
			
			ITHL_5_1->setEnabled( TRUE ) ;
			ITHL_5_2->setEnabled( TRUE ) ;
			ITHL_5_3->setEnabled( TRUE ) ;
			ITHL_5_4->setEnabled( TRUE ) ;
			ITHL_5_5->setEnabled( TRUE ) ;
			ITHL_5_6->setEnabled( TRUE ) ;
			ITHL_5_7->setEnabled( TRUE ) ;
		
			Calib_File_Mod_5->setEnabled( TRUE ) ;
			Calib_ProgressBar_5->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[5] == 1) 
			{
			ConfigFile_Mod_6->setEnabled(TRUE );
			Value_SpinBox_6_1->setEnabled( TRUE );
			Value_SpinBox_6_2->setEnabled( TRUE );
			Value_SpinBox_6_3->setEnabled( TRUE );
			Value_SpinBox_6_4->setEnabled( TRUE );
			Value_SpinBox_6_5->setEnabled( TRUE );
			Value_SpinBox_6_6->setEnabled( TRUE );
			Value_SpinBox_6_7->setEnabled( TRUE );
			SendConfig_Mod_6->setEnabled( TRUE );
			Ack_SendConfig_Mod_6->setEnabled( TRUE );
			
			FlatConfiglineEdit_6_1->setEnabled( TRUE );	
			FlatConfiglineEdit_6_2->setEnabled( TRUE );
			FlatConfiglineEdit_6_3->setEnabled( TRUE );
			FlatConfiglineEdit_6_4->setEnabled( TRUE );
			FlatConfiglineEdit_6_5->setEnabled( TRUE );
			FlatConfiglineEdit_6_6->setEnabled( TRUE );
			FlatConfiglineEdit_6_7->setEnabled( TRUE );
			
			SendFlatConfig_Mod_6->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_6->setEnabled( TRUE);
			
			ITHL_6_1->setEnabled( TRUE ) ;
			ITHL_6_2->setEnabled( TRUE ) ;
			ITHL_6_3->setEnabled( TRUE ) ;
			ITHL_6_4->setEnabled( TRUE ) ;
			ITHL_6_5->setEnabled( TRUE ) ;
			ITHL_6_6->setEnabled( TRUE ) ;
			ITHL_6_7->setEnabled( TRUE ) ;
		
			Calib_File_Mod_6->setEnabled( TRUE ) ;
			Calib_ProgressBar_6->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[6] == 1) 
			{
			ConfigFile_Mod_7->setEnabled( TRUE );
			Value_SpinBox_7_1->setEnabled( TRUE );
			Value_SpinBox_7_2->setEnabled( TRUE );
			Value_SpinBox_7_3->setEnabled( TRUE );
			Value_SpinBox_7_4->setEnabled( TRUE );
			Value_SpinBox_7_5->setEnabled( TRUE );
			Value_SpinBox_7_6->setEnabled( TRUE );
			Value_SpinBox_7_7->setEnabled( TRUE );
			SendConfig_Mod_7->setEnabled( TRUE );
			Ack_SendConfig_Mod_7->setEnabled( TRUE );
			
			FlatConfiglineEdit_7_1->setEnabled( TRUE );	
			FlatConfiglineEdit_7_2->setEnabled( TRUE );
			FlatConfiglineEdit_7_3->setEnabled( TRUE );
			FlatConfiglineEdit_7_4->setEnabled( TRUE );
			FlatConfiglineEdit_7_5->setEnabled( TRUE );
			FlatConfiglineEdit_7_6->setEnabled( TRUE );
			FlatConfiglineEdit_7_7->setEnabled( TRUE );
		
			SendFlatConfig_Mod_7->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_7->setEnabled( TRUE);
			
			ITHL_7_1->setEnabled( TRUE ) ;
			ITHL_7_2->setEnabled( TRUE ) ;
			ITHL_7_3->setEnabled( TRUE ) ;
			ITHL_7_4->setEnabled( TRUE ) ;
			ITHL_7_5->setEnabled( TRUE ) ;
			ITHL_7_6->setEnabled( TRUE ) ;
			ITHL_7_7->setEnabled( TRUE ) ;
			
			Calib_File_Mod_7->setEnabled( TRUE ) ;
			Calib_ProgressBar_7->setEnabled(TRUE ) ;
			
			}
		if (ModuleReady_tab[7] == 1) 
			{
			ConfigFile_Mod_8->setEnabled( TRUE );
			Value_SpinBox_8_1->setEnabled( TRUE );
			Value_SpinBox_8_2->setEnabled( TRUE );
			Value_SpinBox_8_3->setEnabled( TRUE );
			Value_SpinBox_8_4->setEnabled( TRUE );
			Value_SpinBox_8_5->setEnabled( TRUE );
			Value_SpinBox_8_6->setEnabled( TRUE );
			Value_SpinBox_8_7->setEnabled( TRUE );
			SendConfig_Mod_8->setEnabled( TRUE );
			Ack_SendConfig_Mod_8->setEnabled( TRUE );
			
			FlatConfiglineEdit_8_1->setEnabled( TRUE );	
			FlatConfiglineEdit_8_2->setEnabled( TRUE );
			FlatConfiglineEdit_8_3->setEnabled( TRUE );
			FlatConfiglineEdit_8_4->setEnabled( TRUE );
			FlatConfiglineEdit_8_5->setEnabled( TRUE );
			FlatConfiglineEdit_8_6->setEnabled( TRUE );
			FlatConfiglineEdit_8_7->setEnabled( TRUE );
			
			SendFlatConfig_Mod_8->setEnabled( TRUE );
			Ack_SendFlatConfig_Mod_8->setEnabled( TRUE);
			
			ITHL_8_1->setEnabled( TRUE ) ;
			ITHL_8_2->setEnabled( TRUE ) ;
			ITHL_8_3->setEnabled( TRUE ) ;
			ITHL_8_4->setEnabled( TRUE ) ;
			ITHL_8_5->setEnabled( TRUE ) ;
			ITHL_8_6->setEnabled( TRUE ) ;
			ITHL_8_7->setEnabled( TRUE ) ;
		
			Calib_File_Mod_8->setEnabled( TRUE ) ;
			Calib_ProgressBar_8->setEnabled(TRUE ) ;
			
			}	

}
	

/**
* \fn void MainGUI::TestFunctions_Slot()
* \brief Used only to test functionnalities
*
*
* \param
* \return
*/
void MainGUI::TestFunctions_Slot()
{
#define  lTEST_FIBER 						0
#define lCONTINUE						1
#define lSASK_READY_LOOP				2
#define lPASK_READY_LOOP				3
#define lCHECK_ERROR_RATE				4
#define lSIMAGE_US_LOOP					5
#define lFLAT_CONFIG_LOOP				6
#define lTEST_LIBRARY					7
#define lTEST_LIBRARY_2					8

/*
#define lCALIB							4
#define lCONFIGURE_PIXEL					5
#define lADJUST_ITH						6

#define lADJUST_DACL					8
#define lPLOT_MATRIX					9

#define lREAD_OPTO_FIFO					12
#define lSIMPLE_OPTO_LOOP_TEST			13
#define lVARIABLE_OPTO_LOOP_TEST		14
#define lRESET_READ_USEDW				15
#define lTEST_BEHAVIOUR					16
*/

int status = -1 ;
//QString DataFile ;
int datar[MAX_RFIFO_SIZE];
int datar_length ;
long int i = 0;
int loop = 0 ;
int buffer[120] ; 
int wmodule ;
int wchip ;
int k,ii,j ;

int value ;
int timout_indicator ;

int data_error ;
int fifo_empty ;

int HandShake_Val ;
int loop_Nb ;
int module_Nb = 0;
int m_answ[8] ;
int enough_answ ;
int error_detected ;

int size_minmsg ;
int order ; 
int pmodule ;
int pchip ;
int prow ;
int pcol ;
int x ;
int y ;

 //TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
printf("*******************************************************\n") ;
printf("TestFunctions_Slot() CurrentItem selected = %d\033[22;30m\n",comboBox->currentItem()) ;
printf("*******************************************************\n\n\n") ;
loop_Nb = LoopNb_SpinBox->value() ; 

 switch (comboBox->currentItem()) 
  {
  		
		case lTEST_FIBER :
			/*
			loop = 0 ;
			for (j = 0;j<MAX_RFIFO_SIZE;j++) datar[j] = MAX_RFIFO_SIZE ;
			wmodule = ModuleID_ComboBox->currentItem() ;
			wchip = ConfigGWChip_ComboBox->currentItem() ;
			value = LoopNb_SpinBox->value()  ;
			for (i=0;i<value;i++)
				buffer[i] = i ;
			DAQ_MSG("SEND message of %d words\033[22;30m\n",value ) ;
			for (int pmodule = 0 ;pmodule<MAXMODULE;pmodule++)
				{
				wmodule = 1 << pmodule ; 
				WriteWordsToModule(qusb,wmodule,wchip,buffer,value)  ;
				datar_length = ReadRegister(qusb,OPTO_RFIFO_WUSEDW_REG)  ;
				DAQ_MSG("datar_length = %d   =>  \033[22;30m",datar_length) ;
				datar_length = ReadFromModule(qusb,datar,(TIMEOUT*10),8) ;
				for(int j=0;j<datar_length;j++)
					{
					printf("0x%x ",datar[j]) ;
					if (datar[j] == 0xf0f0) 
						printf("\n") ;
					}
				}
			*/
			memset(dacl_matrix,0,sizeof(dacl_matrix)) ;
			printf("Load DACL = 0 \n") ;
			//LoadDACLFromTabToModuleReady(qusb,ModuleReady_tab,dacl_matrix) ;
			value = 0 ;
			for (pmodule=0;pmodule<MAXMODULE;pmodule++)
			{
			if (ModuleReady_tab[pmodule] == 1)
				{
				//for (prow = 0;prow < loop_Nb ; prow++)
				for (prow = 0;prow < ROW_NB; prow++)
					{
					for (pchip = 0;pchip < MAXCHIP; pchip++)
						{
						for (pcol = 0;pcol < MAXCOL; pcol++)
							{
							x = pcol+(pchip*MAXCOL) ;
							y = prow+(pmodule*ROW_NB) ;	
							value = 0 ;
							if ((x > 300) &&  (x < 340))
									if  (y == 40)
										{
										//printf("y=%d x=%d value=%d\n",y,x,value) ;
									 	value = 63 ;
										}
							//if (dacl_matrix[y][x] != (value+1)) printf("Error dacl_matrix[%d][%d] = %d\n",y,x,dacl_matrix[y][x]) ;
							/*	
							if (value >62) value = 0 ;
							else ++value ;
							*/

							dacl_matrix[y][x] = value ;
							}
						}
					
					}
				}
			}
			printf("Load DACL new values \n") ;
			LoadDACLFromTabToModuleReady(qusb,ModuleReady_tab,dacl_matrix) ;
			return ;
			printf("Download DACL values \n") ;
			ResetMatrix(dmatrix) ;
			ResetMatrix(down_dacl_matrix) ;
			status = FastReadImageAndFillMatrix(qusb,MODE_READ_DACL,module_msk,dmatrix,INDEX_0) ;
			ExtractDACLValue(dmatrix,down_dacl_matrix) ;
			printf("Compare DACL values \n") ;
			for (pmodule=0;pmodule<MAXMODULE;pmodule++)
			{
			if (ModuleReady_tab[pmodule] == 1)
				{
				//for (prow = 0;prow < loop_Nb ; prow++)
				for (prow = 0;prow < ROW_NB; prow++)
					{
					for (pchip = 0;pchip < MAXCHIP; pchip++)
						{
						for (pcol = 0;pcol < MAXCOL; pcol++)
							{
							x = pcol+(pchip*MAXCOL) ;
							y = prow+(pmodule*ROW_NB) ;	
							//if (dacl_matrix[y][x] != (value+1)) printf("Error dacl_matrix[%d][%d] = %d\n",y,x,dacl_matrix[y][x]) ;	
							if (down_dacl_matrix[y][x] != dacl_matrix[y][x])
								printf("Error dacl_matrix[%d][%d] = %d download_value = %d\n",y,x,dacl_matrix[y][x],down_dacl_matrix[y][x]) ;
							/*
							if (dacl_matrix[y][x] != 0)
								printf("dacl_matrix[%d][%d] = %d\n",y,x,dacl_matrix[y][x]) ;
							if (down_dacl_matrix[y][x] != 0)
								printf("down_dacl_matrix[%d][%d] = %d\n",y,x,down_dacl_matrix[y][x]) ;
							*/
							}
						}
					
					}
				}
			}
			printf("Load DACL Ended \n") ;
			
		break ;
  		case lCONTINUE :
   			DAQ_MSG("Send CONTINUE message\033[22;30m\n" ) ;
			loop_Nb = LoopNb_SpinBox->value() ; 
			printf("Loop_Nb =  ") ;
			for (k=0;k<loop_Nb ;k++)
				{
				printf("%d ",(k+1)) ;
				for(j=0;j<MAXMODULE;j++)
   					WriteWordToModule(qusb,(j+1),0,CONTINUE)  ; 
				}
			printf("\n") ;
			
  		break ;
  		case lSASK_READY_LOOP :
		
			loop_Nb = LoopNb_SpinBox->value() ; 
			printf("\033[22;33m\n*********************************** \033[22;30m\n") ;
			for (k=0;k<loop_Nb ;k++)
				{
				printf("\033[22;33m%d : Loop on Sequencial AskReady function  \033[22;30m\n",k) ;
				for(int pmodule=0;pmodule<MaxModuleReady;pmodule++)
					{
					if (ModuleReady_tab[pmodule] == 1)
					{
					datar_length = 0 ;
					wmodule = 1 << pmodule ; 
					WriteWordToModule(qusb,wmodule,NULL_PARAM,ASK_READY) ;
					//datar_length = ReadFromModule(wqusb,datar,(TIMEOUT),4) ;
					datar_length = ReadFromModule(qusb,datar,(TIMEOUT),4) ;
					if (datar_length > MAX_RFIFO_SIZE)  datar_length = MAX_RFIFO_SIZE ;
					printf("\033[22;30mModule_%d : %d : ",wmodule,datar_length) ;
					if (datar_length < 1) printf("ERROR fifo empty ") ;
					for (i=0;i<datar_length;i++)
						printf("0x%x ",datar[i]) ;
				 	HandShake_Val = ExtractHandShakeVal(datar,datar_length,&wmodule) ;
				 	if ((HandShake_Val ==0x0101) | (HandShake_Val ==0x1101)) 
				 		{
				 		printf("\033[22;32m\t\t\tPASSED\n") ;
						printf("\033[22;30m") ;
						}
				 	else
				 		{ 
				 		printf("\033[22;31m\t\t\tFAILED\n") ;
						printf("\033[22;30m") ;
						printf("\033[22;33m*********************************** \033[22;30m\n\n") ;
						return ;
						}
						}
					}
				}
			printf("\033[22;33m*********************************** \033[22;30m\n\n") ;	
		
			QApplication::beep() ;
			/*
			ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
			ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
			for(j=0;j<8;j++)
				{
				usleep(1000) ;
				WriteWordToModule(qusb,(j+1),NULL_PARAM,ASK_READY) ;
				}
			fifoc = (int *) malloc(1024*sizeof(int)) ;	
			datar_length = ReadRFIFOContains(qusb,fifoc,1000) ;
			
			printf("Fifo contains data_length = %d \n",datar_length) ;
			for(j=0;j<datar_length;j++)
				{
				printf("0x%x ",fifoc[j]) ;
				if (fifoc[j] == 0xf0f0) 
					printf("\n") ;
				}
			printf("\n") ;
			*/
		break ;
		case lPASK_READY_LOOP :
			size_minmsg = 12  ;
			error_detected = 0 ;
			loop_Nb = LoopNb_SpinBox->value() ; 
			printf("\033[22;33m\n*********************************** \033[22;30m\n") ;
			printf("\033[22;33mLoop on Parrallel AskReady function  \033[22;30m\n") ;
			for (k=0;k<loop_Nb ;k++)
				{
				for (i=0;i<8;i++)
					m_answ[i] = 0 ;
				datar_length = 0 ;
				timout_indicator = 0 ;
				enough_answ = 0 ;
				order = 0 ;
				WriteWordToModule(qusb,ALL_MODULES,NULL_PARAM,ASK_READY) ;
				//for(i=0;i<1000000;i++) ;
				//while ((datar_length <  size_fifo) & (timout_indicator < 10) )
				while (enough_answ  < Max_Answer) 
					{
					datar_length = ReadFromModule(qusb,datar,(TIMEOUT*10),8) ;
					if (datar_length > size_minmsg)
						{ 
						for (i=0;i<(datar_length+1);i++)
							{
							if (datar[i] == 0xaa55) 
								{
								if ((datar[i+1] >0) & (datar[i+1]< 9))
									{
									m_answ[(datar[i+1]-1)] = ++order  ;
									enough_answ += (datar[i+1] )*power(10,(datar[i+1]-1) ) ; 
									}
								//printf("\norder = %d => ",order) ;
								}
							 //printf("0x%x ",datar[i]) ;
							}
						}
					if (timout_indicator < 10) timout_indicator++ ;
					else break ;
					}
				printf("Loop_Nb = %d : ",k) ;
				if (enough_answ  < Max_Answer)
					{
					error_detected++ ;
					for (ii=0;ii<8;ii++)
						printf("\033[22;31m%d ",m_answ[ii])  ;
					}
				else 
					{
					for (ii=0;ii<8;ii++)
						printf("\033[22;32m%d ",m_answ[ii])  ;
					}
				printf("\033[22;30m\n") ;
				}
			printf("\033[22;33mNb error detected = %d/%d loop\033[22;30m\n",error_detected,loop_Nb) ;
			printf("\033[22;33m*********************************** \033[22;30m\n\n") ;	
		break ;
/*
// Deuxième version de lecture // avec plusieurs acces de la FIFO 
		case lPASK_READY_LOOP :
			size_minmsg = 13  ;
			error_detected = 0 ;
			loop_Nb = LoopNb_SpinBox->value() ; 
			printf("\033[22;33m\n*********************************** \033[22;30m\n") ;
			printf("\033[22;33mLoop on Parrallel AskReady function  \033[22;30m\n") ;
			for (k=0;k<loop_Nb ;k++)
				{
				for (i=0;i<8;i++)
					m_answ[i] = 0 ;
				datar_length = 0 ;
				timout_indicator = 0 ;
				enough_answ = 0 ;
				WriteWordToModule(qusb,ALL_MODULES,NULL_PARAM,ASK_READY) ;
				//for(i=0;i<1000000;i++) ;
				//while ((datar_length <  size_fifo) & (timout_indicator < 10) )
				datar_length = ReadFromModule(qusb,datar,(TIMEOUT*10),8) ;
				printf("datar_length = %d\n",datar_length) ;
				while (datar_length > 0)
					{ 
					if (datar_length > size_minmsg)
						{
						for (i=0;i<size_minmsg;i++)
							{
							if (datar[i] == 0xaa55) printf("\n") ;
							printf("0x%x ",datar[i]) ;	
							}
						}
						printf("\n") ;
						datar_length = ReadFromModule(qusb,datar,(TIMEOUT*10),8) ;
						printf("datar_length = %d\n",datar_length) ;
						if (timout_indicator < 10) timout_indicator++ ;
						else break ;
						}
					}
			printf("\033[22;33m*********************************** \033[22;30m\n\n") ;	
		break ;
*/
		case lCHECK_ERROR_RATE :
			/*
			loop_Nb = LoopNb_SpinBox->value() ;
			data_error = 0 ;
			fifo_empty = 0 ;
			printf("\033[22;33m\n**************** \033[22;30m\n") ;
			printf("\033[22;33mCheck error rate \033[22;30m\n") ;
			for (k=0;k<loop_Nb ;k++)
				{
				for(j=0;j<MaxModuleReady;j++)
					{
					if (ModuleReady_tab[j] == 1)
					{
					datar_length = 0 ;
					WriteWordToModule(qusb,(j+1),NULL_PARAM,ASK_READY) ;
					//datar_length = ReadFromModule(wqusb,datar,(TIMEOUT),4) ;
					datar_length = ReadFromModule(qusb,datar,(TIMEOUT),4) ;
					if (datar_length == -1) 
						{
						//printf("ERROR fifo empty ") ;
						fifo_empty++ ;
						}
				 	if (datar_length > 128)  datar_length = 128 ;
					HandShake_Val = ExtractHandShakeVal(datar,datar_length,&wmodule) ;
				 	if ((HandShake_Val !=0x0101) && (HandShake_Val !=0x1101)) data_error++ ;
					}
					}
				}
			printf("\033[22;33m%d Frames : Find %d FIFO_EMPTY and %d DATA_ERROR\033[22;30m\n",(module_Nb*loop_Nb),fifo_empty,data_error) ;
			printf("\033[22;33m*************************************************************\033[22;30m\n\n") ;
			*/
			testFunction((int) LoopNb_SpinBox->value()) ;
			
		break ;
		case lSIMAGE_US_LOOP :
			loop_Nb = LoopNb_SpinBox->value() ; 
			integration_time = IntegrationTimeSpinBox->value();
			error_detected = 0 ;
			
			DAQ_MSG("Loop on Sequencial Image_us Function\033[22;30m\n" ) ;
			for (k=0;k<loop_Nb ;k++)
				{
				for (ii=0;ii<8;ii++)
					m_answ[ii] = 0 ;
				printf("Loop_Nb = %d : ",k) ;
				for(j=0;j<MaxModuleReady;j++)
					{
					if (ModuleReady_tab[j] == 1)
					    break ;
					}
				for(j=0;j<MaxModuleReady;j++)
					{
					if (ModuleReady_tab[j] == 1)
						{
						datar_length = 0 ;
						if (Module_Answer == (j+1))
							ImageUs(qusb,(j+1),GateMode_comboBox->currentItem(),integration_time,1,1,WITH_ANSWER) ;
						else
							ImageUs(qusb,(j+1),GateMode_comboBox->currentItem(),integration_time,1,1,WITHOUT_ANSWER) ;
						}
					}

				status = 0 ;	
				start_time = clock() ;
				while ((status !=  IMAGE_US_ENDED) & (status !=  -1))
					//status = WaitingForHandShakeImageUSEnded(qusb,start_time,(double) integration_time,&progress_acq) ;
				if (status ==  IMAGE_US_ENDED)
					{
					m_answ[0] =  1 ;
					}
				else 
					m_answ[j] = 0 ;
				if (m_answ[j] == 0)
					{
					error_detected++ ;
					for (ii=0;ii<8;ii++)
						printf("\033[22;31m%d ",m_answ[ii])  ;
					}
				else 
					{
					for (ii=0;ii<8;ii++)
						printf("\033[22;32m%d ",m_answ[ii])  ;
					}
				printf("\033[22;30m\n") ;
				}
			printf("\033[22;33mNb error detected = %d/%d loop\033[22;30m\n",error_detected,loop_Nb) ;
			printf("\033[22;33m*********************************** \033[22;30m\n\n") ;	
		break ;
		case lFLAT_CONFIG_LOOP :
			loop_Nb = LoopNb_SpinBox->value() ; 
			error_detected = 0 ;
			printf("\033[22;33m\n*********************************** \033[22;30m\n") ;
			printf("\033[22;33mLoop on Flat config function  \033[22;30m\n") ;
			for (k=0;k<loop_Nb ;k++)
				{
				order = 0 ;
				for (i=0;i<8;i++)
					m_answ[i] = 0 ;
				enough_answ = 0 ;
				printf("Loop_Nb = %d : ",k) ;
				for(j=0;j<MaxModuleReady;j++)
					{
					if (ModuleReady_tab[j] == 1)
						{
						status = 0 ;
						FlatConfig(qusb,(j+1),0x7f,ALL_CHIP,0x0101) ;
  						status = WaitingForHandShakeFlatConfig(qusb)  ;
						//printf("j = %d Status = %d \n",j,status) ;
						if (status == (j+1) )
							{
							m_answ[j] = ++order  ;
							enough_answ += (status)*power(10,(status-1) ) ; 
							}
						else
							m_answ[j] = status  ;
						//enough_answ += status ;
						}
					}
				//printf("Max_Answer = %d enough_answ = %d\n",Max_Answer,enough_answ) ;
				if (enough_answ  != Max_Answer)
					{
					error_detected++ ;
					for (ii=0;ii<8;ii++)
						printf("\033[22;31m%d ",m_answ[ii])  ;
					}
				else 
					{
					for (ii=0;ii<8;ii++)
						printf("\033[22;32m%d ",m_answ[ii])  ;
					}
				printf("\033[22;30m\n") ;
				}
			printf("\033[22;33mNb error detected = %d/%d loop\033[22;30m\n",error_detected,loop_Nb) ;
			printf("\033[22;33m*********************************** \033[22;30m\n\n") ;	
			QApplication::beep() ;
		break ;
		case lTEST_LIBRARY:

			#ifdef USE_XPADDET_LIB
			printf("\033[22;33m*******************************************************\n") ;
			printf("            Test library mode\n",comboBox->currentItem()) ;
			printf("*******************************************************\033[22;30m\n\n\n") ;
			
			
			if (qusb == NULL)
				{
				//qusb = t_DetConfig.qusb ;
				//printf("Max_MODULE = %d \n", t_DetConfig.max_module) ;
				//printf("Hfpga_ver = 0x%x Lfpga_ver = 0x%x \n",t_DetConfig.Hfpga_ver, t_DetConfig.Lfpga_ver) ;
				
				if (t_DetConfig.qusb == NULL)
					xpaddet_init(&t_DetConfig) ;
				if (t_DetConfig.module_msk == 0) 
					{
					printf("BREAK => Module not activated\n") ;
					break ;
					}
				
				xpaddet_default_ConfigAllG(&t_DetConfig, t_DACConfig) ;
				xpaddet_load_AllConfigG_to_ModuleActivated(&t_DetConfig, t_DACConfig) ;

			        for (int pmodule=0;pmodule<MAX_MODULE;pmodule++)
				   {
				   if ((t_DetConfig.module_msk >> pmodule) & 0x01)
					{
				   	t_FlatConfig[pmodule].wmodule = pmodule;
				   	for (int pchip=0;pchip<MAX_CHIP;pchip++)
					   {
					    t_FlatConfig[pmodule].wchip[pchip] = pchip ;
					    t_FlatConfig[pmodule].value[pchip] = 0x0101;
					   }
				         }
				    }
				
				xpaddet_load_FlatConfig_to_ModuleActivated(&t_DetConfig, t_FlatConfig) ;
					
				for (int jj = 0;jj< 10; jj++)
					{  
					t_RegConfig.wmodule = 2;
					t_RegConfig.wchip  = 3 ;
					t_RegConfig.wregister = ITHL ;
					t_RegConfig.value = 50 - (4*jj) ; 
				
					xpaddet_load_Register(&t_DetConfig, &t_RegConfig) ;
					t_AcqParameters.gate_mode = INTERNAL_MODE ;
					t_AcqParameters.integration_time = 2000 ; // 2s 
					t_AcqParameters.index_image = 0;
					t_AcqParameters.nb_image = 1;
					xpaddet_expose(&t_DetConfig, &t_AcqParameters) ;
					t_ImageParameters.mode  = MODE_16b ;
					t_ImageParameters.index_img = 0 ;
					t_ImageParameters.data_delay = 0 ;
					xpaddet_read_image(&t_DetConfig, &t_ImageParameters) ; // t_imageParameters.dmatrix
					//ResetMatrix(dmatrix) ;
					//AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ;
					TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
					RootHistoHitConfigFillPlotMatrix(mClone,h2,t_ImageParameters.dmatrix,"") ;
					printf("Config Ith = %d \n",t_RegConfig.value) ;
					}
				}
				
				xpaddet_default_ConfigAllG(&t_DetConfig, t_DACConfig) ;
				xpaddet_load_AllConfigG_to_ModuleActivated(&t_DetConfig, t_DACConfig) ;
				xpaddet_expose(&t_DetConfig, &t_AcqParameters) ;
				xpaddet_read_image(&t_DetConfig, &t_ImageParameters) ; // t_imageParameters.dmatrix
				TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
				RootHistoHitConfigFillPlotMatrix(mClone,h2,t_ImageParameters.dmatrix,"") ;

				wmodule = 0 ;
				sprintf(t_FileConfig[wmodule].path,"./configs") ;
				sprintf(t_FileConfig[wmodule].prefix,"matlab_calib_D2_cuivredacl_mod") ;
				t_FileConfig[wmodule].wmodule = wmodule ;
				xpaddet_load_Config_from_file(&t_DetConfig, &t_FileConfig[wmodule],&t_DACConfig[wmodule]) ;

				xpaddet_expose(&t_DetConfig, &t_AcqParameters) ;
				xpaddet_read_image(&t_DetConfig, &t_ImageParameters) ; // t_imageParameters.dmatrix
				TH2F *h3 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
				RootHistoHitConfigFillPlotMatrix(mClone,h3,t_ImageParameters.dmatrix,"") ;
				
			#endif
		
		break ;
	
/*
		case lPLOT_MATRIX :
			
			DAQ_MSG("Plot matrix\033[22;30m\n" ) ;
			index = 0 ;
			for (int module =0;module<8;module++)
			{
			for (int chip =0;chip<7;chip++)
			{
			for (int col=0;col<80;col++)
			    {	
			    for (int row=0;row<120;row++)
				{
				value = (int) (module*120) + (chip*80) + col +(row*80*7);
				FillDataMatrix(dmatrix,module,chip,row,col,value) ;
				}
			    }
			}
			}
			//pthread_create(&pthread_plot,NULL,&RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix),NULL) ;
			//pthread_create(&pthread_wfile,NULL,&SaveMatrixToFile("toto.txt",dmatrix),NULL) ;
			
			ResetMatrix(dmatrix) ;
			ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER,dmatrix,WMODULE) ;
			RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,"") ;
			//SaveMatrixToFile("toto.txt",dmatrix) ;
			//RootConfigHistoHit(mClone,h2," ") ;
   //RootFillHistoHitFromMatrix(h2,dmatrix) ;
   //RootPlotHistoHit(mClone,h2) ;
   DAQ_MSG("Plot matrix ended\033[22;30m\n" ) ;
  break ;

  case  lREAD_OPTO_FIFO :

   nbword_expected = NB_WORDS = LoopNb_SpinBox->value()  ;
   status = ReadFromModule(qusb,datar,100,nbword_expected) ;
			for (int j = 0;j<status;j++)
				printf("Ox%x ",datar[j]) ;
			printf("\n") ;	

		break ;
		case lSIMPLE_OPTO_LOOP_TEST :
			error = 0 ;
			stop_opto_loop_test = 1 ;
			ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
			ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
			WriteWordToModule(qusb,0,0,CONTINUE)  ; 
			WriteWordToModule(qusb,0,0,CONTINUE)  ; 		
			DAQ_MSG("Simple Opto loop test function\033[22;30m\n" ) ;
			loop = 0 ;
			i = 0 ;
			for (i = 0;i<100;i++)
				{
				for (int j = 0;j<128;j++) datar[j] = 0 ;
				ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
				ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
				WriteWordToModule(qusb,ModuleID_ComboBox->currentItem(),ConfigGWChip_ComboBox->currentItem(),LOOP_TEST)  ; 
				printf("Loop = %d : ",++loop) ;
				nbword_expected = 10 ; status = -1 ;
				for (int j = 0;j<128;j++) datar[j] = 0 ;
				status = ReadFromModule(qusb,datar,100,nbword_expected) ;
				printf("NbWord = %d : ",status) ;
				if ((datar[1] != 0xbb44) | (datar[2] != 0x3333) | (datar[3] != 0x6) | (datar[4] != ModuleID_ComboBox->currentItem()) | (datar[5] != 0xaa55) | (datar[6] != 0x3) | (datar[7] != 0x7) | (datar[8] != LOOP_TEST) | (datar[9] != 0xf0f0) | (datar[10] != 0x0000))
					{
					error_tab[error++] = i ;
					printf("%d : Error    => ",error) ;
					for (int j = 0;j<status;j++)
						printf("Ox%x ",datar[j]) ;
					printf("\n") ;	
					break ;
					}
				else 
					{
					printf("%d : NO error => ",error) ;
					for (int j = 0;j<status;j++)
						printf("Ox%x ",datar[j]) ;
					printf("\n") ;	
					}
			
				}
			
		printf("Loop test error = %d ; ",error) ;
		for (int jj = 0 ; jj < error ; jj++)
			printf("%d ",error_tab[jj]) ;
		printf("\n") ;
		break ;
		case lVARIABLE_OPTO_LOOP_TEST :
			error = 0 ;
			ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
			ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
			WriteWordToModule(qusb,0,0,CONTINUE)  ; 
			WriteWordToModule(qusb,0,0,CONTINUE)  ; 		
			DAQ_MSG("Variable Opto loop test function\033[22;30m\n" ) ;
			loop = 0 ;
			i = 0 ;

			for (i = 0;i<100;i++)
				{
				NB_WORDS =LoopNb_SpinBox->value() ;
				for (k=0;k<NB_WORDS;k++)
					buffer[k] = k ;
				WriteWordsToModule(qusb,ModuleID_ComboBox->currentItem(),ConfigGWChip_ComboBox->currentItem(),buffer,NB_WORDS)  ;	
				// ***** WriteWordToModule(qusb,ModuleID_ComboBox->currentItem(),0x00,CONTINUE,LVDS_INTERFACE) ; 
//				while (ReadFromModule(qusb,datar,100,OPTO_INTERFACE,nbword) < nbword) ;
				printf("Loop = %d : ",++loop) ;
				for (int j = 0;j<128;j++) datar[j] = 0 ;
				status = ReadFromModule(qusb,datar,100,NB_WORDS) ;
				printf("NbWord = %d : ",status) ;
				data_field_error = 0 ;
			    for (k=0;k<NB_WORDS;k++)
					if (datar[k+8] != buffer[k]) data_field_error++ ;

				if ((datar[1] != 0xbb44) | (datar[2] != 0x3333) | (datar[4] != ModuleID_ComboBox->currentItem()) | (datar[5] != 0xaa55) | (data_field_error != 0))
					{
					error_tab[error++] = i ;
					printf("%d : Error    =>  Data_field_error = %d ",error,data_field_error) ;
					//for (int j = 0;j<status;j++)
					//	 for (k=0;k<NB_WORDS;k++)
					//		if (data[k+8] != buffer[k]) printf("Ox%x ",data[k+8]) ; ;
					printf("\n") ;	
					break ;
					}
				else 
					{
					printf("NO error ") ;
					//for (int j = 0;j<status;j++)
					//	printf("Ox%x ",datar[j]) ;
					printf("\n") ;	
     }
   
    }
  break ;
  case lRESET_READ_USEDW :
   status = ReadRegister(qusb,OPTO_RFIFO_WUSEDW_REG) ;
   printf("OPTO_RFIFO_WUSEDW_REG = 0x%x %d \n", status,status) ;
   ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
   ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
   printf("Applying reset \n") ;
   status = ReadRegister(qusb,OPTO_RFIFO_WUSEDW_REG) ;
   printf("OPTO_RFIFO_WUSEDW_REG = 0x%x %d \n", status, status) ;   
  break ;
  case lTEST_BEHAVIOUR :
  printf("test behaviour \n") ;
 
   status = ReadRegister(qusb,OPTO_RFIFO_WUSEDW_REG) ;
   printf("OPTO_RFIFO_WUSEDW_REG = 0x%x %d \n", status,status) ;
   ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
   ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
   printf("Applying reset \n") ;
   status = ReadRegister(qusb,OPTO_RFIFO_WUSEDW_REG) ;
   printf("OPTO_RFIFO_WUSEDW_REG = 0x%x %d \n", status, status) ;   
   


  break ;
 */
  }
#undef  lTEST_FIBER 				
#undef lCONTINUE			
#undef lSASK_READY_LOOP	
#undef lPASK_READY_LOOP	
#undef lCHECK_ERROR_RATE	
#undef lSIMAGE_US_LOOP		
#undef lFLAT_CONFIG_LOOP	
#undef lTEST_LIBRARY
}


/**
* \fn void MainGUI::StartAcq_Slot()
* \brief Select the mode of the Acquisition, single or "on fly"
*
* MainGUI::SetWriteFile() then two possibilities depend on the control \n
*
* 	- Execute the MainGUI::StartAcqLoopMode_Slot() fucntion, single acquisition \n
* 	- Start a timer which is connected to the MainGUI::StartAcqLoopMode_Slot(), on fly acquisition \n
*
* \param
* \return
*/
void MainGUI::StartAcq_Slot()
{

string FileSelected ;
string pFile ;
string pPath ;
string pPrefix  ;


   	ResetMatrix(dmatrix) ;
   	ResetMatrix(summatrix) ;


   	if( (Save_ComboBox->currentItem() != DONT_SAVE) && (Synchro_ComboBox->currentItem() != D2AM_SYNCHRO))
    		{
    		kindoffile = DATA_FILE ;
    		MainGUI::SetWriteFile() ;
    		if (QStrFileSelected == NULL) return ;
    		std::sprintf(PathAndfileName,"%s.dat",(const char *) QStrFileSelected) ;
    		//ExtractSuffix(folder_tmp,suffix_file) ;
    		DAQ_MSG("%s\033[22;30m\n",PathAndfileName) ; 
    		}

   	index_img = IndexImg_SpinBox->value(); 
   	loop_img = 0 ;
   	loop_synchro_img = 0 ;
   
   	if (StartAcqMode_comboBox->currentItem() == LOOP_ENABLE)
    		{
    		index_img = 0 ;
    		StartAcq_Button->setEnabled(FALSE) ;
    		StopAcq_Button->setEnabled(TRUE) ;
    		timer_type = 2 ;
    		timer_steps = 0 ;
    		gen_timer->start(100, FALSE );
    		return ;
    		}
 	else if (StartAcqMode_comboBox->currentItem() == DIG_LOOP_ENABLE)
		{
		loop_img = 0 ;
		index_img = 0 ;
		total_error = 0 ;
		StartAcq_Button->setEnabled(FALSE) ;
		StopAcq_Button->setEnabled(TRUE) ;
		timer_type = 3 ;
		timer_steps = 0 ;
		ResetMatrix(error_matrix) ;
		gen_timer->start(100, FALSE );
		return ;
    		}
	else if (StartAcqMode_comboBox->currentItem() == DOWNLOAD_LOOP_ENABLE)
		{
		index_img = 0 ;
		StartAcq_Button->setEnabled(FALSE) ;
		StopAcq_Button->setEnabled(TRUE) ;
		timer_type = 4 ;
		timer_steps = 0 ;
		gen_timer->start(100, FALSE );
		return ;
		}
	else 
		{
		StartAcq_Button->setEnabled(FALSE) ; 
		MainGUI::StartAcqLoopMode_Slot() ;
		StartAcq_Button->setEnabled(TRUE) ; 
		return ;
		}
}


/**
* \fn void MainGUI::StartAcqLoopMode_Slot()
* \brief Start acquisition of all modules connected to the detector
*
* ImageUs(qusb,MODULE_1,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image ) ; \n
* WaitingForHandShakeImageUSEnded(qusb) ; \n
* Then for all modules connected \n
* 
* ACQ MODE OVF          => ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_OVF,dmatrix,WMODULE) \n
* ACQ MODE COUNTER  => ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_LSB,dlsbmatrix,WMODULE) and \n
*                                         ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_MSB,dmsbmatrix,WMODULE) ; \n

* Adding of the two matrix => AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ; \n
* then plot matrix to Canvas => RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ; \n
* if selected save matrix to the file => SaveMatrixToFile(PathAndfileName,dmatrix) ; \n
*
* \param
* \return
*/
void MainGUI::StartAcqLoopMode_Slot()
{
int integration_time  ;
int status = 0 ;
int nb_image ;

QString FileSelected ;
char sys_command[500] ;
string pFile ;
char fileName[500] ;
string pPath ;
string pPrefix  ;

string sfileName ;

string lsbfileName ;
string msbfileName ;
string edffileName ;

int i ;
int progress_acq ;

int pmodule ;
int wmodule ;

int fd ;
char buf[255] ;
struct termios oldtio,newtio;

int mCloneStatus ;


	memset(dlsbmatrix,0,sizeof(dlsbmatrix)) ;
	memset(dmsbmatrix,0,sizeof(dmsbmatrix)) ;
	memset(dmatrix,0,sizeof(dmatrix)) ;
	
	if (StartAcqMode_comboBox->currentItem() == LOOP_ENABLE) 
		gen_timer->stop() ;
	
	for (i=0;i<MAXMODULE;i++)	
		{
		if (ModuleReady_tab[i] == 1) MaxModuleReady = i+1 ;
		}
			

   	TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ; 
    
   	integration_time = IntegrationTimeSpinBox->value(); 
  	nb_image = NbImgSpinBox->value();


   
  	 if  (Synchro_ComboBox->currentItem()  == D2AM_SYNCHRO)  
   		{
		read_file_synchro(&synchro,D2AM_SYNCHRO) ; 
		integration_time = synchro.time_param; 
		sprintf(PathAndfileName,"%s/%s.dat", synchro.path, synchro.fileName_param) ;
		DAQ_MSG("d2am_synchro => %s\033[22;30m\n", PathAndfileName) ;
		} 
	
	if  (Synchro_ComboBox->currentItem()  == PIXSCAN_SYNCHRO)  
		{
		read_file_synchro(&synchro,PIXSCAN_SYNCHRO) ; 
		integration_time = synchro.time_param; 
		index_img = synchro.index_param; 
		sprintf(PathAndfileName,"%s/%s.dat", synchro.path, synchro.fileName_param) ;
		DAQ_MSG("pixscan_synchro => %s\033[22;30m\n", PathAndfileName) ;
		} 
  
	if  (Synchro_ComboBox->currentItem()  == SERIAL_SYNCHRO)  
		{	
		if (SERIAL_SYNCHRO_ACTIVATED)
			{ 
			status = config_serial_synchro(&fd,oldtio,newtio,0) ;
			if (status < 0) 
				{
				DAQ_MSG("Unable to open Serial Port\033[22;30m\n") ;
				return ;
				}
			DAQ_MSG("Waiting for data_synchro on serial port\033[22;30m\n") ;
			read_serial(fd,oldtio,buf) ;
			ExtractField(buf,&synchro) ;
			DAQ_MSG("Data extracted => nb_image:%d  path:%s \033[22;30m\n", synchro.nb_image, synchro.fileName_param);
			sprintf(sys_command,"mkdir %s",synchro.fileName_param) ;
			system(sys_command) ; 
			}
		else 
			{
			DAQ_MSG("\033[22;31mCAUTION : Serial synchro generated automatically for test purpose\033[22;30m\n") ;
			synchro.nb_image = 12 ;
			sprintf(synchro.fileName_param, "./data/data_without_synchro") ;
			}

	if (!TEST_SERIAL_COM)
		{
		if (with_imageUS_X_samples)
			{
   			integration_time = 100000 ;// synchro.time_param ;
			//IntegrationTimeSpinBox->setValue(integration_time); 
			index_img = 0 ;
			nb_image = synchro.nb_image ;
			ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
 
			if (FAST_READOUT)
	   			{
	  			 DAQ_MSG("Fast Image us samples, integration time = %d nb_image = %d module_msk = %d\033[22;30m\n",integration_time,nb_image,module_msk) ;
	 			 ImageUs_X_Samples(qusb,module_msk,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
	   //ImageUs(qusb,module_msk,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
	  			 }
			else
				{
				DAQ_MSG("Image us integration time = %d nb_image = %d\033[22;30m\n",integration_time,nb_image) ;
   				for (pmodule=0;pmodule<MAXMODULE;pmodule++)
					{
					if (ModuleReady_tab[pmodule] == 1)
						{
						wmodule = pmodule + 1 ; 
						if (Module_Answer == wmodule)
							ImageUs_X_Samples(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
							//ImageUs(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
						else
							ImageUs_X_Samples(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITHOUT_ANSWER) ;
						}
					}
				}
       			Image_ProgressBar->setProgress(0, nb_image ) ; QApplication::flush() ; 
			while ((status !=  IMAGE_US_ENDED) & (status !=  TIMOUT_DETECTED))
				{
				start_time = clock() ;
				status = 0 ;
				// receive  (IMAGE_ADDED	0x1140) and  (IMAGE_US_ENDED 0x1141) at the end
				status = WaitingForHandShakeImageUSEnded(qusb,start_time,(long) integration_time,&progress_acq) ;
				if (status ==  READY_FOR_GATE) 
					{
					DAQ_MSG("Receive start gate from detector\033[22;30m\n") ;
					status = config_serial_synchro(&fd,oldtio,newtio,0) ;
					if (status < 0) 
						{
						DAQ_MSG("Unable to open Serial Port\033[22;30m\n") ;
						return ;
						}
					sprintf(buf,"READY FOR GATE%c%c",0x0a,0x0d) ;
					write_serial(fd,oldtio,buf) ;
					DAQ_MSG("Send READY FOR GATE message\033[22;30m\n") ;
					status = 0 ;
					}
				progress_acq = nb_image ;
				if (status ==  IMAGE_US_ENDED) progress_acq = nb_image ;
				if(status == TIMOUT_DETECTED) return ;
				//Image_ProgressBar->setProgress(++image_index, nb_image ) ; QApplication::flush() ; 
				Image_ProgressBar->setProgress(progress_acq, nb_image ) ; QApplication::flush() ; 
				}
	
			}

			/************************************************
			//////////// fin de if (with_imageUS_X_samples)
			************************************************/
			for (int kk=0;kk<4;kk++)
				{
				memset(dlsbmatrix,0,sizeof(dlsbmatrix)) ;
				memset(dmsbmatrix,0,sizeof(dmsbmatrix)) ;
				memset(dmatrix,0,sizeof(dmatrix)) ;
				TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ; 
				if (FAST_READOUT)	
					{
					if (MSB_LSB)
						{
						DAQ_MSG("Download fast image_%d mode 32 bits\033[22;30m\n",kk) ;
						status = FastReadImageAndFillMatrix(qusb,MODE_32b,module_msk,dmatrix,kk) ;
						}
					else
						{
						DAQ_MSG("Download fast image_%d index %d mode 16 bits\033[22;30m\n",kk,kk) ;
						status = SecureFastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,kk) ;
						if (status < 0)
							{
							printf("\033[22;31mERROR BAD IMAGE: loop exit033[22;30m\n") ;
							return ;
							}
						}
					}
				else
					{
					for (i=0;i<MaxModuleReady;i++)	
						{
						if (ModuleReady_tab[i] == 1)
							{
							WMODULE = i+1 ;
							// echo "zACQUI 1000 ./data/toto" > /dev/ttyS0
							DAQ_MSG("Download lsb image_%d  module_%d index %d\033[22;30m\n",kk,WMODULE,(2*kk)) ;
							status = ReadImageIndexAndFillMatrix(qusb,READ_IMAGE_COUNTER,WMODULE,(2*kk),dlsbmatrix) ;
							DAQ_MSG("Download msb image_%d module_%d index %d\033[22;30m\n",kk,WMODULE,((2*kk)+1) ) ;
							usleep(100000) ;
							status = ReadImageIndexAndFillMatrix(qusb,READ_IMAGE_COUNTER,WMODULE,((2*kk)+1),dmsbmatrix) ;
							}
						}
					AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ;
					}
					
				ExtractFileName(synchro.fileName_param,&sfileName) ;
				sprintf(fileName,"%s/%s%d.dat",synchro.fileName_param,sfileName.c_str(),kk) ; // nom des fichiers de sortie 
				RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,fileName) ;	
				SaveMatrixToFile(fileName,dmatrix) ;
				DAQ_MSG("Save data to %s\033[22;30m\n",fileName) ;
				if (kk < 4) 	
					delete(h2) ;
			
				if (!FAST_READOUT)	
					{
					sprintf(fileName,"%s/%s_msb_%d.dat",synchro.fileName_param,sfileName.c_str(),kk)  ;
					SaveMatrixToFile(fileName,dmsbmatrix) ;
					sprintf(fileName,"%s/%s_lsb_%d.dat",synchro.fileName_param,sfileName.c_str(),kk)  ;
					SaveMatrixToFile(fileName,dlsbmatrix) ;
					}
				
				//msbfileName = (string) PathAndfileName + (string) sfileName + "_msb_" + kk + ".dat" ;
				//lsbfileName = (string) PathAndfileName +(string)  sfileName + "_lsb_" + kk + ".dat" ;
				}
		
			} // End of if (!TEST_SERIAL_COM) 

			status = config_serial_synchro(&fd,oldtio,newtio,1) ;
			if (status < 0) 
				{
				DAQ_MSG("Unable to open Serial Port\033[22;30m\n") ;
				return ;
				}
			sprintf(buf,"ACQUI ENDED%c%c",0x0a,0x0d) ;
			write_serial(fd,oldtio,buf) ;
			DAQ_MSG("Send ACQUI ENDED message\033[22;30m\n") ;
			
		
			if (loop_synchro_img >NbImgSpinBox->value()) 
				MainGUI::StopAcquisition_Slot() ;
			else
				{
				DAQ_MSG("Next image\033[22;30m\n") ;
				if (StartAcqMode_comboBox->currentItem() == LOOP_ENABLE)
					gen_timer->start(100, FALSE );
				}
		
			if (StartAcqMode_comboBox->currentItem() == LOOP_ENABLE)
				{
				gen_timer->start(100, FALSE );
				delete(h2) ;
				}

			return ;		
			} // nd of if  (Synchro_ComboBox->currentItem()  == SERIAL_SYNCHRO)  

   
  	 ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
  
   	if (FAST_READOUT)
		{
		DAQ_MSG("Image us integration time = %d nb_image = %d module_msk = %d\033[22;30m\n",integration_time,nb_image,module_msk) ;
		ImageUs(qusb,module_msk,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
		}
    	else
		{
		DAQ_MSG("Image us integration time = %d Index img = %d Nb_Image = %d loop_img = %d\033[22;30m\n",integration_time,index_img,nb_image,loop_img) ;
		for (pmodule=0;pmodule<MAXMODULE;pmodule++)
			{
			if (ModuleReady_tab[pmodule] == 1)
				{
				wmodule = pmodule + 1 ; 
				if (Module_Answer == wmodule)
					ImageUs(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITH_ANSWER) ;
				else
					ImageUs(qusb,wmodule,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image,WITHOUT_ANSWER) ;
				}
			}
		}
  
	Image_ProgressBar->setProgress(0, nb_image ) ; QApplication::flush() ; 
	//start_time = clock() ;
	//usleep(integration_time*1000) ;
	status = 0 ;
	
	while ((status !=  IMAGE_US_ENDED) & (status !=  TIMOUT_DETECTED))
		{
		start_time = clock() ;
		// receive  (IMAGE_ADDED	0x1140) and  (IMAGE_US_ENDED 0x1141) at the end
		status = WaitingForHandShakeImageUSEnded(qusb,(long) integration_time,nb_image,&progress_acq) ;
		if (status ==  IMAGE_US_ENDED) progress_acq = nb_image ;
		if(status == TIMOUT_DETECTED) return ;
		//Image_ProgressBar->setProgress(++image_index, nb_image ) ; QApplication::flush() ; 
		//Image_ProgressBar->setProgress(progress_acq, nb_image ) ; QApplication::flush() ; 
		}

    
      	DAQ_MSG("Image_us complete status = %d\033[22;30m\n",status) ;
    
      if  ((Synchro_ComboBox->currentItem()  == PIXSCAN_SYNCHRO)  & (!FAST_READOUT))
		{
		write_file_synchro(&synchro, roi) ;
		printf("NbImgSpinBox->value() = %d <=> loop_synchro_img = %d \n",NbImgSpinBox->value(),loop_synchro_img ) ;
		loop_synchro_img++  ;
		
		if (loop_synchro_img >NbImgSpinBox->value()) 
			MainGUI::StopAcquisition_Slot() ;
		else
			{
			DAQ_MSG("Next image\033[22;30m\n") ;
			if (StartAcqMode_comboBox->currentItem() == LOOP_ENABLE)
				gen_timer->start(100, FALSE );
			}
		if (DELETE_H2)	
			delete(h2) ;
		return ;
		}

	// download Image   
	if (FAST_READOUT)
		{
		if (MSB_LSB)
			{
			DAQ_MSG("Begin download image fast readout mode 32 bits module_msk %d loop_img %d\033[22;30m\n",module_msk,loop_img) ; 
			//FastReadImageAndFillMatrix(qusb,MODE_32b,module_msk,dmatrix,INDEX_0) ;
			status = FastReadImageAndFillMatrix(qusb,MODE_32b,module_msk,dmatrix,INDEX_0) ;
			/*
			status = SecureFastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dlsbmatrix,INDEX_0) ;
			if (status < 0)
				{
				printf("\033[22;31mERROR BAD IMAGE: loop exit033[22;30m\n") ;
				}
			ResetHUBFifo(qusb) ;
			status = SecureFastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmsbmatrix,INDEX_4) ;
			if (status < 0)
				{
				printf("\033[22;31mERROR BAD IMAGE: loop exit033[22;30m\n") ;
				}
			AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ;
			*/
			if (status < 0)
		   		{	
		   		//ResetModule(qusb,module_msk) ;
		   		ResetHUBFifo(qusb) ;
		   		printf("\033[22;31mERROR: Not enougth data in FIFO\033[22;30m\n") ;	
		   		status = FastReadImageAndFillMatrix(qusb,MODE_32b,module_msk,dmatrix,INDEX_0) ;
		   		if (status < 0)
		   			{
    		         		printf("\033[22;31mERROR: Not enougth data in FIFO, exit\033[22;30m\n") ;
					//return  ;
					}
				
		   		}
			  
			}
		else
			{
			DAQ_MSG("Begin download image fast readout mode 16 bits module_msk %d loop_img %d\033[22;30m\n",module_msk,loop_img) ; 
			//FastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,INDEX_0) ;
			status = SecureFastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,INDEX_0) ;
			if (status < 0)
				{
				printf("\033[22;31mERROR BAD IMAGE: loop exit033[22;30m\n") ;
				}
			}

		if (status < 0)
			{
			printf("******************************************\n") ;
			printf("\033[22;31mERROR: Bad image status = %d\033[22;30m\n",status) ;
			printf("******************************************\n") ;
			}
		else
			DAQ_MSG("Download image fast readout, complete\033[22;30m\n" ) ;
			
		} // End of if (FAST_READOUT)
	else
		{
		for (i=0;i<MaxModuleReady;i++)	
			{
			if (ModuleReady_tab[i] == 1)
				{
   				WMODULE = i+1 ;
				DAQ_MSG("Begin download image Module_%d CONTINUE mode\033[22;30m\n",WMODULE ) ; 
				if (MSB_LSB)
					{
    					status = ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_LSB,dlsbmatrix,WMODULE) ;
					if (status < 0)
						DAQ_MSG("\033[22;31mERROR reading Image Module_%d \033[22;30m\n",WMODULE ) ;
					else 
						DAQ_MSG("Download LSB image Module_%d, mode counter, continue mode, complete\033[22;30m\n",WMODULE ) ;
					}
				else 
					{
					//ReadImageIndexAndFillMatrix(qusb,READ_IMAGE_COUNTER,WMODULE,4,dmatrix) ;
    					status = ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER,dmatrix,WMODULE) ;
					if (status < 0)
						DAQ_MSG("\033[22;31mERROR reading Image Module_%d \033[22;30m\n",WMODULE ) ;
					else 
						DAQ_MSG("Download image Module_%d, mode counter, continue mode, complete\033[22;30m\n",WMODULE ) ;
					}
				}
    			}
		} // End of if (!FAST_READOUT)
		
	// download MSB
	if ((MSB_LSB) & (!FAST_READOUT))
		{
		for (i=0;i<MaxModuleReady;i++)	
			{
			if (ModuleReady_tab[i] == 1)
				{
   				WMODULE = i+1 ;
				DAQ_MSG("Begin download MSB image Module_%d \033[22;30m\n",WMODULE ) ; 
    				status = ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_MSB,dmsbmatrix,WMODULE) ;
				if (status < 0)
					DAQ_MSG("\033[22;31mERROR reading Image Module_%d \033[22;30m\n",WMODULE ) ;
				else
					DAQ_MSG("Download MSB image Module_%d, mode counter, continue mode, complete\033[22;30m\n",WMODULE ) ;
				}
   			}	
		AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ;
		}

	if (Save_ComboBox->currentItem() != DONT_SAVE)
    		{ 
		ExtractFileName(PathAndfileName,&sfileName) ;
    		RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ;		
		SaveMatrixToFile(PathAndfileName,dmatrix) ;

		if ((MSB_LSB) & (!FAST_READOUT))
			{
			msbfileName = (string) PathAndfileName + "_msb.dat" ;
   			lsbfileName = (string) PathAndfileName + "_lsb.dat" ;
			SaveMatrixToFile(msbfileName,dmsbmatrix) ;
			SaveMatrixToFile(lsbfileName,dlsbmatrix) ;
			}
    		}
   	else
    		{
    		//AddMatrix(dmatrix,summatrix) ;
		//fprintf(stdout,"1 : mClone = %d canvasID = %d\n",mClone, mClone->GetCanvasID()) ;
		//fflush(stdout) ;
		
		//if (mClone->GetCanvasID() > 100)
		//	{
		//	printf("Create a new Clone Canvas\n") ;
		//	mClone = NULL ;
		//	mClone = (TCanvas *) (tQRootCanvas2->GetCanvas())->DrawClone("MENU") ;
		//	mClone->ToggleEventStatus();
		//	}
		 
		//fprintf(stdout,"2 : mClone = %d canvasID = %d\n",mClone, mClone->GetCanvasID()) ;
		//fflush(stdout) ;
    		RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,"") ;
    		}
		
   	MainGUI::ROI_Slot() ;
   
  	 if (Save_ComboBox->currentItem() == SAVE_EDF)
		{
		ExtractFileName(PathAndfileName,&sfileName) ;
		//sprintf(PathAndfileName,"%s/%s.edf", d2am.path, d2am.fileName_param) ;
		edffileName = (string) PathAndfileName + ".edf" ;
    		DAQ_MSG("Save to EDF file => %s\033[22;30m\n", edffileName.c_str()) ;
		SaveMatrixToEDFFile(edffileName,&EdfConfigFile,dmatrix) ;	
		}
			
   	if ((Save_ComboBox->currentItem() != DONT_SAVE) &&  (Synchro_ComboBox->currentItem() == D2AM_SYNCHRO))
		{
		write_file_synchro(&synchro, roi) ;
		} 
   	if ((Save_ComboBox->currentItem() != DONT_SAVE) &&  (Synchro_ComboBox->currentItem() == PIXSCAN_SYNCHRO))
   		{
		write_file_synchro(&synchro, roi) ;
		}

	if (index_img >900) 
		index_img = 0 ;
	else
		index_img++ ;
		
	loop_img++ ;

	if (DELETE_H2)	
		delete(h2) ;

	if (StartAcqMode_comboBox->currentItem() == LOOP_ENABLE)
		{
		delete(h2) ;
		gen_timer->start(100, FALSE );
		}

}



/**
* \fn void MainGUI::DownloadImageIndex_Slot()
* \brief Download a image index from a specific zone of the DDR2
*
* ACQ MODE OVF          => ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_OVF,dmatrix,WMODULE) \n
* ACQ MODE COUNTER  => ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_LSB,dlsbmatrix,WMODULE) and \n
*                                         ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_MSB,dmsbmatrix,WMODULE) ; \n

* Adding of the two matrix => AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ; \n
* then plot matrix to Canvas => RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ; \n
* if selected save matrix to the file => SaveMatrixToFile(PathAndfileName,dmatrix) ; \n
*
* \param
* \return
*/
void MainGUI::DownloadImageIndex_Slot()
{
//IndexImg_SpinBox
string sfileName ;
int NbImages = 0 ;
int IndexImage = 0 ;
char mess[50] ;
int status = 0 ;
int ii = 0 ;

	//integration_time = IntegrationTimeSpinBox->value();
	//index_img = IndexImg_SpinBox->value();
	//nb_image = NbImgSpinBox->value();

	if ((Save_ComboBox->currentItem() != DONT_SAVE) & (StartAcqMode_comboBox->currentItem() != DOWNLOAD_LOOP_ENABLE))
		{
		kindoffile = DATA_FILE ;
		MainGUI::SetWriteFile() ;
		if (QStrFileSelected == NULL) return ;
		}

	IndexImage = IndexImg_SpinBox->value() ;
	NbImages = NbImgSpinBox->value() ;

    	TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;

	memset(dlsbmatrix,0,sizeof(dlsbmatrix)) ;
	memset(dmsbmatrix,0,sizeof(dmsbmatrix)) ;
	memset(dmatrix,0,sizeof(dmatrix)) ;

	DAQ_MSG("Begin DownLoad the Index image number %d  \033[22;30m\n", ii) ;
	//DAQ_MSG("Begin DownLoad the Index image number  \033[22;30m\n") ;
	if ((Save_ComboBox->currentItem() != DONT_SAVE) & (StartAcqMode_comboBox->currentItem() != DOWNLOAD_LOOP_ENABLE))
		{
	 	std::sprintf(PathAndfileName,"%s_%d.dat",(const char *) QStrFileSelected,ii) ;
        	//ExtractSuffix(folder_tmp,suffix_file) ;
		DAQ_MSG("%s\033[22;30m\n",PathAndfileName) ;
		}
	if (FAST_READOUT)	
		{
		if (MSB_LSB)
			{
			DAQ_MSG("Begin fast read image_%d  mode 32 bit\033[22;30m\n",IndexImage) ;
			//FastReadImageAndFillMatrix(qusb,MODE_32b,module_msk,dmatrix,INDEX_0) ;
			//FastReadImageAndFillMatrix(qusb,MODE_32b,module_msk,dmatrix,IndexImage) ;
			status = FastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dlsbmatrix,(INDEX_0+IndexImage)) ;
			ResetHUBFifo(qusb) ; 
			status = FastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmsbmatrix,(INDEX_4+IndexImage)) ;
			AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ;
			DAQ_MSG("Download read image_%d  mode 32 bit complete\033[22;30m\n",IndexImage) ;
			}
		else
			{
			DAQ_MSG("Begin fast read image index_%d mode 16 bit\033[22;30m\n",IndexImage) ;
			FastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,IndexImage) ;
			//FastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,IndexImage) ;
			DAQ_MSG("Download read image_%d  mode 16 bit complete\033[22;30m\n",IndexImage) ;
			}
		}
	else
		{
		for (int i=0;i<MaxModuleReady;i++)
			{
			if (ModuleReady_tab[i] == 1)
				{
				WMODULE = i+1 ;
				status = ReadImageIndexAndFillMatrix(qusb,READ_IMAGE_COUNTER,WMODULE,ii,dmatrix) ;
				if (status < 0)
					DAQ_MSG("\033[22;31mERROR reading Image Index_%d Module_%d \033[22;30m\n",ii,WMODULE ) ;
				else 
					DAQ_MSG("Download Image Index_%d Module_%d complete\033[22;30m\n",ii,WMODULE ) ;
				}
			}
		}
			
	 if ((Save_ComboBox->currentItem() != DONT_SAVE) & (StartAcqMode_comboBox->currentItem() != DOWNLOAD_LOOP_ENABLE))
		{
		ExtractFileName(PathAndfileName,&sfileName) ;
		RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ;
		SaveMatrixToFile(PathAndfileName,dmatrix) ;
		}
	else
		{
		//AddMatrix(dmatrix,summatrix) ;
		sprintf(mess,"Index Image %d ",ii) ;
		RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,mess) ;
		if (DELETE_H2)	delete(h2) ;
		}
}


void MainGUI::myTimerEvent_Slot()
{

	if (timer_type == 0)
		 { 
 		Image_ProgressBar->setProgress((int) (timer_steps * (timer_totalsteps/100)),timer_totalsteps) ;
 		timer_steps++ ;
 		if(timer_steps > 100) 
  			{
 			 gen_timer->stop() ;  
 			 //ImageUsButton->setEnabled(TRUE) ;
  			 }
 	}
	else if (timer_type == 1)
 		{
 		MainGUI::PlotLoop_Slot() ;
 		timer_steps++ ;
	 	if(timer_steps > PlotNbImages_spinBox->value())  gen_timer->stop() ;
 		}
	else if (timer_type == 2)
 		 MainGUI::StartAcqLoopMode_Slot() ;	
	else if (timer_type == 3)
  		MainGUI::DigitalTest_Slot()  ;
	else if (timer_type == 4)
  		MainGUI::DownloadImageIndex_Slot()  ;
}


void MainGUI::StopAcquisition_Slot()
{

 	gen_timer->stop() ;
 	WriteWordToModule(qusb,ModuleID_ComboBox->currentItem(),NULL_PARAM,STOP_ACQ)  ; 
 	StartAcq_Button->setEnabled(TRUE) ;
	 StopAcq_Button->setEnabled(FALSE) ;
	enable_loop = 0 ;

}



/**
* \fn void MainGUI::ReadImage_Slot()
* \brief Used only for debuging
*
* \param
* \return
*/
void MainGUI::ReadImage_Slot()
{
//QString suffix_file ;
QString FileSelected ;
char folder_tmp[500] ;
char suffix_file[50] ;
char sys_command[500] ;
string pFile ;
string pPath ;
string pPrefix  ;
int status ;
string sfileName ;

 DAQ_MSG("Begin download image \033[22;30m\n" ) ;
 
ResetMatrix(dmatrix) ;
 //IndexImage = IndexImg_SpinBox->value() ;
// NbImages = NbImgSpinBox->value() ;

if (SaveDigTestCheckBox->isOn())
	{
  	kindoffile = DATA_FILE ;
  	MainGUI::SetWriteFile() ;
  	DAQ_MSG("%s\033[22;30m\n",(const char *) QStrFileSelected) ; 
  	std::sprintf(folder_tmp,"%s",(const char *) QStrFileSelected) ;
  	ExtractSuffix(folder_tmp,suffix_file) ;
	}

 
 TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ; 

 status = SecureFastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,INDEX_0) ; 

   if (SaveDigTestCheckBox->isOn())
   	{    
   	ExtractFileName(PathAndfileName,&sfileName) ;
   	RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ;
   	SaveMatrixToFile(PathAndfileName,dmatrix) ;
	}
else
	{
	//AddMatrix(dmatrix,summatrix) ;
	RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,"") ;
	}
 
DAQ_MSG("Download image detector ended\033[22;30m\n" ) ;

}



/**
* \fn void MainGUI::DigitalTest_Slot()
* \brief Function used to test the digital part
*
* Send Command to all modules connected => TestDigitalPart(qusb,NbofHits,target_matrix,WMODULE) ; \n
* Plot the result of digital test to Canvas => RootHistoHitConfigFillPlotMatrix(mClone,target_histo,dmatrix,sfileName) ; \n
* Save results to file, if option is selected => SaveMatrixToFile(PathAndfileName,dmatrix) ; \n
*
* \param
* \return
*/
void MainGUI::DigitalTest_Slot()
{
QString FileSelected ;
string pFile ;
string pPath ;
string pPrefix  ;
string sfileName ;
int Nb_error = 0 ;
int status = 0 ;
int Digital_Mode ;
int tmp_matrix[(120*8)][80*7] ;


	
	gen_timer->stop() ;

	ResetMatrix(dmatrix) ;
	Digital_Mode = StartAcqMode_comboBox->currentItem() ;

	if (SaveDigTestCheckBox->isOn())
		{
		kindoffile = DATA_FILE ;
		MainGUI::SetWriteFile() ;
		if (QStrFileSelected == NULL) return ;
			std::sprintf(PathAndfileName,"%s.dat",(const char *) QStrFileSelected) ;
			//ExtractSuffix(folder_tmp,suffix_file) ;
		DAQ_MSG("%s\033[22;30m\n",PathAndfileName) ;	
    		}


	DAQ_MSG("Begin DigitalTest\033[22;30m\n") ;
	TestDigitalPart(qusb,NbHitSpinBox->value(),dmatrix,module_msk) ;

	status = SecureFastReadImageAndFillMatrix(qusb,MODE_16b,module_msk,dmatrix,INDEX_0) ;
	DAQ_MSG("Download image of digital test ended\033[22;30m\n") ; 
	DAQ_MSG("Digital Test ended, download image\033[22;30m\n") ; 

   	//if ((SaveDigTestCheckBox->isOn()) &  (Digital_Mode != DIG_LOOP_ENABLE))
	if (Digital_Mode != DIG_LOOP_ENABLE)
    		{    
		TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
    		ExtractFileName(PathAndfileName,&sfileName) ;
   		RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ;
    		SaveMatrixToFile(PathAndfileName,dmatrix) ;
		}
	else 
		{
		if (loop_img == 0)
			{
			TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
			RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,"Digital test") ;
			CopyMatrix(dmatrix, refmatrix) ;
			}
		else 
			{
			TH2F *h2_1 = new TH2F("h2_1","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;	
			TH2F *h2_2 = new TH2F("h2_2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
			ResetMatrix(tmp_matrix) ;
			Nb_error = CompareMatrix(refmatrix,dmatrix,tmp_matrix) ;
			AddMatrix(tmp_matrix,error_matrix) ;
			total_error += Nb_error ;
			printf("\n") ;
			DAQ_MSG("\033[22;31m********************************************************************\033[22;30m\n") ;
			DAQ_MSG("\033[22;31m%d : Digital test loop mode Nb Error = %d Total Error = %d\033[22;30m\n",loop_img,Nb_error,total_error) ;
			DAQ_MSG("\033[22;31m********************************************************************\033[22;30m\n\n") ;
			Root2HistoHitConfigFillPlotMatrix(mClone,h2_1,refmatrix,"Reference Img",h2_2,error_matrix,"Error Img") ;
			}
		gen_timer->start(100, FALSE );
		}
	loop_img++ ;
}



/**
* \fn void MainGUI::PlotImage_Slot()
* \brief Used only for debuging
*
* \param
* \return
*/
void MainGUI::PlotImage_Slot()
{
int PlotSelector = 0  ;
string sfileName ;


/*	
	kindoffile = DATA_FILE ;
	MainGUI::SetReadFile() ;
	cout << " FileSelected => " << QStrFileSelected << endl ;
 	ExtractFileName(QStrFileSelected,&pFile) ;
	std::sprintf(fileName,"%s",pFile) ;
	cout << " ExtractFileName => " << fileName << endl ;
	TQRootCanvas *mCanv = new TQRootCanvas(this,"mCanv",NULL) ;
	TCanvas *mClone = (TCanvas *) (mCanv->GetCanvas())->DrawClone("MENU") ;
	TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*NbofModules_spinBox->value()),0,(ROW_NB*NbofModules_spinBox->value())) ;
	ExtractPathPrefixIndex(QStrFileSelected,&pPath,&pPrefix,&pIndex) ;
 std::sprintf(fileName,"%s/",pPrefix,&pIndex) ;
//  fileName = QStrFileSelected.latin1() ;
*/
 //TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*NbofModules_spinBox->value()),0,(ROW_NB*NbofModules_spinBox->value())) ;

 
 //TQRootCanvas *mCanv = new TQRootCanvas(this,"mCanv",NULL) ;
 //TCanvas *mClone = (TCanvas *) (mCanv->GetCanvas())->DrawClone("MENU") ;
 //TCanvas *mClone = (TCanvas *) (tQRootCanvas2->GetCanvas())->DrawClone("MENU") ;

 

if (PlotSelector == 0)
 {
  kindoffile = DATA_FILE ;
 MainGUI::SetReadFile() ;
  if (QStrFileSelected == "") return ;
 
 if (PlotNormalLoop_comboBox->currentItem() == 0)
  {
  TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*8),0,(ROW_NB*8)) ;
  if (ReadFileToMatrix(QStrFileSelected.latin1(),dmatrix) ==0) 
   { 
   ExtractFileName(QStrFileSelected.latin1(),&sfileName) ;
   RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ;// RootConfigHistoHit ;  //RootFillHistoHit ;  //RootPlotHistoHit  ;
   if (DELETE_H2)	
	delete(h2) ;
   }
  else printf("File Error") ;
  DAQ_MSG( "Plot %s ended \033[22;30m\n",QStrFileSelected.latin1())  ;
  
  //RootHistoHitConfigFillPlot(mClone,h2,QStrFileSelected.latin1(),ModuleID) ; 
  //RootConfigHistoHit(mClone,h2,fileName) ; 
  //RootFillHistoHit(h2,QStrFileSelected.latin1(),0) ;
  //RootPlotHistoHit(mClone,h2)  ;
  }
 else
  {
  /*(timer_progress
    for (int i =0;i<PlotNbImages_spinBox->value();i++)
    {
   h2[i] = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*NbofModules_spinBox->value()),0,(ROW_NB*NbofModules_spinBox->value())) ;
   ExtractPathPrefixIndex(QStrFileSelected,&pPath,&pPrefix,&pIndex) ;
   std::sprintf(fileName,"%s/%s_%d/%s_%d_0.dat",pPath,pPrefix,pIndex,pPrefix,(pIndex+i)) ;
   cout << "Open file => " << fileName << endl ;
   RootConfigHistoHit(mClone,h2[i],fileName) ; 
   RootFillHistoHit(h2[i],QStrFileSelected.latin1(),0) ;
			RootPlotHistoHit(mClone,h2[i])  ;
			usleep(1000000) ;
			}
		*/

		timer_type = 1 ;
		timer_steps = 0 ;
		gen_timer->start(1000, FALSE );
		//MainGUI::PlotLoop_Slot() ;
		}
	
	}


/*
if (PlotSelector == 1)
	{
	if (plot_thread != NULL)
		pthread_exit(&ret) ;

	//pthread_create (pthread_t * thread, pthread_attr_t * attr, void * (* start_routine) (void *), void * arg); 	
	ret = pthread_create(&plot_thread,NULL,(void * (*)(void*))PlotImageWithRoot,NULL) ;
	//if (ret == 0)
	}
*/
	
MainGUI::ROI_Slot() ;

}


void MainGUI::PlotNormalLoop_Slot()
{
	if (PlotNormalLoop_comboBox->currentItem() == 0)
		{
		PlotNbImages_spinBox->setEnabled(FALSE) ;
		PlotNbImages_textLabel->setEnabled(FALSE) ;
		}
	else
		{
		PlotNbImages_spinBox->setEnabled(TRUE) ;
		PlotNbImages_textLabel->setEnabled(TRUE) ;
		}
		
}


/**
* \fn void MainGUI::PlotLoop_Slot()
* \brief Used only for debuging
*
* \param
* \return
*/
void MainGUI::PlotLoop_Slot()
{
int i  ;
//RootAPI rAPI ;
char fileName[500] ;
char PathAndfileName[2000] ;
string pFile ;
string pPath ;
string pPrefix  ;
int pIndex ;
int ModuleID = 0 ;


			gen_timer->stop() ;
			i = timer_steps ;
			TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
			ExtractPathPrefixIndex(QStrFileSelected,&pPath,&pPrefix,&pIndex) ;
			ExtractFileName(QStrFileSelected,&pFile) ;
			std::sprintf(PathAndfileName,"%s/%s_%d/%s_%d_0.dat",pPath.c_str(),pPrefix.c_str(),(pIndex+i),pPrefix.c_str(),(pIndex+i)) ;
			std::sprintf(fileName,"%s_%d_0.dat",pPrefix.c_str(),(pIndex+i)) ;
			RootHistoHitConfigFillPlot(mClone,h2,PathAndfileName,ModuleID) ; 
			gen_timer->start(200, FALSE );

}


/*
void MainGUI::CTB_Slot()
{			
		CTB_Button->setEnabled(FALSE) ;
		ctb_timer->start(100, FALSE );
	
}
*/

/*
void MainGUI::CTB_TimerSlot()
{
//#define NB_WORDS	3
//#define ACK_POS		4

int buffer[NB_WORDS] ;
int data_length = 0 ;
int i ;
int datar[100] ;
char message[10] ;




	ctb_timer->stop() ;
	
	if (CounterOVF_ComboBox->currentItem() == COUNTER)	
		buffer[0] = CTB_MODE_COUNTER ;
	if (CounterOVF_ComboBox->currentItem() == OVF)	
  buffer[0] = CTB_MODE_OVF ;
 
 buffer[1] = CTBNbHit_spinBox->value();
 buffer[2] = CTBNbGate_SpinBox->value();
 WriteWordsToModule(qusb,ModuleID_ComboBox->currentItem(),NULL_PARAM,buffer,NB_WORDS)  ;
 
 data_length = 0 ;
 for (i=0;i<100;i++) datar[i] = 0 ;

 while (data_length == 0)
  data_length = ReadFromModule(qusb,datar,TIMEOUT*1000,10) ;
 printf("CTB Nb Words = %d =>  ",data_length) ;

 if (data_length > 0) {for (i=0;i<data_length;i++) printf("%d ",datar[i]) ; printf("\n") ;}
 if (datar[ACK_POS] != CTB_MODE_ACK) for (i=0;i<7;i++) datar[i+3] = -1 ;
 
 sprintf(message,"%d",datar[(ACK_POS+1)]) ; CTB_Chip1_TextEdit->setText(message) ;
 sprintf(message,"%d",datar[(ACK_POS+2)]) ; CTB_Chip2_TextEdit->setText(message) ;
 sprintf(message,"%d",datar[(ACK_POS+3)]) ; CTB_Chip3_TextEdit->setText(message) ;
 sprintf(message,"%d",datar[(ACK_POS+4)]) ; CTB_Chip4_TextEdit->setText(message) ;
sprintf(message,"%d",datar[(ACK_POS+5)]) ;	CTB_Chip5_TextEdit->setText(message) ;
sprintf(message,"%d",datar[(ACK_POS+6)]) ;	CTB_Chip6_TextEdit->setText(message) ;
sprintf(message,"%d",datar[(ACK_POS+7)]) ;	CTB_Chip7_TextEdit->setText(message) ;
	
ctb_timer->start(100, FALSE );
}



void MainGUI::StopCTB_Slot()
{
		ctb_timer->stop() ;
		CTB_Button->setEnabled(TRUE) ;
}
*/



/**
* \fn void MainGUI::DecreaseDACL_Slot()
* \brief Used only for debuging
*
* \param
* \return
*/
void MainGUI::DecreaseDACL_Slot()
{


int status ;
int buffer[11] ;





	/***********************************************************/
	/*  			CONFIG DACL                        */
	/***********************************************************/
	/*
	ith_tab[0] = ITH_1_spinBox->value() ;
	ith_tab[1] = ITH_2_spinBox->value() ;
	ith_tab[2] = ITH_3_spinBox->value() ;
	ith_tab[3] = ITH_4_spinBox->value() ;
	ith_tab[4] = ITH_5_spinBox->value() ;
	ith_tab[5] = ITH_6_spinBox->value() ;
	ith_tab[6] = ITH_7_spinBox->value() ;
 if (AskReadyloop(qusb) < 0) 
  {
  DAQ_MSG("ERROR : System not ready\n") ;
  return ;
  }
 for (i =0;i<7;i++)
  {
      DAQ_MSG("Config ITH  chip %d value 0x%x\033[22;30m\n",i,ith_tab[i] ) ;
  buffer[0] = CMOSDis_spinBox->value() ;
  buffer[1] = AMPTP_spinBox->value() ;
  buffer[2] = ITHH_spinBox->value() ;
  buffer[3] = VADJ_spinBox->value() ;
  buffer[4] = VREF_spinBox->value() ;
  buffer[5] = IMFP_spinBox->value() ;
  buffer[6] = IOTA_spinBox->value() ;
  buffer[7] = IPRE_spinBox->value() ;
  buffer[8] = ith_tab[i] ;
  buffer[9] = ITUNE_spinBox->value() ;
  buffer[10] = IBUFFER_spinBox->value() ;
  if ((status = HandShakeConfig_ith_dacl(qusb,ModuleID_ComboBox->currentItem(),i,buffer) ) < 0) {
  //if ((status = HandShakeConfigAllG(qusb,ModuleID_ComboBox->currentItem(),i,buffer) ) < 0) {
  //if ((status = HandShakeConfigAllG(qusb,ModuleID_ComboBox->currentItem(),ConfigGWChip_ComboBox->currentItem(),buffer) ) < 0) {
   DAQ_MSG("ERROR : unable to receive the HandShakeConfig_All_G\033[22;30m\n") ; return ;} 
  DAQ_MSG("Config ITH chip %d complete\033[22;30m\n",i ) ;
  }
 DAQ_MSG("Config ITH ended\033[22;30m\n") ;
 */

     DAQ_MSG("Decrease DACL \033[22;30m\n" ) ;
 buffer[0] = ITH_1_spinBox->value() ;
 buffer[1] = ITH_2_spinBox->value() ;
 buffer[2] = ITH_3_spinBox->value() ;
 buffer[3] = ITH_4_spinBox->value() ;
 buffer[4] = ITH_5_spinBox->value() ;
 buffer[5] = ITH_6_spinBox->value() ;
	buffer[6] = ITH_7_spinBox->value() ;
	if ((status = HandShakeConfig_ith_dacl(qusb,ModuleID_ComboBox->currentItem(),7,buffer) ) < 0) {
	//if ((status = HandShakeConfigAllG(qusb,ModuleID_ComboBox->currentItem(),i,buffer) ) < 0) {
	//if ((status = HandShakeConfigAllG(qusb,ModuleID_ComboBox->currentItem(),ConfigGWChip_ComboBox->currentItem(),buffer) ) < 0) {
		DAQ_MSG("ERROR : unable to receive the HandShakeConfig_All_G\033[22;30m\n") ; return ;}	
	DAQ_MSG("Decrease DACL complete\033[22;30m\n") ;
}


void MainGUI::ModuleID_Slot()
{
	
	//NbofModules_spinBox->value() ;
	//printf("Module choosen => %d\n",NbofModules_spinBox->value()) ;
	
}


void MainGUI::CounterOVFSelection_Slot()
{
	if (CounterOVF_ComboBox->currentItem() == 0)
		{
		//CTBNbHit_spinBox->setEnabled(TRUE) ;
		//CTBNbHit_textLabel->setEnabled(TRUE) ;
		}
	else 
		{
		//CTBNbHit_spinBox->setEnabled(FALSE) ;
		//CTBNbHit_textLabel->setEnabled(FALSE) ;
		}

}




void MainGUI::TestFnt_Slot()
{

   //new TRootCanvas() ;
//ConfigGUI *c = new ConfigGUI() ;
//ConfigGUI::Which_TextLabel->setText("coucou") ;
//c->show();
//wconfig = "ITHH Config" ;
//ConfigGUI::Which_TextLabel->setText((const QString) wconfig) ;
  
}




/*****************************************************************************************************************************/
/*																										*/
/*    MAIN CONFIG PART																					*/
/*																										*/
/*  void MainGUI::ConfigFile_Mod_1_Slot()) to MainGUI::ConfigFile_Mod_8_Slot())									*/
/*  void MainGUI::SendConfig_Mod_1_Slot()) to void MainGUI::SendConfig_Mod_8_Slot()) 							*/
/*  void MainGUI::SendConfig_Mod_All_Slot()																	*/
/*  void MainGUI::SelectCurrent_Slot()																		*/
/*  void MainGUI::ConfigG_WriteValue_Slot()																	*/
/*  void MainGUI::ConfigG_ReadValue_Slot()																	*/
/*  void MainGUI::ConfigG_SetDefaultValue_Slot()																*/	
/*  void MainGUI::ConfigG_SetGivenValue_Slot()																*/
/*  void MainGUI::ConfigG_Valid_Slot()																		*/
/****************************************************************************************************************************/

void MainGUI::ConfigFile_Mod_1_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[0] = QStrFileSelected.latin1() ;
	AffectFile_id = 0 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;

}


void MainGUI::ConfigFile_Mod_2_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[1] = QStrFileSelected.latin1() ;
	AffectFile_id = 1 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;
}


void MainGUI::ConfigFile_Mod_3_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[2] = QStrFileSelected.latin1() ;
	AffectFile_id = 2 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;
}


void MainGUI::ConfigFile_Mod_4_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[3] = QStrFileSelected.latin1() ;
	AffectFile_id = 3 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;

}


void MainGUI::ConfigFile_Mod_5_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[4] = QStrFileSelected.latin1() ;
	AffectFile_id = 4 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;
}


void MainGUI::ConfigFile_Mod_6_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[5] = QStrFileSelected.latin1() ;
	AffectFile_id = 5 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;
}


void MainGUI::ConfigFile_Mod_7_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[6] = QStrFileSelected.latin1() ;
	AffectFile_id = 6 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;
}


void MainGUI::ConfigFile_Mod_8_Slot()
{
	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	ConfigFile[7] = QStrFileSelected.latin1() ;
	AffectFile_id = 7 ;
	AffectFile =  ConfigFile[AffectFile_id] ;
	MainGUI::AffectConfigGControl_Slot() ;

}


void MainGUI::Check_ConfigFiles_Slot()
{
char tmp_message[500] ;
QMessageBox *mbox = new QMessageBox() ;

		std::sprintf(message,"Config file Module 1 => %s\n",ConfigFile[0].c_str() ) ;
		std::sprintf(tmp_message,"Config file Module 2 => %s\n",ConfigFile[1].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Config file Module 3 => %s\n",ConfigFile[2].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Config file Module 4 => %s\n",ConfigFile[3].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Config file Module 5 => %s\n",ConfigFile[4].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Config file Module 6 => %s\n",ConfigFile[5].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Config file Module 7 => %s\n",ConfigFile[6].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Config file Module 8 => %s\n",ConfigFile[7].c_str() ) ;
		std::strcat(message,tmp_message) ;
		mbox->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
		mbox->setPaletteBackgroundColor( QColor( 255, 0, 0 ) );
		mbox->information(this, tr("CONFIG FILES SELECTED"), tr(message));
		

}


void MainGUI::SendConfig_Mod_1_Slot()
{
//buffer[0] = CMOSDis_spinBox->value() ;
//buffer[1] = AMPTP_spinBox->value() ;
//buffer[2] = ITHH_spinBox->value() ;
//buffer[3] = VADJ_spinBox->value() ;
//buffer[4] = VREF_spinBox->value() ;
//buffer[5] = IMFP_spinBox->value() ;
//buffer[6] = IOTA_spinBox->value() ;
//buffer[7] = IPRE_spinBox->value() ;
//buffer[8] = ITHL_spinBox->value() ;
//buffer[9] = ITUNE_spinBox->value() ;
//buffer[10] = IBUFFER_spinBox->value() ;
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 1 ;

	if (FAST_READOUT) ResetHUBFifo(qusb) ;

	MainGUI::ModifCurrentControl_Slot() ;
	Ack_SendConfig_Mod_1->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_1->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_1 \033[22;30m\n" ) ;
	MainGUI::ConfigG_Valid_Slot() ;
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_1-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_1-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_1-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_1-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_1-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_1-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_1-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_1-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_1-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_1-1)][wchip]  ;
		
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_1,wchip,buffer) ;
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_1->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_1 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_1->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}

/*	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		printf("%d ",ithl_val[0][wchip] ) ;
	printf("\n") ;
*/		
	
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[0] == ""))
		{
		Ack_DACL_Mod_1->setPaletteBackgroundColor(QColor( 150,150,150) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_1 file => %s\033[22;30m\n",ConfigFile[0].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[0].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_1,ConfigFile[0].c_str()) ;
			
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_1, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_1->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_1 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_1->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 

}


void MainGUI::SendConfig_Mod_2_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 2 ;


	if (FAST_READOUT) ResetHUBFifo(qusb) ;

	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_2->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_2->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_2 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_2-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_2-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_2-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_2-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_2-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_2-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_2-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_2-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_2-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_2-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_2,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_2->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_2 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_2->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
		if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[1] == ""))
		{
		Ack_DACL_Mod_2->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_2 file => %s\033[22;30m\n",ConfigFile[1].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[1].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_2,ConfigFile[1].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_2, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_2->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_2 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_2->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_3_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 4 ;


	if (FAST_READOUT) ResetHUBFifo(qusb) ;
	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_3->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_3->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_3 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_3-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_3-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_3-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_3-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_3-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_3-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_3-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_3-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_3-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_3-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_3,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_3->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_2 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_3->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[2] == ""))
		{
		Ack_DACL_Mod_3->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_3 file => %s\033[22;30m\n",ConfigFile[2].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[2].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_3,ConfigFile[2].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_3, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_3->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_3 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_3->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_4_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 8 ;


	if (FAST_READOUT) ResetHUBFifo(qusb) ;
	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_4->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_4->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_4 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_4-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_4-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_4-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_4-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_4-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_4-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_4-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_4-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_4-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_4-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_4,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_4->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_4 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_4->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[3] == ""))
		{
		Ack_DACL_Mod_4->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_4 file => %s\033[22;30m\n",ConfigFile[3].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[3].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_4,ConfigFile[3].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_4, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_4->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_4 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_4->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_5_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 16 ;

	if (FAST_READOUT) ResetHUBFifo(qusb) ;
	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_5->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_5->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_5 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_5-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_5-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_5-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_5-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_5-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_5-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_5-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_5-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_5-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_5-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_5,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_5->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_5 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_5->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[4] == ""))
		{
		Ack_DACL_Mod_5->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_5 file => %s\033[22;30m\n",ConfigFile[4].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[4].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_5,ConfigFile[4].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_5, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_5->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_5 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_5->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_6_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 32 ;


	if (FAST_READOUT) ResetHUBFifo(qusb) ;
	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_6->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_6->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_6 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_6-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_6-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_6-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_6-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_6-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_6-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_6-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_6-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_6-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_6-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_6,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_6->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_6 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_6->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[5] == ""))
		{
		Ack_DACL_Mod_6->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_6 file => %s\033[22;30m\n",ConfigFile[5].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[5].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_6,ConfigFile[5].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_6, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_6->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_6 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_6->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_7_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 64 ;


	if (FAST_READOUT) ResetHUBFifo(qusb) ;
	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_7->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_7->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_7 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_7-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_7-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_7-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_7-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_7-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_7-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_7-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_7-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_7-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_7-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_7,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_7->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_7 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_7->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[6] == ""))
		{
		Ack_DACL_Mod_7->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_7 file => %s\033[22;30m\n",ConfigFile[6].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[6].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_7,ConfigFile[6].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_7, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_7->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_7 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_7->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_8_Slot()
{
int wchip ;
int buffer[20] ;
int status ;
int module_msk = 128 ;


	if (FAST_READOUT) ResetHUBFifo(qusb) ;
	MainGUI::ModifCurrentControl_Slot() ;
	
	Ack_SendConfig_Mod_8->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_8->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	DAQ_MSG("Send  ConfigG Module_8 \033[22;30m\n" ) ;
	
	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = cmos_dis[(MODULE_8-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = ithh_val[(MODULE_8-1)][wchip]  ;
		buffer[3] = vadj_val[(MODULE_8-1)][wchip]  ;
		buffer[4] = vref_val[(MODULE_8-1)][wchip]  ;
		buffer[5] = imfp_val[(MODULE_8-1)][wchip]  ;
		buffer[6] = iota_val[(MODULE_8-1)][wchip]  ;
		buffer[7] = ipre_val[(MODULE_8-1)][wchip]  ;
		buffer[8] = ithl_val[(MODULE_8-1)][wchip]  ;
		buffer[9] = itune_val[(MODULE_8-1)][wchip]  ;
		buffer[10] = ibuffer_val[(MODULE_8-1)][wchip]  ;
	
		if (FAST_READOUT)
			ConfigAllG(qusb,module_msk,wchip,buffer) ;
		else
			ConfigAllG(qusb,MODULE_8,wchip,buffer) ;
			
		if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
			{
			DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n",status,(wchip+1) ) ;
			Ack_SendConfig_Mod_8->setPaletteBackgroundColor( QColor( 0,255,0 ) );QApplication::flush() ; 
			}
		else 
		 	{
			DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_8 Chip_%d\033[22;30m\n",(wchip+1)) ;
			Ack_SendConfig_Mod_8->setPaletteBackgroundColor( QColor( 255,0,0 ) );QApplication::flush() ; 
			return ;
			}
		}
		
	if ((DACG_DACL_ComboBox->currentItem() == 0) | (QStrFileSelected == "") | (ConfigFile[7] == ""))
		{
		Ack_DACL_Mod_8->setPaletteBackgroundColor(QColor( 150,150,150 ) );QApplication::flush() ; 
		return ;
		}
        
	continue_Nb = 0 ;
	DAQ_MSG("Load Calib file MODULE_8 file => %s\033[22;30m\n",ConfigFile[7].c_str()) ;
	
	if (FAST_READOUT)
			status = LoadDACLFromFileConfig(qusb,module_msk,ConfigFile[7].c_str()) ;
		else
			status = LoadDACLFromFileConfig(qusb,MODULE_8,ConfigFile[7].c_str()) ;
	
	if (status < 0)
		{
		DAQ_MSG("\033[22;31mERROR : unable to load DACL file to Module_8, status = %d\033[22;30m\n",status) ;
		Ack_DACL_Mod_8->setPaletteBackgroundColor(QColor( 255,0,0 ) );QApplication::flush() ; 
		return ;
		}
		
	DAQ_MSG("Load DACL MODULE_8 ended\033[22;30m\n" ) ;
	Ack_DACL_Mod_8->setPaletteBackgroundColor(QColor( 0,255,0 ) );QApplication::flush() ; 
	
}


void MainGUI::SendConfig_Mod_All_Slot()
{

	//FlatConfig(qusb,MODULE_2,0x7f,ALL_CHIP,defaultFlat) ;
	Ack_SendConfig_Mod_1->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_1->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_2->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_2->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_3->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_3->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_4->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_4->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_5->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_5->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_6->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_6->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_7->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_7->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_SendConfig_Mod_8->setPaletteBackgroundColor( QColor( 230,230,230 ) );QApplication::flush() ; 
	Ack_DACL_Mod_8->setPaletteBackgroundColor(QColor( 230,230,230 ) );QApplication::flush() ; 
	
	
	
	if(ModuleReady_tab[0] == 1) 
		MainGUI::SendConfig_Mod_1_Slot() ;
	//FlatConfig(qusb,MODULE_2,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[1] == 1) 
		MainGUI::SendConfig_Mod_2_Slot() ;
	//if(NbofModules_spinBox->value() == 2) return ;
	//FlatConfig(qusb,MODULE_3,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[2] == 1) 
		MainGUI::SendConfig_Mod_3_Slot() ;
	//if(NbofModules_spinBox->value() == 3) return ;
	//FlatConfig(qusb,MODULE_4,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[3] == 1) 
		MainGUI::SendConfig_Mod_4_Slot() ;
	//if(NbofModules_spinBox->value() == 4) return ;
	//FlatConfig(qusb,MODULE_5,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[4] == 1) 
		MainGUI::SendConfig_Mod_5_Slot() ;
	//if(NbofModules_spinBox->value() == 5) return ;
	//FlatConfig(qusb,MODULE_6,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[5] == 1) 
		MainGUI::SendConfig_Mod_6_Slot() ;
	//if(NbofModules_spinBox->value() == 6) return ;
	//FlatConfig(qusb,MODULE_7,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[6] == 1) 
		MainGUI::SendConfig_Mod_7_Slot() ;
	//if(NbofModules_spinBox->value() == 7) return ;
	//FlatConfig(qusb,MODULE_8,0x7f,ALL_CHIP,defaultFlat) ;
	if(ModuleReady_tab[7] == 1) 
		MainGUI::SendConfig_Mod_8_Slot() ;
}



void MainGUI::ModifCurrentControl_Slot()
{
	
	MainGUI::ConfigG_ReadValue_Slot() ;
	MainGUI::ConfigG_Valid_Slot() ;
	
	
}



void MainGUI::SelectCurrent_Slot()
{
#define lSelect current 	0
#define lCMOS_dis 		1
#define lITHH			2
#define lVADj			3
#define lVRef				4
#define lIMFP				5
#define lIOTA			6
#define lIPRE				7
#define lITHL				8
#define lITUNE			9
#define lIBuffer			10
	
 	
 	switch (Current_comboBox->currentItem()) 
		{
		case lCMOS_dis :
			CopyTab(cmos_dis ,ConfigG_tab) ;
			//printf("CopyTab(cmos_dis ,ConfigG_tab)  \n") ;
		break ;
		case lITHH :
			CopyTab(ithh_val ,ConfigG_tab) ;
			//printf("CopyTab(ithh_val ,ConfigG_tab)  \n") ;
		break ;
		case lVADj :
			CopyTab(vadj_val ,ConfigG_tab) ;
			//printf("CopyTab(vadj_val ,ConfigG_tab) \n") ;
		break ;
		case lVRef :
			CopyTab(vref_val ,ConfigG_tab) ;
			//printf("CopyTab(vref_val ,ConfigG_tab) \n") ;
		break ;
		case lIMFP :
			CopyTab(imfp_val ,ConfigG_tab) ;
			//printf("CopyTab(imfp_val ,ConfigG_tab)  \n") ;
		break ;
		case lIOTA :
			CopyTab(iota_val ,ConfigG_tab) ;
			//printf("CopyTab(iota_val ,ConfigG_tab)  \n") ;
		break ;
		case lIPRE :
			CopyTab(ipre_val ,ConfigG_tab) ;
			//printf("CopyTab(ipre_val ,ConfigG_tab) \n") ;
		break ;
		case lITHL :
			CopyTab(ithl_val ,ConfigG_tab) ;
			//printf("CopyTab(ithl_val ,ConfigG_tab) \n") ;
		break ;
		case lITUNE :
			CopyTab(itune_val ,ConfigG_tab) ;
			//printf("CopyTab(itune_val ,ConfigG_tab) \n") ;
		break ;
		case lIBuffer :
			CopyTab(ibuffer_val ,ConfigG_tab) ;
			//printf("CopyTab(ibuffer_val ,ConfigG_tab) \n") ;
		break ;
  
  }
  
 MainGUI::ConfigG_WriteValue_Slot() ;

}



void MainGUI::ConfigG_WriteValue_Slot()
{

	MainGUI::Disconnect_ConfiGControl_Slot() ;
	
	Value_SpinBox_1_1->setValue(ConfigG_tab[0][0]) ;
	Value_SpinBox_1_2->setValue(ConfigG_tab[0][1]) ;
	Value_SpinBox_1_3->setValue(ConfigG_tab[0][2]) ;
	Value_SpinBox_1_4->setValue(ConfigG_tab[0][3]) ;
	Value_SpinBox_1_5->setValue(ConfigG_tab[0][4]) ;
	Value_SpinBox_1_6->setValue(ConfigG_tab[0][5]) ;
	Value_SpinBox_1_7->setValue(ConfigG_tab[0][6]) ;
	
	Value_SpinBox_2_1->setValue(ConfigG_tab[1][0]) ;
	Value_SpinBox_2_2->setValue(ConfigG_tab[1][1]) ;
	Value_SpinBox_2_3->setValue(ConfigG_tab[1][2]) ;
	Value_SpinBox_2_4->setValue(ConfigG_tab[1][3]) ;
	Value_SpinBox_2_5->setValue(ConfigG_tab[1][4]) ;
	Value_SpinBox_2_6->setValue(ConfigG_tab[1][5]) ;
	Value_SpinBox_2_7->setValue(ConfigG_tab[1][6]) ;
	
	Value_SpinBox_3_1->setValue(ConfigG_tab[2][0]) ;
	Value_SpinBox_3_2->setValue(ConfigG_tab[2][1]) ;
	Value_SpinBox_3_3->setValue(ConfigG_tab[2][2]) ;
	Value_SpinBox_3_4->setValue(ConfigG_tab[2][3]) ;
	Value_SpinBox_3_5->setValue(ConfigG_tab[2][4]) ;
	Value_SpinBox_3_6->setValue(ConfigG_tab[2][5]) ;
	Value_SpinBox_3_7->setValue(ConfigG_tab[2][6]) ;
	
	Value_SpinBox_4_1->setValue(ConfigG_tab[3][0]) ;
	Value_SpinBox_4_2->setValue(ConfigG_tab[3][1]) ;
	Value_SpinBox_4_3->setValue(ConfigG_tab[3][2]) ;
	Value_SpinBox_4_4->setValue(ConfigG_tab[3][3]) ;
	Value_SpinBox_4_5->setValue(ConfigG_tab[3][4]) ;
	Value_SpinBox_4_6->setValue(ConfigG_tab[3][5]) ;
	Value_SpinBox_4_7->setValue(ConfigG_tab[3][6]) ;
	
	Value_SpinBox_5_1->setValue(ConfigG_tab[4][0]) ;
	Value_SpinBox_5_2->setValue(ConfigG_tab[4][1]) ;
	Value_SpinBox_5_3->setValue(ConfigG_tab[4][2]) ;
	Value_SpinBox_5_4->setValue(ConfigG_tab[4][3]) ;
	Value_SpinBox_5_5->setValue(ConfigG_tab[4][4]) ;
	Value_SpinBox_5_6->setValue(ConfigG_tab[4][5]) ;
	Value_SpinBox_5_7->setValue(ConfigG_tab[4][6]) ;
	
	Value_SpinBox_6_1->setValue(ConfigG_tab[5][0]) ;
	Value_SpinBox_6_2->setValue(ConfigG_tab[5][1]) ;
	Value_SpinBox_6_3->setValue(ConfigG_tab[5][2]) ;
	Value_SpinBox_6_4->setValue(ConfigG_tab[5][3]) ;
	Value_SpinBox_6_5->setValue(ConfigG_tab[5][4]) ;
	Value_SpinBox_6_6->setValue(ConfigG_tab[5][5]) ;
	Value_SpinBox_6_7->setValue(ConfigG_tab[5][6]) ;

	Value_SpinBox_7_1->setValue(ConfigG_tab[6][0]) ;
	Value_SpinBox_7_2->setValue(ConfigG_tab[6][1]) ;
	Value_SpinBox_7_3->setValue(ConfigG_tab[6][2]) ;
	Value_SpinBox_7_4->setValue(ConfigG_tab[6][3]) ;
	Value_SpinBox_7_5->setValue(ConfigG_tab[6][4]) ;
	Value_SpinBox_7_6->setValue(ConfigG_tab[6][5]) ;
	Value_SpinBox_7_7->setValue(ConfigG_tab[6][6]) ;

	Value_SpinBox_8_1->setValue(ConfigG_tab[7][0]) ;
	Value_SpinBox_8_2->setValue(ConfigG_tab[7][1]) ;
	Value_SpinBox_8_3->setValue(ConfigG_tab[7][2]) ;
	Value_SpinBox_8_4->setValue(ConfigG_tab[7][3]) ;
	Value_SpinBox_8_5->setValue(ConfigG_tab[7][4]) ;
	Value_SpinBox_8_6->setValue(ConfigG_tab[7][5]) ;
	Value_SpinBox_8_7->setValue(ConfigG_tab[7][6]) ;
	
	MainGUI::Connect_ConfiGControl_Slot() ;
}




void MainGUI::ConfigG_ReadValue_Slot()
{

	ConfigG_tab[0][0] =  Value_SpinBox_1_1->value() ; 
	ConfigG_tab[0][1] = Value_SpinBox_1_2->value() ; 
	ConfigG_tab[0][2] = Value_SpinBox_1_3->value() ; 
	ConfigG_tab[0][3] = Value_SpinBox_1_4->value() ; 
	ConfigG_tab[0][4] = Value_SpinBox_1_5->value() ; 
	ConfigG_tab[0][5] = Value_SpinBox_1_6->value() ; 
	ConfigG_tab[0][6] = Value_SpinBox_1_7->value() ; 
	
	ConfigG_tab[1][0] = Value_SpinBox_2_1->value() ; 
	ConfigG_tab[1][1] = Value_SpinBox_2_2->value() ; 
	ConfigG_tab[1][2] = Value_SpinBox_2_3->value() ; 
	ConfigG_tab[1][3] = Value_SpinBox_2_4->value() ; 
	ConfigG_tab[1][4] = Value_SpinBox_2_5->value() ; 
	ConfigG_tab[1][5] = Value_SpinBox_2_6->value() ; 
	ConfigG_tab[1][6] = Value_SpinBox_2_7->value() ; 
	
	ConfigG_tab[2][0] = Value_SpinBox_3_1->value() ; 
	ConfigG_tab[2][1] = Value_SpinBox_3_2->value() ; 
	ConfigG_tab[2][2] = Value_SpinBox_3_3->value() ; 
	ConfigG_tab[2][3] = Value_SpinBox_3_4->value() ; 
	ConfigG_tab[2][4] = Value_SpinBox_3_5->value() ; 
	ConfigG_tab[2][5] = Value_SpinBox_3_6->value() ; 
	ConfigG_tab[2][6] = Value_SpinBox_3_7->value() ; 
	
	ConfigG_tab[3][0] = Value_SpinBox_4_1->value() ; 
	ConfigG_tab[3][1] = Value_SpinBox_4_2->value() ; 
	ConfigG_tab[3][2] = Value_SpinBox_4_3->value() ; 
	ConfigG_tab[3][3] = Value_SpinBox_4_4->value() ; 
	ConfigG_tab[3][4] = Value_SpinBox_4_5->value() ; 
	ConfigG_tab[3][5] = Value_SpinBox_4_6->value() ; 
	ConfigG_tab[3][6] = Value_SpinBox_4_7->value() ; 
	
	ConfigG_tab[4][0] = Value_SpinBox_5_1->value() ; 
	ConfigG_tab[4][1] = Value_SpinBox_5_2->value() ; 
	ConfigG_tab[4][2] = Value_SpinBox_5_3->value() ; 
	ConfigG_tab[4][3] = Value_SpinBox_5_4->value() ; 
	ConfigG_tab[4][4] = Value_SpinBox_5_5->value() ; 
	ConfigG_tab[4][5] = Value_SpinBox_5_6->value() ; 
	ConfigG_tab[4][6] = Value_SpinBox_5_7->value() ; 
	
	ConfigG_tab[5][0] = Value_SpinBox_6_1->value() ; 
	ConfigG_tab[5][1] = Value_SpinBox_6_2->value() ; 
	ConfigG_tab[5][2] = Value_SpinBox_6_3->value() ; 
	ConfigG_tab[5][3] = Value_SpinBox_6_4->value() ; 
	ConfigG_tab[5][4] = Value_SpinBox_6_5->value() ; 
	ConfigG_tab[5][5] = Value_SpinBox_6_6->value() ; 
	ConfigG_tab[5][6] = Value_SpinBox_6_7->value() ; 
	
	ConfigG_tab[6][0] = Value_SpinBox_7_1->value() ; 
	ConfigG_tab[6][1] = Value_SpinBox_7_2->value() ; 
	ConfigG_tab[6][2] = Value_SpinBox_7_3->value() ; 
	ConfigG_tab[6][3] = Value_SpinBox_7_4->value() ; 
	ConfigG_tab[6][4] = Value_SpinBox_7_5->value() ; 
	ConfigG_tab[6][5] = Value_SpinBox_7_6->value() ; 
	ConfigG_tab[6][6] = Value_SpinBox_7_7->value() ; 

	ConfigG_tab[7][0] = Value_SpinBox_8_1->value() ; 
	ConfigG_tab[7][1] = Value_SpinBox_8_2->value() ; 
	ConfigG_tab[7][2] = Value_SpinBox_8_3->value() ; 
	ConfigG_tab[7][3] = Value_SpinBox_8_4->value() ; 
	ConfigG_tab[7][4] = Value_SpinBox_8_5->value() ; 
	ConfigG_tab[7][5] = Value_SpinBox_8_6->value() ; 
	ConfigG_tab[7][6] = Value_SpinBox_8_7->value() ; 
	

}


void MainGUI::ConfigG_SetDefaultValue_Slot()
{
/*
#define lSelect current 	0
#define lCMOS_dis 		1
#define lITHH			2
#define lVADj			3
#define lVRef			4
#define lIMFP			5
#define lIOTA			6
#define lIPRE			7
#define lITHL			8
#define lITUNE			9
#define lIBuffer			10
*/
	
int i,j ;

	
	MainGUI::Disconnect_ConfiGControl_Slot() ;
	
	for (i=0;i<MAXMODULE;i++)
		{
		for (j=0;j<MAXCHIP;j++)
			{
			cmos_dis[i][j] = 0 ;
			ithh_val[i][j] = 0 ;
			vadj_val[i][j] = 0 ;
			vref_val[i][j] = 0 ;
			imfp_val[i][j] = 52 ;
			iota_val[i][j] = 40 ;
			ipre_val[i][j] = 60 ;
			ithl_val[i][j] = 22 ;
			itune_val[i][j] = 145 ;
			ibuffer_val[i][j] = 0 ;
			}
		}
	MainGUI::SelectCurrent_Slot() ;
	
	MainGUI::Connect_ConfiGControl_Slot() ;
	
}



void MainGUI::ConfigG_SetGivenValue_Slot()
{
    for (int i=0;i<MAXMODULE;i++)
	{
	for (int j=0;j<MAXCHIP;j++)
		{
		ConfigG_tab[i][j] = ConfigG_InitValue->value() ;
		}
	}
	if (Current_comboBox->currentItem() == 0)
		{
		std::sprintf(message,"Please select a current\n") ;
		QMessageBox::critical(this, tr("SELECTION ERROR"), tr(message));
		return ;
		}
	MainGUI::ConfigG_WriteValue_Slot() ;
	MainGUI::ConfigG_Valid_Slot() ;

}


void MainGUI::ConfigG_Valid_Slot()
{
#define lSelect_current 	0
#define lCMOS_dis 		1
#define lITHH			2
#define lVADj			3
#define lVRef			4
#define lIMFP			5
#define lIOTA			6
#define lIPRE			7
#define lITHL			8
#define lITUNE			9
#define lIBuffer			10


 	//MainGUI::ConfigG_WriteValue_Slot() ;
	
 	switch (Current_comboBox->currentItem()) 
		{
		case lCMOS_dis :
			CopyTab(ConfigG_tab,cmos_dis) ;
		break ;
		case lITHH :
			CopyTab(ConfigG_tab,ithh_val) ;
		break ;
		case lVADj :
			CopyTab(ConfigG_tab,vadj_val ) ;
		break ;
		case lVRef :
			CopyTab(ConfigG_tab,vref_val ) ;
		break ;
		case lIMFP :
			CopyTab(ConfigG_tab,imfp_val) ;
		break ;
		case lIOTA :
			CopyTab(ConfigG_tab,iota_val) ;
		break ;
		case lIPRE :
			CopyTab(ConfigG_tab,ipre_val) ;
		break ;
		case lITHL :
			CopyTab(ConfigG_tab,ithl_val) ;
		break ;
		case lITUNE :
			CopyTab(ConfigG_tab,itune_val) ;
		break ;
		case lIBuffer :
			CopyTab(ConfigG_tab,ibuffer_val) ;
		break ;
  		}
	
#undef lSelect_current 	
#undef lCMOS_dis 		
#undef lITHH			
#undef lVADj			
#undef lVRef			
#undef lIMFP			
#undef lIOTA			
#undef lIPRE			
#undef lITHL			
#undef lITUNE			
#undef lIBuffer			
	
}

void MainGUI::ConfigG_DACL32_Slot()
{
int i ;

	for (i=0;i<MAXMODULE;i++)
		ConfigFile[i] = "" ;

}




/*******************************************************************************************************************/
/*																								*/
/*				FLAT CONFIG PART																*/
/*																								*/
/* MainGUI::FlatConfig_SetValue_Slot()																*/
/* MainGUI::FlatConfig_Valid_Slot()																	*/
/* MainGUI::SendFlatConfig_Mod_1_Slot() to MainGUI::SendFlatConfig_Mod_8_Slot()						*/
/* MainGUI::FlatConfig_SendAll_Slot()																*/
/*******************************************************************************************************************/


void MainGUI::FlatConfig_SetValue_Slot()
{
QString value ;

	value = FlatConfig_InitValue->text() ;
	
	FlatConfiglineEdit_1_1->setText(value) ;
	FlatConfiglineEdit_1_2->setText(value) ;
	FlatConfiglineEdit_1_3->setText(value) ;
	FlatConfiglineEdit_1_4->setText(value) ;
	FlatConfiglineEdit_1_5->setText(value) ;
	FlatConfiglineEdit_1_6->setText(value) ;
	FlatConfiglineEdit_1_7->setText(value) ;
	
	FlatConfiglineEdit_2_1->setText(value) ;
	FlatConfiglineEdit_2_2->setText(value) ;
	FlatConfiglineEdit_2_3->setText(value) ;
	FlatConfiglineEdit_2_4->setText(value) ;
	FlatConfiglineEdit_2_5->setText(value) ;
	FlatConfiglineEdit_2_6->setText(value) ;
	FlatConfiglineEdit_2_7->setText(value) ;
	
	FlatConfiglineEdit_3_1->setText(value) ;
	FlatConfiglineEdit_3_2->setText(value) ;
	FlatConfiglineEdit_3_3->setText(value) ;
	FlatConfiglineEdit_3_4->setText(value) ;
	FlatConfiglineEdit_3_5->setText(value) ;
	FlatConfiglineEdit_3_6->setText(value) ;
	FlatConfiglineEdit_3_7->setText(value) ;
	
	FlatConfiglineEdit_4_1->setText(value) ;
	FlatConfiglineEdit_4_2->setText(value) ;
	FlatConfiglineEdit_4_3->setText(value) ;
	FlatConfiglineEdit_4_4->setText(value) ;
	FlatConfiglineEdit_4_5->setText(value) ;
	FlatConfiglineEdit_4_6->setText(value) ;
	FlatConfiglineEdit_4_7->setText(value) ;
	
	FlatConfiglineEdit_5_1->setText(value) ;
	FlatConfiglineEdit_5_2->setText(value) ;
	FlatConfiglineEdit_5_3->setText(value) ;
	FlatConfiglineEdit_5_4->setText(value) ;
	FlatConfiglineEdit_5_5->setText(value) ;
	FlatConfiglineEdit_5_6->setText(value) ;
	FlatConfiglineEdit_5_7->setText(value) ;
	
	FlatConfiglineEdit_6_1->setText(value) ;
	FlatConfiglineEdit_6_2->setText(value) ;
	FlatConfiglineEdit_6_3->setText(value) ;
	FlatConfiglineEdit_6_4->setText(value) ;
 	FlatConfiglineEdit_6_5->setText(value) ;
 	FlatConfiglineEdit_6_6->setText(value) ;
	FlatConfiglineEdit_6_7->setText(value) ;
	
	FlatConfiglineEdit_7_1->setText(value) ;
	FlatConfiglineEdit_7_2->setText(value) ;
	FlatConfiglineEdit_7_3->setText(value) ;
	FlatConfiglineEdit_7_4->setText(value) ;
	FlatConfiglineEdit_7_5->setText(value) ;
	FlatConfiglineEdit_7_6->setText(value) ;
	FlatConfiglineEdit_7_7->setText(value) ;
	
	FlatConfiglineEdit_8_1->setText(value) ;
	FlatConfiglineEdit_8_2->setText(value) ;
	FlatConfiglineEdit_8_3->setText(value) ;
	FlatConfiglineEdit_8_4->setText(value) ;
	FlatConfiglineEdit_8_5->setText(value) ;
	FlatConfiglineEdit_8_6->setText(value) ;
	FlatConfiglineEdit_8_7->setText(value) ; 
 
 	MainGUI::FlatConfig_Valid_Slot() ;

}


void MainGUI::FlatConfig_Valid_Slot()
{

QString value ;

 
 //printf("%s \n",value.latin1() ) ;
 
	FlatConfig_tab[0][0]  = FlatConfiglineEdit_1_1->text() ;
	FlatConfig_tab[0][1]  = FlatConfiglineEdit_1_2->text() ;
	FlatConfig_tab[0][2]  = FlatConfiglineEdit_1_3->text() ;
	FlatConfig_tab[0][3]  = FlatConfiglineEdit_1_4->text() ;
	FlatConfig_tab[0][4]  = FlatConfiglineEdit_1_5->text() ;
	FlatConfig_tab[0][5]  = FlatConfiglineEdit_1_6->text() ;
	FlatConfig_tab[0][6]  = FlatConfiglineEdit_1_7->text() ;
	
	FlatConfig_tab[1][0]  = FlatConfiglineEdit_2_1->text() ;
	FlatConfig_tab[1][1]  = FlatConfiglineEdit_2_2->text() ;
	FlatConfig_tab[1][2]  = FlatConfiglineEdit_2_3->text() ;
	FlatConfig_tab[1][3]  = FlatConfiglineEdit_2_4->text() ;
	FlatConfig_tab[1][4]  = FlatConfiglineEdit_2_5->text() ;
	FlatConfig_tab[1][5]  = FlatConfiglineEdit_2_6->text() ;
	FlatConfig_tab[1][6]  = FlatConfiglineEdit_2_7->text() ;
	
	FlatConfig_tab[2][0]  = FlatConfiglineEdit_3_1->text() ;
	FlatConfig_tab[2][1]  = FlatConfiglineEdit_3_2->text() ;
	FlatConfig_tab[2][2]  = FlatConfiglineEdit_3_3->text() ;
	FlatConfig_tab[2][3]  = FlatConfiglineEdit_3_4->text() ;
	FlatConfig_tab[2][4]  = FlatConfiglineEdit_3_5->text() ;
	FlatConfig_tab[2][5]  = FlatConfiglineEdit_3_6->text() ;
	FlatConfig_tab[2][6]  = FlatConfiglineEdit_3_7->text() ;
	
	FlatConfig_tab[3][0]  = FlatConfiglineEdit_4_1->text() ;
	FlatConfig_tab[3][1]  = FlatConfiglineEdit_4_2->text() ;
	FlatConfig_tab[3][2]  = FlatConfiglineEdit_4_3->text() ;
	FlatConfig_tab[3][3]  = FlatConfiglineEdit_4_4->text() ;
	FlatConfig_tab[3][4]  = FlatConfiglineEdit_4_5->text() ;
	FlatConfig_tab[3][5]  = FlatConfiglineEdit_4_6->text() ;
	FlatConfig_tab[3][6]  = FlatConfiglineEdit_4_7->text() ;
	
	FlatConfig_tab[4][0]  = FlatConfiglineEdit_5_1->text() ;
	FlatConfig_tab[4][1]  = FlatConfiglineEdit_5_2->text() ;
	FlatConfig_tab[4][2]  = FlatConfiglineEdit_5_3->text() ;
	FlatConfig_tab[4][3]  = FlatConfiglineEdit_5_4->text() ;
	FlatConfig_tab[4][4]  = FlatConfiglineEdit_5_5->text() ;
	FlatConfig_tab[4][5]  = FlatConfiglineEdit_5_6->text() ;
	FlatConfig_tab[4][6]  = FlatConfiglineEdit_5_7->text() ;
	
	FlatConfig_tab[5][0]  = FlatConfiglineEdit_6_1->text() ;
	FlatConfig_tab[5][1]  = FlatConfiglineEdit_6_2->text() ;
	FlatConfig_tab[5][2]  = FlatConfiglineEdit_6_3->text() ;
	FlatConfig_tab[5][3]  = FlatConfiglineEdit_6_4->text() ;
	FlatConfig_tab[5][4]  = FlatConfiglineEdit_6_5->text() ;
	FlatConfig_tab[5][5]  = FlatConfiglineEdit_6_6->text() ;
	FlatConfig_tab[5][6]  = FlatConfiglineEdit_6_7->text() ;
	
	FlatConfig_tab[6][0]  = FlatConfiglineEdit_7_1->text() ;
	FlatConfig_tab[6][1]  = FlatConfiglineEdit_7_2->text() ;
	FlatConfig_tab[6][2]  = FlatConfiglineEdit_7_3->text() ;
	FlatConfig_tab[6][3]  = FlatConfiglineEdit_7_4->text() ;
	FlatConfig_tab[6][4]  = FlatConfiglineEdit_7_5->text() ;
	FlatConfig_tab[6][5]  = FlatConfiglineEdit_7_6->text() ;
	FlatConfig_tab[6][6]  = FlatConfiglineEdit_7_7->text() ;
	
	FlatConfig_tab[7][0]  = FlatConfiglineEdit_8_1->text() ;
	FlatConfig_tab[7][1]  = FlatConfiglineEdit_8_2->text() ;
	FlatConfig_tab[7][2]  = FlatConfiglineEdit_8_3->text() ;
	FlatConfig_tab[7][3]  = FlatConfiglineEdit_8_4->text() ;
	FlatConfig_tab[7][4]  = FlatConfiglineEdit_8_5->text() ;
	FlatConfig_tab[7][5]  = FlatConfiglineEdit_8_6->text() ;
	FlatConfig_tab[7][6]  = FlatConfiglineEdit_8_7->text() ;
}


void MainGUI::SendFlatConfig_Mod_1_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 1 ;
	
	Ack_SendFlatConfig_Mod_1->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[0][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[0][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[0][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[0][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[0][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[0][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[0][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_1  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[0][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_1,0x7f,ALL_CHIP,fconfig[0]) ;
	
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
		//DAQ_MSG("Status WaitingForHandShakeFlatConfig = %d\n",status) ;
  		if (status <1) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_1\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_1->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			// modified by Arek
			//if (i != 3)
			//{
				DAQ_MSG("Flat Config Module_1  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[0][i].latin1()) ;	
				if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_1,(i+1),ONE_CHIP,fconfig[i])  ;
				status = WaitingForHandShakeFlatConfig(qusb) ;
  				if (status <1) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_1 chip_%d \033[22;30m\n",i) ;
							Ack_SendFlatConfig_Mod_1->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			//}
			}
		}	
	DAQ_MSG("Flat Config Module_1 complete\033[22;30m\n") ;		
	Ack_SendFlatConfig_Mod_1->setPaletteBackgroundColor( QColor(0, 255, 0 ) );

}


void MainGUI::SendFlatConfig_Mod_2_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 2 ;
	
	Ack_SendFlatConfig_Mod_2->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[1][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[1][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[1][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[1][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[1][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[1][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[1][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_2  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[1][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_2,0x7f,ALL_CHIP,fconfig[0]) ;
			
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_2\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_2->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_2  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[1][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_2,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_2 chip_%d \033[22;30m\n",i) ;
						Ack_SendFlatConfig_Mod_2->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_2 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_2->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::SendFlatConfig_Mod_3_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 4 ;
	
	Ack_SendFlatConfig_Mod_3->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[2][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[2][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[2][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[2][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[2][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[2][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[2][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_3  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[2][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_3,0x7f,ALL_CHIP,fconfig[0]) ;
			
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_3\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_3->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_3  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[2][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_3,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_3 \033[22;30m\n") ;
						Ack_SendFlatConfig_Mod_3->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_3 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_3->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::SendFlatConfig_Mod_4_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 8 ;

	Ack_SendFlatConfig_Mod_4->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[3][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[3][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[3][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[3][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[3][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[3][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[3][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_4  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[3][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_4,0x7f,ALL_CHIP,fconfig[0]) ;
			
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_4\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_4->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_4  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[3][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_4,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_4 chip_%d \033[22;30m\n",i) ;
						Ack_SendFlatConfig_Mod_4->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_4 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_4->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::SendFlatConfig_Mod_5_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 16 ;
	
	Ack_SendFlatConfig_Mod_5->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[4][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[4][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[4][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[4][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[4][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[4][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[4][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_5  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[4][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_5,0x7f,ALL_CHIP,fconfig[0]) ;
		
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_5\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_5->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_5  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[4][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_5,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_5 chip_%d \033[22;30m\n",i) ;
						Ack_SendFlatConfig_Mod_5->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_5 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_5->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::SendFlatConfig_Mod_6_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 32 ;
	
	Ack_SendFlatConfig_Mod_6->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[5][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[5][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[5][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[5][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[5][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[5][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[5][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_6  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[5][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_6,0x7f,ALL_CHIP,fconfig[0]) ;
			
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_6\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_6->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_6  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[5][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_6,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_6 chip_%d \033[22;30m\n",i) ;
						Ack_SendFlatConfig_Mod_6->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_6 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_6->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::SendFlatConfig_Mod_7_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 64 ;
	
	Ack_SendFlatConfig_Mod_7->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[6][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[6][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[6][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[6][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[6][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[6][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[6][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_7  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[6][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_7,0x7f,ALL_CHIP,fconfig[0]) ;
			
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_7\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_7->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_7  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[6][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_7,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_7 chip_%d \033[22;30m\n",i) ;
						Ack_SendFlatConfig_Mod_7->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_7 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_7->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::SendFlatConfig_Mod_8_Slot()
{
int fconfig[8] ;
int i ;
bool ok ;
int status = 0 ;
int module_msk = 128 ;
	
	Ack_SendFlatConfig_Mod_8->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
	MainGUI::FlatConfig_Valid_Slot() ;
	fconfig[0] = FlatConfig_tab[7][0].toInt(&ok,16) ;
	fconfig[1] = FlatConfig_tab[7][1].toInt(&ok,16) ;
	fconfig[2] = FlatConfig_tab[7][2].toInt(&ok,16) ;
	fconfig[3] = FlatConfig_tab[7][3].toInt(&ok,16) ;
	fconfig[4] = FlatConfig_tab[7][4].toInt(&ok,16) ;
	fconfig[5] = FlatConfig_tab[7][5].toInt(&ok,16) ;
	fconfig[6] = FlatConfig_tab[7][6].toInt(&ok,16) ;
	if ((fconfig[0] == fconfig[1]) and (fconfig[1] == fconfig[2])and (fconfig[2] == fconfig[3])and (fconfig[3] == fconfig[4])and (fconfig[4] == fconfig[5])and (fconfig[5] == fconfig[6]))
		{
		DAQ_MSG("Flat Config Module_8  all chips => 0x%s\033[22;30m\n", FlatConfig_tab[7][0].latin1()) ;
		
		if (FAST_READOUT)
			FlatConfig(qusb,module_msk,0x7f,ALL_CHIP,fconfig[0]) ;
		else
			FlatConfig(qusb,MODULE_8,0x7f,ALL_CHIP,fconfig[0]) ;
			
  		status = WaitingForHandShakeFlatConfig(qusb)  ;
  		if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_8\033[22;30m\n") ;
					Ack_SendFlatConfig_Mod_8->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
		}
	else
		{
		for (i=0;i<MAXCHIP;i++)
			{
			DAQ_MSG("Flat Config Module_8  chip_%d => %s\033[22;30m\n",(i+1),FlatConfig_tab[7][i].latin1()) ;
			if (FAST_READOUT)
					FlatConfig(qusb,module_msk,(i+1),ONE_CHIP,fconfig[i])  ;
				else
					FlatConfig(qusb,MODULE_8,(i+1),ONE_CHIP,fconfig[i])  ;
			status = WaitingForHandShakeFlatConfig(qusb) ;
  			if (status <0) {DAQ_MSG("\033[22;31mERROR : unable to receive the HandShake of FlatConfig Module_8 chip_%d \033[22;30m\n",i) ;
						Ack_SendFlatConfig_Mod_8->setPaletteBackgroundColor( QColor(255, 0, 0 ) );return ;}  
     			
			}
		}	
	DAQ_MSG("Flat Config Module_8 complete\033[22;30m\n" ) ;		
	Ack_SendFlatConfig_Mod_8->setPaletteBackgroundColor( QColor(0, 255, 0 ) );
}


void MainGUI::FlatConfig_SendAll_Slot()
{

	if(ModuleReady_tab[0] == 1) 
		MainGUI::SendFlatConfig_Mod_1_Slot() ;
	//if(NbofModules_spinBox->value() == 1) return ;
	if(ModuleReady_tab[1] == 1) 
		MainGUI::SendFlatConfig_Mod_2_Slot();
	//if(NbofModules_spinBox->value() == 2) return ;
	if(ModuleReady_tab[2] == 1) 
		MainGUI::SendFlatConfig_Mod_3_Slot();
	//if(NbofModules_spinBox->value() == 3) return ;
	if(ModuleReady_tab[3] == 1) 
		MainGUI::SendFlatConfig_Mod_4_Slot();
	//if(NbofModules_spinBox->value() == 4) return ;
	if(ModuleReady_tab[4] == 1) 
		MainGUI::SendFlatConfig_Mod_5_Slot();
	//if(NbofModules_spinBox->value() == 5) return ;
	if(ModuleReady_tab[5] == 1) 
		MainGUI::SendFlatConfig_Mod_6_Slot();
	//if(NbofModules_spinBox->value() == 6) return ;
	if(ModuleReady_tab[6] == 1) 
		MainGUI::SendFlatConfig_Mod_7_Slot();
	//if(NbofModules_spinBox->value() == 7) return ;
	if(ModuleReady_tab[7] == 1) 
		MainGUI::SendFlatConfig_Mod_8_Slot();
}


/***********************************************/
/*									       */
/*			CALIBRATION PART		       */
/*									       */	
/***********************************************/


void MainGUI::Calibration_Slot()
{
#define COMBOBOX_LOCAL_NOISE_CALIB_FROM_ZERO	6
#define COMBOBOX_LOCAL_NOISE_CALIB_FROM_MAXCNT	7
#define COMBOBOX_LOCAL_NOISE_CALIB_FROM_FIRST_NOISY	8
#define COMBOBOX_LOCAL_CALIB_BEAM			9
#define MESSAGE_NB 						7
//ITHLMin_SpinBox
//ITHLMax_SpinBox
//DACLMax_SpinBox
//DACLMin_SpinBox
int datar_length = 0  ;
int i ,k ;
int datar[1024] ;
int calib_ended[8] ;
int local_ith[8] ;
int start_val ;
int end_val ;
QString FileSelected ;
char folder_tmp[500] ;
char suffix_file[50] ;
char sys_command[500] ;
int MaxModuleReady = 1 ;
int walgo = 0 ;
int dec_dacl_val ;


			//DACLMin_SpinBox=> ith_search
			//DACLMax_SpinBox =>custom_2
			//CustomParameter_SpinBox =>custom_1
			/*
			MainGUI::Reset_ITHL_Values_Slot() ;
			DAQ_MSG("Config G\033[22;30m\n" ) ;
			MainGUI::ConfigG_Slot() ;
			DAQ_MSG("Flat Config\033[22;30m\n" ) ;
			MainGUI::FlatConfig_Slot() ;
			i = 0 ;
			*/
			
			// RESET CALIBRATION PROGRESS_BAR 
			Calib_ProgressBar_1->reset(); QApplication::flush() ;
			Calib_ProgressBar_2->reset(); QApplication::flush() ;
			Calib_ProgressBar_3->reset(); QApplication::flush() ;
			Calib_ProgressBar_4->reset(); QApplication::flush() ;
			Calib_ProgressBar_5->reset(); QApplication::flush() ;
			Calib_ProgressBar_6->reset(); QApplication::flush() ;
			Calib_ProgressBar_7->reset(); QApplication::flush() ;
			Calib_ProgressBar_8->reset(); QApplication::flush() ;
			
			// Specify the folder where files will be stored
			if ((CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_ZERO)  | (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_MAXCNT) | (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_FIRST_NOISY) | (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_CALIB_BEAM))
				kindoffile = DATA_FILE ;
			else
				kindoffile = CFG_FILE ;
				
			MainGUI::SetWriteFile() ;
			if (QStrFileSelected.latin1() == NULL) return ;
			DAQ_MSG("%s\033[22;30m\n",(const char *) QStrFileSelected) ;	
			std::sprintf(folder_tmp,"%s",(const char *) QStrFileSelected) ;
			ExtractSuffix(folder_tmp,suffix_file) ;
			std::sprintf(sys_command,"mkdir %s",(const char *) QStrFileSelected) ;
			system(sys_command) ;


/*
			// CHECK CALIBRATION FILE 
			for (int i =0;i<NbofModules_spinBox->value();i++)
				{
				if (strcmp(CalibFile[i].c_str(),"") == 0) 
					{
					DAQ_MSG("Please specify a calib filename for the module_%d\033[22;30m\n",(i+1)) ;
					DAQ_MSG("Calibration aborted\033[22;30m\n") ;
					return ;
					}
				}	
*/			

			k=0 ;
			for (i=0;i<8;i++)  
				{
				calib_ended[i] = 1 ;
				local_ith[i] = 0 ;
				}
			/*
			if (CalibModID_ComboBox->currentItem() == ALL_MODULES)  
				{
				start_val = 0 ;
				end_val = MaxModuleReady ;
				}
			else
				{
				if (CalibModID_ComboBox->currentItem() > MaxModuleReady) 
					{
					printf("\033[22;31mERROR => Module_%d not avaliable\033[22;30m\n",CalibModID_ComboBox->currentItem() ) ;
					return ;
					}
				else 
					{
					start_val = (CalibModID_ComboBox->currentItem() - 1) ;
					end_val = CalibModID_ComboBox->currentItem()  ;
					//printf("start_val = %d ; end_val = %d\n",start_val,end_val) ;
					}
				}
			*/
			start_val = 0 ;
			end_val = MaxModuleReady ;
			
			if ((CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_ZERO)  | (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_MAXCNT) | (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_FIRST_NOISY)  | (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_CALIB_BEAM))
				{
				//MainGUI::ConfigG_SetDefaultValue_Slot() ;
				MainGUI::SendConfig_Mod_All_Slot() ;
				if (SkipITHSearch_SpinBox->value() == 0)
					{
					MainGUI::FlatConfig_SendAll_Slot() ;
					DAQ_MSG("Begin Local ITH calibration\033[22;30m\n") ;
					Local_ITHL_Calibration(qusb,ModuleReady_tab,ITHLMin_SpinBox->value(),ITHLMax_SpinBox->value(),GateCalib_SpinBox->value(),QStrFileSelected.toStdString(),mClone,FAST_READOUT) ;
					}

				DAQ_MSG("Begin Local DACL calibration\033[22;30m\n") ;
				if (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_ZERO) walgo = LOCAL_NOISE_CALIB_FROM_ZERO ;
				if (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_MAXCNT) walgo = LOCAL_NOISE_CALIB_FROM_MAXCNT ;
				if (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_NOISE_CALIB_FROM_FIRST_NOISY) walgo = LOCAL_NOISE_CALIB_FROM_FIRST_NOISY;
				if (CalibType_ComboBox->currentItem() == COMBOBOX_LOCAL_CALIB_BEAM) walgo = LOCAL_BEAM_CALIB ;

				dec_dacl_val = decVal_SpinBox->value() ;
				if (CalibFrom->isChecked()== 0)
					{
					///      A RESTAURER Local_DACL_Calibration	///
					Local_DACL_Calibration(qusb,ModuleReady_tab,walgo,GateCalib_SpinBox->value(),QStrFileSelected.toStdString(),mClone,NoiseAccepted_SpinBox->value(),FAST_READOUT,dec_dacl_val) ;
					///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					//Debug_DACL_Evolution(qusb,ModuleReady_tab,walgo,GateCalib_SpinBox->value(),QStrFileSelected,mClone,NoiseAccepted_SpinBox->value(),FAST_READOUT) ;
					}
				else 
					{
					kindoffile = SCAN_FILE ;
					MainGUI::SetReadFile() ;
					Local_DACL_Calibration(qusb,ModuleReady_tab,walgo,GateCalib_SpinBox->value(),QStrFileSelected.toStdString(),mClone,NoiseAccepted_SpinBox->value(),FAST_READOUT,dec_dacl_val) ;
					}
					
				}

			return ;
				
			for (i=start_val;i<end_val;i++)
				{
				if (ModuleReady_tab[i] == 1)
				{
				WMODULE = i+1 ;
				calib_ended[i] = 0 ;
				DAQ_MSG("Make Calibration Module_%d\033[22;30m\n",WMODULE ) ;
				 MakeCalibration(qusb,WMODULE,ITHLMin_SpinBox->value(),ITHLMax_SpinBox->value(),DACLMin_SpinBox->value(),DACLMax_SpinBox->value(),CalibType_ComboBox->currentItem(),GateCalib_SpinBox->value(),NbPixelNAccepted_SpinBox->value(),SkipITHSearch_SpinBox->value())  ;
				 }
				 }
				 

			while ((calib_ended[0] == 0) | (calib_ended[1] == 0) | (calib_ended[2] == 0) | (calib_ended[3] == 0) | (calib_ended[4] == 0) | (calib_ended[5] == 0) | (calib_ended[6] == 0) | (calib_ended[7] == 0) ) // 0x2105
				{
				datar_length = ReadFromModule(qusb,datar,10000,4) ;	
				if (datar_length > 0)
					{
					handShake_val = ExtractHandShakeWord(datar,datar_length,&addr_return,local_ith) ;
					switch (handShake_val)
						{
						case 0x1105 :
							DAQ_MSG("HandShake Calib_Command Module_%d\033[22;30m\n",addr_return) ;
							MainGUI::AffectCalibControl_Slot() ;
							QApplication::flush() ;
						break ;
						case 0xf001 :
							DAQ_MSG("Calibration STEP_1 Module_%d ended\033[22;30m\n",addr_return) ;
							MainGUI::AffectCalibControl_Slot() ;
							QApplication::flush() ;
						break ;
			   			case 0xf002 :
							printf("ith_return Module_%d => ",addr_return) ;
							for (int k=0;k<7;k++)
								{
								ith_return[(addr_return-1)][k] = local_ith[k] ;
								printf("%d ",ith_return[(addr_return-1)][k]) ;
								}
							printf("\n") ;
							DAQ_MSG("Calibration STEP_2 Module_%d ended\033[22;30m\n",addr_return) ;
							MainGUI::AffectCalibControl_Slot() ;
							QApplication::flush() ;
						break ;
						case 0xf003 :
							DAQ_MSG("Calibration STEP_3 Module_%d ended\033[22;30m\n",addr_return) ;
							MainGUI::AffectCalibControl_Slot() ;
							QApplication::flush() ;
						break ;
						case 0xf004 :
							DAQ_MSG("Calibration  STEP_4 Module_%d ended\033[22;30m\n",addr_return) ;
							MainGUI::AffectCalibControl_Slot() ;
							QApplication::flush() ;
						break ;
						case CALIB_COMPLETE : 
							calib_ended[(addr_return-1)] = 1 ;
							//DAQ_MSG("Calibration complete Module %d\033[22;30m\n",addr_return) ;	
							DAQ_MSG("Calibration complete Module %d => ",addr_return) ;	
							for (i=0;i<8;i++)
								printf("%d ",calib_ended[i]) ;
							printf("\n") ;
							MainGUI::AffectCalibControl_Slot() ;
							QApplication::flush() ;
							/*
							printf ("Calib_ended_tab => ") ;
							for (i=0;i<8;i++)
								printf("%d ",calib_ended[i]) ;
							printf("\n") ;
							*/
						break ;
						}
					}
				}

			DAQ_MSG("Start download calibration\033[22;30m\n" ) ;	
			for (int pmodule =0;pmodule<MaxModuleReady;pmodule++)
				{
				if (ModuleReady_tab[pmodule] == 1)
					{
					WMODULE = pmodule +1 ;
					module_msk = 1 <<  pmodule ;
					QTextOStream (&FileSelected) << QStrFileSelected << "/" << suffix_file << "_"<<WMODULE <<".dacs";	
					if (FAST_READOUT)
						{
						//if (ReadCalib(qusb,READ_CALIB,FileSelected,module_msk,cmos_dis,ithh_val,vadj_val,vref_val,imfp_val,iota_val,ipre_val,ithl_val,itune_val,ibuffer_val) == 0)
							DAQ_MSG("Download calibration file module_msk %d complete\033[22;30m\n",module_msk) ;
						}
					else
						{
						//if (ReadCalib(qusb,READ_CALIB,FileSelected,WMODULE,cmos_dis,ithh_val,vadj_val,vref_val,imfp_val,iota_val,ipre_val,ithl_val,itune_val,ibuffer_val) == 0)
							DAQ_MSG("Download calibration file module %d complete\033[22;30m\n",WMODULE) ;
						}
				}
				}
			
			DAQ_MSG("Download calibration ended\033[22;30m\n" ) ;
	
			/*	
			cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
			CalibFile[0] = QStrFileSelected.latin1() ;
			DAQ_MSG("Start download calibration\033[22;30m\n" ) ;	
			for (int i =0;i<NbofModules_spinBox->value();i++)
				{
				WMODULE = i + 1 ;
				if (ReadCalib(qusb,READ_CALIB,CalibFile[i] ,WMODULE,cmos_dis,ithh_val,vadj_val,vref_val,imfp_val,iota_val,ipre_val,ithl_val,itune_val,ibuffer_val) == 0)
					DAQ_MSG("Download calibration file module %d complete\033[22;30m\n",WMODULE) ;
				}
			DAQ_MSG("Download calibration ended\033[22;30m\n" ) ;
			*/
			
#undef COMBOBOX_LOCAL_CALIB_NOISE_1	
#undef COMBOBOX_LOCAL_CALIB_NOISE_2
#undef COMBOBOX_LOCAL_CALIB_BEAM	
#undef MESSAGE_NB 
}



void MainGUI::Reset_ITHL_Values_Slot()
{

	ITHL_1_1->setText("0") ;
	ITHL_1_2->setText("0") ;
	ITHL_1_3->setText("0") ;
	ITHL_1_4->setText("0") ;
	ITHL_1_5->setText("0") ;
	ITHL_1_6->setText("0") ;
	ITHL_1_7->setText("0") ;
	
	ITHL_2_1->setText("0") ;
	ITHL_2_2->setText("0") ;
	ITHL_2_3->setText("0") ;
	ITHL_2_4->setText("0") ;
	ITHL_2_5->setText("0") ;
	ITHL_2_6->setText("0") ;
	ITHL_2_7->setText("0") ;

	ITHL_3_1->setText("0") ;
	ITHL_3_2->setText("0") ;
	ITHL_3_3->setText("0") ;
	ITHL_3_4->setText("0") ;
	ITHL_3_5->setText("0") ;
	ITHL_3_6->setText("0") ;
	ITHL_3_7->setText("0") ;

	ITHL_4_1->setText("0") ;
	ITHL_4_2->setText("0") ;
	ITHL_4_3->setText("0") ;
	ITHL_4_4->setText("0") ;
	ITHL_4_5->setText("0") ;
	ITHL_4_6->setText("0") ;
	ITHL_4_7->setText("0") ;
	
	ITHL_5_1->setText("0") ;
	ITHL_5_2->setText("0") ;
	ITHL_5_3->setText("0") ;
	ITHL_5_4->setText("0") ;
	ITHL_5_5->setText("0") ;
	ITHL_5_6->setText("0") ;
	ITHL_5_7->setText("0") ;
	
	ITHL_6_1->setText("0") ;
	ITHL_6_2->setText("0") ;
	ITHL_6_3->setText("0") ;
	ITHL_6_4->setText("0") ;
	ITHL_6_5->setText("0") ;
	ITHL_6_6->setText("0") ;
	ITHL_6_7->setText("0") ;
	
	ITHL_7_1->setText("0") ;
	ITHL_7_2->setText("0") ;
	ITHL_7_3->setText("0") ;
	ITHL_7_4->setText("0") ;
	ITHL_7_5->setText("0") ;
	ITHL_7_6->setText("0") ;
	ITHL_7_7->setText("0") ;
	
	ITHL_8_1->setText("0") ;
	ITHL_8_2->setText("0") ;
	ITHL_8_3->setText("0") ;
	ITHL_8_4->setText("0") ;
	ITHL_8_5->setText("0") ;
	ITHL_8_6->setText("0") ;
	ITHL_8_7->setText("0") ;

}



void MainGUI::Calib_File_Mod_1_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	if (QStrFileSelected.latin1() == NULL) return ;
	QStrFileSelected += ".dacs" ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[0] = QStrFileSelected.latin1() ;

}


void MainGUI::Calib_File_Mod_2_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[1] = QStrFileSelected.latin1() ;
	
}


void MainGUI::Calib_File_Mod_3_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[2] = QStrFileSelected.latin1() ;

}


void MainGUI::Calib_File_Mod_4_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[3] = QStrFileSelected.latin1() ;
	
}


void MainGUI::Calib_File_Mod_5_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[4] = QStrFileSelected.latin1() ;

}


void MainGUI::Calib_File_Mod_6_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[5] = QStrFileSelected.latin1() ;

}


void MainGUI::Calib_File_Mod_7_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[6] = QStrFileSelected.latin1() ;

}


void MainGUI::Calib_File_Mod_8_Slot()
{

	kindoffile = CFG_FILE ;
	MainGUI::SetWriteFile() ;
	QStrFileSelected += ".dacs" ;
	if (QStrFileSelected.latin1() == NULL) return ;
	cout << "QStrFileSelected  = " << QStrFileSelected.latin1()  << endl ;
	CalibFile[7] = QStrFileSelected.latin1() ;

}


void MainGUI::Check_Calib_File_Slot()
{
char tmp_message[500] ;
QMessageBox *mbox = new QMessageBox() ;

		std::sprintf(message,"Calib file Module 1 => %s\n",CalibFile[0].c_str() ) ;
		std::sprintf(tmp_message,"Calib file Module 2 => %s\n",CalibFile[1].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Calib file Module 3 => %s\n",CalibFile[2].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Calib file Module 4 => %s\n",CalibFile[3].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Calib file Module 5 => %s\n",CalibFile[4].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Calib file Module 6 => %s\n",CalibFile[5].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Calib file Module 7 => %s\n",CalibFile[6].c_str() ) ;
		std::strcat(message,tmp_message) ;
		std::sprintf(tmp_message,"Calib file Module 8 => %s\n",CalibFile[7].c_str() ) ;
		std::strcat(message,tmp_message) ;
		mbox->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
		mbox->setPaletteBackgroundColor( QColor( 255, 0, 0 ) );
		mbox->information(this, tr("CALIB FILES SELECTED"), tr(message));
}


void MainGUI::Reset_Calib_Files_Slot()
{
int i ;

	for (i=0;i<MAXMODULE;i++)
		CalibFile[i] = "";
}





void MainGUI::DownloadCalib_Slot()
{
//QString suffix_file ;
char folder_tmp[500] ;
char suffix_file[50] ;
char sys_command[500] ;

string sfileName ;
int status ;

 
   			DAQ_MSG("Download calibration files\033[22;30m\n" ) ;   



			TH2F *h2 = new TH2F("h2","",(MAXCOL*MAXCHIP),0,(MAXCOL*MAXCHIP),(ROW_NB*MaxModuleReady),0,(ROW_NB*MaxModuleReady)) ;
			/*
			kindoffile = CFG_FILE ;
			MainGUI::SetWriteFile() ;
			if (QStrFileSelected.latin1() == NULL) return ;
			DAQ_MSG("%s\033[22;30m\n",(const char *) QStrFileSelected) ;	
			std::sprintf(folder_tmp,"%s",(const char *) QStrFileSelected) ;
			ExtractSuffix(folder_tmp,suffix_file) ;
			std::sprintf(sys_command,"mkdir %s",(const char *) QStrFileSelected) ;
			system(sys_command) ;			ResetMatrix(dmatrix) ;
			
			for (int i =0;i<MaxModuleReady;i++)
				{
				if (ModuleReady_tab[i] == 1)
				{
				WMODULE = i +1 ;
				//QTextOStream (&FileSelected) << QStrFileSelected << "/" << suffix_file << "_"<<WMODULE <<".dacs";	
				//sprintf(FileSelected,"%s/%s_%d.dacs",(const char *) QStrFileSelected,(const char *)  suffix_file,WMODULE) ;
				std::sprintf(PathAndfileName,"%s/%s_%d.dacs",(const char *) QStrFileSelected,(const char *)  suffix_file,WMODULE) ;
				//FileSelected = string(file) ;	
				if (ReadCalib(qusb,READ_CALIB,PathAndfileName,WMODULE,cmos_dis,ithh_val,vadj_val,vref_val,imfp_val,iota_val,ipre_val,ithl_val,itune_val,ibuffer_val) == 0)
					DAQ_MSG("Download calibration file module %d complete\033[22;30m\n",WMODULE) ;
				}
				}
			*/
			ResetMatrix(dmatrix) ;
			ResetMatrix(dacl_matrix) ;
			status = FastReadImageAndFillMatrix(qusb,MODE_READ_DACL,module_msk,dmatrix,INDEX_0) ;
			ExtractDACLValue(dmatrix,dacl_matrix) ;
			RootHistoHitConfigFillPlotMatrix(mClone,h2,dacl_matrix,sfileName) ;
		
			DAQ_MSG("Download calibration ended\033[22;30m\n" ) ;

}



void MainGUI::LoadCalibration_Slot()
{

int cnt_line = 0 ; 
int dataTemp ;
int dataTab[100] ;
string line ;
int start_read = -1 ;
/*
	DAQ_MSG("Config G\033[22;30m\n" ) ;
	MainGUI::ConfigG_Slot() ;
	DAQ_MSG("Flat Config\033[22;30m\n" ) ;
	MainGUI::FlatConfig_Slot() ;
	LoadCalib_Button->setEnabled(FALSE) ;
*/

	kindoffile = CFG_FILE ;
	MainGUI::SetReadFile() ;
	if (CalibModID_ComboBox->currentItem() == ALL_MODULES) return ;
        DAQ_MSG("Module_%d Load Calib file => %s\033[22;30m\n",CalibModID_ComboBox->currentItem(),(const char *) QStrFileSelected ) ;
	ifstream myFile( QStrFileSelected.latin1()) ;
	while (getline(myFile,line))
		{
		istrstream linestream(line.c_str()) ;
	   	if (start_read < 0) 
	   		{
	   		if (strcmp(line.c_str(),"DACL") == 0) start_read = 0 ;
			if (strcmp(line.c_str(),"DACL ") == 0) start_read = 0 ;
			if (strcmp(line.c_str(),"DACL  ") == 0) start_read = 0 ;
	   		}
	 	else
	 		{
			start_read++ ;
			}
	 	//printf("%d : %s \n",start_read,line.c_str()) ;
	  	if (start_read > 0) 
	   		{		
			for (int i=0;i<MAXDATALINE;i++)
				{
				linestream >> dataTemp ; 
				if ((i >0) && ( i<(MAXCOL+3)))
					dataTab[i] = dataTemp ;
				}
			dataTab[0] = LOAD_CALIB ;
		
  			//WriteWordsToModule(qusb,CalibModID_ComboBox->currentItem() ,NULL_PARAM,dataTab,(MAXCOL+3))  ;
			WriteWordsToModule(qusb,CalibModID_ComboBox->currentItem() ,NULL_PARAM,dataTab,(MAXCOL+3))  ;
  			///cout << cnt_line << ": Load calibration " << endl ; 
  			if (cnt_line == 839)
  				 WaitingForHandShakeMessage(qusb,LOAD_CALIB_ENDED) ;
  			else
   				{
   				WaitingForHandShakeMessage(qusb,CONTINUE) ;
   				cnt_line++ ;
   				}
  			}
		}
 	myFile.close() ;
 	DAQ_MSG("Load calibration ended\033[22;30m\n" ) ;
 	LoadCalib_Button->setEnabled(TRUE) ;
}



void MainGUI::PlotCalibration_Slot()
{
char fileName[500] ;
string pFile ;
string pPath ;
string pPrefix  ;
int pIndex ;

	

	int ChipID = ChipPlotCalib_spinBox->value() ;
	if (ChipID != 0)
		{
		kindoffile = CFG_FILE ;
		MainGUI::SetReadFile() ;
		if (QStrFileSelected == NULL) return ;
		DAQ_MSG("Plot Calib ChipID = %d\n",ChipID) ;
		DAQ_MSG(" FileSelected => %s\n", QStrFileSelected.latin1()) ;;
		ExtractFileName(QStrFileSelected,&pFile) ;
		std::sprintf(fileName,"%s",pFile.c_str()) ;
		//mCanv->show() ;
		TH2F *h2 = new TH2F("h2","",80,0,80,120,0,120) ;
        	ExtractPathPrefixIndex(QStrFileSelected,&pPath,&pPrefix,&pIndex) ;
		RootConfigHistoDACL(mClone,h1,fileName,ChipID) ;
		RootFillHistoDACL(h1,h2,QStrFileSelected.latin1(),0,ChipID) ;
 		RootPlotHistoDACL(mClone,h1,h2)  ;
		if (DELETE_H2)	
			delete(h2) ;
		}
	else 
		{
		kindoffile = CFG_FILE ;
		MainGUI::SetReadFile() ;
		if (QStrFileSelected == NULL) return ;
		DAQ_MSG("Plot Calib ChipID = %d\n",ChipID) ;
		DAQ_MSG(" FileSelected => %s\n", QStrFileSelected.latin1()) ;;
		ExtractFileName(QStrFileSelected,&pFile) ;
		std::sprintf(fileName,"%s",pFile.c_str()) ;
		//mCanv->show() ;
		TH2F *h2 = new TH2F("h2","",80*7,0,80*7,120,0,120) ;
        	ExtractPathPrefixIndex(QStrFileSelected,&pPath,&pPrefix,&pIndex) ;
		RootConfigHistoDACLAllChips(mClone) ;
		RootFillHistoDACLAllChips(h2,QStrFileSelected.latin1()) ;
 		RootPlotHistoDACLAllChips(mClone,h2)  ;
		if (DELETE_H2)		
			delete(h2) ;
		}

}


void MainGUI::ChipPlotCalib_Slot()
{
char fileName[500] ;
string pFile ;
string pPath ;
string pPrefix  ;
int pIndex ;

	int ChipID = ChipPlotCalib_spinBox->value() ;
	DAQ_MSG("ChipID = %d; FileSelected => %s\n", ChipID,QStrFileSelected.latin1()) ;
	ExtractFileName(QStrFileSelected,&pFile) ;
	std::sprintf(fileName,"%s",pFile.c_str()) ;
	TH2F *h2 = new TH2F("h2","",80,0,80,120,0,120) ;
        ExtractPathPrefixIndex(QStrFileSelected,&pPath,&pPrefix,&pIndex) ;
	RootConfigHistoDACL(mClone,h1,fileName,ChipID) ;
	RootFillHistoDACL(h1,h2,QStrFileSelected.latin1(),0,ChipID) ;
	RootPlotHistoDACL(mClone,h1,h2)  ;
	if (DELETE_H2)	
		delete(h2) ;

}



void MainGUI::AffectCalibControl_Slot()
{
 
 QString value ;


					//printf("AffectCalibControl_Slot : handShake_val = 0x%x ; addr_return = %d\n",handShake_val,addr_return) ;
					
					switch (handShake_val)
							{
							case 0xf001 :
								if (addr_return == 1) { Calib_ProgressBar_1->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 2) { Calib_ProgressBar_2->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 3) { Calib_ProgressBar_3->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 4) { Calib_ProgressBar_4->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 5) { Calib_ProgressBar_5->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 6) { Calib_ProgressBar_6->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 7) { Calib_ProgressBar_7->setProgress(1,5) ; QApplication::flush() ; break ;}
								if (addr_return == 8) { Calib_ProgressBar_8->setProgress(1,5) ; QApplication::flush() ; break ;}
							break ;
			   				case 0xf002 :
								if (addr_return == 1) 
									{ 
									Calib_ProgressBar_1->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[0][0]) ;
									ITHL_1_1->setText(value); QApplication::flush() ;
									value = QString("%1").arg(ith_return[0][1]) ;
									ITHL_1_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[0][2]) ;
									ITHL_1_3->setText(value)  ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[0][3]) ;
									ITHL_1_4->setText(value)  ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[0][4]) ;
									ITHL_1_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[0][5]) ;
									ITHL_1_6->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[0][6]) ;
									ITHL_1_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[0][wchip] = ith_return[0][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 2) 
									{ 
									Calib_ProgressBar_2->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[1][0]) ;
									ITHL_2_1->setText(value); QApplication::flush() ; 
									value = QString("%1").arg(ith_return[1][1]) ;
									ITHL_2_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[1][2]) ;
									ITHL_2_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[1][3]) ;
									ITHL_2_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[1][4]) ;
									ITHL_2_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[1][5]) ;
									ITHL_2_6->setText(value) ; QApplication::flush() ;
									value = QString("%1").arg(ith_return[1][6]) ;
									ITHL_2_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[1][wchip] = ith_return[1][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 3) 
									{ 
									Calib_ProgressBar_3->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[2][0]) ;
									ITHL_3_1->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[2][1]) ;
									ITHL_3_2->setText(value); QApplication::flush() ; 
									value = QString("%1").arg(ith_return[2][2]) ;
									ITHL_3_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[2][3]) ;
									ITHL_3_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[2][4]) ;
									ITHL_3_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[2][5]) ;
									ITHL_3_6->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[2][6]) ;
									ITHL_3_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[2][wchip] = ith_return[2][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 4) 
									{ 
									Calib_ProgressBar_4->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[3][0]) ;
									ITHL_4_1->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[3][1]) ;
									ITHL_4_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[3][2]) ;
									ITHL_4_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[3][3]) ;
									ITHL_4_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[3][4]) ;
									ITHL_4_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[3][5]) ;
									ITHL_4_6->setText(value); QApplication::flush() ; 
									value = QString("%1").arg(ith_return[3][6]) ;
									ITHL_4_7->setText(value) ; QApplication::flush() ;
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[3][wchip] = ith_return[3][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 5) 
									{ 
									Calib_ProgressBar_5->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[4][0]) ;
									ITHL_5_1->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[4][1]) ;
									ITHL_5_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[4][2]) ;
									ITHL_5_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[4][3]) ;
									ITHL_5_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[4][4]) ;
									ITHL_5_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[4][5]) ;
									ITHL_5_6->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[4][6]) ;
									ITHL_5_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[4][wchip] = ith_return[4][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 6) 
									{ 
									Calib_ProgressBar_6->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[5][0]) ;
									ITHL_6_1->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[5][1]) ;
									ITHL_6_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[5][2]) ;
									ITHL_6_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[5][3]) ;
									ITHL_6_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[5][4]) ;
									ITHL_6_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[5][5]) ;
									ITHL_6_6->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[5][6]) ;
									ITHL_6_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[5][wchip] = ith_return[5][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 7) 
									{ 
									Calib_ProgressBar_7->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[6][0]) ;
									ITHL_7_1->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[6][1]) ;
									ITHL_7_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[6][2]) ;
									ITHL_7_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[6][3]) ;
									ITHL_7_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[6][4]) ;
									ITHL_7_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[6][5]) ;
									ITHL_7_6->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[6][6]) ;
									ITHL_7_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[6][wchip] = ith_return[6][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
								if (addr_return == 8) 
									{ 
									Calib_ProgressBar_8->setProgress(2,5) ; 
									value = QString("%1").arg(ith_return[7][0]) ;
									ITHL_8_1->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[7][1]) ;
									ITHL_8_2->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[7][2]) ;
									ITHL_8_3->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[7][3]) ;
									ITHL_8_4->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[7][4]) ;
									ITHL_8_5->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[7][5]) ;
									ITHL_8_6->setText(value) ; QApplication::flush() ; 
									value = QString("%1").arg(ith_return[7][6]) ;
									ITHL_8_7->setText(value) ; QApplication::flush() ; 
									for (int wchip=0;wchip<MAXCHIP;wchip++)
										ithl_val[7][wchip] = ith_return[7][wchip] ;
									MainGUI::SelectCurrent_Slot() ;
									break ;
									}
							break ;
							case 0xf003 :
								if (addr_return == 1) { Calib_ProgressBar_1->setProgress(3,5) ; QApplication::flush() ; break ;}
								if (addr_return == 2) { Calib_ProgressBar_2->setProgress(3,5) ; QApplication::flush() ; break ;}
								if (addr_return == 3) { Calib_ProgressBar_3->setProgress(3,5) ; QApplication::flush() ; break ;}
								if (addr_return == 4) { Calib_ProgressBar_4->setProgress(3,5) ; QApplication::flush() ; break ;}
								if (addr_return == 5) { Calib_ProgressBar_5->setProgress(3,5) ; QApplication::flush() ; break ;}
								if (addr_return == 6) { Calib_ProgressBar_6->setProgress(3,5) ; QApplication::flush() ; break; }
								if (addr_return == 7) { Calib_ProgressBar_7->setProgress(3,5) ; QApplication::flush() ; break ;}
								if (addr_return == 8) { Calib_ProgressBar_8->setProgress(3,5) ; QApplication::flush() ; break ;}
							break ;
							case 0xf004 :
								if (addr_return == 1) { Calib_ProgressBar_1->setProgress(4,5) ; QApplication::flush() ; break ;}
								if (addr_return == 2) { Calib_ProgressBar_2->setProgress(4,5) ; QApplication::flush() ; break ;}
								if (addr_return == 3) { Calib_ProgressBar_3->setProgress(4,5) ; QApplication::flush() ; break ;}
								if (addr_return == 4) { Calib_ProgressBar_4->setProgress(4,5) ; QApplication::flush() ; break ;}
								if (addr_return == 5) { Calib_ProgressBar_5->setProgress(4,5) ;  QApplication::flush() ; break ;}
								if (addr_return == 6) { Calib_ProgressBar_6->setProgress(4,5) ; QApplication::flush() ;  break; }
								if (addr_return == 7) { Calib_ProgressBar_7->setProgress(4,5) ; QApplication::flush() ; break ;}
								if (addr_return == 8) { Calib_ProgressBar_8->setProgress(4,5) ; QApplication::flush() ; break ;}
							break ;
							case CALIB_COMPLETE : 
								if (addr_return == 1) { Calib_ProgressBar_1->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 2) { Calib_ProgressBar_2->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 3) { Calib_ProgressBar_3->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 4) { Calib_ProgressBar_4->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 5) { Calib_ProgressBar_5->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 6) { Calib_ProgressBar_6->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 7) { Calib_ProgressBar_7->setProgress(5,5) ; QApplication::flush() ; break ;}
								if (addr_return == 8) { Calib_ProgressBar_8->setProgress(5,5) ; QApplication::flush() ; break ;}
							break ;
							}

}


void MainGUI::AffectConfigGControl_Slot()
{
string line ;
string str_tmp ;


	ifstream datafile(AffectFile.c_str()) ; // open file in read mode
	if (!datafile) { 
		if (strcmp(AffectFile.c_str(),"") != 0)
			DAQ_MSG("\033[22;31mERROR Config file not found => %s\033[22;30m\n",AffectFile.c_str());
		return  ; }

	while (getline(datafile,line))
	 {
	 istrstream linestream(line.c_str()) ;
	 linestream >> str_tmp ;
	 if (strcmp(str_tmp.c_str(),"CMOS_DIS") == 0 ) {
	  	for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  cmos_dis[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"ITHH") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  ithh_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"VADJ") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  vadj_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"VREF") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  vref_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}	
	if (strcmp(str_tmp.c_str(),"IMFP") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  imfp_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"IOTA") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  iota_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"IPRE") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  ipre_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"ITHL") == 0 ) {
		for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  ithl_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"ITUNE") == 0 ) {
	  	for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  itune_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	if (strcmp(str_tmp.c_str(),"IBUFFER") == 0 ) {
	  	for (Int_t i=0;i<MAXCHIP;i++)  linestream >>  ibuffer_val[AffectFile_id][i] ; 
		MainGUI::SelectCurrent_Slot() ;
		}
	}
	
	datafile.close() ;

}



void MainGUI::ROI_Slot()
{
int x ;
int y ;
int x_min[4] ;
int x_max[4] ;
int y_min[4]  ;
int y_max[4]  ;
//int roi[4] ;  en global pour les retrouver dans les fichiers de synchro
QString value ;

		
		
		x_min[0] = ROI_XMin_1->value() ;
		x_min[1] = ROI_XMin_2->value() ;
		x_min[2] = ROI_XMin_3->value() ;
		x_min[3] = ROI_XMin_4->value() ;
		
		x_max[0] = ROI_XMax_1->value() ;
		x_max[1] = ROI_XMax_2->value() ;
		x_max[2] = ROI_XMax_3->value() ;
		x_max[3] = ROI_XMax_4->value() ;
		
		y_min[0] = ROI_YMin_1->value() ;
		y_min[1] = ROI_YMin_2->value() ;
		y_min[2] = ROI_YMin_3->value() ;
		y_min[3] = ROI_YMin_4->value() ;
		
		y_max[0] = ROI_YMax_1->value() ;
		y_max[1] = ROI_YMax_2->value() ;
		y_max[2] = ROI_YMax_3->value() ;
		y_max[3] = ROI_YMax_4->value() ;
		
		
		
		//dmatrix[y][x] ; x=80*7 ; y=120*8
		//*************  ROI  *********************
		
		for(int i=0;i<4;i++)	roi[i] = 0 ;
		
		for (int i=0;i<4;i++)
			{
			for (x=0;x<(80*7);x++)
				{
				for (y=0;y<(120*8);y++)
					{
					if ((x >= x_min[i]) &&  (x <= x_max[i])) 
						{
						if (( y >= y_min[i]) &&  (y <= y_max[i]))
							{
			 				roi[i] = roi[i] +dmatrix[y][x]  ;
							//printf("y = %d ; x = %d ; roi[%d] = %d \n",y,x,i,roi[i]) ;
							}
						}
					}
				}	
			}
			
		value = QString("%1").arg(roi[0]) ; ROI_1->setText(value); 
		value = QString("%1").arg(roi[1]) ; ROI_2->setText(value); 
		value = QString("%1").arg(roi[2]) ; ROI_3->setText(value); 
		value = QString("%1").arg(roi[3]) ; ROI_4->setText(value); 
		
		/*	
		if ((x >= x2_min) &&  (x <= x2_max)) 
			if (( y >= y2_min) &&  (y <= y2_max))
			 	roi2 = roi2 + value_fifo  ;
		if ((x >= x3_min) &&  (x <= x3_max)) 
			if (( y >= y3_min) &&  (y <= y3_max))
			 	roi3 = roi3 + value_fifo  ;
		if ((x >= x4_min) &&  (x <= x4_max)) 
			if (( y >= y4_min) &&  (y <= y4_max))
			 	roi4 = roi4 + value_fifo  ;
*/										

}


void MainGUI::Init_Slot()
{
	MainGUI::exec() ;
	Init_Button->setEnabled( FALSE );
	
}

void MainGUI::init()
{
	MainGUI::exec() ;
	Init_Button->setEnabled( FALSE );
}


void MainGUI::ConfigGValueModified_Slot()
{

	//printf("MainGUI::ConfigGValueModified_Slot()\n") ;
	MainGUI::ConfigG_ReadValue_Slot() ;
	MainGUI::ConfigG_Valid_Slot() ;

	
}


void MainGUI::ModuleReady_Slot()
{

}


void MainGUI::ValidateModule_1_Slot()
{
int module_id = 1 ;
int module_msk = 1 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M1_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 1) M1_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
				DAQ_MSG("Module_%d ready \n", module_id) ;
				ModuleReady_tab[( module_id-1)] = 1 ;
				}
			else 
				{
				M1_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\033[22;30m\n", module_id) ;
				}
			}
		for (int i =0;i<8;i++)
			{
			if (ModuleReady_tab[( module_id-1)] == 1) 
				if (ModuleReady_tab[9] < ModuleReady_tab[( module_id-1)])
					ModuleReady_tab[9] = ModuleReady_tab[( module_id-1)] ;
			}
		
		MainGUI::EnableDisableControls_Slot() ;	
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
}


void MainGUI::ValidateModule_2_Slot()
{
int module_id = 2 ;
int module_msk = 2 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M2_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 2) 
					{
					M2_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M2_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}
		for (int i =0;i<8;i++)
			{
			if (ModuleReady_tab[( module_id-1)] == 1) 
				if (ModuleReady_tab[9] < ModuleReady_tab[( module_id-1)])
					ModuleReady_tab[9] = ModuleReady_tab[( module_id-1)] ;
			}
		
		MainGUI::EnableDisableControls_Slot() ;
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
}


void MainGUI::ValidateModule_3_Slot()
{
int module_id = 3 ;
int module_msk = 4 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M3_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 3)
					{
					 M3_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M3_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}
		for (int i =0;i<8;i++)
			{
			if (ModuleReady_tab[( module_id-1)] == 1) 
				if (ModuleReady_tab[9] < ModuleReady_tab[( module_id-1)])
					ModuleReady_tab[9] = ModuleReady_tab[( module_id-1)] ;
			}

		MainGUI::EnableDisableControls_Slot() ;
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
}


void MainGUI::ValidateModule_4_Slot()
{
int module_id = 4 ;
int module_msk = 8 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M4_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 4) 
					{
					M4_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M4_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}
			
		MainGUI::EnableDisableControls_Slot() ;
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}

}


void MainGUI::ValidateModule_5_Slot()
{
int module_id = 5 ;
int module_msk = 16 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M5_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 5) 
					{
					M5_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M5_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}

			MainGUI::EnableDisableControls_Slot() ;
			MainGUI::SetAskReadyTab_Slot() ;
			return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
}

void MainGUI::ValidateModule_6_Slot()
{
int module_id = 6 ;
int module_msk = 32 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M6_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 6) 
					{
					M6_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M6_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}
		for (int i =0;i<8;i++)
			{
			if (ModuleReady_tab[( module_id-1)] == 1) 
				if (ModuleReady_tab[9] < ModuleReady_tab[( module_id-1)])
					ModuleReady_tab[9] = ModuleReady_tab[( module_id-1)] ;
			}
	
		MainGUI::EnableDisableControls_Slot() ;
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}

}

void MainGUI::ValidateModule_7_Slot()
{
int module_id = 7 ;
int module_msk = 64 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M7_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 7) 
					{
					M7_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M7_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}
		for (int i =0;i<8;i++)
			{
			if (ModuleReady_tab[( module_id-1)] == 1) 
				if (ModuleReady_tab[9] < ModuleReady_tab[( module_id-1)])
					ModuleReady_tab[9] = ModuleReady_tab[( module_id-1)] ;
			}
		
		MainGUI::EnableDisableControls_Slot() ;
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
}


void MainGUI::ValidateModule_8_Slot()
{
int module_id = 8 ;
int module_msk = 128 ;
int status = -1 ;

	DAQ_MSG("Send Command to Module_%d  => ", module_id) ;
	if (MFunction_ComboBox->currentItem() == 0)
		{
		if ((ModuleReady_tab[( module_id-1)] == 1)  | (ModuleReady_tab[( module_id-1)] == -1))
			{
			ModuleReady_tab[( module_id-1)] = 0 ; 
			DAQ_MSG("Module_%d is deactivated \n", module_id) ;
			M8_pushButton->setPaletteBackgroundColor( QColor(230, 230, 230 ) );
			}
		else
			{
			if (FAST_READOUT)
				status = AskReadyModule(qusb,module_msk,&addr_return) ;	
			else
				status = AskReadyModule(qusb,module_id,&addr_return) ;	
			//printf("i = %d status = %d addr_return = %d\n",i,status,addr_return) ;
			if (status >0 ) 
				{
				if (addr_return == 8) 
					{
					M8_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
					DAQ_MSG("Module_%d ready \n", module_id) ;
					ModuleReady_tab[( module_id-1)] = 1 ;
					}
				}
			else 
				{
				M8_pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
				ModuleReady_tab[(module_id-1)] = -1 ;
				DAQ_MSG("\033[22;31mERROR => Module_%d not avaliable\n", module_id) ;
				}
			}
			
		MainGUI::EnableDisableControls_Slot() ;
		MainGUI::SetAskReadyTab_Slot() ;
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 1)
		{
		if (FAST_READOUT)
			{
			status = ResetModule(qusb,module_msk)  ;
			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
			status =ResetModule(qusb,module_id)  ;
 			DAQ_MSG("Reset Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
		
	if (MFunction_ComboBox->currentItem() == 2)
		{
		if (FAST_READOUT)
			{
			ReconfigModule(qusb,module_msk)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_msk) ;
			}
		else
			{
 			ReconfigModule(qusb,module_id)  ;
			DAQ_MSG("Reconfigure Module_%d\033[22;30m\n",module_id) ;
			}
		return ;
		}
}


void MainGUI::ValidateModule_ALL_Slot()
{
int module_id = 0xff ;
	
	if (MFunction_ComboBox->currentItem() == 0)
		{
		MALL_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
		MainGUI::AskReadyLoop_Slot() ;
		return ;
		}
	if (MFunction_ComboBox->currentItem() == 1)
		{
		DAQ_MSG("Reset All Modules\033[22;30m\n") ;
		ResetModule(qusb,ModuleReady_tab[( module_id-1)])  ;
		return ;
		}
	if (MFunction_ComboBox->currentItem() == 2)
		{
		DAQ_MSG("Reconfigure All Modules\033[22;30m\n") ;
		ReconfigModule(qusb,ModuleReady_tab[( module_id-1)])  ;
		return ;
		}
	
}


void MainGUI::SetAskReadyTab_Slot()
{

	MaxModuleReady = 0 ;
	//Max_Answer = 0 ;
	Module_Answer = 0;
	module_msk = 0 ;
	for (int i =0;i<8;i++)
		{
		//printf("%d ",ModuleReady_tab[i]) ;
		if (ModuleReady_tab[i] == 1) 
			{
			//Max_Answer += (i+1)*power(10,i) ; 
			if (Module_Answer == 0)
				 Module_Answer = (i+1)  ;
			if (MaxModuleReady < (i+1))
				MaxModuleReady = (i+1)  ;
			module_msk += 1 <<  i ;
			}
		}
	DAQ_MSG("MaxModuleReady = %d First module answer = %d Max_Answer = %d module_msk=%d\033[22;30m\n",MaxModuleReady,Module_Answer,Max_Answer,module_msk) ;
	// printf("Max Anwser = %d \n",Max_Anwser) ;
	/*
	sprintf(message,"%d ",ModuleReady_tab[8]) ;
	NbModule_LineEdit->setText(message) ;
	
	printf("Message NbModule = ") ;
	for (int i =0;i<9;i++)
		printf("%d ",ModuleReady_tab[i]) ;
	printf("\n") ;
	*/
}




void MainGUI::MFunction_Slot()
{
QPushButton *pushButton = NULL  ;

	if (MFunction_ComboBox->currentItem() == 0)
		{
		for (int i =0;i<8;i++)
			{
			if (i == 0) pushButton = M1_pushButton ;
			if (i == 1) pushButton = M2_pushButton ;
			if (i == 2) pushButton = M3_pushButton ;
			if (i == 3) pushButton = M4_pushButton ;
			if (i == 4) pushButton = M5_pushButton ;
			if (i == 5) pushButton = M6_pushButton ;
			if (i == 6) pushButton = M7_pushButton ;
			if (i == 7) pushButton = M8_pushButton ;
			if (ModuleReady_tab[i] == 0)  pushButton->setPaletteBackgroundColor( QColor( 230,230,230)  );
			if (ModuleReady_tab[i] == 1)  pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
			if (ModuleReady_tab[i] == -1)  pushButton->setPaletteBackgroundColor( QColor( 230,42,13)  );
			}
		MALL_pushButton->setPaletteBackgroundColor( QColor(70, 230, 25  ) );
		}
	if (MFunction_ComboBox->currentItem() == 1)
		{
		 for (int i =0;i<8;i++)
			{
			if (i == 0) pushButton = M1_pushButton ;
			if (i == 1) pushButton = M2_pushButton ;
			if (i == 2) pushButton = M3_pushButton ;
			if (i == 3) pushButton = M4_pushButton ;
			if (i == 4) pushButton = M5_pushButton ;
			if (i == 5) pushButton = M6_pushButton ;
			if (i == 6) pushButton = M7_pushButton ;
			if (i == 7) pushButton = M8_pushButton ;
			pushButton->setPaletteBackgroundColor( QColor( 255,152,51)  );
			}
		MALL_pushButton->setPaletteBackgroundColor( QColor( 255,152,51)  );
		}
	if (MFunction_ComboBox->currentItem() == 2)
		{
		for (int i =0;i<8;i++)
			{
			if (i == 0) pushButton = M1_pushButton ;
			if (i == 1) pushButton = M2_pushButton ;
			if (i == 2) pushButton = M3_pushButton ;
			if (i == 3) pushButton = M4_pushButton ;
			if (i == 4) pushButton = M5_pushButton ;
			if (i == 5) pushButton = M6_pushButton ;
			if (i == 6) pushButton = M7_pushButton ;
			if (i == 7) pushButton = M8_pushButton ;
		 	pushButton->setPaletteBackgroundColor( QColor( 255,0,255)  );
			}
		MALL_pushButton->setPaletteBackgroundColor( QColor( 255,0,255)  );
		}
	
}





void MainGUI::IncrementValue_Slot()
{
int i , j ;
	
	MainGUI::Disconnect_ConfiGControl_Slot() ;
	//printf("Increment control()\n") ;
	MainGUI::ConfigG_ReadValue_Slot() ;
	for (i=0;i<8;i++)
		{
		for (j=0;j<7;j++)
			{
			ConfigG_tab[i][j]++ ;
			}
		}
	
	MainGUI::ConfigG_WriteValue_Slot() ;
	MainGUI::Connect_ConfiGControl_Slot() ;
}


void MainGUI::DecrementValue_Slot()
{
int i , j ;


	MainGUI::Disconnect_ConfiGControl_Slot() ;	
	//printf("Decrement control()\n") ;
	MainGUI::ConfigG_ReadValue_Slot() ;
	for (i=0;i<8;i++)
		{
		for (j=0;j<7;j++)
			{
			ConfigG_tab[i][j]-- ;
			}
		}
	
	MainGUI::ConfigG_WriteValue_Slot() ;
	MainGUI::Connect_ConfiGControl_Slot() ;
}


void MainGUI::MSBandLSB_Slot()
{

	if (MSB_LSB_ComboBox->currentItem() == 0) 
		{
		MSB_LSB = 0 ;
		DAQ_MSG("Switch to LSB readout\n") ;
		}
	else
		{
		MSB_LSB = 1 ;
		DAQ_MSG("Switch to MSB and LSB readout\n") ;
		}

}



void MainGUI::FastReadout_Slot()
{

	if (FastReadout_ComboBox->currentItem() == 0) 
		{
		FAST_READOUT = 0 ;
		DAQ_MSG("Switch to continue readout protocole\n") ;
		}
	else
		{
		FAST_READOUT = 1 ;
		DAQ_MSG("Switch to Fast readout\n") ;
		}


}


void MainGUI::FillEdfStructure_Slot()
{
	EdfConfigFile.HeaderID = "EH:000001:000000:000000" ;
	EdfConfigFile.DetectorIdent = "D1";
	EdfConfigFile.ByteOrder = "LowByteFirst" ;
	EdfConfigFile.DataType = "16/32bits" ;
	EdfConfigFile.Dim_X = 7 ;
	EdfConfigFile.Dim_Y = 8 ;
	sprintf(EdfConfigFile.ConfigFileNameModule1,"%s", CalibFile[0].c_str()) ; 
	sprintf(EdfConfigFile.ConfigFileNameModule2,"%s", CalibFile[1].c_str()) ;
	sprintf(EdfConfigFile.ConfigFileNameModule3,"%s", CalibFile[2].c_str()) ;
	sprintf(EdfConfigFile.ConfigFileNameModule4,"%s", CalibFile[3].c_str()) ;
	sprintf(EdfConfigFile.ConfigFileNameModule5,"%s", CalibFile[4].c_str()) ;
	sprintf(EdfConfigFile.ConfigFileNameModule6,"%s", CalibFile[5].c_str()) ;
	sprintf(EdfConfigFile.ConfigFileNameModule7,"%s", CalibFile[6].c_str()) ;
	sprintf(EdfConfigFile.ConfigFileNameModule8,"%s", CalibFile[7].c_str()) ;
	EdfConfigFile.Exposure_sec =  integration_time/1000000 ;
	EdfConfigFile.Experiment_date = "Today" ;
	EdfConfigFile.Experiment_time = "RightNow";
	
}


void MainGUI::ConfigFileMasterMode_Slot()
{
#define MST_LINE_MAX 5000
FILE *masterfile ;
char line[MST_LINE_MAX] ;
int i = 0 ;
char mod_id[50] ;
char dacs_file[5000] ;



	 kindoffile = MST_FILE ;
    	MainGUI::SetReadFile() ;
	if (QStrFileSelected == "") return ;
	masterfile = fopen((const char*) QStrFileSelected,"r") ;

	for (int i = 0; i<8;i++)
		ConfigFile[i] = "" ;

	while (fgets(line,MST_LINE_MAX,masterfile) != NULL)
		{
		sscanf(line,"%s %s",mod_id,dacs_file) ;
		if (strcmp(mod_id,"mod_1") == 0) 
			ConfigFile[0] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_2") == 0) 
			ConfigFile[1] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_3") == 0) 
			ConfigFile[2] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_4") == 0)
			ConfigFile[3] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_5") == 0) 
			ConfigFile[4] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_6") == 0) 
			ConfigFile[5] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_7") == 0) 
			ConfigFile[6] = string(dacs_file) ;
		if (strcmp(mod_id,"mod_8") == 0) 
			ConfigFile[7] = string(dacs_file) ;

		}
	fclose(masterfile) ;
	
	for (i=0;i<MAXMODULE;i++)
	    {
	    AffectFile_id = i ;
	    AffectFile =  ConfigFile[AffectFile_id] ;
	    MainGUI::AffectConfigGControl_Slot() ;
	    }
	    
#undef MST_LINE_MAX 
}
