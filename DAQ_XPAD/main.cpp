#include "TQApplication.h"
#include "TQRootApplication.h"
#include "DAQ_XPAD.h"

/**
* \mainpage DAQ PIXSCAN II
* \n 
* \n 
* \n
* \n
* \n
* \n
* \image html "/home/maracas/Applications/DAQ_OPTO_DMULTI/soft/kdev_daq/scr/images/Software_Arch.jpg"
* \n
* \n
* \n
* \section install_sec Installation of the Software
*
* \subsection step1  Install ROOT 
*  1 - Download Root from the address ftp://root.cern.ch/root/root_v5.20.00.source.tar.gz  \n

*  2 - Extract the gunzip file in /usr/local then type \n
*  $ gunzip   root_v5.20.00.source.tar.gz \n
*  $ tar xvf  root_v5.20.00.source.tar \n

*  3 - In the .bashrc write \n
* ROOTSYS=/usr/local/root \n
* PATH=\$PATH:\$ROOTSYS/bin \n
* LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH \n

*  4 -Configure Root before compilation in the folder "/usr/local/root" excecute "./configure --enable-qtgsi" \n
* Some files will be created after compilation \n 
*  "path/root/gui/qtgsi/TQApplication.h" \n
*  "path/root/gui/qtgsi/TQRootApplication.h" \n
*   "path/root/gui/qtgsi/TQRootCanvas.h" \n
 *  "path/root/gui/qtgsi/TQRootCanvas.cw" \n

*
*
* \subsection step2  Install QuickUSB
* 1 - Copy files located in ./QuickUSB folder to /usr/lib, files named \n
* libquickusb.a \n
* libquickusb.so \n
* libquickusb.so.2 \n
* libquickusb.so.2.0.0 \n
*
* 2 - Copy files QuickUSb and quickusb located in the ./QuickUSB folder  "/etc/hotplug/usb" \n
*
* 3 - In the Hotplug directory = "/etc/hotplug/" edit the file "/etc/hotplug/usb.usermap" and add the line \n
* # Bitwise Systems QuickUSB Module \n
* QuickUSB_Module 0x0003 0x0FBB 0x01 0x0000 0x0000 0x00 0x00 0x00 0x00 0x00 0x00  0x00000000  \n

* 4 - Hotplug will detect the QuickUSB module \n
*
*
*
* \subsection step2  Compile the application
* 1 - In the main folder "./scr" type "qmake DAQ_XPAD.pro", that will create a Makefile. \n
* 2 - Type "make" to compile the project, that will create a executable file DAQ_XPAD. \n
* 3 - Then type "./DAQ_XPAD" to launch the application
* 
*
*
*/


int main( int argc, char ** argv ){
    
    TQApplication app("uno",&argc,argv);
    TQRootApplication a( argc, argv, 0);
    MainGUI *w = new MainGUI  ;
    w->show();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}

