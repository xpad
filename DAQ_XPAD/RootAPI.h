#ifndef _ROOTAPI_H_
#define _ROOTAPI_H_

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <strstream>
#include <istream>
#include <vector>


#include "TQRootCanvas.h" 
#include "TCanvas.h" 
#include "TH2F.h" 
#include "TStyle.h" 
#include "TSystem.h" 
#include "TAttImage.h"
#include "TColor.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TFrame.h"
#include "defines.h"
#include "TRint.h"
#include "TVirtualPad.h"
#include "TObject.h"

#include "utilities.h"
#include "cyclone.h"


int RootConfigHistoDACL(TCanvas *myCanvas,TH1F *Myhisto1d,char *Title,int ChipID) ;
//int RootConfigHistoDACLAllChips(TCanvas *myCanvas,TH1F *Myhisto1d,char *Title) ;
int RootConfigHistoDACLAllChips(TCanvas *myCanvas) ;
int RootFillHistoDACL(TH1F *Myhisto1d,TH2F *Myhisto2d,const char *fileName,int ModuleID,int ChipID) ;
int RootFillHistoDACLAllChips(TH2F *Myhisto2d,const char *fileName) ;
int RootPlotHistoDACL(TCanvas *myCanvas,TH1F *Myhisto1d,TH2F *Myhisto2d) ;
//int RootPlotHistoDACLAllChips(TCanvas *myCanvas,TH1F *Myhisto1d,TH2F *Myhisto2d) ;
int RootPlotHistoDACLAllChips(TCanvas *myCanvas,TH2F *Myhisto2d) ;

//int RootConfigHistoHit(TCanvas *myCanvas,TH2F *Myhisto2d,char *Title) ;
int RootConfigHistoHit(TH2F *Myhisto2d,char *Title) ;
int RootFillHistoHit(TH2F *Myhisto2d,const char *fileName,int ModuleID) ;
int RootPlotHistoHit(TCanvas *myCanvas,TH2F *Myhisto2d) ;
int RootHistoHitConfigFillPlot(TCanvas *myCanvas,TH2F *Myhisto2d,const char *QStrFileSelected,int ModuleID) ;


int RootFillHistoHitFromMatrix(TH2F *Myhisto2d,int matrix[][(80*7)]) ;
int RootHistoHitConfigFillPlotMatrix(TCanvas *myCanvas,TH2F *Myhisto2d,int matrix[][(80*7)],std::string fileName) ;
int Root2HistoHitConfigFillPlotMatrix(TCanvas *myCanvas,TH2F *histo2d_1,int matrix_1[][(80*7)],std::string fileName_1,TH2F *histo2d_2,int matrix_2[][(80*7)],std::string fileName_2) ;

//int RootShowHitCanvas(TQRootCanvas *myCanvas,TH2F *Myhisto2d,const char *fileName) ;

#endif

