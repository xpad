/**
* \file cyclone.h
* \brief The header file of cyclone.cpp
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */

#ifndef _CYCLONE_H_
#define _CYCLONE_H_

#include <stdio.h>
#include <stdlib.h>
#include "CQuickUsb.h"


void ResetSignal(CQuickUsb *wqusb,int wsig) ;
void ForceSignal(CQuickUsb *wqusb,int wsig,int value) ;
void WriteToModule(CQuickUsb *wqusb,int module,int chip,unsigned char *data,int size) ;
void WriteWordToModule(CQuickUsb *wqusb,int module,int chip,int data) ;
void WriteWordToHub(CQuickUsb *wqusb,int module,int data) ;
void WriteWordsToModule(CQuickUsb *wqusb,int module,int chip,int *data,int wsize) ;
int ReadFromModule(CQuickUsb *wqusb,int *data,int timeout_val,int NbWord_Expected) ;

void WriteWordsWithoutHeader(CQuickUsb *wqusb,int module,int chip,int *data,int wsize) ;

int WriteGlobalSettingsRegister(CQuickUsb *wqusb,int value) ;

int ReadDataFromFifo(CQuickUsb *wqusb,int *data,int data_length) ;
int PutMarker(CQuickUsb *wqusb) ;

void* PollingFIFOContains_thread(void *parameters) ;

int ReadRFIFOContains(CQuickUsb *wqusb,int *data,clock_t timout_ms) ;

#endif 
