#ifndef __XPAD_H__
#define __XPAD_H__

#include <iostream>
#include <cstdlib>
#include <cstring>

#include <fstream>
#include <sstream>
#include <istream>
#include <vector>

#include <unistd.h>

#include <xpad/defines.h>
#include <xpad/cyclone.h>
#include <xpad/calibration.h>
#include <xpad/utilities.h>
#include <xpad/barrette.h>
//#include <pthread.h>

#ifndef DAQ_MSG
#define DAQ_MSG(fmt,args...) printf("\033[22;34mdaq_msg :  " fmt,##args)  
#else
#define DAQ_MSG(fmt,args...)
#endif

#define XPAD_CHIP_WIDTH 80
#define XPAD_CHIP_HEIGHT 120
#define XPAD_WIDTH 7 * XPAD_CHIP_WIDTH
#define XPAD_HEIGHT 8 * XPAD_CHIP_HEIGHT

typedef struct _XpadConfig XpadConfig;
typedef struct _XpadCalibrationConfig XpadCalibrationConfig;

struct _XpadConfig {
	int cmos_dis[8][7];
	int ithh_val[8][7];
	int vadj_val[8][7];
	int vref_val[8][7];
	int imfp_val[8][7]; 
	int iota_val[8][7];
	int ipre_val[8][7];
	int ithl_val[8][7];
	int itune_val[8][7];
	int ibuffer_val[8][7];
	int dacl[8*120][7*80];
	int ModuleReady_tab[9]; // 9 to send to all modules
	int mask;
	struct struct_imageUS acq;
	int mode;
	int image[120*8][80*7];
	struct struct_ith ith_calib[8];
};

enum XpadDACLCalibrationAlgoType {
	FROM_ZERO = LOCAL_NOISE_CALIB_LAST_ZERO,
	FROM_MAXCNT = LOCAL_NOISE_CALIB_FROM_MAXCNT,
	FROM_BEAM = LOCAL_BEAM_CALIB
};

struct _XpadCalibrationConfig {
	int ith_min;
	int ith_max;
	XpadDACLCalibrationAlgoType walgo;
	int noise_accepted;
	int dec_dacl_val;
	std::string path;
};

static XpadConfig xpadConfigDefault = {0};


void xpad_init(void);
void xpad_acq(XpadConfig *config);
void xpad_save_image(XpadConfig *config, 
		     std::string const & directory,
		     std::string const & filename);
void xpad_is_ready(XpadConfig *config);
void xpad_reset_hub(void);
void xpad_reset_modules(void);

/* config part */
void xpad_config_init(XpadConfig *self, int dacl);
void xpad_config_init_config_g(XpadConfig *self);
void xpad_config_send(XpadConfig *self);
void xpad_config_send_config_g(XpadConfig *self);
void xpad_config_send_ithl(XpadConfig *self);
void xpad_config_send_itune(XpadConfig *self);
void xpad_config_send_imfp(XpadConfig *self);
void xpad_config_send_dacl(XpadConfig *self);
void xpad_config_send_dacl_flat(XpadConfig *self, int dacl);
void xpad_config_read_dacl(XpadConfig *self);
void xpad_config_read_from_file(XpadConfig *self,
				std::string const & directory,
				std::string const & base);

void xpad_load_calibration_new(XpadConfig *self,
			       std::string const & directory,
			       std::string const & base);

/* calibration part */
void xpad_config_calibrate_ith(XpadConfig *self, const XpadCalibrationConfig *config);
void xpad_config_calibrate_dacl(XpadConfig *self, const XpadCalibrationConfig * config);
void xpad_config_calibrate(XpadConfig *self, const XpadCalibrationConfig * config);

#endif
