
/**
* \file barrette.cpp
* \brief 
* \author Patrick BREUGNON
* \version 1.0
* \date April 08 2008
 */
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/times.h>
#include <unistd.h> 

#include "usbwrap.h"
#include "cyclone.h" 
#include "utilities.h" 
#include "defines.h"
#include "barrette.h"



using namespace std;

#ifdef BARRETTE_MSG
#define BARRETTE_MSG(fmt,args...) printf("\033[22;37mbarrette_msg :  " fmt,##args)  
#define BARRETTE_NDEF(fmt,args...)
#else
#define BARRETTE_NDEF(fmt,args...) printf("\033[22;37m " fmt,##args)  
#define BARRETTE_MSG(fmt,args...)
#endif



int buffer[9600] ;
pthread_t GetDataThread ;

int line ;

int continue_number ;
int data_log[8][120][7]   ;

int cmos_dis[8][7] ;
int ithh_val[8][7] ;
int vadj_val[8][7] ;
int vref_val[8][7] ;
int imfp_val[8][7] ; 
int iota_val[8][7] ;
int ipre_val[8][7] ;
int ithl_val[8][7] ;
int itune_val[8][7]  ;
int ibuffer_val[8][7] ;
int ith_return[8][7] ;


/**
* \fn int DummyMsg(CQuickUsb *wqusb)
* \brief Send a dummy message which is not taken care by modules neither by the HUB card. \brief
* It is just for tests only, don't use this function.
*
* \param *wqusb Pointer to the usb interface.
* \return no return value.
*/
int DummyMsg(CQuickUsb *wqusb)
{
int chip = 0x06 ;
int module = 0x09 ;
int datar[MAX_RFIFO_SIZE] ;


       //printf("just to test git") ;
	//datar = (int*) malloc(9600) ;
	ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	WriteWordToModule(wqusb,module,chip,DUMMY_MSG) ;
	//free(datar) ;

return datar[FIRST_WORD] ;
}

/**
* \fn int ResetHUBFifo(CQuickUsb *wqusb)
* \brief Reset FIFOs located on the HUB card.
* \param *wqusb Pointer to the usb interface. 
* \return no return value.
*/
int ResetHUBFifo(CQuickUsb *wqusb)
{

	WriteWordToHub(wqusb,NULL_PARAM,RESET_HUB_FIFO) ;
	usleep(100000) ;


return 0 ;
}

/**
* \fn int ResetModule(CQuickUsb *wqusb,int wmodule)
* \brief Reset the module specified by the address parameter.
* \param *wqusb Pointer to the usb interface. 
* \param wmodule The address of the module. 
* \return no return value
*/
int ResetModule(CQuickUsb *wqusb,int wmodule)
{
			
	WriteWordToHub(wqusb,wmodule,RESET_MODULE) ;
	usleep(5000000) ;

return 0 ;
}


/**
* \fn int ReconfigModule(CQuickUsb *wqusb,int wmodule)
* \brief Reconfigure the module specified by the address parameter.
* \param *wqusb Pointer to the usb interface. 
* \param wmodule The address of the module. 
* \return no return value
*/
int ReconfigModule(CQuickUsb *wqusb,int wmodule)
{
			
	WriteWordToHub(wqusb,wmodule,RECONFIG_MODULE) ;
	usleep(1000000) ;

return 0 ;
}


/**
* \fn int AskReady(CQuickUsb *wqusb,int wmodule,int *addr_return)
* \brief Send AskReady command to a specific the module. If the module doesn't answer, a timout is generated. \brief
*
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0101 0x0 0x0 0x0> <0xf0f0>. \n
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1101 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.
*
* \param *wqusb Pointer to the usb interface.
* \param wmodule the address of the specific module. 
* \param *addr_return return value of the address of the module which answer.
* \return the HandShake word from an AskReady command <0x1101>
*/
int AskReady(CQuickUsb *wqusb,int wmodule,int *addr_return)
{
int i = 0 ;
int datar[MAX_RFIFO_SIZE];
int datar_length = 0;
int paddr = 1 ;
int preturn_cmd = 0 ;
int pheader = 0 ;



	//datar = (int*) malloc(9600) ;
	datar[preturn_cmd] = -1 ;
	datar[paddr] = 0 ;
	*addr_return = 0 ;
	WriteWordToModule(wqusb,wmodule,NULL_PARAM,ASK_READY) ;
	datar_length = ReadFromModule(wqusb,datar,(TIMEOUT),4) ;
	if (datar_length < 0)
		{
		BARRETTE_MSG("AskReady Module_%d => Timout detected, FIFO empty, datar_length = %d\033[22;30m\n",wmodule,datar_length) ;
		BARRETTE_NDEF("AskReady Module_%d => Timout detected, FIFO empty, datar_length = %d\033[22;30m\n",wmodule,datar_length) ;
		return -1 ;
		}
	BARRETTE_MSG("AskReady Module_%d => ",wmodule) ;
	for (i=0;i<datar_length;i++)
		printf("0x%x ",datar[i]) ;
	printf("\n") ;

	for (i=0;i<datar_length;i++)
		if (datar[i] == 43605)   pheader = i ;
	if (pheader >0)
		{
		paddr = pheader + 1 ;
		*addr_return = datar[paddr] & 0x000f  ;
		preturn_cmd  = pheader + 3 ;
		}
		
	//free(datar) ;
return datar[preturn_cmd] ;
}



/**
* \fn int AskReadyModule(CQuickUsb *wqusb,int wmodule,int *addr_return)
* \brief Make a loop on AskReady function until the module answer, the module specified by the address parameter.\brief
*
* If the module doesn't answer, a Timeout is generated. \n.
* Sending Data to the HUB =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0101 0x0 0x0 0x0> <0xf0f0>. \n
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1101 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.
*
* \param *wqusb Pointer to the usb interface 
* \param wmodule the address of the specific module 
* \param *addr_return return value of the address of the module which answer
* \return -1 if failed and 1 if ok
*/
int AskReadyModule(CQuickUsb *wqusb,int wmodule,int *addr_return)
{
int timeout = 0 ;
int laddr_return ;

			//DummyMsg(wqusb) ; usleep(10000) ; DummyMsg(wqusb) ; usleep(10000) ;
			while (AskReady(wqusb,wmodule,&laddr_return) != IAM_READY)
				{
				usleep(NIOS_DELAY) ;
				ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
				ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
				if (timeout++ > 5) 
					{
					return -1 ;
					}
				}
			*addr_return = laddr_return  ;
			
return 1 ;
}


/**
* \fn void ConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister,int value)
* \brief Send a ConfigG command, configution to general register, to a module at the chip specified by the address. \brief
* 
* Register could be  cmos_dis, ithh, vadj, vref, imfp, iota, ipre, ithl, itune, ibuffer. \n
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0103> <chip_id> <register_id> <the_value> <0xf0f0>.\n
* This function is not used in the program, ConfigAllG is used rather than. 
*
* \param *wqusb Pointer to the usb interface 
* \param wmodule The address of the specific module 
* \param wchip The address of the specific chip
* \param wregister The address of the register
*  \param value The register value we want to set
*  \return no return value
*/
void ConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister,int value)
{
//#define  NB_WORDS  4 
//int buffer[NB_WORDS] ;
int CHIP_ID[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x7f} ;



	buffer[0] = CONFIG_G ;
	buffer[1] = CHIP_ID[wchip] ;
	buffer[2] = wregister ;
	buffer[3] = value ;		
	WriteWordsToModule(wqusb,wmodule,wchip,buffer,4) ;
	//printf("wmodule_%d wchip_%d wregister_%d value_%d \n",wmodule,wchip,wregister,value) ;
	
}

/**
* \fn int WaitingForHandShakeConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister)
* \brief Waiting for an HandShake from a ConfigG command. \brief
*
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1103 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>\n
* As ConfigG function, WaitingForHandShakeConfigG is not used in the program, WaitingForHandShakeConfigAllG is used rather than.
*
* \param *wqusb Pointer to the usb interface 
* \param	 wmodule The address of the specific module 
* \param wchip The address of the specific chip
* \param wregister The address of the register
* \return no return value
*/
int WaitingForHandShakeConfigG(CQuickUsb *wqusb)
//int WaitingForHandShakeConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;
int datar_length = 0 ;
int HandShake_Val = 0 ;
int module_id = 0;
int loop_en = 1 ;


	while (loop_en == 1)
		{
		datar_length  = ReadFromModule(wqusb,datar,TIMEOUT,4) ;
		/*
		printf("WaitingForHandShakeConfigG function cnt = %d loop_en = %d\n",cnt,loop_en) ;
		for (int i = 0;i<datar_length;i++)
		 	printf("0x%x ",datar[i]) ;
		printf("\n") ;
		*/
		if (datar_length > 0) 
			{
			HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
			//printf("HandShake_Val => 0x%x \n",HandShake_Val) ;
			if (HandShake_Val == CONFIG_G_RECV)
				{
				return CONFIG_G_RECV ;
				}
			else 
				return -1 ;
			}
		if (cnt++ == (TIMEOUT*100))
			return -1;
		}
return 0 ;
}


/**
* \fn void ConfigAllG(CQuickUsb *wqusb,int wmodule,int wchip,int *value)
* \brief Send a ConfigAllG command, configution to all general registers, to the module specified by the address. \brief
* 
* Register could be  cmos_dis, ithh, vadj, vref, imfp, iota, ipre, ithl, itune, ibuffer. \n
* Sending  Data to the HUB => <0xbb44> <0x3333> <0x6> <module_id> <0xaa55> <0x3> <0x0> <0x0203> <chip_id> <register_id> <the_value> <0xf0f0>
*
* \param *wqusb Pointer to the usb interface 
* \param	 wmodule The address of the specific module 
* \param wchip The address of the specific chip
* \param wregister The address of the register
*  \param *value pointer to the tab values we want to set
*  \return no return value
*/
void ConfigAllG(CQuickUsb *wqusb,int wmodule,int wchip,int *value)
{
//int NB_WORDS = 13 ;
//int buffer[NB_WORDS] ;
//int CHIP_ID[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x7f} ;
int ii ;
int CHIP_ID[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x7f} ;

	buffer[0] = ALL_CONFIG_G ;
	buffer[1] = CHIP_ID[wchip] ; ;
	for (ii=0;ii<11;ii++)
		buffer[2+ii] = value[ii] ;
	WriteWordsToModule(wqusb,wmodule,wchip,buffer,13) ;
}


/**
* \fn int WaitingForHandShakeConfigAllG(CQuickUsb *wqusb)
* \brief Waiting for an HandShake from a ConfigAllG command. \brief
*
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1203 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.
*
* \param *wqusb Pointer to the usb interface 
* \return -1 if failed otherwise the module_id
*/
int WaitingForHandShakeConfigAllG(CQuickUsb *wqusb)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;
int datar_length = 0 ;
int HandShake_Val = 0 ;
int module_id = 0;


	while (1)
		{
		datar_length  = ReadFromModule(wqusb,datar,TIMEOUT,4) ;
		if (datar_length > 0) 
			{
			HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
			if (HandShake_Val == ALL_CONFIG_G_RECV)	return module_id ;
			else return -1 ;
			}
		if (cnt++ == (TIMEOUT*100))
			return -1;
		}
}


/**
* \fn void FlatConfig(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int config)
* \brief Send a FlatConfig command to a specific module at a specific chip specified by the adresses. \brief
*
* Sending  Data to the HUB, ONE_CHIP Mode =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0104> <Ox0> <chip_id> <config> <0xf0f0>.\n
* Sending  Data to the HUB, ALL_CHIP Mode =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0104> <Ox1> <0x7f> <config> <0xf0f0>.\n
* \n
* [15..9] => Not used \n
* [8..3]   => DACL 6bits \n
* [2]       => Disable Analog Preamp \n
* [1]       => Enable analog or Digital. \n ‏
* [0]       => Enable compter \n
*
* \param *wqusb Pointer to the usb interface 
* \param wmodule the address of the module  
* \param wchip the address of the chip 
* \param mode Two possibilities ALL_CHIP or ONE_CHIP 
* \param config the value of the register 
* \return -1 if failed otherwise the module_id
*/
void FlatConfig(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int config)
{
//int NB_WORDS = 4  ;
//int buffer[(NB_WORDS+1)] ;
int CHIP_ID[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x7f} ;


	buffer[0] = FLAT_CONFIG ;
	if (mode == ALL_CHIP)
		{
		buffer[1] =  ALL_CHIP ;
		buffer[2] =  0x7f ;
		buffer[3] =  config ;
		}
	else
		{

		buffer[1] =  ONE_CHIP;
		buffer[2] =  CHIP_ID[wchip-1] ;
		buffer[3] =  config ;
		}
	WriteWordsToModule(wqusb,wmodule,wchip,buffer,4) ;	
}


/**
* \fn int WaitingForHandShakeFlatConfig(CQuickUsb *wqusb)
* \brief Waiting for an HandShake from a FlatConfig command, If the module doesn't answer, a timout is generated. \brief
*
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1104 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \return -1 if failed otherwise the module_id
*/
int WaitingForHandShakeFlatConfig(CQuickUsb *wqusb)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;
int datar_length = 0 ;
int HandShake_Val = 0 ;
int module_id = 0;


	while (1)
		{
		datar_length  = ReadFromModule(wqusb,datar,TIMEOUT,4) ;
		if (datar_length > 0) 
			{
			HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
			/*
			BARRETTE_MSG("HandShakeFlatConfig datar =>  ") ;
			for (i=0;i<datar_length;i++)
				printf("0x%x ",datar[i]) ;
			printf("\n") ;
			*/
			if (HandShake_Val == FLAT_CONFIG_RECV)	return FLAT_CONFIG_RECV ;
			else return -HandShake_Val ;
			}
		else
			{
			if (cnt++ == (TIMEOUT*100000))
				{
				BARRETTE_MSG("TIMOUT HandShakeFlatConfig  ") ;
				return -1;
				}
			}
		}
}


/**
* \fn void MakeCalibration(CQuickUsb *wqusb,int wmodule,int min_ith,int max_ith,int min_dacl,int max_dacl,int type_test,int nios_delay,int nb_pixel_not_accepted)
* \brief Send a start calibration command to a specific module. \brief
*
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <hub_field_length> <module_id> <0xaa55> <module_field_length> <0x0> <0x0105> <min_ith> <max_ith> <min_dacl> <max_dacl> <type_test> <nios_delay> <nb_pixel_not_accepted> <0xf0f0>.\n
* In order to indicate the progress of the calibration, each module send messages to the DAQ \n
* Message 0 => <0x0> <0xaa55> <module_id> <0x8> <0x1105 0x0 0x0 0x0 0x0 0x0> <0xf0f0>    *** HandShake of Calibration command <0x1105> *** \n
* Message 1 => <0x0> <0xaa55> <module_id> <0x8> <0xf001 0x0 0x0 0x0 0x0 0x0> <0xf0f0>     *** Calib STEP_1 ended <0xf001> *** \n
* Message 2 => <0x0> <0xaa55> <module_id> <0x8> <0xf002 <ith_mod1> <ith_mod2> <ith_mod3> <ith_mod4> <ith_mod5> <ith_mod6> <ith_mod7> <ith_mod8> <0xf0f0> \n
* Message 3 => <0x0> <0xaa55> <module_id> <0x8> <0xf003 0x0 0x0 0x0 0x0 0x0> <0xf0f0>     *** Calib STEP_3 ended <0xf003> *** \n
* Message 4 => <0x0> <0xaa55> <module_id> <0x8> <0xf004 0x0 0x0 0x0 0x0 0x0> <0xf0f0>     *** Calib STEP_4 ended <0xf004> *** \n
* Message 5 => <0x0> <0xaa55> <module_id> <0x8> <0x2105 0x0 0x0 0x0 0x0 0x0> <0xf0f0>    *** Calibration ended <0x2105> *** \n
* 
* \param *wqusb Pointer to the usb interface 
* \param wmodule address of the module you want to calibrate
* \param min_ith minimun value of ith
* \param max_ith maximum value of ith
* \param min_dacl minimum value of DACL
* \param max_dacl maximum value of DACL
* \param type_test could be NOISE_LIMIT = 0; BEAM_CALIB = 1; TEST_PULSE = 2
* \param nios_delay set the integration time
* \param nb_pixel_not_accepted allow to reduce the time of the calibration, by default put 10
* \return no return value
*/
void MakeCalibration(CQuickUsb *wqusb,int wmodule,int min_ith,int max_ith,int min_dacl,int max_dacl,int type_test,int nios_delay,int nb_pixel_not_accepted,int custom_parameter)
{
//int NB_WORDS = 9 ;
//int buffer[10] ;


	buffer[0] = CALIB ;
	buffer[1] = min_ith ;	
	buffer[2] = max_ith ;
	buffer[3] = min_dacl ;
	buffer[4] = max_dacl ;
	buffer[5] = type_test ; // 0 calibration with noise, 1 with the beam, 2 with internal pulseur
	buffer[6] = 0 ;
	buffer[7] = nios_delay ;
	buffer[8] = nb_pixel_not_accepted ;
	buffer[9] = custom_parameter  ;
	WriteWordsToModule(wqusb,wmodule,NULL_PARAM,buffer,10) ;	
}



/**
* \fn int LoadDACLConfigs(CQuickUsb *wqusb,int wmodule,const char *file)
* \brief Send DAL config from a file to a specific module \brief
*
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <hub_field_length> <module_id> <0xaa55> <module_field_length> <0x0> <0x0380> <the_data_file> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param wmodule address of the specific module
* \param *file the calibration file
* \return negative value if failed otherwise 1.
*/
/*
int LoadDACLConfigs(CQuickUsb *wqusb,int wmodule,const char *file)
{
int cnt_line = 0 ; 
int dataTemp ;
int dataTab[100] ;
string line ;
int status ;
int start_read = -1 ;

	ifstream myFile(file) ;
	while (getline(myFile,line))
		{
		istrstream linestream(line.c_str()) ;
		
		 if (start_read < 0) 
	   		{
	   		if (strcmp(line.c_str(),"DACL") == 0) start_read = 0 ;
			if (strcmp(line.c_str(),"DACL ") == 0) start_read = 0 ;
			if (strcmp(line.c_str(),"DACL  ") == 0) start_read = 0 ;
	   		}
	 	else
	 		{
			start_read++ ;
			}
			
		 if (start_read > 0) 
	   		{	
			for (int i=0;i<MAXDATALINE;i++)
				{
				linestream >> dataTemp ; 
				//if ((i >0) && ( i<(MAXCOL+3)))
				if ( i<(MAXCOL+3))
					dataTab[i+1] = dataTemp ;
				}
			dataTab[0] = LOAD_CALIB ;
		
  			WriteWordsToModule(wqusb,wmodule,NULL_PARAM,dataTab,(MAXCOL+3))  ;
 		 
  			if (cnt_line == 839)
				{
   				status = WaitingForHandShakeMessage(wqusb,LOAD_CALIB_ENDED) ;
				if (status == -1) return -(1000+cnt_line) ;
				}
 			 else
   				{
   				status = WaitingForHandShakeMessage(wqusb,CONTINUE) ;
				if (status == -1) return -(2000+cnt_line) ;
   				cnt_line++ ;
   				}
			}
  		}
		
 	myFile.close() ;
	
return 1 ;
}
*/


int LoadDACLFromTab(CQuickUsb *wqusb,int wmodule,int dacl_tab[][80*7])
{
int buffer[100] ;
int status ;
int pchip ;
int pcol ;
int real_row ;
int prow ;
int cnt_line = 0 ;
int y ;
int x ;
int module_msk = 0  ;
int value = 0 ;
	
	module_msk = 1 << wmodule ;
	//printf("LoadDACLFromTab wmodule = %d module_msk = Ox%x\n",wmodule,module_msk ) ;
	// on commence par row = 120 pour rester compatible avec le single module
	for (prow=(ROW_NB-1);prow>=0;prow--)
	//for (prow=0;prow<ROW_NB;prow++)
		{
		for (pchip=0;pchip<MAXCHIP;pchip++)
			{
			buffer[0] = LOAD_CALIB ;
			buffer[1] = pchip +1 ;	
			buffer[2] = prow + 1 ;
			for (pcol=0;pcol<MAXCOL;pcol++)
				{			
				//real_row = (ROW_NB -1) - prow ;
				y = prow+(wmodule*ROW_NB) ;	
				x = pcol+(pchip*MAXCOL) ;
				buffer[3+pcol] = dacl_tab[y][x] ;
				}

  			WriteWordsToModule(wqusb,module_msk,NULL_PARAM,buffer,(MAXCOL+3))  ;

  			if (cnt_line == 839)
				{
   				status = WaitingForHandShakeMessage(wqusb,LOAD_CALIB_ENDED) ;
				//printf("\n cnt_line = 839 status = %d \n", status) ;
				if (status == -1) return -(1000+cnt_line) ;
				}
 		 	else
   				{
   				status = WaitingForHandShakeMessage(wqusb,CONTINUE) ;
				if (status == -1) return -(2000+cnt_line) ;
   				cnt_line++ ;
   				}
			}
		}
return 0 ;
}


int LoadDACLFromFileConfig(CQuickUsb *wqusb,int wmodule,const char *file)
{
int cnt_line = 0 ; 
int dataTemp ;
int dataTab[100] ;
string line[120*8] ;
int pline = 0 ;
int nbline = 0 ;
int status ;
int i ;

	ifstream myFile(file) ;
	pline=0 ;
	while (getline(myFile,line[pline]))
		{
		stringstream linestream(line[pline].c_str()) ;
		 if (pline == 0) 
	   		{
	   		if (strcmp(line[pline].c_str(),"DACL") == 0) pline++ ;
			if (strcmp(line[pline].c_str(),"DACL ") == 0) pline++ ;
			if (strcmp(line[pline].c_str(),"DACL  ") == 0) pline++ ;
			//printf("Nbline = %d\n",pline) ;
			//printf("%s \n",line[pline].c_str()) ;
			
	   		}
	 	else
	 		{
			//printf("Nbline = %d\n",pline) ;
			//printf("%s \n",line[pline].c_str()) ;
			if (line[pline].length() > 0)
				pline++ ;
			}
		}
	nbline = pline -1 ;
	//printf("Nbline = %d, length = %d \n",pline,line[pline].length()) ;
	//printf("%s \n",line[pline].c_str()) ;
	for (i=0;i<nbline;i++)
		{
		 stringstream linestream(line[nbline-i].c_str()) ;
		 for (int i=0;i<MAXDATALINE;i++)
			{
			linestream >> dataTemp ; 
			//if ((i >0) && ( i<(MAXCOL+3)))
			if ( i<(MAXCOL+3))
				dataTab[i+1] = dataTemp ;
			}
		dataTab[0] = LOAD_CALIB ;
		
  		WriteWordsToModule(wqusb,wmodule,NULL_PARAM,dataTab,(MAXCOL+3))  ;
 		
		/*
		if (dataTab[1]==1 &&dataTab[2]==1)
			{
			printf ("LoadDACLFromFileConfig = > ") ;
			for (int i=0;i<(MAXCOL+3);i++)
				printf("%d ",dataTab[i]) ;
			printf ("\n") ; 
			}
		*/
		
  		if (cnt_line == 839)
			{
   			status = WaitingForHandShakeMessage(wqusb,LOAD_CALIB_ENDED) ;
			if (status == -1) return -(1000+cnt_line) ;
			}
 		 else
   			{
   			status = WaitingForHandShakeMessage(wqusb,CONTINUE) ;
			if (status == -1) return -(2000+cnt_line) ;
   			cnt_line++ ;
   			}
		}
	 
	 myFile.close() ;	
	/*
	while (getline(myFile,line))
		{
		istrstream linestream(line.c_str()) ;
		
		 if (start_read < 0) 
	   		{
	   		if (strcmp(line.c_str(),"DACL") == 0) start_read = 0 ;
			if (strcmp(line.c_str(),"DACL ") == 0) start_read = 0 ;
			if (strcmp(line.c_str(),"DACL  ") == 0) start_read = 0 ;
	   		}
	 	else
	 		{
			start_read++ ;
			}
			
		 if (start_read > 0) 
	   		{	
			for (int i=0;i<MAXDATALINE;i++)
				{
				linestream >> dataTemp ; 
				//if ((i >0) && ( i<(MAXCOL+3)))
				if ( i<(MAXCOL+3))
					dataTab[i+1] = dataTemp ;
				}
			dataTab[0] = LOAD_CALIB ;
		
  			WriteWordsToModule(wqusb,wmodule,NULL_PARAM,dataTab,(MAXCOL+3))  ;
 		 
  			if (cnt_line == 839)
				{
   				status = WaitingForHandShakeMessage(wqusb,LOAD_CALIB_ENDED) ;
				if (status == -1) return -(1000+cnt_line) ;
				}
 			 else
   				{
   				status = WaitingForHandShakeMessage(wqusb,CONTINUE) ;
				if (status == -1) return -(2000+cnt_line) ;
   				cnt_line++ ;
   				}
			}
  		}
		
 	myFile.close() ;
	*/
	
return 1 ;
}

/**
* \fn int ReadCalib(CQuickUsb *wqusb,int FirstCommand,std::string DataFile,int wmodule,int cmos_dis[][7],int ithh[][7],int vadj[][7],int vref[][7],int imfp[][7],int iota[][7],int ipre[][7],int ithl[][7],int itune[][7],int ibuffer[][7])
* \brief Read the calibration result from a specific module and store to a file \brief
* This function is excecuted righ after the MakeCalibration fucntion \n
* In the same time store the condition of the calibration, value of general registers cmos_dis, ithh, vadj, vref, imfp, iota, ipre, ithl, itune, ibuffer \n
* ithl values are found automatically during the  MakeCalibration fucntion
*
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x01C0 0x0 0x0 0x0> <0xf0f0>. \n
*
* \param *wqusb Pointer to the usb interface 
* \param FirstCommand
* \param DataFile
* \param wmodule
* \param cmos_dis[][7]
* \param ithh[][7]
* \param vadj[][7]
* \param vref[][7]
* \param imfp[][7]
* \param iota[][7]
* \param ipre[][7]
* \param ithl[][7]
* \param itune[][7]
* \param ibuffer[][7]
* \param wmodule address of the specific module
* \param *file the calibration file
* \return negative value if failed otherwise 1.
*/
int ReadCalib(CQuickUsb *wqusb,int FirstCommand,std::string DataFile,int wmodule,int cmos_dis[][7],int ithh[][7],int vadj[][7],int vref[][7],int imfp[][7],int iota[][7],int ipre[][7],int ithl[][7],int itune[][7],int ibuffer[][7])
{
#define SIZE_DATA	2

//QString DataFile = QString("%1%2").arg(QDir::currentDirPath(),"/data/data.dat") ;
int datar[MAX_RFIFO_SIZE] ;
int datar_length ;
int ii = 0 ;
int scan_row, scan_chip ;
int tab_index ;

//	SaveDACToFile(DataFile,tab_index,cmos_dis,ithh,vadj,vref,imfp,iota,ipre,ithl,itune,ibuffer) ;

	if (strcmp(DataFile.c_str(),"")  == 0) { BARRETTE_MSG("ERROR No file selected\033[22;30m\n") ; return -1;}
	BARRETTE_MSG("Begin Transmission\033[22;30m\n") ;
	for (scan_row = 0; scan_row < 120; scan_row++)
		{
		BARRETTE_NDEF("*\033[22;30m") ;
		for (scan_chip = 0;scan_chip < 7;scan_chip++)
			{	
//			ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
			ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
			if (scan_row == 0 and scan_chip == 0) WriteWordToModule(wqusb,wmodule,NULL_PARAM,FirstCommand) ;
			else WriteWordToModule(wqusb,wmodule,NULL_PARAM,CONTINUE) ;
				
			datar_length = ReadFromModule(wqusb,datar,(TIMEOUT*100),80) ;
			//printf(" ReadFromModule NbWord = %d\n",datar_length) ;
			
			if (datar_length > 0)
				{
				if (scan_row == 0 and scan_chip == 0) 
					{
					//printf(" ReadCalib => scan_row == 0 and scan_chip == 0\n") ;
					tab_index = wmodule - 1 ;
					SaveDACToFile(DataFile,tab_index,cmos_dis,ithh,vadj,vref,imfp,iota,ipre,ithl,itune,ibuffer) ;
					SaveConfigToFile(DataFile,datar,datar_length,_APPEND) ;	
					}
				else SaveConfigToFile(DataFile,datar,datar_length,_APPEND) ;	
				//printf("SaveDataToFile => %s \n",(const char*) DataFile) ;
				}

			if (ii++==839) 
				{
				BARRETTE_MSG("Transmission ended\033[22;30m\n") ;
				WriteWordToModule(wqusb,wmodule,NULL_PARAM,CONTINUE) ;
				return 0;
				}
			
			}
		}

#undef SIZE_DATA	

return 0 ;
}



/**
* \fn void Config_ith_dacl(CQuickUsb *wqusb,int wmodule,int wchip,int *value)
* \brief Function used only for the test \brief
*
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0380> <the_value> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param wmodule 
* \param wchip 
* \param *value 
* \return no return value
*/
void Config_ith_dacl(CQuickUsb *wqusb,int wmodule,int wchip,int *value)
{
//int NB_WORDS = 13 ;
//int buffer[NB_WORDS] ;
//int CHIP_ID[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x7f} ;
int ii ;

	buffer[0] = LOAD_DACL_ITH ;
	//buffer[1] = CHIP_ID[wchip-1] ;
	buffer[1] = wchip ;
	for (ii=0;ii<11;ii++)
		buffer[2+ii] = value[ii] ;
	WriteWordsToModule(wqusb,wmodule,wchip,buffer,13) ;
}



/**
* \fn int TestDigitalPart(CQuickUsb *wqusb,int nbhit,int matrix[][(80*7)],int module_id) 
* \brief Send TestDigital command to a specific module then download the result and store in a matrix
*
* The parameter nb_hit allow to specify the number of digital pulses \n
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0102> <0x0> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param nbhit specify the number of digital pulses 
* \param matrix[][(80*7)] matrix in which the result will be stored 
* \param module_id the address of the module we want to test 
* \return no return value
*/
int TestDigitalPart(CQuickUsb *wqusb,int nbhit,int matrix[][(80*7)],int module_id) 
//int ReadOptoRFIFOAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int matrix[][(80*7)])
{
#define SIZE_DATA		2
#define NB_WORDS	2

int datar[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;

int ii = 0 ;
int buffer[3] ;

struct timeval start ;
int HandShake_Val = 0 ;
int progress_acq = 0 ;
int elapsed_time = 0 ;

	
	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
	buffer[0] = DIGITAL_TEST ;
	buffer[1] = nbhit ;	

	WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,NB_WORDS) ;
	
	gettimeofday(&start,NULL) ;
	//image_time = (image_time*nb_image*2) + 1000 ; // 2xImage_time + 1s image_time en mseconde
	while (1)
		{
		datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;   
		if (datar_length > 8) 
			{
			datar_length  = ReadFromModule(wqusb,datar,TIMEOUT,4) ;
			//HandShake_Val =  ExtractHandShakeVal_ImageUS(datar,datar_length,&module_id,&progress_acq) ;
			HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
		
			if (HandShake_Val == DIGITAL_TEST_ENDED)	
				{
				//elapsed_time = (clock_t) (((current-start)/clk_tck)*1000) ; //convertie mseconde
				BARRETTE_MSG("Receive HandShakeDigitalTest=> fifo_len=%d word=0x%x \n",datar_length,HandShake_Val) ;	
				 elapsed_time = elapsed_time_ms(start) ;
	         		   //BARRETTE_MSG("Timout_image  =ms elapsed_time = %u ms \n",(unsigned int) image_time,(unsigned int) elapsed_time)  ;
				BARRETTE_MSG("elapsed_time = %u ms \n",(unsigned int) elapsed_time)  ;
				return   DIGITAL_TEST_ENDED;
				   }
			else 
				{
				BARRETTE_MSG("Bad HandShakeDigitalTest HandShakeVal = 0x%x  fifo_len=%d \n",HandShake_Val,datar_length) ;
				}
			}
		}

/*
	for (int row = 0; row < ROW_NB; row++)
	  {
	  for (int pchip = 0 ;pchip<7;pchip++)
		{ 
		ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
		if (row == 0 and pchip == 0)
			{
			WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,NB_WORDS) ;
			 usleep(3000000) ;
			 }
		else WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;

		while (datar_length  < 80) 
				datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;
		
		ReadDataFromFifo(wqusb,datar,datar_length ) ;
		FillMatrixFromDatar(datar,matrix,datar_length)  ;
		datar_length = 0 ;
			
		if (ii++==839) 
			{
			WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;
			return 0;
			} 
	
		}
	   }
*/

#undef SIZE_DATA	
#undef NB_WORDS	

return 0 ;
}


/**
* \fn int TestDigitalPartMultiModule(CQuickUsb *wqusb,int nbhit,int matrix[][(80*7)],int module_id) 
* \brief For the tests only, don't use it
*
*
* \param *wqusb 
* \param nbhit 
* \param matrix[][(80*7)] 
* \param module_id 
* \return no return value
*/
int TestDigitalPartMultiModule(CQuickUsb *wqusb,int nbhit,int matrix[][(80*7)],int module_id) 
//int ReadOptoRFIFOAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int matrix[][(80*7)])
{
#define SIZE_DATA		2
#define NB_WORDS	2

int datar[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;

int ii = 0 ;
int buffer[3] ;

	
	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
	buffer[0] = DIGITAL_TEST ;
	buffer[1] = nbhit+(module_id*10) ;	
	for (int row = 0; row < ROW_NB; row++)
	  {
	  for (int pchip = 0 ;pchip<7;pchip++)
		{ 
		ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
		if (row == 0 and pchip == 0)
			{
			WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,NB_WORDS) ;
			 usleep(3000000) ;
			 }
		else WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;

		while (datar_length  < 80) 
				datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;
		
		ReadDataFromFifo(wqusb,datar,datar_length ) ;
		FillMatrixFromDatar(datar,matrix,datar_length)  ;
		datar_length = 0 ;
			
		if (ii++==839) 
			{
			WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;
			return 0;
			} 
	
		}
	   }

#undef SIZE_DATA	
#undef NB_WORDS	

return 0 ;
}


/**
* \fn int ImageUs(CQuickUsb *wqusb,int module_id,int gate_mode,int integration_time,int index_img,int nb_image)
* \brief Send ImageUS command, validate the counting, to a specific module 
*
* Sending  Data to the HUB =>  <0xbb44> <0x3333> <hub_field_length> <module_id> <0xaa55> <module_field_length> <0x0> <0x0140> <gate_mode> <msb_itime> <lsb_itime> <i_img> <nb_image> <0x0> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param module_id the address of the module 
* \param gate_mode internal (0) or external (1)
* \param integration_time 
* \param index_img
* \param nb_image 
* \return no return value
*/
int ImageUs(CQuickUsb *wqusb,int module_id,int gate_mode,int integration_time,int index_img,int nb_image,int answer)
{

int msb_integration_time  ;
int lsb_integration_time  ;


        lsb_integration_time = integration_time & 0xffff ;
	msb_integration_time = (integration_time & 0xffff0000) >> 16 ;
	buffer[0] = IMAGE_US ;
	buffer[1] = gate_mode ;	
	buffer[2] = msb_integration_time ;
	buffer[3] = lsb_integration_time ;
	buffer[4] = index_img ;
	buffer[5] = nb_image ;
	buffer[6] = answer ; 

	//printf("integration_time = 0x%x msb_integration_time = 0x%x lsb_integration_time = 0x%x\n",integration_time,msb_integration_time,lsb_integration_time) ;
	WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,7) ;

return -1 ;
}

int ImageUs_X_Samples(CQuickUsb *wqusb,int module_id,int gate_mode,int integration_time,int index_img,int nb_image,int answer)
{

int msb_integration_time  ;
int lsb_integration_time  ;


        lsb_integration_time = integration_time & 0xffff ;
	msb_integration_time = (integration_time & 0xffff0000) >> 16 ;
	buffer[0] = IMAGE_US_X_SAMPLES ;
	buffer[1] = gate_mode ;	
	buffer[2] = msb_integration_time ;
	buffer[3] = lsb_integration_time ;
	buffer[4] = index_img ;
	buffer[5] = nb_image ;
	buffer[6] = answer ; 

	//printf("integration_time = 0x%x msb_integration_time = 0x%x lsb_integration_time = 0x%x\n",integration_time,msb_integration_time,lsb_integration_time) ;
	WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,7) ;

return -1 ;
}



/**
* \fn int ReadImage(CQuickUsb *wqusb,int FirstCommand,std::string DataFile,int image_pointer,int wmodule,int save_image)
* \brief  Send A ReadImage to a specific module and store data in a file, ReadImage OVF or ReadImage Counter
*
* Sending Data to the HUB ReadImage Counter  =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0180> <0x0> <0xf0f0>.\n
* Sending Data to the HUB ReadImage OVF       =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0280> <0x0> <0xf0f0>.\n
* Use ReadOptoRFIFOAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int matrix[][(80*7)],int module_id) rather than
*
* \param *wqusb Pointer to the usb interface 
* \param FirstCommand
* \param DataFile
* \param image_pointer
* \param wmodule
* \param save_image 1 the image is saved in the specify file, 0 not save
* \return -1 if file is not found, 0 if ok
*/
int ReadImage(CQuickUsb *wqusb,int FirstCommand,std::string DataFile,int wmodule,int save_image)
//int ReadImage(CQuickUsb *wqusb,int FirstCommand,std::string DataFile,int image_pointer,int wmodule,int save_image)
{
#define SIZE_DATA	2
int chip = 0x00 ;
//QString DataFile = QString("%1%2").arg(QDir::currentDirPath(),"/data/data.dat") ;
int datar[MAX_RFIFO_SIZE] ;
int datar_length ;
int ii = 0 ;
int scan_row, scan_chip ;


	if (strcmp(DataFile.c_str(),"")  == 0){ BARRETTE_MSG("ERROR No file selected\033[22;30m\n") ; return -1;}
	BARRETTE_MSG("Begin Transmission\033[22;30m\n") ;

	for (scan_row = 0; scan_row < 120; scan_row++)
		{
		BARRETTE_NDEF("*\033[22;30m") ;
		for (scan_chip = 0;scan_chip < 7;scan_chip++)
			{	
			//ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
			ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
			if (scan_row == 0 and scan_chip == 0)	WriteWordToModule(wqusb,wmodule,chip,FirstCommand) ;
			else WriteWordToModule(wqusb,wmodule,chip,CONTINUE) ;

			datar_length = ReadFromModule(wqusb,datar,TIMEOUT,80) ;
			
			
			
			if (datar_length > 0)
				{
				if (save_image == 1)
					{
					if (scan_row == 0 and scan_chip == 0) SaveDataToFile(DataFile,datar,datar_length,_READ_ONLY) ;	
					else SaveDataToFile(DataFile,datar,datar_length,_APPEND) ;	
					}
				//printf("SaveDataToFile => %s \n",(const char*) DataFile) ;
				}
			
			if (ii++==839) 
				{
				BARRETTE_MSG("Transmission ended\033[22;30m\n") ;
				WriteWordToModule(wqusb,wmodule,chip,CONTINUE) ;
				return 0;
				}
			
			}
		}
//printf("End of ReadImage i = %d\n",i) ;
return 0 ;
}


/**
* \fn int ReadOptoRFIFOAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int matrix[][(80*7)],int module_id)
* \brief  Send A ReadImage to a specific module and store data in a matrix, ReadImage OVF or ReadImage Counter
*
* Sending Data to the HUB ReadImage Counter  =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0180> <0x0> <0xf0f0>.\n
* Sending Data to the HUB ReadImage OVF       =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0280> <0x0> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param FirstCommand
* \param matrix[][80*7]
* \param module_id
* \return 0
*/
int ReadOptoRFIFOAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int matrix[][(80*7)],int module_id)
{
#define SIZE_DATA		2

//int *data_buff ;
int data_buff[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;
int ii = 0 ;
int timout = 0 ;

	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	
	datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;
	/*
	if (datar_length > 0)
		{
		data_buff =  (int*) malloc(datar_length*sizeof(*data_buff)) ;
		ReadDataFromFifo(wqusb,data_buff,datar_length ) ;
		printf("\033[22;31m ERROR FIFO NOT EMPTY\033[22;30m\n") ; 
		printf("\n\033[22;31m Data_Fifo = %d words => ",datar_length) ; 
		for (int jj = 0;jj <datar_length;jj++)
			printf("0x%x ",data_buff[jj]) ;
		printf("\033[22;30m\n") ;
		free(data_buff) ;
		}
	*/
	line = 0 ;
	for (int row = 0; row < ROW_NB; row++)
	  {
	  for (int pchip = 0 ;pchip<7;pchip++)
		{
		 
		if (row == 0 and pchip == 0) 
			WriteWordToModule(wqusb,module_id,NULL_PARAM,FirstCommand) ;
		else 
			WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;
		
		datar_length = 0 ;

		while (datar_length  < 80)
			{
			datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;
			if (timout++ > 200000)
				{
				printf("\033[22;31mTimout detected Module_%d Row_%d Chip_%d datar_length = %d\033[22;30m ",module_id,row,pchip,datar_length) ;
				if (datar_length > 0)
					{
					//data_buff =  (int*) malloc(datar_length*sizeof(int)) ;
					ReadDataFromFifo(wqusb,data_buff,datar_length ) ;
					printf("\n\033[22;31m Data_Fifo = %d words => ",datar_length) ; 
					for (int jj = 0;jj <datar_length;jj++)
						printf("0x%x ",data_buff[jj]) ;
					printf("\033[22;30m\n") ;
					/*
					if (sizeof(data_buff) > 0)
						free(data_buff) ;
					*/
					}
				else 
					printf("\033[22;31m ERROR FIFO EMPTY \033[22;30m\n") ; 
					
				return -1 ;
				}
			}

		//data_buff = (int*) malloc(datar_length*sizeof(*data_buff)) ;
		//data_buff = (int*) malloc(datar_length*sizeof(int)) ;
		if (data_buff == NULL)
			{
			printf("\033[22;31m ERROR Memory allocation unvaliable \033[22;30m\n") ; 
			return 0 ;
			}
		ReadDataFromFifo(wqusb,data_buff,datar_length ) ;
		
		/*
		if ((module_id == 2) & (row==0) & (pchip==0))
			{
			printf("data_buff = ") ;
			for (k=0;k<datar_length;k++)
				printf("0x%x ",data_buff[k]) ;
			printf("\n") ;
			}
		*/
		
		FillMatrixFromDatar(data_buff,matrix,datar_length)  ;
		/*
		if (sizeof(data_buff) > 0)
			free(data_buff) ;
		*/
			
		if (ii++==839) 
			{
			WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;
			return 0;
			} 
	
		}
	   }

return 0 ;
}



/**
* \fn int ReadImageIndexAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int module_id,int Image_index,int matrix[][(80*7)])
* \brief  Send A ReadImage Index to a specific module and store data in a matrix, ReadImage OVF or ReadImage Counter
*
* Sending Data to the HUB ReadImage Counter  =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0180> <0x0> <0xf0f0>.\n
* Sending Data to the HUB ReadImage OVF       =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0280> <0x0> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param FirstCommand
* \param matrix[][80*7]
* \param module_id
* \return 0
*/
int ReadImageIndexAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int module_id,int Image_index,int matrix[][(80*7)])
{
#define SIZE_DATA		2

//int *data_buff ;
int data_buff[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;
int ii = 0 ;
int timout = 0 ;


	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	//for (ii=0;ii<128;ii++)
	//	ldatar[ii] = 0 ;
	line = 0 ;
	buffer[0] = FirstCommand ;
	buffer[1] =  Image_index ;
	
	for (int row = 0; row < ROW_NB; row++)
	  {
	  for (int pchip = 0 ;pchip<7;pchip++)
		{ 

		if (row == 0 and pchip == 0) 
			WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,SIZE_DATA) ; 
		else 
			WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;
		
		datar_length = 0 ;
		while (datar_length  < 80) 
			{
			datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;
			if (timout++ > 200000) 
				{
				BARRETTE_MSG("\033[22;31m%d : Timout detected Module_%d Chip_%d Row_%d datar_length = %d\033[22;30m\n",line,module_id,pchip,row,datar_length) ;
				//return -1 ;
				}
			}		
			
		//data_buff = (int*) malloc(datar_length*sizeof(*data_buff)) ;
		ReadDataFromFifo(wqusb,data_buff,datar_length ) ;
		FillMatrixFromDatar(data_buff,matrix,datar_length)  ;
		//free(data_buff) ;
		
		datar_length = 0 ;	
		
		if (ii++==839) 
			{
			WriteWordToModule(wqusb,module_id,NULL_PARAM,CONTINUE) ;
			return 0;
			} 
	
		}
	   }

return 0 ;
}

/**
* \fn int FastReadImageIndexAndFillMatrix(CQuickUsb *wqusb,int module_id,int Image_index,,int delay_between_data,int matrix[][(80*7)])
* \brief  Send A Fast ReadImage Index to a specific module and store data in a matrix, ReadImage OVF or ReadImage Counter
*
* Sending Data to the HUB ReadImage Counter  =>  <0xbb44> <0x3333> <0x9> <module_id> <0xaa55> <0x6> <0x0> <0x0182> <index> <delay> <0xf0f0>.\n
*
* \param *wqusb Pointer to the usb interface 
* \param module_id
* \param image_index
* \param matrix[][80*7]
* \return 0
*/
/*
int FastReadImageIndexAndFillMatrix(CQuickUsb *wqusb,int module_id,int Image_index,int delay_between_data,int matrix[][(80*7)])
{
#define SIZE_DATA		2

int datar[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;
int ii = 0 ;
int timout = 0 ;
int line_received = 0 ;

int pmodule ;
int prow ;
int pchip ;




	for (pmodule=0;pmodule<8;pmodule++)
		{
		for (prow = 0;prow < 120; prow++)
			{
			for (pchip = 0;pchip < 7; pchip++)
				data_log[pmodule][prow][pchip] = 0  ;
			}
		}

	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	for (ii=0;ii<128;ii++)
		datar[ii] = 0 ;
	line = 0 ;
	buffer[0] = FAST_READ_IMAGE ;
	//buffer[0] = READ_IMAGE_COUNTER ;
	buffer[1] =  Image_index ;
	buffer[2] =  delay_between_data ;
	buffer[3] =  delay_between_data ;
	buffer[4] =  delay_between_data ;
	WriteWordsToModule(wqusb,module_id,NULL_PARAM,buffer,5) ; 
	//usleep(10000) ;
	
	// datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;
	// if (datar_length >0 ) 	ReadDataFromFifo(wqusb,datar,datar_length ) ;
	// printf("datar => ") ;
	// for (int i = 0;i<datar_length;i++)
	//	printf("0x%x ",datar[i]) ;
	// printf("\n") ;
	
	while (line_received < 840) // max 839 (ii++==839)  return 0
		{
		while (datar_length  < (86))
			{
			datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) ;
			//if (timout++ > 1000000) 
			if (timout++ > TIMEOUT*10000) 
				{
				printf("\033[22;31mTimout detected Module_%d line_received = %d datar_length = %d\033[22;30m\n",module_id,line_received,datar_length) ;
				if (datar_length >0 ) 	ReadDataFromFifo(wqusb,datar,datar_length*4 ) ;
				printf("Timeout datar => ") ;
				for (int i = 0;i<(datar_length*4);i++)
					printf("0x%x ",datar[i]) ;
				printf("\n") ;
				return -1 ;
				}
			}
		timout = 0 ;
		datar_length = (88) ;
		ReadDataFromFifo(wqusb,datar,datar_length ) ;
		
		// printf("datar => ") ;
		//		for (int i = 0;i<datar_length;i++)
		//			printf("0x%x ",datar[i]) ;
		//		printf("\n") ;
		
		FillMatrixFromDatar(datar,matrix,datar_length)  ;
		line_received++ ;
		datar_length = 0 ;
	   	}
		
		
	//for (i=0;i<MAX_MODULE;i++)
	//	{
	
	//	for (prow = 0;prow < 120; prow++)
	//		{
	//		for (pchip = 0;pchip < 80; pchip++)
	//			{
	//			if (data_log[0][prow][pchip] == 0)  printf("\033[22;31mERROR : Row = %d Chip = %d => NO DATA\033[22;30m\n",prow,pchip) ;
	//			if (data_log[0][prow][pchip] > 1)  printf("\033[22;31mERROR : Row = %d Chip = %d data_log = %d => TOO MUCH DATA\033[22;30m\n",prow,pchip,data_log[0][prow][pchip]) ;
	//			}
	//		}
	
	//	}
		
return 0 ;
}
*/

/**
* \fn int FillMatrixFromDatar(int datar[],int matrix[][(80*7)],int datar_length)
* \brief  Change the format of the data tab, form a 1 dim tab to a matrix 
*
* \param datar[] source tab
* \param matrix[][80*7] target tab
* \param datar_length The length of the source tab
* \return 0
*/
int FillMatrixFromDatar(int datar[],int matrix[][(80*7)],int datar_length)
{

/*
int row, chip ;
int ii = 0 ;
int p_data ;
int index = 0 ;
int first_pos = 1 ;
int pheader = 0 ;
int pmodule = 0 ;
*/

int module_fifo ;
int chip_fifo ;
int row_fifo ;
int col_fifo ;
int value_fifo ;
int y ;
int x ;
int pcol = 0 ;
int pheader ;


int error = 0 ;
 // 0x00 	0xaa55 		0xMod_Id 	0xNb_Words 		0xChip_Id 	0xLine_Id  	0xdata ---------- 0xdata 0xf0f0 0xf0f0 0x0
 //             	header 		
 
	
	for (int kk = 0 ; kk<datar_length; kk++)
		   {
		   if (datar[kk] == HEADER) 
		      	{
			error = 0 ;
			pheader = kk ;

			module_fifo =  (datar[pheader+1]  & 0x000f) - 1 ;
			if (module_fifo > 8) 
				{
				printf("\033[22;31mERROR : Bad Module_id , return val = %d\033[22;30m\n",module_fifo) ;
				module_fifo = 0 ;
				error = -1 ;
				}
				
			chip_fifo = datar[pheader+3]  & 0x000f ;
			if (chip_fifo > 7) 
				{
				printf("\033[22;31mERROR : Bad Chip_id , return val = %d\033[22;30m\n",chip_fifo) ;
				chip_fifo = 0 ;
				error = -1 ;
				}
				
			row_fifo = datar[pheader+4]  & 0x00ff ;
			if (row_fifo > 120) 
				{
				printf("\033[22;31mERROR : Bad Row_id , return val = %d\033[22;30m\n",row_fifo) ;
				row_fifo = 0 ;
				error = -1 ;
				}
				
			if (error == 0)
				{
		       		for (pcol=0;pcol<datar_length;pcol++)
			    		{	
			        	col_fifo = pcol ;
			        	value_fifo = datar[pheader+5+pcol] ;
					++data_log[module_fifo][row_fifo-1][chip_fifo-1] ;
					y =  (module_fifo*120) + row_fifo -1  ;
					x = ((chip_fifo-1)*80) + col_fifo ;
					if (value_fifo == 0xaa55) 
						{
						printf("\033[22;31mWARNING : Header detected = 0x%x Module_%d Chip_%d Row_%d Col_%d Y=%d X=%d\033[22;30m\n",value_fifo,module_fifo,chip_fifo,row_fifo,col_fifo,x,y) ;
						value_fifo = 0 ;
						}
					//value_fifo_modified = value_fifo & 0xfff ;
					matrix[y][x] = value_fifo & 0xffff;
					if (pcol<datar_length)
						{
						if ((datar[(pheader+5+pcol)+1] == 0xf0f0) | (datar[(pheader+5+pcol)+1] == 0xaa55)) 
			  				pcol = datar_length ;
						}
					}
				}
			kk = pheader+5+pcol ;
			}
		    }

return 0 ;
}


int FastReadImageAndFillMatrix(CQuickUsb *wqusb,int mode,int module_msk,int matrix[][(80*7)],int index)
{
#define WRAPPER_SIZE		5 	// Header, Module_ID,Nb_Word,CRC,Trailer
//#define DATA_NB				(560*3)	// mode 3 lines
//int *datar ;
int datar[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;
int timout = 0 ;
int Frame_received = 0 ;
int pmodule ;
int nb_modules = 0 ;
int nb_header_detected = 0 ;
int nb_header_expected = 0 ;
int command = 0 ;
int MAX_R0W_NB = 0 ;
int DATA_NB = 0 ;
bool error_detected = false ;
struct timeval start ;

int inc = 0 ;
int i = 0 ;

	ResetHUBFifo(wqusb) ;
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (((module_msk >> pmodule) & 0x01) == 1) 
			{
			nb_modules++ ;
			}
		}
	
	if (mode == MODE_READ_DACL)	
		{
		MAX_R0W_NB = 120/ 3 ;  // lecture par bloc de 3 lignes ;
		DATA_NB = (80*7) * 3 ;
		nb_header_expected = nb_modules*3 ;
		command = READ_DACL ;
		WriteWordToHub(wqusb,NULL_PARAM,FAST_READ_IMAGE_16b) ; // Necessaire pour la HUB de connaitre le format des données 16 ou 32 bit
		}
	else if (mode == MODE_16b)	
		{
		
		MAX_R0W_NB = 120/ 3 ;  // lecture par bloc de 3 lignes ;
		DATA_NB = (80*7) * 3 ;
		nb_header_expected = nb_modules*3 ;
		command = FAST_READ_IMAGE_16b ;
		WriteWordToHub(wqusb,NULL_PARAM,command) ; // Necessaire pour la HUB de connaitre le format des données 16 ou 32 bit
				
		// lecture mode 16b ligne par ligne
		/*
		MAX_R0W_NB = 120 ;  // lecture par bloc de 3 lignes ;
		DATA_NB = (80*7) ; // lsb et msb
		nb_header_expected = nb_modules ;
		command = FAST_READ_IMAGE_16b ;
		*/
		}
	else if (mode == MODE_32b) 
		{
		MAX_R0W_NB = 120 ;  // lecture  ligne par ligne ;
		DATA_NB = (80*7) * 2 ; // lsb et msb
		nb_header_expected = nb_modules ;
		command = FAST_READ_IMAGE_32b ;
		WriteWordToHub(wqusb,NULL_PARAM,command) ; // Necessaire pour la HUB de connaitre le format des données 16 ou 32 bit
		}
	

	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	
	buffer[0] = command ; 
	buffer[1] =  index ;
	buffer[2] =  0 ;
	buffer[3] =  0 ;
	buffer[4] =  0 ;
	
	//printf("Begin fast Read image \n") ;
	//printf("write words to module  => command=0x%x module_msk=0x%x nb_modules=%d\n",command,module_msk,nb_modules) ;
	
	
	gettimeofday(&start,NULL) ;
	
	WriteWordsToModule(wqusb,module_msk,NULL_PARAM,buffer,5) ; 
	//datar = (int*) malloc(((DATA_NB+WRAPPER_SIZE)*nb_modules)*sizeof(*datar)) ;	
	
	while (Frame_received < MAX_R0W_NB)
		{
		error_detected = false ;
		inc = 0 ;
		while (datar_length  < (DATA_NB*nb_modules))
			{
			datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) ;
			//printf("%d  : data_length = %d \n",inc,datar_length) ;
			if (timout++ >(TIMEOUT*100)) // corresponding to 250us*500=1.25s
				{
				error_detected = true ;
				if (datar_length > 0 ) 
					{
					BARRETTE_MSG("\033[22;31mTimout detected => fifo_datar_length = %d, data_length_expected = %d \033[22;30m\n",datar_length,(DATA_NB*nb_modules)) ;
					ReadDataFromFifo(wqusb,datar,datar_length ) ;
					
					printf("Timeout datar => ") ;
					for (int i = 0;i<datar_length;i++)
						printf("0x%x ",datar[i]) ;
					printf("\n") ;
					
					//return -(datar_length) ;
					}
				else
					{
					BARRETTE_MSG("\033[22;31mTimout detected => FIFO empty\033[22;30m\n") ;
					return -1 ;
					}
				
				}
			}
		
		//datar = (int*) malloc(datar_length*sizeof(*datar)) ;
		if (datar_length > 0 ) 	
			ReadDataFromFifo(wqusb,datar,datar_length) ;

		//printf ("Read Data from FIFO datar_length = %d \n",datar_length) ;

/*
		if (RESET_HUB_BETWEEN_CONTINUE)
			ResetHUBFifo(wqusb) ;
*/		
		/*
		printf("Frame_nb = %d datar_length = %d\n",Frame_received,datar_length) ;
		if (datar_length > 0)
			{
			for (i=0;i<datar_length;i++)
				printf("0x%x ",datar[i]) ;
			printf("\n") ;
			}
		*/
		
		nb_header_detected = FillMatrixFromFastDatar(mode,datar_length,datar,matrix)  ;
		//printf ("FillMatrixFromFastDatar => nb_header_detected = %d \n",nb_header_detected) ;
		if (nb_header_detected != nb_header_expected) // mode 3 lines
			{
	 		BARRETTE_MSG("Error nb_header_detected = %d ; nb_header_expected = %d ; nb_modules = %d \n",nb_header_detected,nb_header_expected,nb_modules) ;
			printf("FIFO Datar\n") ;
			for  (i=0;i<datar_length;i++)
				printf("%x ",datar[i]) ;
			printf("\n") ;
			}
		//else
		//	printf("nb_header_detected = %d  \n",nb_header_detected) ; 
		Frame_received++ ;
		datar_length = 0 ;
		//free(datar) ;
		WriteWordToModule(wqusb,module_msk,NULL_PARAM,CONTINUE) ;
		//printf("Send continue\n") ;
		timout = 0 ;
	   	}		
		
	//printf("Fast Read image complete\n") ;
	//free(datar) ; 

if (error_detected == false)
	{
	
	//current = clock() ;
	//elapsed_time = (double) (((current-start)/CLOCKS_PER_SEC)*1000) ; //convertie mseconde
	//elapsed_time = (clock_t) (((current-start)/clk_tck)*1000) ; //convertie mseconde
	//BARRETTE_MSG("elapsed_time to download image = %u us \n",(clock_t) (current-start)/(double) CLOCKS_PER_SEC)  ;
	//gettimeofday(&end,NULL) ;
	//elapsed_time = (unsigned long int) (((end.tv_sec*1000000)+end.tv_usec)-((start.tv_sec*1000000)+start.tv_usec)) ;
	BARRETTE_MSG("elapsed_time to download image = %d us \n",elapsed_time_us(start))  ;
	//if (elapsed_time < 0)
		//BARRETTE_MSG("start_time = %d ; end_time = %d ; elapsed_time = %d\n",(unsigned long int) ((start.tv_sec*1000000)+start.tv_usec),(unsigned long int) ((end.tv_sec*1000000)+end.tv_usec),elapsed_time)  ;	
	return 1 ;
	}
else
	return -(datar_length) ;
	
#undef WRAPPER_SIZE
}



int SecureFastReadImageAndFillMatrix(CQuickUsb *wqusb,int mode,int module_msk,int matrix[][(80*7)],int index)
{
int status = 0 ;

	status = FastReadImageAndFillMatrix(wqusb,mode,module_msk,matrix,index) ;
	if (status < 0) 
	    {
    	    ResetModule(wqusb,module_msk) ;
	    printf("\033[22;31mWARNING BAD IMAGE: Reset HUB and modules then waiting for 10s\033[22;30m\n") ;
	    ResetHUBFifo(wqusb) ;
	    usleep(10000000) ;
	    printf("\033[22;31mWARNING: Try to reload image\033[22;30m\n") ;
	    status = FastReadImageAndFillMatrix(wqusb,mode,module_msk,matrix,index) ;
	    }
return status ;
}
		




int FillMatrixFromFastDatar(int mode,int datar_length,int datar[],int matrix[][(80*7)])
{

int module_id_fifo ;
int row_id_fifo ;
int value_fifo_lsb ;
int value_fifo_msb ;
int value_fifo = 0 ;
int y ;
int x ;
int pheader ;
int nb_header_detected = 0 ;
int tab_index = 0 ;
int error = 0 ;
int true_col ;
int col_mode = COL_NSWAP ;
 
	// format = [<header><nb_word><module_id><row_id><datax560><CRC><trailer>] x nb_modules
	for (int kk = 0 ; kk<datar_length; kk++)
		{
		   if (datar[kk] == HEADER) 
		      	{
			pheader = kk ;
			module_id_fifo =  (datar[pheader+1]  & 0x000f) - 1 ;
			//nb_word_fifo = datar[pheader+2]  ;
			if (module_id_fifo > 8) 
				{
				printf("\033[22;31mERROR : Bad Module_id, bad value = %d forced to 8\033[22;30m\n",module_id_fifo) ;
				module_id_fifo = 8 ;
				//error = -1 ;
				}	
			row_id_fifo = datar[pheader+4]  & 0x00ff ;
			if (row_id_fifo > 120) 
				{
				printf("\033[22;31mERROR : Bad Row_id, bad value = %d forced to 120\033[22;30m\n",row_id_fifo) ;
				row_id_fifo = 120 ;
				//error = -1 ;
				}
			
			if (error == 0)
				{
				nb_header_detected++ ;
				for (int pchip=0;pchip<MAXCHIP;pchip++)
			    		{	
		       			for (int pcol=0;pcol<MAXCOL;pcol++)
			    			{	
						if ((mode == MODE_16b) | (mode ==  MODE_READ_DACL))
							{
							if (col_mode == COL_SWAP)
								true_col = (MAXCOL-1)-pcol ;
							else
								 true_col = pcol ;
			        			tab_index = pheader+5+true_col+(MAXCOL*pchip) ; 
							value_fifo = datar[tab_index] ;
							/*
							if (mode == MODE_16b)
			        				value_fifo = datar[tab_index] ;
							else if (mode ==  MODE_READ_DACL)
								value_fifo = datar[tab_index] & 0x1f8 ;
							*/
							}
						if (mode == MODE_32b)
							{
			        			tab_index = pheader+5+(pcol*2)+(MAXCOL*pchip*2) ;
							// change by LSB first 
			        			value_fifo_lsb = datar[tab_index] ;
							value_fifo_msb = datar[tab_index+1] ;
							value_fifo = (value_fifo_msb << 16) + value_fifo_lsb ;
							}
						y =  (module_id_fifo*120) + row_id_fifo -1  ;
						x = (pchip*80) + pcol ;
						matrix[y][x] = value_fifo ;
						
						//if ((pchip == 0) & (pcol == 0))
							//printf("msb = 0x%x lsb = 0x%x value = %d\n",value_fifo_msb,value_fifo_lsb,value_fifo) ;
						//	printf("%d : pchip = %d pcol = %d msb = 0x%x lsb = 0x%x value = %d\n",inc ,pchip,pcol,value_fifo_msb ,value_fifo_lsb,value_fifo) ;
						
						}
					}
					
				}
			//kk = pheader+3+tab_index - 1 ;
			kk = tab_index ;
			error = 0 ;
			}
		}

return nb_header_detected ;
}


/*
int FillMatrixFromDatar(int datar[],int matrix[][(80*7)],int datar_length)
{

//int row, chip ;
//int ii = 0 ;
//int p_data ;
//int index = 0 ;
//int first_pos = 1 ;
//int pheader = 0 ;
//int pmodule = 0 ;


int module_fifo ;
int chip_fifo ;
int row_fifo ;
int col_fifo ;
int value_fifo ;
int y ;
int x ;
int pcol ;
int pheader ;


int error = 0 ;
 // 0x00 	0xaa55 		0xMod_Id 	0xNb_Words 		0xChip_Id 	0xLine_Id  	0xdata ---------- 0xdata 0xf0f0 0xf0f0 0x0
 //             	header 		
 
	
	for (int kk = 0 ; kk<datar_length; kk++)
		   {
		   if (datar[kk] == HEADER) 
		      	{
			pheader = kk ;
			module_fifo =  (datar[pheader+1]  & 0x000f) - 1 ;
			chip_fifo = datar[pheader+3]  & 0x000f ;
			row_fifo = datar[pheader+4]  & 0x00ff ;
		       	for (pcol=0;pcol<datar_length;pcol++)
			    	{	
			        col_fifo = pcol ;
			        value_fifo = datar[pheader+5+pcol] ;
				++data_log[module_fifo][row_fifo-1][chip_fifo-1] ;
				if (module_fifo > 8) 
					{
					printf("\033[22;31mERROR : Bad Module_id , return val = %d\033[22;30m\n",module_fifo) ;
					module_fifo = 0 ;
					error = -1 ;
					}
				if (chip_fifo > 7) 
					{
					printf("\033[22;31mERROR : Bad Chip_id , return val = %d\033[22;30m\n",chip_fifo) ;
					chip_fifo = 0 ;
					error = -1 ;
					}
				if (row_fifo > 120) 
					{
					printf("\033[22;31mERROR : Bad Row_id , return val = %d\033[22;30m\n",row_fifo) ;
					row_fifo = 0 ;
					error = -1 ;
					}
					
				if (error == 0)
					{
					y =  (module_fifo*120) + row_fifo -1  ;
					x = ((chip_fifo-1)*80) + col_fifo ;
					if (value_fifo < 0) 
						{
						printf("\033[22;31mWARNING : Negative value = 0x%x Module_%d Chip_%d Row_%d Col_%d Y=%d X=%d\033[22;30m\n",value_fifo,module_fifo,chip_fifo,row_fifo,col_fifo,x,y) ;
						value_fifo = 0 ;
						}
					matrix[y][x] = value_fifo ;	
					}
					
				error = 0 ;

				  if (datar[(pheader+5+pcol)+1] == 0xf0f0) 
				  	{
					pcol = datar_length ;
				  	}
				}
			kk = pheader+5+pcol ;
			
			}
		    }

return 0 ;
}
*/

/**
* \fn int WaitingForMessage(CQuickUsb *wqusb,int Message)
* \brief  Make a loop until a specific message is found 
*
* \param *wqusb Pointer to the usb interface 
* \param Message the message we want to detect
* \return 0
*/
int WaitingForMessage(CQuickUsb *wqusb,int Message)
{
int datar[MAX_RFIFO_SIZE];

	datar[FIRST_WORD] = 0x00 ;
	while (datar[FIRST_WORD] != Message)
		{
		//usleep(100) ;
		ResetSignal(wqusb,LVDS_RFIFO_ACLR) ;
		ReadFromModule(wqusb,datar,0,4) ;
				
		}

return 0 ;
}



/**
* \fn int WaitingForHandShakeMessage(CQuickUsb *wqusb,int handshakeword)
* \brief  Make a loop, polling on the RFIFO then read the contains
*
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <HandShakeword 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.
* If the HandWord specify is not found a timout is generated
* \param *wqusb Pointer to the usb interface 
* \param handshakeword the word we want to detect
* \return 0
*/
int WaitingForHandShakeMessage(CQuickUsb *wqusb,int handshakeword)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;
int datar_length = 0 ;
int HandShake_Val = 0 ;
int module_id = 0;

	while (1)
		{
		datar_length  = ReadFromModule(wqusb,datar,(TIMEOUT*1000),4) ;
		if (datar_length > 0) 
			{
			HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
			/*
			BARRETTE_MSG("%d : HandShakeConfigAllG datar =>  ",continue_number++) ;
			
			for (i=0;i<datar_length;i++)
				printf("0x%x ",datar[i]) ;
			printf("\n") ;
			*/
			if (HandShake_Val == handshakeword)	return module_id ;
			else return -1 ;
			}
		if (cnt++ == (TIMEOUT*10000))
			return -1;
		}
}


/*
int WaitingForHandShakeImageUSEnded(CQuickUsb *wqusb)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;
int datar_length = 0 ;
int HandShake_Val = 0 ;
int module_id = 0;
int i ;

	while (1)
		{
		datar_length  = ReadFromModule(wqusb,datar,(TIMEOUT*1000),4) ;
		if (datar_length > 0) 
			{
			HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
			BARRETTE_MSG("%d : HandShakeImageUS datar =>  ",continue_number++) ;
			for (i=0;i<datar_length;i++)
				printf("0x%x ",datar[i]) ;
			printf("\n") ;
			if (HandShake_Val == IMAGE_US_ENDED)	return module_id ;
			else return -2 ;
			}
		if (cnt++ == (TIMEOUT*1000000))
			return -1;
		}
}
*/


/**
* \fn int WaitingForHandShakeImageUSEnded(CQuickUsb *wqusb)
* \brief Waiting for a HandShake from a module after sending a ImageUS Command
*
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1141 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.
* \param *wqusb Pointer to the usb interface 
* \return -1 if failed, HandShake word otherwise
*/
// void RecupererHeure()
// {
// time_t timer1;
// time(&timer1);
// int secondes, minutes, heures;
// struct tm *newTime1;
// newTime1 = localtime(&timer1);
// heures = newTime1->tm_hour; // Les heures sont dans "heures"
// minutes = newTime1->tm_min; // Les minutes sont dans "minutes"
// secondes = newTime1->tm_sec; // Les secondes sont dans "secondes"
// } 
int WaitingForHandShakeImageUSEnded(CQuickUsb *wqusb,long int image_time,int nb_image, int *progress_acq)
{
#define SIZE_MSG 12
int datar[MAX_RFIFO_SIZE] ;
int datar_length = 0 ;
int HandShake_Val = 0 ;
int module_id = 0;
int i = 0 ;
struct timeval start ; 
unsigned long int elapsed_time ;


	
	gettimeofday(&start,NULL) ;
	image_time = (image_time*nb_image*3) + 1000 ; // 3xImage_time + 1s image_time en mseconde
	while (1)
		{
		datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;   
	
		if (datar_length > 6) 
			{
			//current = clock() ;	
			//ldatar =  (int*) malloc(datar_length*sizeof(*ldatar)) ;
			//HandShake_Val = ExtractHandShakeVal(datar,datar_length,&module_id) ;
			datar_length  = ReadFromModule(wqusb,datar,TIMEOUT,4) ;
			HandShake_Val =  ExtractHandShakeVal_ImageUS(datar,datar_length,&module_id,progress_acq) ;
			//free(ldatar) ;
			/*
			BARRETTE_MSG("HandShakeImageUS datar =>  ") ;
			for (i=0;i<datar_length;i++)
				printf("0x%x ",datar[i]) ;
			printf("\n") ;
			*/
			//HandShake_Val = ExtractHandShakeValImageUS(datar,datar_length,&module_id,&Index_Img) ;
			    //************* 	A implementer	*************
			//HandShake_Val = ExtractHandShakeWord(datar,datar_length,&module_id) ;
			//*******************************************************************
			/*
			BARRETTE_MSG("%d : HandShakeImageUS datar =>  ",continue_number++) ;
			*/
			/*
			if (HandShake_Val == IMAGE_US_RECV)	
				{
				BARRETTE_MSG("Receive HandShakeImageUS command\n") ;	
				//if (datar_length > 6)	free(datar) ;
				return   IMAGE_US_RECV;
				}
			*/

			if (HandShake_Val == IMAGE_US_ENDED)	
				{
				//elapsed_time = (clock_t) (((current-start)/clk_tck)*1000) ; //convertie mseconde
				BARRETTE_MSG("Receive HandShakeImageUSEnded => fifo_len=%d word=0x%x \n",datar_length,HandShake_Val) ;	
				if (image_time < 60000)
				   {
				   elapsed_time = elapsed_time_ms(start) ;
	         		   BARRETTE_MSG("Timout_image ((3*image_time)+1s) = %u ms elapsed_time = %u ms \n",(unsigned int) image_time,(unsigned int) elapsed_time)  ;
				   }
				else
				  {
				   elapsed_time = elapsed_time_s(start) ;
	         		   BARRETTE_MSG("Timout_image ((3*image_time)+1s) = %u s elapsed_time = %u s \n",(unsigned int) (image_time/1000),(unsigned int) elapsed_time)  ;
				   }
				return   IMAGE_US_ENDED;
				}
			if (HandShake_Val == READY_FOR_GATE)	
				{
				//elapsed_time = (clock_t) (((current-start)/clk_tck)*1000) ; //convertie mseconde
				BARRETTE_MSG("Receive ReadyForGate => fifo_len=%d word=0x%x \n",datar_length,HandShake_Val) ;	
				if (image_time < 60000)
				   {
				   elapsed_time = elapsed_time_ms(start) ;
	         		   BARRETTE_MSG("Timout_image ((3*image_time)+1s) = %u ms elapsed_time = %u ms \n",(unsigned int) image_time,(unsigned int) elapsed_time)  ;
				   }
				else
				  {
				   elapsed_time = elapsed_time_s(start) ;
	         		   BARRETTE_MSG("Timout_image ((3*image_time)+1s) = %u s elapsed_time = %u s \n",(unsigned int) (image_time/1000),(unsigned int) elapsed_time)  ;
				   }
				return   READY_FOR_GATE;
				}
			if ((HandShake_Val != READY_FOR_GATE) & (HandShake_Val != IMAGE_US_ENDED))	
				{
				for (i=0;i<datar_length;i++)
				printf("0x%x ",datar[i]) ;
				printf("\n") ;
				}
				
			}
		/*
		current = clock() ;	
		elapsed_time = (clock_t) (((current-start)/clk_tck) *1000) ; //convertie seconde
BARRETTE_MSG("elapsed_time to download image = %d us \n",elapsed_time_ms(start)
		*/
		if (image_time < 60000)
		   {
		   elapsed_time = elapsed_time_ms(start) ;
		   if(elapsed_time > image_time) 
			{
			datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;   
			BARRETTE_MSG("\033[22;31mTIMOUT detected => elapsed_time(%u ms) > image_time(%u ms) fifo_len = %d\033[22;30m\n",(unsigned int) elapsed_time,(unsigned int) image_time,datar_length) ;
			return -1 ;	
			}
		   }
		else
		   {
		   elapsed_time = elapsed_time_s(start) ;
		   if(elapsed_time > image_time/1000) 
			{
			datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;   
			BARRETTE_MSG("\033[22;31mTIMOUT detected => elapsed_time(%u s) > image_time(%u s) fifo_len = %d\033[22;30m\n",(unsigned int) elapsed_time,(unsigned int) (image_time/1000),datar_length) ;
			return -1 ;	
			}	
		    }
		}	
#undef SIZE_MSG 
}



/**
* \fn int HandShakeConfig_ith_dacl(CQuickUsb *wqusb,int wmodule,int wchip,int *value)
* \brief Function used only for the tests
*
* Expect Data from the HUB => <0xaa55> <module_id> <0xa> <0x1381 0x0 0x0 0x0 0x0 0x0 0x0 0x0 0x0> <0xf0f0>.
* \param *wqusb Pointer to the usb interface 
* \param wmodule
* \param wchip
* \param *value
* \return 
*/
int HandShakeConfig_ith_dacl(CQuickUsb *wqusb,int wmodule,int wchip,int *value)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;

	datar[FIRST_WORD] = 0x00 ;
	while (datar[FIRST_WORD] != LOAD_DACL_ITH_ACK)
		{
		Config_ith_dacl(wqusb,wmodule,wchip,value) ;
		ReadFromModule(wqusb,datar,(TIMEOUT*1000),4) ;
		if (cnt++ == (TIMEOUT*100))
			return -datar[FIRST_WORD];
		}
return 0 ;
}


/**
* \fn int WriteDummyData(CQuickUsb *wqusb,int wmodule,int scan_chip,int scan_row)
* \brief Function used only for the tests
*
* \param *wqusb Pointer to the usb interface 
* \param wmodule
* \param scan_chip
* \param scan_row
* \return 
*/
//  Zn+1 = Zn2 + C
int WriteDummyData(CQuickUsb *wqusb,int wmodule,int scan_chip,int scan_row)
{
int buffer[MAX_RFIFO_SIZE] ;
int i ;
			//printf("scan_chip %d scan_row %d \n",scan_chip, scan_row) ;
			//buffer[0] = 84 ;
			//buffer[1] = (scan_chip+1) ;
			buffer[0] = (scan_row+1) ;
			/*
			for (i=2;i<82;i++)
				{
				x = (i-2) + (80*scan_chip) ;
				y = scan_row + (120*wmodule) ;
			*/	
			for (i=1;i<83;i++)	
				buffer[i] = i + scan_row + (80*scan_chip) + (120*wmodule);
				//buffer[i] =  i ;
	    	//DAQ_MSG("WriteDummyData\033[22;30m\n" ) ;
			//printf("WriteDummyData i = %d\n",i ) ;
			//printf("WriteDummyData => data = ") ;
			WriteWordsToModule(wqusb,0,(scan_chip+1),buffer,i) ;	
return 0 ;
}


int WriteConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister,int value)
{
int status = -1 ;
int timout = 0 ;
int loop = 0 ;


		while (status == -1)
			{
			loop = 0 ;
			ConfigG(wqusb,wmodule,wchip,wregister,value) ;
			while (loop<100)
				{
				status = WaitingForHandShakeConfigG(wqusb) ;
				//status = WaitingForHandShakeConfigG(wqusb,wmodule,wchip,wregister) ;
				if (status == CONFIG_G_RECV)  return ((timout*100)+loop) ;
				}
			timout++ ;
			if (timout++ > 10) return -1 ;
			}
return 0 ;
}


int SendWriteConfigGToModuleReady(CQuickUsb *wqusb,int *module_ready,int wchip,int wregister,int value,int fastReadout)
{
int pmodule ;
int wmodule ;
int status = 0 ;
int module_msk = 0;

/*
 for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1; 
			module_msk += 1 <<  pmodule ;
			}
		}
			
	if (fastReadout)
		{
		WriteConfigG(wqusb,module_msk,wchip,wregister,value) ;	
		}
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)
			{
			wmodule = pmodule + 1;
			WriteConfigG(wqusb,wmodule,wchip,wregister,value) ;
			}
		}
*/		

	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1; 
			module_msk = 1 <<  pmodule ;
			// printf("pmodule = %d module_msk = %d \n",pmodule,module_msk ) ;
			//if (fastReadout)
			WriteConfigG(wqusb,module_msk,wchip,wregister,value) ;	
			//printf("SendWriteConfigGToModuleReady module_msk = 0x%x wchip = %d wregister = 0x%x value = %d \n",module_msk,wchip,wregister,value) ;
			//else
			//	WriteConfigG(wqusb,wmodule,wchip,wregister,value) ;
			}
		}
return status ;
}

int WriteFlatConfig(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int value)
{
int status = -1 ;
int timout = 0 ;
int loop = 0 ;
		

		while (status == -1)
			{
			loop = 0 ;
			FlatConfig(wqusb,wmodule,wchip,mode,value) ;
			while (loop<100)
				{
				status = WaitingForHandShakeFlatConfig(wqusb)  ;
				//if (status == wmodule)  return ((timout*100)+loop) ;
				if (status == FLAT_CONFIG_RECV)  return ((timout*100)+loop) ;
				}
			timout++ ;
			if (timout++ > 10) return -1 ;
			}
return 0 ;
}

int WriteFlatConfigFastReadout(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int value)
{
int status = -1 ;
int timout = 0 ;
int loop = 0 ;
		

		while (status == -1)
			{
			loop = 0 ;
			FlatConfig(wqusb,wmodule,wchip,mode,value) ;
			while (loop<100)
				{
				status = WaitingForHandShakeFlatConfigFastReadout(wqusb)  ;
				//if (status == wmodule)  return ((timout*100)+loop) ;
				if (status == FLAT_CONFIG_RECV)  return ((timout*100)+loop) ;
				}
			timout++ ;
			if (timout++ > 10) return -1 ;
			}
return 0 ;
}

int WaitingForHandShakeFlatConfigFastReadout(CQuickUsb *wqusb)
{
int datar[MAX_RFIFO_SIZE];
int cnt = 0 ;
int datar_length = 0 ;
int handshakeVal[MAXMODULE] ;
int module_id[MAXMODULE] ;
int nb_header_detected = 0 ;



	while (1)
		{
		datar_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) ;
		if (datar_length > 8) 
			{
			datar_length  = ReadFromModule(wqusb,datar,TIMEOUT,8) ;
			nb_header_detected = ExtractHandShakeValuesAndModuleID(datar,datar_length,module_id,handshakeVal) ;
			for (int i = 0;i<nb_header_detected;i++)
				BARRETTE_MSG("moduleID[%d] = %d hadshakeVal[%d] = %d\n ",i,module_id[i],i,handshakeVal[i]) ;
			return 1 ;
			/*
			if (handshakeVal[0] == FLAT_CONFIG_RECV)	return module_id[0] ;
			else return -handshakeVal ;
			*/
			}
		else
			{
			if (cnt++ == (TIMEOUT*100000))
				{
				BARRETTE_MSG("TIMOUT HandShakeFlatConfig  ") ;
				return -1;
				}
			}
		}
}


int SendFlatConfigValToModuleReady(CQuickUsb *wqusb,int *module_ready,int value,int fastReadout)
{
int pmodule ;
int wmodule ;
int status = 0 ;
int module_msk ;

	
/*
	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1; 
			module_msk += 1 <<  pmodule ;
			}
		}
	if (fastReadout)
		{
		WriteFlatConfigFastReadout(wqusb, module_msk,0x7f,ALL_CHIP,value) ;
		}
	else
		{
		for (pmodule=0;pmodule<MAXMODULE;pmodule++)
			{
			if (module_ready[pmodule] == 1)	
				{
				WriteFlatConfig(wqusb, wmodule,0x7f,ALL_CHIP,value) ;
				}
			}
		}
*/	

	for (pmodule=0;pmodule<MAXMODULE;pmodule++)
		{
		if (module_ready[pmodule] == 1)	
			{
			wmodule = pmodule + 1; 
			module_msk = 1 <<  pmodule ;
			if (fastReadout)
				WriteFlatConfig(wqusb, module_msk,0x7f,ALL_CHIP,value) ;
			else
				WriteFlatConfig(wqusb, wmodule,0x7f,ALL_CHIP,value) ;
			}
		}

return status  ;
}





int SendConfigG_DefaultValue(CQuickUsb *wqusb,int wmodule)
{
#define lSelect current 	0
 #define lCMOS_dis 		1
 #define lITHH			2
 #define lVADj			3
 #define lVRef			4
 #define lIMFP			5
 #define lIOTA			6
 #define lIPRE			7
 #define lITHL			8
 #define lITUNE			9
 #define lIBuffer			10

/*	
for (i=0;i<MAXMODULE;i++)
for (j=0;j<MAXCHIP;j++)
cmos_dis[i][j] = 0 ;
ithh_val[i][j] = 0 ;
vadj_val[i][j] = 0 ;
vref_val[i][j] = 0 ;
imfp_val[i][j] = 52 ;
iota_val[i][j] = 40 ;
ipre_val[i][j] = 60 ;
ithl_val[i][j] = 22 ;
itune_val[i][j] = 145 ;
ibuffer_val[i][j] = 0 ;
*/


int buffer[11] ;
int wchip ;

	for (wchip = 0;wchip < MAXCHIP; wchip++)
		{	
		buffer[0] = 0 ; //cmos_dis[(MODULE_1-1)][wchip]  ;
		buffer[1] = 0  ;
		buffer[2] = 0 ; //ithh_val[(MODULE_1-1)][wchip]  ;
		buffer[3] = 0 ; //vadj_val[(MODULE_1-1)][wchip]  ;
		buffer[4] = 0 ; //vref_val[(MODULE_1-1)][wchip]  ;
		buffer[5] = 52 ; //imfp_val[(MODULE_1-1)][wchip]  ;
		buffer[6] = 40 ; //iota_val[(MODULE_1-1)][wchip]  ;
		buffer[7] = 60 ; //ipre_val[(MODULE_1-1)][wchip]  ;
		buffer[8] = 22 ; //ithl_val[(MODULE_1-1)][wchip]  ;
		buffer[9] = 145 ;//itune_val[(MODULE_1-1)][wchip]  ;
		buffer[10] = 0 ; //ibuffer_val[(MODULE_1-1)][wchip]  ;
		ConfigAllG(wqusb,wmodule,wchip,buffer) ;
		WaitingForHandShakeConfigAllG(wqusb) ;
		}

/*
#undef lSelect current 	
#undef lCMOS_dis 		
#undef lITHH			
#undef lVADj			
#undef lVRef			
#undef lIMFP		
#undef lIOTA			
#undef lIPRE			
#undef lITHL		
#undef lITUNE			
#undef lIBuffer	
*/

return 0 ;
}


int testFunction(int value)
{
int i = 0 ;
struct timeval start, end ;
	
	gettimeofday(&start,NULL) ;
	for (i=0;i<value;i++) ;
	gettimeofday(&end,NULL) ;
	BARRETTE_MSG("elapsed_time to download image = %d us \n",(end.tv_usec-start.tv_usec))  ;
return 0 ;
}
