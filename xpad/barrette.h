/**
* \file barrette.h
* \brief The header file of barrette.cpp
* \author Patrick BREUGNON
* \version 1.0
* \date April 08 2008
 */

#ifndef _BARRETTE_H_
#define _BARRETTE_H_
/*
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <fstream.h>
*/
#include <stdio.h>
#include <stdlib.h>
#include "CQuickUsb.h" 




int DummyMsg(CQuickUsb *wqusb) ;
int ResetHUBFifo(CQuickUsb *wqusb) ;
int ResetModule(CQuickUsb *wqusb,int wmodule) ;
int ReconfigModule(CQuickUsb *wqusb,int wmodule) ;

int AskReady(CQuickUsb *wqusb,int wmodule,int *addr_return) ;
int AskReadyModule(CQuickUsb *wqusb,int wmodule,int *add_return) ;

void ConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister,int value) ;
//int WaitingForHandShakeConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister) ;
int WaitingForHandShakeConfigG(CQuickUsb *wqusb) ;

void ConfigAllG(CQuickUsb *wqusb,int wmodule,int wchip,int *value) ;
int WaitingForHandShakeConfigAllG(CQuickUsb *wqusb) ;

void FlatConfig(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int config) ;
int WaitingForHandShakeFlatConfig(CQuickUsb *wqusb) ;

int WriteFlatConfigFastReadout(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int value) ;
int WaitingForHandShakeFlatConfigFastReadout(CQuickUsb *wqusb) ;


void MakeCalibration(CQuickUsb *wqusb,int wmodule,int min_ith,int max_ith,int min_dacl,int max_dacl,int type_test,int nios_delay,int nb_pixel_not_accepted,int custom_parameter) ;
int LoadDACLFromTab(CQuickUsb *wqusb,int wmodule,int dacl_tab[][80*7]) ;
int LoadDACLFromFileConfig(CQuickUsb *wqusb,int wmodule,const char *file) ;

int ReadCalib(CQuickUsb *wqusb,int FirstCommand,string DataFile,int wmodule,int cmos_dis[][7],int ithh[][7],int vadj[][7],int vref[][7],int imfp[][7],int iota[][7],int ipre[][7],int ithl[][7],int itune[][7],int ibuffer[][7]) ;

void Config_ith_dacl(CQuickUsb *wqusb,int wmodule,int wchip,int *value) ;

int TestDigitalPart(CQuickUsb *wqusb,int nbhit,int matrix[][(80*7)],int module_id)  ;
int ImageUs(CQuickUsb *wqusb,int module_id,int gate_mode,int integration_time,int index_img,int nb_image,int answer) ;
int ImageUs_X_Samples(CQuickUsb *wqusb,int module_id,int gate_mode,int integration_time,int index_img,int nb_image,int answer) ;
int WaitingForHandShakeImageUSEnded(CQuickUsb *wqusb,long int image_time,int nb_image, int *progress_acq) ;

//int ReadImage(CQuickUsb *wqusb,int FirstCommand,string DataFile,int image_pointer,int wmodule,int save_image) ;
int ReadImage(CQuickUsb *wqusb,int FirstCommand,string DataFile,int wmodule,int save_image) ;

int WaitingForMessage(CQuickUsb *wqusb,int Message) ;
int WaitingForHandShakeMessage(CQuickUsb *wqusb,int handshakeword) ;

int HandShakeConfig_ith_dacl(CQuickUsb *wqusb,int wmodule,int wchip,int *value) ;

int ReadOptoRFIFOAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int matrix[][(80*7)],int module_id) ;
int ReadImageIndexAndFillMatrix(CQuickUsb *wqusb,int FirstCommand,int module_id,int Image_index,int matrix[][(80*7)]) ;

//int FastReadImageIndexAndFillMatrix(CQuickUsb *wqusb,int module_id,int Image_index,int delay_between_data,int matrix[][(80*7)]) ;
int FillMatrixFromDatar(int datar[],int matrix[][(80*7)],int datar_length) ;

//int FastReadImageAndFillMatrix(CQuickUsb *wqusb,int command,int *module_ready,int matrix[][(80*7)]) ;
//int FastReadImageAndFillMatrix(CQuickUsb *wqusb,int command,int module_msk,int mode,int matrix[][(80*7)]) ;
int FastReadImageAndFillMatrix(CQuickUsb *wqusb,int mode,int module_msk,int matrix[][(80*7)],int index) ;
int SecureFastReadImageAndFillMatrix(CQuickUsb *wqusb,int mode,int module_msk,int matrix[][(80*7)],int index) ;
//int FillMatrixFromFastDatar(int datar_length,int datar[],int matrix[][(80*7)]) ;
//int FillMatrixFromFastDatar(int mode,int datar_length,int datar[],int matrix[][(80*7)]) ;
int FillMatrixFromFastDatar(int mode,int datar_length,int datar[],int matrix[][(80*7)]) ;

int WriteDummyData(CQuickUsb *wqusb,int wmodule,int scan_chip,int scan_row) ;

int WriteConfigG(CQuickUsb *wqusb,int wmodule,int wchip,int wregister,int value) ;
int SendWriteConfigGToModuleReady(CQuickUsb *wqusb,int *module_ready,int wchip,int wregister,int value,int fastReadout) ;

int WriteFlatConfig(CQuickUsb *wqusb,int wmodule,int wchip,int mode,int value) ;
//int SendFlatConfigValToModuleReady(CQuickUsb *wqusb,int *module_ready,int value) ;
int SendFlatConfigValToModuleReady(CQuickUsb *wqusb,int *module_ready,int value,int fastReadout) ;

int SendConfigG_DefaultValue(CQuickUsb *wqusb,int wmodule) ;
int testFunction(int value) ;


#endif
