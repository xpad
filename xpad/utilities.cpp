/**
* \file utilities.cpp
* \brief Contains all utilities functions needed in the program 
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */

#include <cstring>

#include "utilities.h"
#include "defines.h"

#ifdef UTILITIES_MSG
#define UTILITIES_MSG(fmt,args...) printf("\033[22;34mcyclone_msg :  " fmt,##args)  
#else
#define UTILITIES_MSG(fmt,args...)
#endif

/*
extern int cmos_dis[8][7] ;
extern int ithh_val[8][7] ;
extern int vadj_val[8][7] ;
extern int vref_val[8][7] ;
extern int imfp_val[8][7] ; 
extern int iota_val[8][7] ;
extern int ipre_val[8][7] ;
extern int ithl_val[8][7] ;
extern int itune_val[8][7]  ;
extern int ibuffer_val[8][7] ;
*/



using namespace std;



/**
* \fn int ConvertIntToChar(int input, unsigned char *output)
* \brief Convert a interger to two char 
*
* \param input The value you want to convert 
		 *output Array of two char, [0] for lsb word, [1] for msb word
* \return no return value
*/
int ConvertIntToChar(int input, unsigned char *output)
{
	*(output+1) = (unsigned char) ((input >> 8) & 0xFF) ;  
	*(output) = (unsigned char) (input & 0xFF) ;
	//printf("input = 0x%x, output[1] = 0x%x, output[0] = 0x%x\n",input,output[1],output[0]) ;
return 0 ;
}


int DetectTimeout(int timeout_loop,int timeout_val,int interface)
{

	usleep(1) ;
	if (timeout_loop == timeout_val)  
		{
		if (interface == LVDS_INTERFACE) 
			UTILITIES_MSG("TIMOUT: No message detected on LVDS interface, timeout val = %d\033[22;30m\n",timeout_val) ; 
		if (interface == OPTO_INTERFACE) 
			UTILITIES_MSG("TIMOUT: No message detected on OPTO interface, timeout val = %d\033[22;30m\n",timeout_val) ;
		return -1 ;
		}
	else return timeout_val ;

}

/*
void SaveDataToFile(string sFileName,int *data,int data_length,int mode)
{
#define MAX_VAL		80
int pheader ;
int pdata ;
int i ;

	QFile file(sFileName) ;
	if ( file.open( IO_WriteOnly | IO_Append ) ) 
		{
        	QTextStream stream( &file );
		//for (int i =2;i<MAX_VAL;i++)
		for (i=0;i<data_length;i++)
			if (data[i] == 43605)   pheader = i ;

		pdata = pheader + 3 ;	
		for ( i =0;i<MAX_VAL;i++)
			{
			if (data[pdata+i] == 61680) i = MAX_VAL ;
			else stream << data[pdata+i] << " " ;
			}
		stream << endl;
       		file.close();
		}
}
*/	

void SaveDataToFile(string sFileName,int *data,int data_length,int mode)
{
#define MAX_VAL		80
FILE *pFile ;
int pheader = 0 ;
int pdata ;
int i ;

	pFile = fopen(sFileName.c_str(),"w") ; 
	if (pFile != NULL ) 
		{
		for (i=0;i<data_length;i++)
			if (data[i] == 43605)   pheader = i ;

		pdata = pheader + 3 ;	
		for ( i =0;i<MAX_VAL;i++)
			{
			if (data[pdata+i] == 61680) i = MAX_VAL ;
			else  fprintf(pFile,"%d ",data[pdata+i]) ;
			}
		 fprintf(pFile,"\n");
       		fclose(pFile);
		}

#undef MAX_VAL
}

/*
void SaveConfigToFile(string sFileName,int *data,int data_length,int mode)
{
#define MAX_VAL		82
FILE *file ;
int pheader ;
int pdata ;
int i ;

	//QFile file(sFileName) ;
	file = fopen(sFileName.c_str(),"w") ; 
	if ( file.open( IO_WriteOnly | IO_Append ) ) 
		{
        	QTextStream stream( &file );
		//printf("fifo => ") ;
		for (i=0;i<data_length;i++)
			{
			//printf("0x%x ",data[i]) ;
			if (data[i] == 43605)   pheader = i ;
			}
		//printf("\n") ;
		
		pdata = pheader + 3 ;	
		//printf("data => ") ;
		for ( i =0;i<MAX_VAL;i++)
			{
			if (data[pdata+i] == 61680) i = MAX_VAL ;
			else
				{ 
				stream << data[pdata+i] << " " ;
				//printf("0x%x ",data[pdata+i]) ;
				}
			}
		stream << endl;
		//printf("\n") ;
       		//file.close();
		fclose(file)
		}
}
*/

void SaveConfigToFile(std::string sFileName,int *data,int data_length,int mode)
{
#define MAX_VAL		82
FILE *pFile ;
int pheader = 0 ;
int pdata ;
int i ;

	//QFile file(sFileName) ;
	if (mode == _APPEND)
		pFile = fopen(sFileName.c_str(),"a") ; 
	else 
		pFile = fopen(sFileName.c_str(),"w") ; 
		
	if (pFile != NULL ) 
		{
		for (i=0;i<data_length;i++)
			{
			//printf("0x%x ",data[i]) ;
			if (data[i] == 43605)   pheader = i ;
			}
		//printf("\n") ;
		
		pdata = pheader + 3 ;	
		//printf("data => ") ;
		for ( i =0;i<MAX_VAL;i++)
			{
			if (data[pdata+i] == 61680) i = MAX_VAL ;
			else
				{ 
				fprintf(pFile,"%d ", data[pdata+i]) ;
				//printf("0x%x ",data[pdata+i]) ;
				}
			}
		fprintf(pFile,"\n") ;
		//printf("\n") ;
       		//file.close();
		fclose(pFile) ;
		}

#undef MAX_VAL
}


void SaveDACToFile(std::string sFileName,int  tab_index,int cmos_dis[][7],int ithh[][7],int vadj[][7],int vref[][7],int imfp[][7],int iota[][7],int ipre[][7],int ithl[][7],int itune[][7],int ibuffer[][7])
{
int i ;
FILE *pFile ;

	pFile = fopen(sFileName.c_str(),"w") ; 
	if (pFile == NULL )  return ;
		
	fprintf(pFile,"CMOS_DIS ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",cmos_dis[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"ITHH ") ;	
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",ithh[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"VADJ ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",vadj[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"VREF ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",vref[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"IMFP ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",imfp[tab_index][i]);
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"IOTA ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",iota[tab_index][i]);
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"IPRE ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",ipre[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"ITHL ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",ithl[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"ITUNE ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",itune[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"IBUFFER ") ;
	for (i=0;i<MAXCHIP;i++)
		fprintf(pFile,"%d ",ibuffer[tab_index][i]) ;
	fprintf(pFile,"\n") ;
	fprintf(pFile,"\n") ;
	
	fprintf(pFile,"DACL") ;
	fprintf(pFile,"\n") ;
		
	 fclose(pFile);
}

void SaveDACLToFile(std::string sFileName,int *data,int data_length,int mode)
{
#define MAX_VAL		82
FILE *pFile ;
int pheader = 0 ;
int pdata ;
int i ;

	//QFile file(sFileName) ;
	if (mode == _APPEND)
		pFile = fopen(sFileName.c_str(),"a") ; 
	else 
		pFile = fopen(sFileName.c_str(),"w") ; 
		
	if (pFile != NULL ) 
		{
		for (i=0;i<data_length;i++)
			{
			//printf("0x%x ",data[i]) ;
			if (data[i] == 43605)   pheader = i ;
			}
		//printf("\n") ;
		
		pdata = pheader + 3 ;	
		//printf("data => ") ;
		for ( i =0;i<MAX_VAL;i++)
			{
			if (data[pdata+i] == 61680) i = MAX_VAL ;
			else
				{ 
				fprintf(pFile,"%d ", data[pdata+i]) ;
				//printf("0x%x ",data[pdata+i]) ;
				}
			}
		fprintf(pFile,"\n") ;
		//printf("\n") ;
       		//file.close();
		fclose(pFile) ;
		}

#undef MAX_VAL
}

/*
int ReadDataFromFile(string sFileName,int *data)
{

	QFile file(sFileName) ;
	if ( file.open( IO_ReadOnly ) ) {
        QTextStream stream( &file );
        for ( QStringList::Iterator it = lines.begin(); it != lines.end(); ++it )
		for (int i =0;i<data_length;i++)
	           stream << data[i] << "\n";
        file.close();
	}
	
return 0 ;
}
*/

int ExtractSuffix(char *path,char  *folderName)
{
string str_loc = string(path) ;
string tmp_str  ;
int i ;

	int str_len = (int) str_loc.length() ;
	//int str_len = str_len(path) ;
	for (i=0;i<str_len;i++)
		if (path[(str_len-1)-i] == '/') break ;
	//cout << "Path  = " << path << "  " << endl ;
	//cout << "i = " << i << "  " << endl ;
	tmp_str = str_loc.substr((str_len-i),str_len) ;
	//cout << "tmp_str  = " << tmp_str << endl ;
	strcpy(folderName,tmp_str.c_str()) ;
return 0 ;

}



int ExtractFileName(const char *PathAndFile,string *FileName)
{
string string_PathAndFile = string(PathAndFile) ;
string locFileName  ;
int i ;

	//cout << "WhichFile => " << WhichFile << endl ;
	int length_PathAndFile = (int) string_PathAndFile.length() ;
	// searching for the first slash 
	for (i=0;i<length_PathAndFile;i++)
		if (PathAndFile[(length_PathAndFile-1)-i] == '/') break ;
	locFileName = string_PathAndFile.substr((length_PathAndFile-i),length_PathAndFile) ;
	//*FileName = (char *) locFileName.c_str() ;
	*FileName = locFileName ;
return 0 ;
}

//int ExtractPrefixIndex(const char *PathAndFile,string *Prefix,int *index)
int ExtractPrefixIndex(const char *PathAndFile)
{
string string_PathAndFile = string(PathAndFile) ;
string locFileName  ;
int i ;

	//cout << "WhichFile => " << WhichFile << endl ;
	int length_PathAndFile = (int) string_PathAndFile.length() ;
	// searching for the first slash 
	for (i=0;i<length_PathAndFile;i++)
		if (PathAndFile[(length_PathAndFile-1)-i] == '/') break ;
	locFileName = string_PathAndFile.substr((length_PathAndFile-i),length_PathAndFile) ;
	// searching for the point
	//*FileName = (char *) locFileName.c_str() ;
	//*FileName = locFileName ;
return 0 ;
}



int ExtractPathPrefixIndex(const char *PathAndFile,string *Path,string *Prefix,int *Index)
{
string string_PathAndFile = string(PathAndFile) ;
string locFileName  ;
string locPrefixFileName  ;
string locFullPrefix ;
string locPrefix ;
string locIndex ;
string locPath ;
const char* chartmp ;
int i ;
int Index_tmp ;


	//cout << "WhichFile => " << WhichFile << endl ;
	int length_PathAndFile = (int) string_PathAndFile.length() ;
	// searching for the first slash 
	for (i=0;i<length_PathAndFile;i++)
		if (PathAndFile[(length_PathAndFile-1)-i] == '/') break ;
	locFileName = string_PathAndFile.substr((length_PathAndFile-i),length_PathAndFile) ;
	//cout << "FileName => " << locFileName.c_str() << endl ;
	int length_locFileName = locFileName.length() ;
	// searching for the second slash 
	for (i=(length_locFileName+1);i<length_PathAndFile;i++)
		if (PathAndFile[(length_PathAndFile-1)-i] == '/') break ;
	locPrefixFileName = string_PathAndFile.substr((length_PathAndFile-i),length_PathAndFile) ;
	//cout << "locPrefixFileName => " << locPrefixFileName.c_str() << endl ;
	int length_locPrefixFileName = locPrefixFileName.length() ;
	locFullPrefix = locPrefixFileName.substr(0,(length_locPrefixFileName - (length_locFileName+1))) ;
	//cout << "locFullPrefix => " << locFullPrefix.c_str() << endl ;
	//cout << "length_locPrefixFileName = " << length_locPrefixFileName << " length_PathAndFile = " << length_PathAndFile << endl ;
	locPath = string_PathAndFile.substr(0,(length_PathAndFile -length_locPrefixFileName)) ;
	//cout << "locPath => " << locPath.c_str() << endl ;
	//*Path = (char *) locPath.c_str() ;
	*Path = locPath ;
	// extract the index and Prefix => searching for the underscore 
	int length_locFullPrefix = locFullPrefix.length() ;
	chartmp = locFullPrefix.c_str() ;
	for (i=0;i<length_locFullPrefix;i++)
		if (chartmp[(length_locFullPrefix-1)-i] == '_') break ;
	locPrefix = locFullPrefix.substr(0,(length_locFullPrefix - i -1)) ;
	//cout << "locPrefix = " << locPrefix.c_str() << endl ; 
	//*Prefix = (char *) locPrefix.c_str() ;
	*Prefix = locPrefix ;
	locIndex = locFullPrefix.substr((length_locFullPrefix - i),length_locFullPrefix) ;
	//cout << "locIndex = " << locIndex.c_str() << endl ;
	std::sscanf(locIndex.c_str(),"%d",&Index_tmp) ;
	//cout << "Index = " << locIndex << endl ; 
	*Index = Index_tmp ;
	
/*
	// searching for the second "/" 
	for (j=(i+1);j<str_len;j++)
		if (WhichFile[(str_len-1)-j] == '/') break ;
	locPrefix = str_loc.substr((str_len-j),(str_len-j)) ;
	cout << "Prefix => " << locPrefix.c_str() << endl ;	
	str_prefix = (int) locPrefix.length() ;
        locPath = str_loc.substr(0,(str_len-str_prefix)) ;
	cout << "str_len = " << str_len << endl ;
	cout << "Path => " << locPath .c_str() << endl ;
	//strcpy(Path,tmp_str.c_str()) ;
*/
return 0 ;
}



int ResetMatrix(int matrix[][(80*7)])
{
	for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
		matrix[y][x] =0 ;
	   }
return 0 ;
}

int AddMatrix(int source_matrix[][(80*7)],int dest_matrix[][(80*7)])
{
	for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
		dest_matrix[y][x] += source_matrix[y][x] ;
	   }
return 0 ;
}

int CompareMatrix(int refmatrix[][(80*7)],int dmatrix[][(80*7)],int errormatrix[][(80*7)])
{
int error = 0 ;

	for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
		{
		if (dmatrix[y][x] != refmatrix[y][x])
			{
			errormatrix[y][x]++ ; 
			printf("\033[22;30mERROR refmatrix[%d][%d] = %d ; dmatrix[%d][%d]  = %d \n",y,x,refmatrix[y][x],y,x,dmatrix[y][x]) ;
			error ++ ;
			}
		} 
	   }
return error ;
}

int CopyMatrix(int source_matrix[][(80*7)],int dest_matrix[][(80*7)])
{
	for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
	   	{
		dest_matrix[y][x] = source_matrix[y][x] ;
		}
	   }
return 0 ;
}

int ExtractDACLValue(int source_matrix[][(80*7)],int dacl_matrix[][(80*7)])
{
	for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
	   	{
		dacl_matrix[y][x] = (source_matrix[y][x] & 0x1f8) >> 3;
		}
	   }
return 0 ;
}


int AddLSBMatrixandMSBMatrix(int lsb_source_matrix[][(80*7)],int msb_source_matrix[][(80*7)],int dest_matrix[][(80*7)])
{
	for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
	   	{
		// attention OVF sur 15 bits en SRAM et non 16
		// Le 16eme bit utilisé pour la valeur curante OVF
		dest_matrix[y][x] = ((msb_source_matrix[y][x] << 16) & 0x07ff0000) + (lsb_source_matrix[y][x] & 0xffff);
		}
	   }
return 0 ;
}

int FillDataMatrix(int matrix[][(80*7)],int module_id,int chip_id,int row,int column,int value)
{
int y ;
int x ;
	y =  (module_id*120) + row ;
	x = (chip_id*80) + column ;
	matrix[y][x] = value ;
return 0 ;
}


int SaveMatrixModuleReadyToFile(std::string fileSelected,int matrix[][(80*7)],int *moduleReady_tab)
{
FILE *fd ;
int x ;
int y ;
int pmodule ;

	fd = fopen(fileSelected.c_str(),"w") ;
	//printf("pfile = %d =>%s\n",fd,fileSelected.c_str());
	if (fd == NULL) return 0 ;
	for (pmodule = 0 ;pmodule <8;pmodule++)
		{
		if (moduleReady_tab[pmodule] == 1)
			{
			//printf("Module ready = %d \n",pmodule) ;
			for (y=0;y<120;y++)
			//for (y=0;y<(120*8);y++)
	   			{
	   			for (x=0;x<(80*7);x++)
					{
					//if (matrix[y+(pmodule*120)][x] != 0) printf("y=%d x=%d value=%d\n",y,x,matrix[y+(pmodule*120)][x] ) ;
					fprintf(fd,"%d ",matrix[y+(pmodule*120)][x]) ;
					}
	   			fprintf(fd,"\n ") ;
          		 	}
			}
		}
	 fclose(fd) ;
return 0 ;
}


int SaveMatrixToFile(std::string fileSelected,int matrix[][(80*7)])
{
FILE *fd ;
int x ;
int y ;
int pmodule = 0 ;

	fd = fopen(fileSelected.c_str(),"w") ;
	//printf("pfile = %d =>%s\n",fd,fileSelected.c_str());
	if (fd == NULL) return 0 ;
	
	//printf("Module ready = %d \n",pmodule) ;
	for (y=0;y<(120*8);y++)
		{
	   	for (x=0;x<(80*7);x++)
			{
			//if (matrix[y+(pmodule*120)][x] != 0) printf("y=%d x=%d value=%d\n",y,x,matrix[y+(pmodule*120)][x] ) ;
			fprintf(fd,"%d ",matrix[y+(pmodule*120)][x]) ;
			}
	   	fprintf(fd,"\n ") ;
          	}
	 fclose(fd) ;
return 0 ;
}


/*
On utilise fit2d pour lire les fichiers edf
Tu peux le charger depuis:  http://www.esrf.fr/computing/scientific/FIT2D/
Ton probleme vient  de ton  tete  qui doit etre tel quel:
{ suivi de retour chariot
HeaderID = EH:000001:000000:000000 ; suivi de retour chariot
more_parameters = here ; suivi de retour chariot
... ; suivi de retour chariot
... ; suivi de retour chariot
} suivi de retour chariot

Chaque ligne est suivi de son retoutr chariot (\n)
Chaque ligne de enete est finie par ";\n"

Tout ce bloc entete du { au } (avec son retour chariot) doit etre un multiple de 512
bytes (entite de 8bits)
Donc il faut completer ton entete avec des bytes vides (0) jusqu'a concurence de 512 ou
1024 bytes
Puis vient le binaire des data
\n
{\r\n
EDF_DataBlockID = 1.Image.Psd ; \r\n
EDF_BinarySize = 1048576 ; \r\n
EDF_HeaderSize = 8192 ; \r\n
ByteOrder = LowByteFirst ; \r\n
DataType = FloatValue ; \r\n
Dim_1 = 512 ; \r\n
Dim_2 = 512 ; \r\n
<other keyword value pairs>
}\n<binary image data>


struct EdfConfigFile_
	{
	int ImageNb ;
	char *HeaderID ;
	char *DetectorIdent ;
	char *ByteOrder ;
	char *DataType ;
	int Dim_X ;
	int Dim_Y ;
	char *ConfigFileNameModule1 ; 
	char *ConfigFileNameModule2 ;
	char *ConfigFileNameModule3 ;
	char *ConfigFileNameModule4 ;
	char *ConfigFileNameModule5 ;
	char *ConfigFileNameModule6 ;
	char *ConfigFileNameModule7 ;
	char *ConfigFileNameModule8 ;
	float Exposure_sec ;
	char *Experiment_date ;
	char *Experiment_time ;
	} ;


int SaveMatrixToEDFFile(std::string fileSelected,struct EdfConfigFile_ *EdfConfigFile , int matrix[][(80*7)])
{
FILE *fd ;
int data[80*7] ;
int str_length ;
int end_entete ;

	char *entete = (char *) malloc(4096*sizeof(char)) ;
	char *lmess = (char *) malloc(1024*sizeof(char)) ;
	char *fileName = (char *) malloc(1024*sizeof(char)) ;
	
	EdfConfigFile->ImageNb = 1 ;
	EdfConfigFile->HeaderID = "EH:000001:000000:000000" ;
	EdfConfigFile->DetectorIdent = "D1" ;
	EdfConfigFile->ByteOrder = "LowByteFirst" ;
	EdfConfigFile->DataType = "Unsigned16" ;
	EdfConfigFile->Dim_X = 80*7 ;
	EdfConfigFile->Dim_Y = 120*8 ;
	
	sprintf(entete,"{\r\n") ;
	//sprintf(lmess,"EDF_BinarySize  = %s\r\r",EdfConfigFile->BinarySize) ; strcat(entete,lmess) ;
	//sprintf(lmess,"EDF_HeaderSize  = %s\r\r",EdfConfigFile->HeaderSize) ; strcat(entete,lmess) ;
	sprintf(lmess,"Image  = %d; \r\n",EdfConfigFile->ImageNb) ; strcat(entete,lmess) ;
	sprintf(lmess,"HeaderID = %s; \r\n",EdfConfigFile->HeaderID) ; strcat(entete,lmess) ;
	sprintf(lmess,"DetectorIdent = %s; \r\n",EdfConfigFile->DetectorIdent) ; strcat(entete,lmess) ;
	sprintf(lmess,"ByteOrder = %s; \r\n",EdfConfigFile->ByteOrder) ; strcat(entete,lmess) ;
	sprintf(lmess,"DataType = %s; \r\n",EdfConfigFile->DataType) ; strcat(entete,lmess) ; // f
	sprintf(lmess,"Dim_X = %d; \r\n",EdfConfigFile->Dim_X) ; strcat(entete,lmess) ;
	sprintf(lmess,"Dim_Y = %d; \r\n",EdfConfigFile->Dim_Y) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule1 = %s; \r\n",EdfConfigFile->ConfigFileNameModule1) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule2 = %s; \r\n",EdfConfigFile->ConfigFileNameModule2) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule3 = %s; \r\n",EdfConfigFile->ConfigFileNameModule3) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule4 = %s; \r\n",EdfConfigFile->ConfigFileNameModule4) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule5 = %s; \r\n",EdfConfigFile->ConfigFileNameModule5) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule6 = %s; \r\n",EdfConfigFile->ConfigFileNameModule6) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule7 = %s; \r\n",EdfConfigFile->ConfigFileNameModule7) ; strcat(entete,lmess) ;
	sprintf(lmess,"ConfigFileNameModule8 = %s; \r\n",EdfConfigFile->ConfigFileNameModule8) ; strcat(entete,lmess) ;
	sprintf(lmess,"Exposure_sec = %.3lf; \r\n",EdfConfigFile->Exposure_sec) ; strcat(entete,lmess) ;
	sprintf(lmess,"Experiment_date = %s; \r\n",EdfConfigFile->Experiment_date) ; strcat(entete,lmess) ;
	sprintf(lmess,"Experiment_time = %s; \r\n",EdfConfigFile->Experiment_time) ; strcat(entete,lmess) ;
	
	end_entete = 6 ; // correspondant a la fin du la partie header  "; \r\n}\n" soit 6 caractères
	
	str_length = strlen(entete) ;
	if (str_length < (512-end_entete))	
		{
		for (int i=0;i<((511-end_entete)-str_length);i++)
			strcat(entete,"0") ;
		}
	if (str_length < (1024-end_entete))	
		{
		for (int i=0;i<((1024-end_entete)-str_length);i++)
			strcat(entete,"0") ;
		}
	sprintf(lmess,"; \r\n") ; strcat(entete,lmess) ;
	sprintf(lmess,"}\n") ; strcat(entete,lmess) ;
	
	fd = fopen(fileSelected.c_str(),"w") ;
	if (fd == NULL) return 0 ;
	fprintf(fd,entete) ;
	fclose(fd) ;
	
	free(entete) ;
	free(lmess) ;
	free(fileName) ;
	 
	 fd = fopen(fileSelected.c_str(),"ab");
	 if (fd == NULL) 
	 	{
		printf("ERROR : Unable to open the file %s \n",fileSelected.c_str()) ;
	 	return 0 ;
		}
	 for (int y=0;y<(120*8);y++)
	   {
	   for (int x=0;x<(80*7);x++)
		data[x] = matrix[y][x] ;
	   fwrite(data, 2, sizeof(data), fd); // ecrite en 16 bits "2 bytes"
           }
	 fclose(fd) ;

return 0 ;
}
*/

/*
#------------------------------------------------------------
resultat de la commande: od -c fileName.edf |more ->
0000000   {  \n       H   e   a   d   e   r   I   D           =       E
0000020   H   :   0   0   0   0   0   1   :   0   0   0   0   0   0   :
0000040   0   0   0   0   0   0   ;      \n       B   y   t   e   O   r
0000060   d   e   r       =       L   o   w   B   y   t   e   F   i   r
0000100   s   t       ;  \n       I   m   a   g   e
0000120   =       1       ;  \n       D   i   m   _   1       =       5
0000140   6   0   ;  \n       D   i   m   _   2       =       1   2   0
0000160   ;  \n       D   a   t   a   T   y   p   e       =       F   l
0000200   o   a   t   V   a   l   u   e       ;  \n
0000220
*
0000760                                                      \n   }  \n
0001000  viennent les donnees binaires ....
#------------------------------------------------------------
*/

int SaveMatrixToEDFFile(std::string fileSelected,struct EdfConfigFile_ *EdfConfigFile , int matrix[][(80*7)])
{
FILE *fd ;
//int data[80*7] ;
int data[80*7] ;
int str_length ;
int end_entete ;

    char *entete = (char *) malloc(4096*sizeof(char)) ;
    char *lmess = (char *) malloc(1024*sizeof(char)) ;
    char *fileName = (char *) malloc(1024*sizeof(char)) ;

    EdfConfigFile->HeaderID = "EH:000001:000000:000000" ;
    EdfConfigFile->ImageNb = 1 ;
    EdfConfigFile->DetectorIdent = "D1" ;
    EdfConfigFile->ByteOrder = "LowByteFirst" ; // LowByteFirst ou HighByteFirst
    EdfConfigFile->DataType = "SignedInteger" ; // SignedInteger
    EdfConfigFile->Dim_X = 560 ;
    EdfConfigFile->Dim_Y = 120 ;

    sprintf(entete,"{\n") ;
    //sprintf(lmess,"EDF_BinarySize  = %s\r\r",EdfConfigFile->BinarySize) ; strcat(entete,lmess) ;
    //sprintf(lmess,"EDF_HeaderSize  = %s\r\r",EdfConfigFile->HeaderSize) ; strcat(entete,lmess) ;
    sprintf(lmess," HeaderID = %s ; \n",EdfConfigFile->HeaderID) ; strcat(entete,lmess) ;
    sprintf(lmess," Image  = %d; \n",EdfConfigFile->ImageNb) ; strcat(entete,lmess) ;
    sprintf(lmess," ByteOrder = %s; \n",EdfConfigFile->ByteOrder) ; strcat(entete,lmess) ;
    sprintf(lmess," DataType = %s; \n",EdfConfigFile->DataType) ; strcat(entete,lmess) ; // f
    sprintf(lmess," Dim_1 = %d; \n",EdfConfigFile->Dim_X) ; strcat(entete,lmess) ;
    sprintf(lmess," Dim_2 = %d; \n",EdfConfigFile->Dim_Y) ; strcat(entete,lmess) ;
    sprintf(lmess," DetectorIdent = %s; \n",EdfConfigFile->DetectorIdent) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule1 = %s; \n",EdfConfigFile->ConfigFileNameModule1) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule2 = %s; \n",EdfConfigFile->ConfigFileNameModule2) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule3 = %s; \n",EdfConfigFile->ConfigFileNameModule3) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule4 = %s; \n",EdfConfigFile->ConfigFileNameModule4) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule5 = %s; \n",EdfConfigFile->ConfigFileNameModule5) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule6 = %s; \n",EdfConfigFile->ConfigFileNameModule6) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule7 = %s; \n",EdfConfigFile->ConfigFileNameModule7) ; strcat(entete,lmess) ;
    sprintf(lmess," ConfigFileNameModule8 = %s; \n",EdfConfigFile->ConfigFileNameModule8) ; strcat(entete,lmess) ;
    sprintf(lmess," Exposure_sec = %.3lf; \n",EdfConfigFile->Exposure_sec) ; strcat(entete,lmess) ;
    sprintf(lmess," Experiment_date = %s; \n",EdfConfigFile->Experiment_date) ; strcat(entete,lmess) ;
    sprintf(lmess," Experiment_time = %s; \n",EdfConfigFile->Experiment_time) ; strcat(entete,lmess) ;

    end_entete = 3 ; // correspondant a la fin du header  "\n}\n" soit 3 caractï¿½res
    str_length = strlen(entete) ;
    if (str_length < (512-end_entete))
        {
        for (int i=0;i<((512-end_entete)-str_length);i++)
            strcat(entete," ") ;
        }
    if (str_length < (1024-end_entete))
        {
        for (int i=0;i<((1024-end_entete)-str_length);i++)
            strcat(entete," ") ;
        }
    sprintf(lmess,"\n}\n") ; strcat(entete,lmess) ;

    fd = fopen(fileSelected.c_str(),"w") ;
    if (fd == NULL) return 0 ;
    fprintf(fd,entete) ;
    fclose(fd) ;

    free(entete) ;
    free(lmess) ;
    free(fileName) ;

     fd = fopen(fileSelected.c_str(),"ab");
     if (fd == NULL)
         {
        printf("ERROR : Unable to open the file %s \n",fileSelected.c_str()) ;
         return 0 ;
        }
     for (int y=0;y<120;y++)
       {
       for (int x=0;x<(80*7);x++)
        data[x] = (short) matrix[y][x] ;
       fwrite(data, sizeof(int), sizeof(data), fd); // ecrite en 16 bits "2 bytes"
           }
     fclose(fd) ;
     
     printf("sizeof(data) = %d \n",sizeof(data)) ;

return 0 ;
}


int ReadFileToMatrix(std::string fileSelected,int matrix[][(80*7)])
{
string line ;
int pline = 0 ;

	ifstream datafile(fileSelected.c_str()) ; // open file in read mode
	if (!datafile) { 
	cout << " file not found => " << fileSelected << endl ;
	return -1 ; }
	
	//assure(datafile,WhichFile) ;  // verify open
	while (getline(datafile,line))
	 {
	 stringstream linestream(line.c_str()) ;
	  for (int i=0;i<(80*7);i++)
	    {
	     linestream >>matrix[pline][i] ;
	     }
	 pline++ ;
	}
	datafile.close() ;
return 0 ;
}


int CopyTab(int source[][7] ,int target[][7])
{
for (int i=0;i<MAXMODULE;i++)
	{
	for (int j=0;j<MAXCHIP;j++)
		{
		target[i][j] = source[i][j] ;
		//printf("source[%d][%d] = %d ; target[%d][%d] = %d\n",i,j,source[i][j],i,j,target[i][j]) ;
		}
	}

return 0 ;
}


 int ExtractHandShakeVal(int datar[],int datar_length,int *wmodule)
 {
 int i ;
 int pheader = 0 ;
 int HandShake_Val = 0 ;
 
 
 	for (i=0;i<datar_length;i++)
		{
		if (datar[i] == 0xaa55) pheader = i ;
		}
	*wmodule = datar[pheader+1] ;
	HandShake_Val = datar[pheader+3] ;
		
 return HandShake_Val ;
 }
 
 
 int ExtractHandShakeValuesAndModuleID(int datar[],int datar_length,int module_id[],int handshakeVal[])
 {
 int i ;
 int pheader = 0 ;
 int ptab = 0;
 int nb_header_detected = 0 ;
 
 	for (i=0;i<datar_length;i++)
		{
		if (datar[i] == 0xaa55) 
			{
			pheader = i ;
			//printf("ptab = %d i = %d \n",ptab,i) ;
			module_id[ptab] = datar[pheader+1] ;
			handshakeVal[ptab] = datar[pheader+3] ;
			i = pheader + 3 ;
			nb_header_detected = ptab++ ;
			}
		}
		
 return nb_header_detected ;
 }
 
 
 int ExtractHandShakeVal_ImageUS(int datar[],int datar_length,int *wmodule,int *progress_acq)
 {
 int i ;
 int pheader = 0 ;
 int HandShake_Val = 0 ;
 int pwords = 0 ;
 
   if (HandShakeImgUS_debug)	     printf("PWORD = ") ;	
   
   /*
    printf("HandShake_ImageUS => ") ;
    for (i=0;i< datar_length;i++)
	printf("0x%x ",datar[i]) ;
   printf("\n") ;
   */
   
     while (pwords < datar_length)
     	{
 	for (i=pwords;i<datar_length;i++)
		{
		pheader = i ;
		if (datar[i] == 0xaa55) 
			{
			i = datar_length ;
			 if (HandShakeImgUS_debug) printf(" //%d// ",pheader) ;
			}
		}
		
	if (pheader < (datar_length - 4))
		{
		*wmodule = datar[pheader+1] ;
		HandShake_Val = datar[pheader+3] ;
		*progress_acq = datar[pheader+4] ;

		if ((HandShake_Val == IMAGE_US_ENDED) | (HandShake_Val == READY_FOR_GATE))
			{
			 //printf("ExtractHandShakeVal_ImageUS => 0x%x \n",HandShake_Val) ;
			 return HandShake_Val ;
			 }
		else
			{
			pwords = pheader + 3 ;
			if (HandShakeImgUS_debug) printf("%d ",pwords);	
			}
		}
	else 
		{
		if (HandShakeImgUS_debug) printf(" *%d*\n",datar_length);		
		return 0;
		}
	}
	if (HandShakeImgUS_debug) printf(" *%d*\n",datar_length);		
 	return HandShake_Val ;

}
 
 
int ExtractHandShakeWord(int *datar,int datar_length,int *addr_return,int *data_return) 
{
//  0x0 0xaa55 0x1 0x8 0xf002 0x12 0x11 0x15 0x12 0x13 0x13 0x13 0xf0f0
int pheader = 0 ;
int paddr = 0 ;
int phandshake = 0 ;


	for (int i=0;i<datar_length;i++)
		if (datar[i] == 43605)   pheader = i ;
	if (pheader >0)
		{
		paddr = pheader + 1 ;
		*addr_return = datar[paddr] & 0x000f  ;
		phandshake =  pheader + 3 ;
		//printf("data_return => ") ;
		for (int i=0;i<7;i++)
			{
			*(data_return+i) = datar[phandshake+1+i] ;
			//printf("%d ",data_return[i]) ;
			}
		//printf("\n") ;	
		}
return datar[phandshake] ;
}

int PlotImageWithRoot(void)
{

	system("pkill root") ;
	system("root -l PlotImage.c >> PlotImage.log") ;

return 0 ;
}
 
 int power (int value, int exp)
  {
    int     i,
            p;
    p = 1;
    for (i = 1; i <= exp; ++i)
	p *= value;
    return p;
}


/*
int WriteStructToFile(std::string wfile,struct data* data_struct,int NbElement)
{
FILE *fd ;
	
	fd = fopen(wfile.c_str(),"w") ;
	if (fd == NULL) return 0 ;
	printf ("sizeof struct = %d\n",sizeof(data_struct)) ;
	for (int i = 0;i<NbElement;i++)
		fprintf(fd,"%d : %d %d %d %d %d\n",i,data_struct[i].module_id,	data_struct[i].chip_id,data_struct[i].row,data_struct[i].column,data_struct[i].value) ;
	//fprintf(fd,"%d %d %d %d %d\n",data_struct.module_id,data_struct.chip_id,data_struct.row,data_struct.column,data_struct.value) ;
	fclose (fd) ;
return 0 ;
}
*/



/*
int ReadStructFromFile(string wfile,struct fifo_c data_struct)
{
FILE *fd ;
	
	fd = fopen(wfile,"w") ;
	if (fd == NULL) return 0 ;
	fprintf(fd,"%s",data_struct) ;
	fclose (fd) ;
return 0 ;
}
*/
/*
int ReadDatarFromFile(string file,int Module_id,struct data *dataf)
{
string line ;
int dataTemp[85] ;

	ifstream in(file) ;
	if (!in) return -1 ;
	while (getline(in,line))
		{
		istrstream linestream(line.c_str()) ;
		for (int i=0;i<MAXDATALINE;i++) linestream >> dataTemp ; 
		datadataTemp[1] ;
		if ((i >0) && ( i<(MAXCOL+3)))
				dataTab[i] = dataTemp ;
			} 

*/

unsigned long int elapsed_time_us(struct timeval start)
{
struct timeval end ;
unsigned long int value ;

	gettimeofday(&end,NULL) ;
	value = (unsigned long int) (((end.tv_sec*1000000)+end.tv_usec)-((start.tv_sec*1000000)+start.tv_usec)) ;
	
return value ;
}


unsigned long int elapsed_time_ms(struct timeval start)
{
struct timeval end ;
unsigned long int value ;

	gettimeofday(&end,NULL) ;
	value = (unsigned long int) (((end.tv_sec*1000000)+end.tv_usec)-((start.tv_sec*1000000)+start.tv_usec)) ;
	value = value / 1000 ;
	//if (value % 1000) 
	//	printf("end.tv_sec = %d end.tv_usec = %d value = %d\n",end.tv_sec,end.tv_usec,value) ;
return value ;
}


unsigned long int elapsed_time_s(struct timeval start)
{
struct timeval end ;
unsigned long int value ;

	gettimeofday(&end,NULL) ;
	value = (unsigned long int) (end.tv_sec-start.tv_sec) ;
return value ;
}
