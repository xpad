/**
* \file cyclone.cpp
* \brief Contains low level cyclone fpga functions 
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h> 

#include "defines.h"
#include "utilities.h"
#include "usbwrap.h"

using namespace std; 

#ifdef CYCLONE_MSG
#define CYCLONE_MSG(fmt,args...) printf("\033[22;37mcyclone_msg :  " fmt,##args)  
#else
#define CYCLONE_MSG(fmt,args...)
#endif


/**
* \fn void ResetSignal(CQuickUsb *wqusb,int wsig)
* \brief Low level function, Make a reset of the fifo specify by the parameter
*
* \param *wqusb Pointer to the usb interface 
* \param wsig parameter to select the fifo OPTO_TFIFO_ACLR or OPTO_RFIFO_ACLR
* \return no return value
*/
void ResetSignal(CQuickUsb *wqusb,int wsig)
{
	switch (wsig)
		{
		case OPTO_TFIFO_ACLR :
			WriteRegister(wqusb,GLOBAL_RESETS_REG,0x00) ;
			WriteRegister(wqusb,GLOBAL_RESETS_REG,OPTO_TFIFO_ACLR) ;
			WriteRegister(wqusb,GLOBAL_RESETS_REG,0x00) ;
			CYCLONE_MSG("reset signal OPTO_TFIFO_ACLR\033[22;30m\n") ;
		break ;
		case OPTO_RFIFO_ACLR	:
			WriteRegister(wqusb,GLOBAL_RESETS_REG,0x00) ;
			WriteRegister(wqusb,GLOBAL_RESETS_REG,OPTO_RFIFO_ACLR) ;
			WriteRegister(wqusb,GLOBAL_RESETS_REG,0x00) ;
			CYCLONE_MSG("reset signal OPTO_RFIFO_ACLR\033[22;30m\n") ;
		break ;
	
	} 
}


/**
* \fn void ForceSignal(CQuickUsb *wqusb,int wsig,int value)
* \brief Low level function, Force a signal to a given value 0 or 1.
*
* \param *wqusb Pointer to the usb interface 
* \param wsig the signal OPTO_TFIFO_ACLR or OPTO_RFIFO_ACLR
* \param value 0 or 1
* \return no return value
*/
void ForceSignal(CQuickUsb *wqusb,int wsig,int value)
{
	switch (wsig)
		{
		case OPTO_TFIFO_ACLR :
			WriteRegister(wqusb,GLOBAL_RESETS_REG,value) ;
			CYCLONE_MSG("force signal OPTO_TFIFO_ACLR to 0x%x\033[22;30m\n",value) ;
		break ;
		case OPTO_RFIFO_ACLR	:
			WriteRegister(wqusb,GLOBAL_RESETS_REG,value) ;
			CYCLONE_MSG("force signal OPTO_RFIFO_ACLR to 0x%x\033[22;30m\n",value) ;
		break ;
		} 
}


/**
* \fn void WriteWordToModule(CQuickUsb *wqusb,int module,int chip,int data)
* \brief Low level function, Construct and send a module data frame
*
* Module data frame look likes => <HUB_Header> <0x3333> <hub_size_field> <module_id> <Module_header> <module_size_field> <the_data> <trailer>.\n
* \n
* <HUB_Header>       => <0xbb44> \n
* <Module_header>  =>  <0xaa55> \n
* <trailer>                  => <0xf0f0>. \n
* \param *wqusb Pointer to the usb interface 
* \param module The address of the module you want to write
* \param chip The address of the chip you want to write
* \param data the word
* \return no return value
*/
void WriteWordToModule(CQuickUsb *wqusb,int module,int chip,int data)
{
// wrapper size without hub message 10
// wrapper size with hub message 10+

#define WRAPPER_SIZE				8
#define HUB_FIELD					10

//unsigned char *buffer ;
unsigned char buffer[MAX_TFIFO_SIZE] ;
int i = 0 ;
unsigned char output[2] ;


	// buffer = (unsigned char*) malloc(HUB_FIELD +  WRAPPER_SIZE + WORD_SIZE + 2) ;
	
	//if (winterface == OPTO_INTERFACE)

		ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
		
		/************************************************************************************************************************************/
		/************************************************************************************************************************************/
		//		Reset RFIFO
		ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
		/************************************************************************************************************************************/
		/************************************************************************************************************************************/
	
		WriteRegister(wqusb,OPTO_FIFO_OUT_START,(int) ((HUB_FIELD+WRAPPER_SIZE)/2)) ;	
		*(buffer+i++) = 0x00 ;
		*(buffer+i++) = 0x00 ;
		
		*(buffer+i++) = (unsigned char) HUB_HEADER_LSB ;
		*(buffer+i++) = (unsigned char) HUB_HEADER_MSB ;
		
		*(buffer+i++) = (unsigned char) MODULE_MESSAGE ;
		*(buffer+i++) = (unsigned char) MODULE_MESSAGE ;
		ConvertIntToChar((short int) ((WRAPPER_SIZE + WORD_SIZE)/2)+1,output) ;
		*(buffer+i++) = output[0] ; ;
		*(buffer+i++) = output[1] ;
		*(buffer+i++) = (unsigned char) module ;
		*(buffer+i++) = 0x00 ;

	/*
	else if (winterface == LVDS_INTERFACE)
		{
		ResetSignal(wqusb,LVDS_TFIFO_ACLR) ;
		ResetSignal(wqusb,LVDS_RFIFO_ACLR) ;
		WriteRegister(wqusb,OPTO_FIFO_OUT_START,(int) (WRAPPER_SIZE/2)) ;	
		}
	*/
	*(buffer+i++) = (unsigned char) HEADER_LSB ;
	*(buffer+i++) = (unsigned char) HEADER_MSB ;
	// size = nb word of 16 bits
	ConvertIntToChar((short int) (1 + 2 ),output) ;
	*(buffer+i++) = output[0] ; ;
	*(buffer+i++) = output[1] ;
	*(buffer+i++) = (unsigned char) chip ;
	*(buffer+i++) = 0x00 ;
	output[1] = (unsigned char) ((data >> 8) & 0xFF) ;  output[0] = (unsigned char) (data & 0xFF) ;
	/*
	for (ii=0;ii<size;i++)
		*(buffer+i+ii) = (unsigned char) chip ;
	*/
	memcpy((buffer+i),output,2) ;
	
	//for (ii = 0; ii<2;ii++)
	//	*(buffer+i+ii) = data[ii] ;	

	i = i + 2 ;
	*(buffer+i++)= (unsigned char) TRAILER  ;
	*(buffer+i++) = (unsigned char) TRAILER  ;
	*(buffer+i++) = (unsigned char) END_MESSAGE;
	*(buffer+i++) = (unsigned char) END_MESSAGE;
	
	//CYCLONE_MSG("write word to module %d chip %d Nb bytes = %d\033[22;30m\n",module,chip,wsize) ;
	WriteData(wqusb,buffer,i) ;
	
/*	
	printf("WriteWordToModule = > ") ;
	for (ii=0;ii<i;ii++)
		printf("0x%x ",buffer[ii]) ;
	printf("\n") ;
*/

	//free(buffer) ;
//	usleep(1000) ;
}



/**
* \fn void WriteWordToHub(CQuickUsb *wqusb,int module,int data)
* \brief Low level function, Construct and send a HUB data frame
*
* Module data frame look likes => <HUB_Header> <0xcccc> <hub_size_field> <hub_message> <module_id> <trailer>.\n
* \n
* <HUB_Header>       => <0xbb44> \n
* <trailer>                  => <0xf0f0>. \n
* \param *wqusb Pointer to the usb interface 
* \param module The address of the module
* \param data the word
* \return no return value
*/
void WriteWordToHub(CQuickUsb *wqusb,int module,int data)
{
// wrapper size without hub message 10
// wrapper size with hub message 10+
//const int  HUB_FIELD = 6 ; 
#define LHUB_FIELD	6
unsigned char buffer[30] ;
int i = 0 ;
unsigned char output[2] ;

	ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
	ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
	WriteRegister(wqusb,OPTO_FIFO_OUT_START,(int) (6)) ;	
	
	*(buffer+i++) = 0x00 ;
	*(buffer+i++) = 0x00 ;
	
	*(buffer+i++) = (unsigned char) HUB_HEADER_LSB ;
	*(buffer+i++) = (unsigned char) HUB_HEADER_MSB ;
		
	*(buffer+i++) = (unsigned char) HUB_MESSAGE ;
	*(buffer+i++) = (unsigned char) HUB_MESSAGE ;
	ConvertIntToChar((short int) ((LHUB_FIELD)/2),output) ;
	*(buffer+i++) = output[0] ; ;
	*(buffer+i++) = output[1] ;
	output[1] = (unsigned char) ((data >> 8) & 0xFF) ;  output[0] = (unsigned char) (data & 0xFF) ;
	memcpy((buffer+i),output,2) ;
	i = i + 2 ;
	*(buffer+i++) = (unsigned char) module ;
	*(buffer+i++) = 0x00 ;
	*(buffer+i++)= (unsigned char) TRAILER  ;
	*(buffer+i++) = (unsigned char) TRAILER  ;
	*(buffer+i++) = (unsigned char) END_MESSAGE;
	*(buffer+i++) = (unsigned char) END_MESSAGE;

	//CYCLONE_MSG("write word to module %d chip %d Nb bytes = %d\033[22;30m\n",module,chip,wsize) ;
	WriteData(wqusb,buffer,i) ;
/*
	for (ii=0;ii<i;ii++)
		printf("0x%x ",buffer[ii]) ;
	printf("\n") ;
*/
}



/**
* \fn void WriteWordsToModule(CQuickUsb *wqusb,int module,int chip,int *data,int wsize)
* \brief Low level function, Construct and send a module data frame
*
* Module data frame look likes => <HUB_Header> <0x3333> <hub_size_field> <module_id> <Module_header> <module_size_field> <data_tab     > <trailer>.\n
* \n
* <HUB_Header>       => <0xbb44> \n
* <Module_header>  =>  <0xaa55> \n
* <trailer>                  => <0xf0f0>. \n
* \param *wqusb Pointer to the usb interface 
* \param module The address of the module you want to write
* \param chip The address of the chip you want to write
* \param *data the tab of the data
* \param wsize the size of the data tab
* \return no return value
*/
void WriteWordsToModule(CQuickUsb *wqusb,int module,int chip,int *data,int wsize)
{
//int WRAPPER_SIZE = 10 ;
//int HUB_FIELD = 8 ; 
//unsigned char *buffer ;
unsigned char buffer[MAX_TFIFO_SIZE] ;
int i = 0 ;
int jj = 0 ;
//unsigned char *output ;
unsigned char output[MAX_TFIFO_SIZE] ;


	//buffer = (unsigned char*) malloc((wsize*2) + 10 +8+2) ;
	//output = (unsigned char*) malloc(wsize*2) ;

	//if (winterface == OPTO_INTERFACE)
		
		ResetSignal(wqusb,OPTO_TFIFO_ACLR) ;
		ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
		
		WriteRegister(wqusb,OPTO_FIFO_OUT_START,(int) (((2+8+10)/2)+(wsize-4))) ;	
		*(buffer+i++) = 0x00 ;
		*(buffer+i++) = 0x00 ;
		
		*(buffer+i++) = (unsigned char) HUB_HEADER_LSB ;
		*(buffer+i++) = (unsigned char) HUB_HEADER_MSB ;
		
		*(buffer+i++) = (unsigned char) MODULE_MESSAGE ;
		*(buffer+i++) = (unsigned char) MODULE_MESSAGE ;
		ConvertIntToChar((short int) (((WRAPPER_SIZE)/2)+wsize+1),output) ;
		*(buffer+i++) = output[0] ; ;
		*(buffer+i++) = output[1] ;
		*(buffer+i++) = (unsigned char) module ;
		*(buffer+i++) = 0x00 ;
		
	/*
	else if (winterface == LVDS_INTERFACE)
		{
		ResetSignal(wqusb,LVDS_TFIFO_ACLR) ;
		//ResetSignal(wqusb,LVDS_RFIFO_ACLR) ;
		WriteRegister(wqusb,OPTO_FIFO_OUT_START,(int) ((WRAPPER_SIZE/2)+(wsize-4))) ;	
		}
	*/
	
	*(buffer+i++) = (unsigned char) HEADER_LSB ;
	*(buffer+i++) = (unsigned char) HEADER_MSB ;
	ConvertIntToChar((short int) (wsize + 2 ),output) ;
	*(buffer+i++) = output[0] ; ;
	*(buffer+i++) = output[1] ;
	*(buffer+i++) = (unsigned char) chip ;
	*(buffer+i++) = 0x00 ;
	for (jj=0;jj<wsize;jj++)
		{
		output[(jj*2)+1] = (unsigned char) ((data[jj] >> 8) & 0xFF) ;  output[(jj*2)] = (unsigned char) (data[jj] & 0xFF) ;
		//printf("data[%d] = 0x%x output[%d] = 0x%x : output[%d] = 0x%x \n",jj,data[jj],((jj*2)+1),output[(jj*2)+1],(jj*2),output[(jj*2)+1]) ;
		}
	/*
	for (ii=0;ii<size;i++)
		*(buffer+i+ii) = (unsigned char) chip ;
	*/
	memcpy((buffer+i),output,(wsize*2)) ;
	/*
	for (ii = 0; ii<size;ii++)
		*(buffer+i+ii) = data[ii] ;	
	*/
	
	i = i + (wsize*2) ;

	*(buffer+i++)= (unsigned char) TRAILER  ;
	*(buffer+i++) = (unsigned char) TRAILER  ;
	*(buffer+i++) = (unsigned char) END_MESSAGE;
	*(buffer+i++) = (unsigned char) END_MESSAGE;

	//CYCLONE_MSG("write word to module %d chip %d Nb bytes = %d\033[22;30m\n",module,chip,wsize) ;
	/*
	if (i > (MAX_TFIFO*2)) 
		WriteByBlock(wqusb,buffer,i,winterface)	 ;
	else
	*/	
	WriteData(wqusb,buffer,i) ;

/*
	printf("WriteWordToModule = > ") ;
	for (ii=0;ii<i;ii++)
		printf("0x%x ",buffer[ii]) ;
	printf("\n") ;
*/

	//free(buffer) ;
//	usleep(1000) ;
}




/**
* \fn void WriteByBlock(CQuickUsb *wqusb,unsigned char *data,int size,int winterface)
* \brief Low level function, not used in the program
*
* \param *wqusb Pointer to the usb interface 
* \param data The buffer of the data which contains data to send
* \param size The length of the buffer
* \param winterface
* \return no return value
*/
void WriteByBlock(CQuickUsb *wqusb,unsigned char *data,int size,int winterface)
{
//unsigned char *buffer ;
unsigned char buffer[MAX_TFIFO_SIZE] ;
int size_transmit = 0 ;
int buf_transmit = 0 ;
int buf_size = 0 ;
int readregister = 0 ;


	CYCLONE_MSG("Write By Block => size of data %d\033[22;30m\n",size) ;
	while (size_transmit < size)
		{
		buf_size = size - size_transmit ;
		//printf("1 : size=%d size_transmit=%d buf_size=%d\n",size,size_transmit,buf_size) ;
		if (buf_size > (int) (MAX_TFIFO*2)) buf_size = (int) (MAX_TFIFO*2) ;
		//buffer = (unsigned char*) malloc(buf_size) ;
		//printf("2 : buf_size=%d\n",buf_size) ;
		memcpy(buffer,(data+size_transmit),buf_size) ;
		//WriteRegister(wqusb,SETTINGS_REG,0x01) ;
		if (winterface == LVDS_INTERFACE) readregister = (int) ReadRegister(wqusb,LVDS_TFIFO_WUSEDW_REG) ;
		//if (winterface == OPTO_INTERFACE) readregister = (int) ReadRegister(wqusb,OPTO_TFIFO_WUSEDW_REG) ;
		//WriteRegister(wqusb,SETTINGS_REG,0x00) ;
		buf_transmit = 	(int) (buf_size - (readregister*2)) ;
		//printf("3 : readregister=%d buf_transmit=%d\n",readregister,buf_transmit) ;
		WriteData(wqusb,buffer,buf_transmit) ;
		size_transmit = size_transmit + buf_transmit ; 
		//printf("4 : size_transmit=%d buf_transmit=%d\n",size_transmit,buf_transmit) ;
		//free(buffer) ;
		//CYCLONE_MSG("transmission of block %d size=%d buf_transmit=%d size_transmit=%d readreg => %d\033[22;30m\n",block_id++,size,buf_transmit,size_transmit,readregister) ;
		}
}



void* PollingFIFOContains_thread(void *parameters)
{
int length ;

	length = ReadRegister((CQuickUsb *) parameters,OPTO_RFIFO_WUSEDW_REG) & 0x7f ;
	
return (void *) length ; 

}


/**
* \fn int ReadFromModule(CQuickUsb *wqusb,int *data,int timeout_val,int NbWord_Expected)
* \brief Low level function, not used in the program
*
* \param *wqusb Pointer to the usb interface 
* \param *data
* \param timeout_val
* \param NbWord_Expected
* \return -1 if failed, length of the contains of the fifo otherwise
*/
int ReadFromModule(CQuickUsb *wqusb,int *data,int timeout_val,int NbWord_Expected)
{
//int *buffer ;
int buffer[MAX_RFIFO_SIZE] ;
int timeout_loop = 0 ;
int i = 0 ;
/*
pthread_t thread_id = NULL ;
pthread_attr_t thread_attr ;
struct thread_params* thread_arg ;
void *ret ;
*/
int local_length  = 0 ;
			/*
			if (pthread_attr_init (&thread_attr) != 0) {
   				printf("ERROR =>  ReadRegister pthread_attr_init error");
   			return -1 ;
			}
			
			 if (pthread_attr_setdetachstate (&thread_attr, PTHREAD_CREATE_DETACHED) != 0) {
				printf("ERROR =>  ReadRegister pthread_attr_setdetachstate error");
 			 return -1 ;
			}
			*/
			while (local_length  < NbWord_Expected) 
				{
				
				//local_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;	
				/*
				pthread_create(&thread_id,NULL,&PollingFIFOContains_thread,wqusb) ;
				pthread_join(thread_id,&ret) ;
				local_length = (int) ret ;
				*/
				//pthread_join(thread_id,(void*) &local_length) ;
				//if (timeout_loop > 0)
				 //   {
				local_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;    
				//printf("local_length = %d \n",local_length ) ;
				
				//if (local_length > 0)
				//	buffer = (int *) malloc(local_length*sizeof(int)) ;
				
				    if (timeout_loop == timeout_val)  
					{
					CYCLONE_MSG("TIMOUT No message detected on OPTO interface\033[22;30m\n") ;
					//ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
					return -1 ;
					}
			            timeout_loop++ ;
				}
				
			ReadData(wqusb,buffer,local_length)	; 
			//ReadData(wqusb,buffer, NbWord_Expected)	; 
			//CYCLONE_MSG("%d => Message detected, OPTO_RFIFO_WUSEDW_REG words = %d \033[22;30m\n",++ii,local_length) ;	
			//CYCLONE_MSG("Message number %d detected, size = %d, read OPTO_RFIFO words = %d \033[22;30m\n",++ii,buffer[1],pointer_inc) ;
			//ResetSignal(wqusb,OPTO_RFIFO_ACLR) ;
//			}

		for (i=0;i<local_length;i++)
			{
			data[i] = buffer[i] ;
			//printf("0x%x ",data[i]) ;			
			}
		//if (local_length > 0)
		//	free(buffer) ;
		//printf("\n") ;		
		//printf(" ***** timout_loop = %d \n",timeout_loop) ;

return local_length  ;
}


int ReadRFIFOContains(CQuickUsb *wqusb,int *data,clock_t timout_ms)
{
//int *buffer ;
int buffer[MAX_RFIFO_SIZE] ;
int i = 0 ;
int local_length  = 0 ;
int plocal_length  = 0 ;

clock_t t1,t2 ;
long clk_tck = CLOCKS_PER_SEC; 
long elapsed_time ;


			

			t1 = clock() ;
			while ((local_length  < 1) | (plocal_length < local_length)) 
				{
				plocal_length = local_length ;
				local_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;    
				t2 = clock() ;
				//elapse_time = (long) (((double) (t2-start_time)/(double) clk_tck) *1000000) ;
				elapsed_time = (long) (((double) (t2-t1)/(double) clk_tck) *1000) ;// en ms
				if(elapsed_time > timout_ms) 
					CYCLONE_MSG("TIMOUT ReadDataFromModules elapsed_time = %ums \033[22;30m\n") ;
				}
				
			local_length = ReadRegister(wqusb,OPTO_RFIFO_WUSEDW_REG) & 0x3fff ;    
			printf("plocal_length = %d local_length = %d \n",plocal_length,local_length) ;
			/*
			if (local_length > 0) 
				buffer = (int *) malloc(local_length*sizeof(int)) ;	
			*/
			ReadData(wqusb,buffer,local_length)	; 

			for (i=0;i<local_length;i++)
				data[i] = buffer[i] & 0xffff ; // masquage 16 bits ;
			/*	
			if (local_length > 0)
				free(buffer) ;
			*/
return local_length  ;
}



/**
* \fn int ReadDataFromFifo(CQuickUsb *wqusb,int *data,int data_length)
* \brief 
*
* \param *wqusb Pointer to the usb interface 
* \param *data
* \param data_length
* \return 
*/
int ReadDataFromFifo(CQuickUsb *wqusb,int *data,int data_length)
{
//int *buffer ;
int buffer[MAX_RFIFO_SIZE] ;
int i = 0 ; 


		/*
		if (data_length > 0)
			buffer = (int *) malloc(data_length*sizeof(int)) ;
		*/
		ReadData(wqusb,buffer,data_length)	; 
		
		for (i=0;i<data_length;i++)
			{
			data[i] = buffer[i] ;
			//printf("0x%x ",data[i]) ;			
			}
		//printf("\n") ;		
		//printf(" ***** timout_loop = %d \n",timeout_loop) ;

return i  ;
}



/**
* \fn int WriteGlobalSettingsRegister(CQuickUsb *wqusb,int value)
* \brief 
*
* \param *wqusb Pointer to the usb interface 
* \param value
* \return 
*/
int WriteGlobalSettingsRegister(CQuickUsb *wqusb,int value)
{ 
int keep_value ;

	keep_value = ReadRegister(wqusb,GLOBAL_SETTINGS_REG) & 0x8080 ; // keep global setting register OPTO_FD_DATA or LVDS_FD_DATA config
	WriteRegister(wqusb,GLOBAL_SETTINGS_REG,keep_value | value) ;
return 0 ;
}


/*
	//Polling the wrempty of rfifo
		printf("Waiting for message \n") ;

		while (((ReadRegister(wqusb,STATUS_RFIFO_REG) & (RFIFO_WREMPTY | RFIFO_RDEMPTY)) & 0x0f) == 0) 
				{
				//usleep(500000) ;
				timeout_val++ ;
				if (timeout_val > timeout)
					{
					CYCLONE_MSG("\033[22;31mtimout detected = %d \033[22;30m\n", timeout) ;
					return 0 ;
					}
				}
		printf("Message detected \n") ;
		ii = 0 ;
		//while ((ReadRegister(wqusb,STATUS_RFIFOS_REG) & (RFIFO_WREMPTY | RFIFO_RDEMPTY)) != 0) 
		//		{
				printf("STATUS_RFIFOS_REG = 0x%x\n",ReadRegister(wqusb,STATUS_RFIFO_REG) ) ;
		//while ((ReadRegister(wqusb,R_WUSEDW_REG) & R_WUSEDW_DIM) == 0)  ;
		//while ((ReadRegister(wqusb,R_WUSEDW_REG)) == 0)  ;
				printf("RFIFO_WUSEDW = 0x%x : %d\n",ReadRegister(wqusb,RFIFO_WUSEDW_REG),ReadRegister(wqusb,RFIFO_WUSEDW_REG)) ;
				printf("RFIFO_RUSEDW = 0x%x : %d\n",ReadRegister(wqusb,RFIFO_RUSEDW_REG),ReadRegister(wqusb,RFIFO_RUSEDW_REG)) ;
				//data_length = 16 ;
				data_length = ReadRegister(wqusb,RFIFO_WUSEDW_REG) ;
				ReadData(wqusb,buffer,data_length)	;
				printf("Message length of %d is ",data_length);
				for (i=0;i<	data_length;i++)
					printf("0x%x ",buffer[i]) ;
				printf("\n") ;
				printf("RFIFO_WUSEDW = 0x%x : %d\n",ReadRegister(wqusb,RFIFO_WUSEDW_REG),ReadRegister(wqusb,RFIFO_WUSEDW_REG)) ;
				printf("RFIFO_RUSEDW = 0x%x : %d\n",ReadRegister(wqusb,RFIFO_RUSEDW_REG),ReadRegister(wqusb,RFIFO_RUSEDW_REG)) ;
				printf("STATUS_RFIFOS_REG = 0x%x\n",ReadRegister(wqusb,STATUS_RFIFO_REG) ) ;
				
				//if (loop++ == 3) return 0 ;
				//	printf("Max loop reached\n") ;	
	
				//if (ii++ == 3) break ;
			//}
		free(buffer) ;

return 0 ;
}
*/

