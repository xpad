#include "xpad.h"
#include "CQuickUsb.h"
#include "usbwrap.h"

// temporary until swithc to PCI express
static CQuickUsb *qusb = NULL ;

static int SHIT = 1;

void xpad_init(void)
{
	char nameList[256];
	unsigned long length = sizeof(nameList);
	int result;
	string currentDevice ;
	unsigned char data[3] = {0};        
	unsigned short length_data = sizeof(data)  ;
	printf("\033[22;31m MainGUI::init(void)\n") ;
	result = qusb->FindModules(nameList, length);
	if (result == 0)  
	{
		//IdentifyQuickUSBError(qusb) ;
		printf("\033[22;31mERROR : Unable to find the QuickUSB module\033[22;30m\n") ;
		exit(-1);
	}else
		fprintf(stdout, "found %s device\n", nameList);        

	qusb = new CQuickUsb(nameList);
	result = qusb->Open() ;
	if (result == 0)  
	{
		IdentifyQuickUSBError(qusb) ;
		printf("\033[22;31mERROR : Unable to open the QuickUSB module\033[22;30m\n") ;
	}else
		fprintf (stdout, "Opened the QuickUSB %s module\n", nameList);

   	length_data = 2 ;
   
	ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
	ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
		
     	qusb->ReadCommand((unsigned short) FPGA_VERSION, data, &length_data);
    	qusb->ReadCommand((unsigned short) FPGA_VERSION, data, &length_data);
    	
    	DAQ_MSG("\033[22;31mFPGA Program version => 0x%x%x\033[22;30m\n",data[1],data[0]) ;
    	if ((data[1] == 0x2b) & (data[0] == 0x3c)){
		ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
		ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
		WriteWordToModule(qusb,0,0,DUMMY_MSG) ; 
		WriteWordToModule(qusb,0,0,DUMMY_MSG) ; 
    		ResetSignal(qusb,OPTO_RFIFO_ACLR) ;
		ResetSignal(qusb,OPTO_TFIFO_ACLR) ;
		fprintf(stdout, "Reset of the FGPA done\n");
	}else{	
		fprintf(stdout, "Interface not avaliable\n") ;
		fprintf(stdout, "You must follow this procedure\n1: Disconnect and reconnect the USB cable on the USB_OPTO card\n2: Program the FPGA on excecuting the command\n    nios2-configure-sof USB_OPTO_V2.sof\n") ;
		exit(-1);
	}
}

/**
 * \fn void MainGUI::StartAcqLoopMode_Slot()
 * \brief Start acquisition of all modules connected to the detector
 *
 * ImageUs(qusb,MODULE_1,GateMode_comboBox->currentItem(),integration_time,index_img,nb_image ) ; \n
 * WaitingForHandShakeImageUSEnded(qusb) ; \n
 * Then for all modules connected \n
 * 
 * ACQ MODE OVF          => ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_OVF,dmatrix,WMODULE) \n
 * ACQ MODE COUNTER  => ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_LSB,dlsbmatrix,WMODULE) and \n
 *                                         ReadOptoRFIFOAndFillMatrix(qusb,READ_IMAGE_COUNTER_MSB,dmsbmatrix,WMODULE) ; \n

 * Adding of the two matrix => AddLSBMatrixandMSBMatrix(dlsbmatrix,dmsbmatrix,dmatrix) ; \n
 * then plot matrix to Canvas => RootHistoHitConfigFillPlotMatrix(mClone,h2,dmatrix,sfileName) ; \n
 * if selected save matrix to the file => SaveMatrixToFile(PathAndfileName,dmatrix) ; \n
 *
 * \param
 * \return
 */
void xpad_acq(XpadConfig *config)
{
	int status = 0;
	int loop_img = 0;
	int i ;
	int progress_acq ;
	int index = 0; // for now only one picture at a time.
  
	fprintf(stdout, "module mask : %0x\n", config->mask);
	fflush(stdout) ;
	
	DAQ_MSG("Image us integration time = %d Index img = %d Nb_Image = %d loop_img = %d\033[22;30m\n",
		config->acq.integration_time,
		config->acq.index_img,
		config->acq.nb_image,
		loop_img) ;

	ImageUs(qusb, config->mask, config->acq.Gate_Mode, config->acq.integration_time,
		config->acq.index_img, config->acq.nb_image, SHIT) ;
 
	while (status !=  IMAGE_US_ENDED){
		// receive  (IMAGE_ADDED	0x1140) and  (IMAGE_US_ENDED 0x1141) at the end
		status = WaitingForHandShakeImageUSEnded(qusb,
							 (long) config->acq.integration_time,
							 config->acq.nb_image, &progress_acq);
		if (status ==  IMAGE_US_ENDED)
			progress_acq = config->acq.nb_image;
	}
 
	DAQ_MSG("Image_us complete status = %d\033[22;30m\n", status) ;
    
	usleep(100000);

	// reset the matrix.
	memset(config->image, 0, sizeof(config->image)) ;
	
	fprintf(stdout, "start\n");
	fflush(stdout);

	// calcul effectué dans barrette.cpp avec la fonction gettimeofday(..) rajouter la directive de compilation -DBARRETTE_MSG
	//start = clock();

	switch (config->mode){
	case MODE_32b:
		DAQ_MSG("Download fast image index %d mode 32 bits\033[22;30m\n", index);
		break;
	case MODE_16b:
		DAQ_MSG("Download fast image index %d mode 16 bits\033[22;30m\n", index);
		break;
	}
	status = SecureFastReadImageAndFillMatrix(qusb, config->mode,
						  config->mask, config->image, index);

	if (status < 0){
		printf("\033[22;31mERROR BAD IMAGE 033[22;30m\n");
		return ;
	}
}

void xpad_save_image(XpadConfig *config,
		     std::string const & directory, std::string const & filename)
{
	std::string imgfilename = directory + '/' + filename + "_img.dat";
	
	SaveMatrixToFile(imgfilename, config->image) ;
}

void xpad_load_calibration_new(XpadConfig *self, 
			       std::string const & directory, std::string const & base)
{
	xpad_config_read_from_file(self, directory, base);
	xpad_config_send(self);
}

void xpad_is_ready(XpadConfig *config)
{
	int i;
	int status = -1;
	int addr_return = 0;

	config->mask = 0;
	for(i=0; i<8; ++i){
		int module_msk = 1 << i;
		int module_addr = i + 1;
		status = AskReadyModule(qusb, module_msk, &addr_return) ;
		if (status > 0 && addr_return == module_addr){
			config->mask |= module_msk;
			config->ModuleReady_tab[i] = 1;
			DAQ_MSG("Module_%d ready \n", i + 1);
		}else{
			config->ModuleReady_tab[i] = -1;
			DAQ_MSG("ERROR => Module_%d not avaliable (status = %d addr_return %d)\n",
				i + 1, status, addr_return);
		}
	}
}

void xpad_reset_hub(void)
{
	ResetHUBFifo(qusb);
}

void xpad_reset_modules(void)
{
	int i;

	fprintf(stdout, "Reset of the Module and the HUB in progress...");
	fflush(stdout);
	for(i=0; i<MAXMODULE; ++i){
		ResetModule(qusb, 1 << i);
		fprintf(stdout, " %i", i);
		fflush(stdout);
	}
	sleep(10);
	fprintf(stdout, "done.\n");
	fflush(stdout);
}

/***************/
/* config part */
/***************/

/**
 * fill the XpadConfig with the default values
 */
void xpad_config_init(XpadConfig *self, int dacl)
{
	int i;
	int j;

	xpad_config_init_config_g(self);

	/* then the dacl values */
	for(i=0; i<XPAD_HEIGHT; ++i)
		for(j=0;j<XPAD_WIDTH; ++j)
			self->dacl[i][j] = dacl;
}

/**
 * fill the XpadConfig with the ConfigG default values
 */
void xpad_config_init_config_g(XpadConfig *self)
{
	int i;
	int j;

	if (!self)
		return;

	/* set the general default parameters */
	for (i=0; i<MAXMODULE; ++i)
		for (j=0; j<MAXCHIP; ++j){
			self->cmos_dis[i][j] = 0;
			self->ithh_val[i][j] = 0;
			self->vadj_val[i][j] = 0;
			self->vref_val[i][j] = 0;
			self->imfp_val[i][j] = 52;
			self->iota_val[i][j] = 40;
			self->ipre_val[i][j] = 60;
			self->ithl_val[i][j] = 22;
			self->itune_val[i][j] = 145;
			self->ibuffer_val[i][j] = 0;
		}
}

/**
 * send all the config General and local to the xpad
 */
void xpad_config_send(XpadConfig *self)
{
	if(!self)
		return;

	/* send the config g part */
	xpad_config_send_config_g(self);

	/* send the dacl */
	xpad_config_send_dacl(self);
}

/**
 * send the config General to the xpad
 */
void xpad_config_send_config_g(XpadConfig *self)
{
	int module;

	if(!self)
		return;

	/* send general parameters to all modules */
	for(module=0; module < MAXMODULE; ++module){
		if(self->ModuleReady_tab[module] > 0){
			int mask;
			int chip;

			mask = 1 << module;
			DAQ_MSG("Send  ConfigG Module_%d \033[22;30m\n", module + 1) ;
			for (chip = 0;chip < MAXCHIP; ++chip){
				int status;
				int buffer[20];

				buffer[0] = self->cmos_dis[module][chip];
				buffer[1] = 0  ;
				buffer[2] = self->ithh_val[module][chip];
				buffer[3] = self->vadj_val[module][chip];
				buffer[4] = self->vref_val[module][chip];
				buffer[5] = self->imfp_val[module][chip];
				buffer[6] = self->iota_val[module][chip];
				buffer[7] = self->ipre_val[module][chip];
				buffer[8] = self->ithl_val[module][chip];
				buffer[9] = self->itune_val[module][chip];
				buffer[10] = self->ibuffer_val[module][chip];
		
				ConfigAllG(qusb, mask, chip, buffer) ;
				if ((status = WaitingForHandShakeConfigAllG(qusb) ) > 0)
					DAQ_MSG("ConfigG Module_%d  Chip_%d complete\033[22;30m\n", status, (chip+1) ) ;
				else{
					DAQ_MSG("\033[22;31mERROR : unable to receive the HandShakeConfigG Module_1 Chip_%d\033[22;30m\n", (chip+1)) ;
					return ;
				}
			}
		}
	}
}

static void xpad_config_send_xxx(XpadConfig *self, int reg, int values[MAXMODULE][MAXCHIP])
{
	int chip;
	int module;
	int mask;

	for(module=0; module<MAXMODULE; ++module)
		if (self->ModuleReady_tab[module] > 0){
			mask = 1 << module;
			for(chip=0; chip<MAXCHIP; ++chip)
				WriteConfigG(qusb, mask, chip, reg, values[module][chip]);
		}
}

void xpad_config_send_ithl(XpadConfig *self)
{
	xpad_config_send_xxx(self, ITHL, self->ithl_val);
}

void xpad_config_send_itune(XpadConfig *self)
{
	xpad_config_send_xxx(self, ITUNE, self->itune_val);
}

void xpad_config_send_imfp(XpadConfig *self)
{
	xpad_config_send_xxx(self, IMFP, self->imfp_val);
}

void xpad_config_send_dacl(XpadConfig *self)
{
	LoadDACLFromTabToModuleReady(qusb, self->ModuleReady_tab, self->dacl);
}

void xpad_config_read_dacl(XpadConfig *self)
{
	FastReadImageAndFillMatrix(qusb, MODE_READ_DACL, self->mask, self->dacl, INDEX_0);
	ExtractDACLValue(self->dacl, self->dacl);

	for(int i=0; i<XPAD_WIDTH; ++i)
		for(int j=0;j<XPAD_HEIGHT; ++j)
			fprintf(stdout, " %d", self->dacl[j][i]);

	fprintf(stdout, "/n");
}

void xpad_config_send_dacl_flat(XpadConfig *self, int dacl)
{
	int i;

	for(i=0; i<MAXMODULE; ++i)
		if(self->ModuleReady_tab[i] > 0){
			int mask;
			int value = 0x1 | dacl << 3;

			mask = 1 << i;
			fprintf(stdout, "write flat config %d %d to module %d\n", dacl, value, i + 1);
//			WriteFlatConfig(qusb, mask ,0, ALL_CHIP, 0x0101 | (i << 3)); // FOR TEST
			WriteFlatConfig(qusb, mask ,NULL, ALL_CHIP, value);
		}
}

static void xpad_config_read_module_from_file(XpadConfig *self,
					      std::string const & directory,
					      std::string const & base,
					      int module)
{
	char filename[255];
	std::string line ;
	std::string str_tmp ;
	int wchip ;

	// read all globals parameters.
	std::sprintf(filename, "%s/%s/%s_%d.dacs", directory.c_str(), base.c_str(), base.c_str(), module + 1);

	std::ifstream datafile(filename) ; // open file in read mode
	if (!datafile) { 
		cout << " file not found => " << filename << endl ;
		return;
	}
	while(getline(datafile, line)){
		istringstream linestream(line.c_str()) ;
		linestream >> str_tmp ;
		if (strcmp(str_tmp.c_str(),"CMOS_DIS") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->cmos_dis[module][wchip]; 
		if (strcmp(str_tmp.c_str(),"ITHH") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->ithh_val[module][wchip]; 
		if (strcmp(str_tmp.c_str(),"VADJ") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->vadj_val[module][wchip]; 
		if (strcmp(str_tmp.c_str(),"VREF") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->vref_val[module][wchip] ; 
		if (strcmp(str_tmp.c_str(),"IMFP") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->imfp_val[module][wchip] ; 
		if (strcmp(str_tmp.c_str(),"IOTA") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->iota_val[module][wchip] ; 
		if (strcmp(str_tmp.c_str(),"IPRE") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->ipre_val[module][wchip] ; 
		if (strcmp(str_tmp.c_str(),"ITHL") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->ithl_val[module][wchip] ; 
		if (strcmp(str_tmp.c_str(),"ITUNE") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->itune_val[module][wchip] ; 
		if (strcmp(str_tmp.c_str(),"IBUFFER") == 0 )
			for (wchip=0; wchip<MAXCHIP; ++wchip)
				linestream >>  self->ibuffer_val[module][wchip] ;
		if (strcmp(str_tmp.c_str(),"DACL") == 0 ){
			while(getline(datafile, line)){
				int i;
				int row;
				istringstream stream(line);

				stream >> wchip;
				stream >> row;
				for(i=0; i<MAXCOL; ++i)
					stream >> self->dacl[module * 120 + (row - 1)][(wchip-1) * 80 + i];
					
			}
		}
	}
	datafile.close();
}

void xpad_config_read_from_file(XpadConfig *self,
				std::string const & directory, std::string const & base)
{
	int i;

	for(i=0; i<MAXMODULE; ++i)
		if(self->ModuleReady_tab[i] > 0) 
			xpad_config_read_module_from_file(self, directory, base, i);
}

/***************/
/* calibration */
/***************/

void xpad_config_calibrate_ith(XpadConfig *self, const XpadCalibrationConfig *config)
{
	Local_ITHL_Calibration(qusb, self->ModuleReady_tab,
			       config->ith_min, config->ith_max,
			       self->acq.integration_time, config->path, FAST_READOUT_FORCED,
			       &self->ith_calib[0], false);
}

void xpad_config_calibrate_dacl(XpadConfig *self, const XpadCalibrationConfig *config)
{
	Local_DACL_Calibration(qusb, self->ModuleReady_tab, config->walgo,
			       self->acq.integration_time, config->path, config->noise_accepted,
			       FAST_READOUT_FORCED, config->dec_dacl_val, false);
	xpad_config_read_dacl(self);
}

void xpad_config_calibrate(XpadConfig *self, const XpadCalibrationConfig *config)
{
	/* first send the default general configuration */
	xpad_config_init(self, 32);
	xpad_config_send(self);

	/* now calibrate */
	xpad_config_calibrate_ith(self, config);
	xpad_config_calibrate_dacl(self, config);
}

#ifdef MAIN

int main()
{
	xpad_init();
	xpad_reset_hub();
	xpad_reset_modules();
	xpad_is_ready();
	//xpad_load_calibration("calibrations", "matlab_calib_D2_cuivredacl+4_mod");
	xpad_acq(100, 1, INTERNAL_GATE, dlsbmatrix, dmsbmatrix, dmatrix);
	xpad_save_image(".", "img", dlsbmatrix, dmsbmatrix, dmatrix);
}

#endif
