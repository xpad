/**
* \file usbwrap.h
* \brief The header file of usbwrap.cpp
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */

#ifndef _USBWRAP_H_
#define _USBWRAP_H_

#include <stdio.h>
#include <stdlib.h>
#include "CQuickUsb.h"


int IdentifyQuickUSBError(CQuickUsb *wqusb) ;
int WriteData(CQuickUsb *wqusb,unsigned char *data,unsigned int length) ;
int ReadData(CQuickUsb *wqusb,int *data,long unsigned int length) ;
int WriteRegister(CQuickUsb *wqusb,int wregister,unsigned short reg_value) ;
int ReadRegister(CQuickUsb *wqusb,int wregister) ;
long int ReadLongRegister(CQuickUsb *wqusb,int wregister) ;

#endif 
