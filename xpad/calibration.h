/**
* \file calibration.h
* \brief The header file of calibration.cpp
* \author Patrick BREUGNON
* \version 1.0
* \date April 20 2009
 */


#ifndef _CALIBRATION_H_
#define _CALIBRATION_H_
/*
#include <iostream>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <istream>
#include <fstream>
#include <strstream>
#include <sstream>
#include <vector>
*/
#include "stdio.h"
#include "stdlib.h"

#include <signal.h>
#include <sys/time.h>


/******************************/
/*    Include ROOT                 */
/*****************************/
#ifdef USE_ROOT_API
#include "TQRootCanvas.h"
#include "TCanvas.h" 
#include "TH2F.h" 
#include "TStyle.h" 
#include "TSystem.h" 
#include "TAttImage.h"
#include "TColor.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TFrame.h"
#include "defines.h"
#include "TGClient.h"
#include "TROOT.h"
#include "TBrowser.h"
#endif

#include "CQuickUsb.h"


struct struct_imageUS
	{
	int integration_time ;
	int index_img ;
	int nb_image ; 
	int Gate_Mode;
	int FastReadout ;
};
	
struct struct_readImg
	{
	int IndexImg ;
	int DataDelay ;
	int FastReadout ;
};
	
	
struct struct_ith
	{
	int val[7] ;
	int found[7] ;
	int cnt[7] ;
	};

// int Local_ITHL_Calibration(CQuickUsb *wqusb,int *module_ready,int ith_min,int ith_max,int gate_time,string PathAndfileName,TCanvas *mClone) ;

void Search_ITHL(int ith_min, int ith_max,int *module_ready,int cmatrix[][120*8][80*7]) ;
int Load_Ith_Adjusted(CQuickUsb *wqusb,int *module_ready,struct struct_ith *ithl,int fastReadout) ;

#ifdef USE_ROOT_API	
int Local_ITHL_Calibration(CQuickUsb *wqusb,int *module_ready,int ith_min,int ith_max,int gate_time,string PathAndfileName,TCanvas *mClone,int fast_readout, struct struct_ith *ithl, bool save_result_to_file) ;
	int Debug_DACL_Evolution(CQuickUsb *wqusb,int *module_ready,int walgo,int gate_time,string PathAndfileName,TCanvas *mClone,int noise_accepted,int fast_readout) ;
int Local_DACL_Calibration(CQuickUsb *wqusb,int *module_ready,int walgo,int gate_time,string PathAndfileName,TCanvas *mClone,int noise_accepted,int fast_readout,int dec_dacl_val, bool save_result_to_file) ;
	int ReAdjustDACL(CQuickUsb *wqusb,int *module_ready,int gate_time,int dacl_val[][80*7],string PathAndfileName,int walgo,TCanvas *mClone) ;
	int ConfigureDACGandScanDACGthenExpose(CQuickUsb *wqusb,int *module_ready,int *ith_index,int ith_max,string PathAndfileName,TCanvas *mClone,int fast_readout) ;
	int ConfigureDACGOnlythenExpose(CQuickUsb *wqusb,int *module_ready,int *ith_index,int ith_max,string PathAndfileName,TCanvas *mClone,int fast_readout) ;
#else
	int Local_ITHL_Calibration(CQuickUsb *wqusb,int *module_ready,int ith_min,int ith_max,int gate_time,string PathAndfileName,int fast_readout, struct struct_ith *ithl, bool save_result_to_file) ;
int Local_DACL_Calibration(CQuickUsb *wqusb,int *module_ready,int walgo,int gate_time,string PathAndfileName,int noise_accepted,int fast_readout,int dec_dacl_val, bool save_result_to_file) ;	
	int ReAdjustDACL(CQuickUsb *wqusb,int *module_ready,int gate_time,int dacl_val[][80*7],string PathAndfileName,int walgo) ;
	int ConfigureDACGandScanDACGthenExpose(CQuickUsb *wqusb,int *module_ready,int *ith_index,int ith_max,string PathAndfileName,int fast_readout) ;
	int ConfigureDACGOnlythenExpose(CQuickUsb *wqusb,int *module_ready,int *ith_index,int ith_max,string PathAndfileName,int fast_readout) ;
#endif

int LoadDACLTabAndEstimateNoisyPix(CQuickUsb *wqusb,int *module_ready,int gate_time,int dacl_val[][80*7]) ;

int SaveCalibScanValueToOutputFile(int wcalib,int max_tab,int cmatrix[][120*8][80*7],string PathAndfileName) ;
//void Search_DACL(int cmatrix[][120*8][80*7],int *module_ready,int walgo,int noise_accepted) ;
void Search_DACL(int cmatrix[][120*8][80*7],int *module_ready,int walgo,int noise_accepted,int dec_dacl_val) ;

int ModifyDACLStepByStep(int *module_ready,int datamatrix[][80*7],int daclmatrix[][80*7], int dec_or_inc) ;

int SaveDACLToDataFile(string PathAndfileName,int dacl_val[][80*7]) ;
int SaveDACLToConfigFile(string PathAndfileName,int wmodule,int dacl_val[][80*7]) ;

int GenerateMasterFile(string PathAndfileName,int wmodule) ;

int reset_matrix(int wmatrix[][80*7] ) ;
int move_matrix_value_to(int wmatrix[][80*7] ,int dec_or_inc,int value) ;
int correct_12b_matrix(int wmatrix[][80*7] ) ;
int reset_cmatrix(void ) ;

int ExposeModuleReady(CQuickUsb *wqusb,int *module_ready,struct struct_imageUS imageUS) ;
int ReadImageFromModuleReady(CQuickUsb *wqusb,int *module_ready,struct struct_readImg readImg) ;
int LoadDACLFromTabToModuleReady(CQuickUsb *wqusb,int *module_ready,int dacl_tab[][80*7])  ;

int EstimateNoisyPixel(int *module_ready,int data[][80*7], int noisy[][80*7]) ;
int CalculatePixelEqualToSpecificDACLValue(int *module_ready,int dacl_data[][80*7],int cnt_data[][80*7],int value,int print_data) ;
int CalculateNoisyPixel(int *module_ready,int data[][80*7]) ;
int FillcmatrixFromFile(char *wfile,int cmatrix[][(120*8)][80*7]) ;

int printf_intermediate_result(int step,int *nb_pixel_noisy,int *nb_pixel_corrected) ;

int WriteFlatConfigValueToModuleReady(CQuickUsb *wqusb,int *module_ready,int dacl_value) ;

#endif

