/**
* \file defines.h
* \brief Contains all the constants and defines values
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */

/*
	Files Project

		- DAQ_XPAD.pro
		- DAQ_XPAD.ui
		- DAQ_XPAD.ui.h
		- defines.h
		- general.h
		- CQuickUsb.h
		- CQuickUsb.cpp
		- QuickUsb.h
		- usbwrap.h
		- usbwrap.cpp
		- cyclone.h
		- cyclone.cpp
*/


//\033[22;30m - black
//\033[22;31m - red
//\033[22;32m - green
//\033[22;33m - brown
//\033[22;34m - blue
//\033[22;35m - magenta
//\033[22;36m - cyan
//\033[22;37m - gray
//\033[01;30m - dark gray
//\033[01;31m - light red
//\033[01;32m - light green
//\033[01;33m - yellow
//\033[01;34m - light blue
//\033[01;35m - light magenta
//\033[01;36m - light cyan
//\033[01;37m - white



/*
#ifdef USB_ERR
#define USB_ERR(fmt,args...) printf("\033[22;31musb_error :  " fmt,##args)  
#else
#define USB_ERR(fmt,args...)
#endif
#ifdef USB_MSG
#define USB_MSG(fmt,args...) printf("\033[22;33musb_msg :  " fmt,##args)  
#else
#define USB_MSG(fmt,args...)
#endif

#ifdef CYCLONE_MSG
#define CYCLONE_MSG(fmt,args...) printf("\033[22;34mcyclone_msg :  " fmt,##args)  
#else
#define CYCLONE_MSG(fmt,args...)
#endif
*/


#ifndef _DEFINES_H_
#define _DEFINES_H_


#define DEMONSTRATOR			1

#define LSB_WORD_INDEX				0
#define MSB_WORD_INDEX				1


#define LOCAL_NOISE_CALIB_LAST_ZERO			0
#define LOCAL_NOISE_CALIB_FROM_MAXCNT		1
#define LOCAL_BEAM_CALIB						2

#define NEW_ACQ					1

#define COUNTER					0
#define OVF						1

#define MAXDATALINE			85

#define MAXMODULE				8
#define ROW_NB					120
#define MAXROW					120*MAXMODULE
#define MAXCOL	 				80
#define MAXCHIP 					7
#define MAXPIXEL 				(MAXROW*MAXCOL*MAXCHIP)
//#define MAXDATA				537600
#define MAXDATA				62700

#define MAX_MODULE				8
#define MAX_CHIP				7
#define MAX_ROW					120
#define MAX_COL					80

#define MAX_RFIFO_SIZE			16384 
#define MAX_TFIFO_SIZE			8192

#define TIMEOUT					500

#define NIOS_DELAY				10

#define WRITE_DUMMY			0
#define ADDR_RETURN			2
#define HANDSHAKE_WORD		4
#define FIRST_WORD				4

#define _READ_ONLY				0
#define _APPEND					1
#define DATA_FILE				0
#define CFG_FILE					1
#define SCAN_FILE				2
#define MST_FILE					3

#define ONE_CHIP				0
#define ALL_CHIP				1

#define INTERNAL_GATE				0
#define EXTERNAL_GATE				1

#define INTERNAL_MODE				0
#define EXTERNAL_MODE				1

#define CMOS_DISABLE				0x01
#define AMP_TP					0x32
#define ITHH					0x33
#define VADJ					0X35
#define VREF					0x36
#define IMFP					0x37
#define IOTA					0x38
#define IPRE					0x3B
#define ITHL					0x3C
#define ITUNE					0x41
#define IBUFFER					0x42

#define DUMMY_MSG				0x0000
#define ASK_READY				0x0101
#define IAM_READY				0x1101

#define DIGITAL_TEST				0x0102
#define CONFIG_G				0x0103
#define ALL_CONFIG_G			0x0203
#define FLAT_CONFIG				0x0104
#define CALIB					0x0105
#define CONFIGURE_PIXEL			0x0108
#define ADJUST_ITH				0x0110
#define ADJUST_DACL				0x0120

#define IMAGE_US				0x0140
#define IMAGE_US_X_SAMPLES			0x0150
#define READY_FOR_GATE				0x0160
//#define IMAGE_US_12BIT			0x0140
//#define IMAGE_US_24BIT			0x0140

#define READ_IMAGE_COUNTER			0x0180
#define READ_IMAGE_COUNTER_LSB		0x0180
#define READ_IMAGE_COUNTER_MSB		0x0181

#define FAST_READ_IMAGE				0x0182
#define FAST_READ_IMAGE_16b			0x0182
#define FAST_READ_IMAGE_32b			0x0183

#define FAST_READOUT_FORCED			1

//#define READ_IMAGE_COUNTER_12BIT	0x0180
//#define READ_IMAGE_COUNTER_24BIT	0x0180

#define READ_IMAGE_OVF			0x0280
#define READ_DACL				0x01C0
#define CTB_MODE_COUNTER			0x01F0
#define CTB_MODE_OVF			0x02F0
#define READ_CALIB_TMP				0x0280
#define LOAD_CALIB				0x0380
#define LOAD_CALIB_ENDED			0x1380
#define LOAD_DACL_ITH				0x0381
#define LOAD_DACL_ITH_ACK			0x1381

#define STOP_ACQ				0x0480


//#define IMAGE_US_RECV				0x1140
#define IMAGE_ADDED				0x1140
#define IMAGE_US_ENDED				0x1141

#define CALIB_HANDSHAKE				0x1105
#define CALIB_COMPLETE				0x2105

#define DIGITAL_TEST_ENDED			0x1102
#define CONFIG_G_RECV				0x1103
#define ALL_CONFIG_G_RECV			0x1203
#define FLAT_CONFIG_RECV			0x1104
#define CTB_MODE_ACK				0x11F0

#define CONTINUE				0x1111
#define LOOP_TEST				0xa5a5

#define WORD_SIZE				2

#define FULL_DETECTOR        	      		0
#define MAX_TFIFO				252

#define SHORT_REG      				0
#define LONG_REG          			1


#define  HUB_HEADER_MSB        			0xbb
#define  HUB_HEADER_LSB        			0x44
#define  MODULE_MESSAGE	       			0x33
#define  HUB_MESSAGE	       			0xcc

#define  HEADER_MSB	         		0xaa
#define  HEADER_LSB	         		0x55
#define  HEADER	         			0xaa55
#define  TRAILER          			0xf0f0
#define  END_MESSAGE         	 		0x00

#define LVDS_INTERFACE				0x0080
#define LVDS_INTERFACE_FIFO_250			0x0081
#define OPTO_INTERFACE				0x8000

/* READ WRITE REGISTERS */
#define GLOBAL_RESETS_REG      			0x01  
#define GLOBAL_SETTINGS_REG    			0x02
#define OPTO_FIFO_OUT_START    			0x04

/* READ ONLY REGISTERS */
// LVDS registers
#define LVDS_RFIFO_STATUS_REG  			0x10
#define LVDS_RFIFO_STATUS_SIZE  		0x007f
#define LVDS_RFIFO_WUSEDW_REG     		0x11
#define LVDS_RFIFO_WUSEDW_SIZE    		0x7000
#define LVDS_RFIFO_RUSEDW_REG     		0x12

#define LVDS_TFIFO_STATUS_REG  			0x14
#define LVDS_TFIFO_WUSEDW_REG    		0x15   
#define LVDS_TFIFO_RUSEDW_REG    		0x16   

// OPTO registers
#define OPTO_RFIFO_STATUS_REG  			0x20
#define OPTO_RFIFO_STATUS_SIZE  		256
#define OPTO_RFIFO_WUSEDW_REG     		0x21
#define OPTO_RFIFO_WUSEDW_SIZE    		256
#define OPTO_RFIFO_RUSEDW_REG     		0x22
 
#define FPGA_VERSION        			0x100

/* GLOBAL_RESETS_REG addr = 0x01 */
#define LVDS_TFIFO_ACLR     			0x0001
#define LVDS_RFIFO_ACLR       			0x0002

#define OPTO_TFIFO_ACLR     			0x0100
#define OPTO_RFIFO_ACLR       			0x0200

/* GLOBAL_SETTINGS_REG addr = 0x02 */
#define MARKER    					0x0001
#define LVDS_RFIFO_WRREQ     			0x0002
#define OPTO_RFIFO_WRREQ     		0x0200
#define LVDS_PATTERN_EN     			0x8000

/* LVDS_STATUS_RFIFO_REG addr = 0x10*/
#define LVDS_RFIFO_WREMPTY			0x01
#define LVDS_RFIFO_WRFULL			0x02
#define LVDS_RFIFO_RDEMPTY			0x04
#define LVDS_RFIFO_RDFULL			0x08
#define LVDS_RFIFO_ALMOST_FULL			0x10
#define LVDS_RFIFO_TRAILER_DETECTED		0x20
#define LVDS_RFIFO_HEADER_DETECTED		0x40

/* OPTO_STATUS_RFIFO_REG addr = 0x20*/
#define OPTO_RFIFO_WREMPTY			0x01
#define OPTO_RFIFO_WRFULL			0x02
#define OPTO_RFIFO_RDEMPTY			0x04
#define OPTO_RFIFO_RDFULL			0x08
#define OPTO_RFIFO_ALMOST_FULL			0x10
#define OPTO_RFIFO_TRAILER_DETECTED		0x20
#define OPTO_RFIFO_HEADER_DETECTED		0x40


#define MODULE_1					1
#define MODULE_2					2
#define MODULE_3					3
#define MODULE_4					4
#define MODULE_5					5
#define MODULE_6					6
#define MODULE_7					7
#define MODULE_8					8
#define ALL_MODULES				9

#define IMAGE_US_ALL_MODULES		9

#define ALL_CHIPS					7

#define NULL_PARAM					0


#define RESET_MODULE					0x02FF
#define RESET_HUB_FIFO					0x05FF
#define RECONFIG_MODULE				0x03FF

#define HandShakeImgUS_debug 		0

#define 	LOOP_ENABLE				1
#define DIG_LOOP_ENABLE 			2
#define DOWNLOAD_LOOP_ENABLE	3

#define	NO_SYNCHRO		0
#define 	D2AM_SYNCHRO		1
#define 	PIXSCAN_SYNCHRO	2
#define 	SERIAL_SYNCHRO		3
#define 	SOCKET_SYNCHRO	4


#define 	DONT_SAVE		0
#define	SAVE_TXT			1
#define	SAVE_EDF			2

#define 	CR				0x0D
#define 	LF				0x0A

#define 	WITHOUT_ANSWER		0
#define 	WITH_ANSWER			1

#define MODE_16b				0
#define MODE_32b				1
#define MODE_READ_DACL		3

#define TIMOUT_DETECTED		-1

#define INDEX_0			0
#define INDEX_1			1
#define INDEX_2			2
#define INDEX_3			3
#define INDEX_4			4
#define INDEX_5			5
#define INDEX_6			6
#define INDEX_7			7

#define COL_NSWAP		0
#define COL_SWAP		1

#define RESET_HUB_BETWEEN_CONTINUE	0

#define TEST_LIBRARY		1

#define DECREASE			0
#define INCREASE			1

#endif

