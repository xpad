/**
* \file usbwrap.cpp
* \brief Wrapper of CQuickUsb library
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */

//#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "CQuickUsb.h"
#include "defines.h"



#ifdef USB_ERR_DEF
#define USB_ERR(fmt,args...) printf("\033[22;31musb_error :  " fmt,##args)  
#else
#define USB_ERR(fmt,args...)
#endif
#ifdef USB_MSG_DEF
#define USB_MSG(fmt,args...) printf("\033[22;33musb_msg :  " fmt,##args)  
#else
#define USB_MSG(fmt,args...)
#endif


#define SETTING_EP26CONFIG		0
#define SETTING_WORDWIDE		1
#define SETTING_DATAADRESS	2
#define SETTING_FIFO_CONFIG		3
#define SETTING_FPGATYPE		4
#define SETTING_CPUCONFIG		5
#define SETTING_VERSIONSPEED	17


static unsigned int result ;

/**
* \fn int IdentifyQuickUSBError(CQuickUsb *wqusb)
* \brief Identify the type of error returned by the USB driver
*
* \param *wqusb Pointer to the usb interface 
* \return no return value
*/
int IdentifyQuickUSBError(CQuickUsb *wqusb)
{
int error ;
	error = wqusb->GetLastError() ;
    switch (error) 
    	{
        case 0:
        	//USB_ERR("%d => Unknown ERROR\033[22;30m\n",error) ;
        break ;
      	case 1:
        	USB_ERR("%d => Out of memory, please free some memory and try again\033[22;30m\n",error) ;
      	break ;
      	case 2:
          	USB_ERR("%d => Cannot open module\033[22;30m\n",error) ;
        break ;
    	case 3:
        	USB_ERR("%d => Cannot find specifies QuickUSB module\033[22;30m\n",error) ;
    	break ;
       	case 4:
        	USB_ERR("%d => IOCTL failed, error is returned by the QuickUSB driver\n\033[22;30m",error) ;
        break ;
        case 5:
            USB_ERR("%d => Cannot read or write data length of zero or greather than 16MBytes\033[22;30m\n",error) ;
        break ;
    	case 6:
            USB_ERR("%d => Timeout occurred when attempting to read or write data\033[22;30m\n",error) ;
        break ;
    	case 7:
            USB_ERR("%d => This function is not supported by the version of QuickUSB driver\033[22;30m\n",error) ;
        break ;
    	default :
      		USB_ERR("%d => Error code unknowned\033[22;30m\n",error) ;
    	break ;
  		}
return 0 ;
}


/**
* \fn int WriteData(CQuickUsb *wqusb,unsigned char *data,unsigned int length)
* \brief Low level function, Write data to the TFIFO and send automatically to modules
*
* \param *wqusb Pointer to the usb interface 
* \param *data The buffer of data to send
* \param *length The length of the data to send 
* \return no return value
*/
int WriteData(CQuickUsb *wqusb,unsigned char *data,unsigned int length)
{
	USB_MSG("write data, length = %d\033[22;30m\n",length) ;
        result = wqusb->WriteData(data,length) ;
	if (result == 0)  { IdentifyQuickUSBError(wqusb) ; return -1 ;}
return 0 ;
}


/**
* \fn int ReadData(CQuickUsb *wqusb,unsigned char *data,long unsigned int length)
* \brief Low level function, read data from the rfifo 
*
* \param *wqusb Pointer to the usb interface 
* \param *data The buffer of data to read
* \param *length The length of the data to read 
* \return no return value
*/
int ReadData(CQuickUsb *wqusb,int *data,long unsigned int length)
{
unsigned char *buffer ;
int i ;
long unsigned int byte_length ;

	byte_length = length *2 ;
	buffer = (unsigned char*) malloc(byte_length) ;
	USB_MSG("read data, length = %d\033[22;30m\n",length) ;
	result = wqusb->ReadData(buffer,&byte_length) ;
	if (result == 0)  { IdentifyQuickUSBError(wqusb) ; return -1 ;}
	for (i=0;i<(int) length;i++)
		{
		data[i] = (buffer[(i*2)]) | (buffer[(i*2)+1] << 8) ;
		//printf("(buffer[%d] = 0x%x, buffer[%d] = 0x%x, data[%d] = 0x%x\n",((i*2)+1),buffer[(i*2)+1],(i*2),buffer[(i*2)],i,data[i]) ;
		}
	free(buffer) ;
return 0 ;
}



/**
* \fn int WriteRegister(CQuickUsb *wqusb,int wregister,unsigned short reg_value)
* \brief Low level function, write word of 16 bits to specific a register
*
* \param *wqusb Pointer to the usb interface 
* \param wregister The address of the register
* \param reg_value The value you want to write 
* \return no return value
*/
int WriteRegister(CQuickUsb *wqusb,int wregister,unsigned short reg_value)
{
unsigned char command[2] ;        
unsigned short length_command = sizeof(command)  ;
 
	USB_MSG("write command => %x\033[22;30m\n",reg_value) ;
	command[1] = (unsigned char) ((reg_value >> 8) & 0xFF) ;  command[0] = (unsigned char) (reg_value & 0xFF) ;
  	result = wqusb->WriteCommand((unsigned short)wregister,command,length_command);
	//printf("command[1] = 0x%x command[0] = 0x%x length_command = 0x%x\n",command[1],command[0],length_command) ;
  	if (result == 0)  { IdentifyQuickUSBError(wqusb) ; return -1 ;}
return 0 ;
}


/**
* \fn int ReadRegister(CQuickUsb *wqusb,int wregister)
* \brief Low level function, read word of 16 bits from a specific register
*
* \param *wqusb Pointer to the usb interface 
* \param wregister The address of the register you want to read

* \return The contains of the register
*/
int ReadRegister(CQuickUsb *wqusb,int wregister)
{
unsigned char data[2] ;           
unsigned short length_data = 2  ;
unsigned short reg_value ;

      result = wqusb->ReadCommand((unsigned short) wregister,data,&length_data);
//    result = wqusb->ReadCommand(((unsigned short) wregister ),msb_data,&length_data);
//    reg_value = ((((long unsigned int) msb_data[1]) << 24) | (((long unsigned int) msb_data[0]) << 16) | (((long unsigned int) lsb_data[1]) << 8) | ((long unsigned int) lsb_data[0])) ;  
	reg_value = (((long unsigned int) data[1]) << 8) | ((long unsigned int) data[0]) ;  
 //   USB_MSG("read register => %x\033[22;30m\n",reg_value) ;	
//	if (result == 0)  { IdentifyQuickUSBError(wqusb) ; return -1 ;}
return (int) reg_value ; 
}


long int ReadLongRegister(CQuickUsb *wqusb,int wregister)
{
unsigned char lsb_data[4] ;        
unsigned short length_data = 4  ;
long int reg_value ;

    result = wqusb->ReadCommand((unsigned short) wregister,lsb_data,&length_data);
	reg_value = (((long unsigned int) lsb_data[3]) << 24) | (((long unsigned int) lsb_data[2]) << 16) | (((long unsigned int) lsb_data[1]) << 8) | ((long unsigned int) lsb_data[0]) ;  
    USB_MSG("read long register => %x\033[22;30m\n",reg_value) ;
	//printf("ReadRegister : msb_data[1] = 0x%x msb_data[0] = 0x%x\n",msb_data[1],msb_data[0]) ;
	//printf("ReadRegister : lsb_data[3] = 0x%x lsb_data[2] = 0x%x lsb_data[1] = 0x%x lsb_data[0] = 0x%x\n",lsb_data[3],lsb_data[2],lsb_data[1],lsb_data[0]) ;
	
	if (result == 0)  { IdentifyQuickUSBError(wqusb) ; return -1 ;}
return reg_value ; 
}


int ReadUSBControllerConfig(CQuickUsb *wqusb)
{
unsigned short value ;
	wqusb->ReadSetting(SETTING_EP26CONFIG,&value) ;
	printf("SETTING_EP26CONFIG        = 0x%x\n",value) ;
	wqusb->ReadSetting(SETTING_WORDWIDE,&value) ;
	printf("SETTING_WORDWIDE	    = 0x%x\n",value) ;
	wqusb->ReadSetting(SETTING_DATAADRESS,&value) ;
	printf("SETTING_DATAADRESS     = 0x%x\n",value) ;
	wqusb->ReadSetting(SETTING_FIFO_CONFIG,&value) ;
	printf("SETTING_FIFO_CONFIG       = 0x%x\n",value) ;
	wqusb->ReadSetting(SETTING_FPGATYPE,&value) ;
	printf("SETTING_FPGATYPE             = 0x%x\n",value) ;
	wqusb->ReadSetting(SETTING_CPUCONFIG ,&value) ;
	printf("SETTING_CPUCONFIG          = 0x%x\n",value) ;
	wqusb->ReadSetting(SETTING_VERSIONSPEED,&value) ;
	printf("SETTING_VERSIONSPEED    = 0x%x\n",value) ;
return 0 ;
}	


//long unsigned int ReadCommandRegister(CQuickUsb *wqusb,int wregister,int reg_size)
//{
//unsigned char msb_data[2] ;        
//unsigned char lsb_data[2] ;        
//unsigned short length_data = 2  ;

// if (reg_size == SHORT_REG)
//   {
//   result = wqusb->ReadCommand((unsigned short) wregister,lsb_data,&length_data);
//    return (((long unsigned int) lsb_data[1]) << 8) | ((long unsigned int) lsb_data[0]) ;  
//   }
// else if (reg_size == LONG_REG)
//   {
//    result = wqusb->ReadCommand((unsigned short) wregister,lsb_data,&length_data);
//     result = wqusb->ReadCommand(((unsigned short) wregister + 1 ),msb_data,&length_data);
//     return ((((long unsigned int) msb_data[1]) << 24) | (((long unsigned int) msb_data[0]) << 16) | (((long unsigned int) lsb_data[1]) << 8) | ((long unsigned int) lsb_data[0])) ;  
//   }
//}

