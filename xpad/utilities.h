/**
* \file utilities.h
* \brief The header file of utilities.cpp
* \author Patrick BREUGNON
* \version 1.0
* \date February 19 2008
 */


#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include <iostream>
#include <cstdio>
#include <string>
#include <cstdlib>
#include <istream>
#include <fstream>
#include <sstream>
#include <vector>

#include <signal.h>
#include <sys/time.h>

#include "defines.h"
#include "pthread.h"




#define DATA_FILE	0
#define CFG_FILE		1

struct EdfConfigFile_
    {
    char *HeaderID ;
    char *DetectorIdent ;
    char *ByteOrder ;
    char *DataType ;
    int Dim_X ;
    int Dim_Y ;
    char *ConfigFileNameModule1 ;
    char *ConfigFileNameModule2 ;
    char *ConfigFileNameModule3 ;
    char *ConfigFileNameModule4 ;
    char *ConfigFileNameModule5 ;
    char *ConfigFileNameModule6 ;
    char *ConfigFileNameModule7 ;
    char *ConfigFileNameModule8 ;
    int ImageNb ;
    float Exposure_sec ;
    char *Experiment_date ;
    char *Experiment_time ;
    } ;


int ConvertIntToChar(int input, unsigned char *output) ;
int DetectTimeout(int timeout_loop,int timeout_val,int interface) ;
void SaveConfigToFile(std::string sFileName,int *data,int data_length,int mode) ;
void SaveDataToFile(std::string sFileName,int *data,int data_length,int mode) ;
void SaveDACToFile(std::string sFileName,int  tab_index,int cmos_dis[][7],int ithh[][7],int vadj[][7],int vref[][7],int imfp[][7],int iota[][7],int ipre[][7],int ithl[][7],int itune[][7],int ibuffer[][7]) ;

//int ReadDataFromFile(std::string sFileName,int *data) ;
//string SetReadFile(int kindoffile) ;
//string SetWriteFile(int kindoffile) ;

int PlotImageWithRoot(void) ;
int ExtractSuffix(char *path,char *folderName) ;
int ExtractFileName(const char *PathAndFile,std::string *FileName) ;
int ExtractPathPrefixIndex(const char *PathAndFile,std::string *Path,std::string *Prefix,int *Index) ;
//int ExtractPrefixIndex(const char *PathAndFile,std::string *Prefix,int *index) ;
int ExtractPrefixIndex(const char *PathAndFile) ;

int ResetMatrix(int matrix[][(80*7)]) ;
int AddMatrix(int source_matrix[][(80*7)],int dest_matrix[][(80*7)]) ;
int CopyMatrix(int source_matrix[][(80*7)],int dest_matrix[][(80*7)]) ;
int ExtractDACLValue(int source_matrix[][(80*7)],int dacl_matrix[][(80*7)]) ;
//int CompareMatrix(int refmatrix[][(80*7)],int dmatrix[][(80*7)]) ;
int CompareMatrix(int refmatrix[][(80*7)],int dmatrix[][(80*7)],int errormatrix[][(80*7)]) ;
int AddLSBMatrixandMSBMatrix(int lsb_source_matrix[][(80*7)],int msb_source_matrix[][(80*7)],int dest_matrix[][(80*7)]) ;

int FillDataMatrix(int matrix[][(80*7)],int module_id,int chip_id,int row,int column,int value) ;
int SaveMatrixToFile(std::string fileSelected,int matrix[][(80*7)]) ;
int SaveMatrixModuleReadyToFile(std::string fileSelected,int matrix[][(80*7)],int *moduleReady_tab) ;
int SaveMatrixToEDFFile(std::string fileSelected,struct EdfConfigFile_ *EdfConfigFile , int matrix[][(80*7)]) ;
int ReadFileToMatrix(std::string fileSelected,int matrix[][(80*7)]) ;

int CopyTab(int source[][7] ,int target[][7]) ;

int ExtractHandShakeVal(int datar[],int datar_length,int *wmodule) ;
 int ExtractHandShakeVal_ImageUS(int datar[],int datar_length,int *wmodule,int *progress_acq) ;
int ExtractHandShakeWord(int *datar,int datar_length,int *addr_return,int* data_return) ;

int ExtractHandShakeValuesAndModuleID(int datar[],int datar_length,int module_id[],int handshakeVal[]) ;

int power (int value, int exp) ;

unsigned long int elapsed_time_us(struct timeval start) ;
unsigned long int elapsed_time_ms(struct timeval start) ;
unsigned long int elapsed_time_s(struct timeval start) ;

//int WriteStructToFile(std::string wfile,struct data* data_struct,int NbElement) ;


#endif
