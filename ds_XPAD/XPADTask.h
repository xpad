#ifndef _XPAD_TASK_H_
#define _XPAD_TASK_H_

#include <tango.h>
#include <yat/memory/DataBuffer.h>
#include <yat/threading/Task.h>
#include <xrays/xrays-image.h>
#include <xpad/xpad.h>

typedef enum XPADType {
	XPAD_TYPE_500K,
	XPAD_TYPE_SINGLE_MODULE,
	XPAD_TYPE_QUAD_CDTE,
} XPADType;

typedef enum XPADTaskMsgID {
	TASK_XPAD_START = yat::FIRST_USER_MSG,
	TASK_XPAD_LOAD_CALIBRATION,
	TASK_XPAD_RESET,
	TASK_XPAD_CALIBRATION,
	TASK_XPAD_SET_ITHL,
	TASK_XPAD_SET_ITUNE,
	TASK_XPAD_SET_IMFP,
	TASK_XPAD_GET_DACL,
	TASK_XPAD_SET_DACL,
	TASK_XPAD_LOAD_FLAT_DACL,
	TASK_XPAD_LOAD_DEFAULT_CONFIG_G,
} XPADTaskMsgID;

typedef struct XPADStartConfig
{
	double exposure;
	long gate_number;
	long gate_mode;
} XPADStartConfig;

/* will be use for ithl, itune and imfp */
typedef struct XPADSetIthlConfig
{
	const Tango::DevLong *img;
	long width;
	long height;
} XPADSetIthlConfig;

typedef struct XPADSetDaclConfig
{
	const Tango::DevLong *img;
	long width;
	long height;
} XPADSetDaclConfig;

class XPADTask : public yat::Task, public Tango::LogAdapter
{
public:
	XPADTask(Tango::DeviceImpl *device, const std::string &type);

	virtual ~XPADTask(void);
public:
	//- override the Yat::exit method to stop the task
	void exit(void) throw(yat::Exception);
 
	//! start a droplet process
	void start(XPADStartConfig const &) throw (yat::Exception);

	//! stop the current process
	void stop(void);

	//! load clibration
	void load_calibration(std::string const &) throw (yat::Exception);

	//! reset
	void reset(void) throw(yat::Exception);

	//- calibration
	void calibration(XpadCalibrationConfig const &) throw (yat::Exception);

	//- set ithl
	void set_ithl(XPADSetIthlConfig const & c) throw (yat::Exception);

	//- set itune
	void set_itune(XPADSetIthlConfig const & c) throw (yat::Exception);

	//- set imfp
	void set_imfp(XPADSetIthlConfig const & c) throw (yat::Exception);

	//- read dacl
	void read_dacl(void) throw (yat::Exception);

	//- set dacl
	void set_dacl(XPADSetDaclConfig const & c) throw (yat::Exception);

	//- load flat dacl
	void load_flat_dacl(Tango::DevULong dacl) throw (yat::Exception);

	//- load default config g
	void load_default_config_g(void) throw (yat::Exception);


	XRaysImage *get_image(void);
	XRaysImage *get_ithl(void);
	XRaysImage *get_itune(void);
	XRaysImage *get_imfp(void);
	XRaysImage *get_dacl(void);
	Tango::DevState get_xpad_state(void) {return _state;}
	std::string get_xpad_status(void) {return _status;}

protected:
	virtual void handle_message(yat::Message &msg) throw (yat::Exception);

	//- Access image and histogram without lock
	XRaysImage *get_image_i(void);
	XRaysImage *get_ithl_i(void);
	XRaysImage *get_itune_i(void);
	XRaysImage *get_imfp_i(void);
	XRaysImage *get_dacl_i(void);

private: //! msg handling routines

	void on_init(void) throw (yat::Exception);
	void on_start(XPADStartConfig const &) throw (yat::Exception);
	void on_load_calibration(std::string const &) throw (yat::Exception);
	void on_reset(void)  throw (yat::Exception);
	void on_calibration(XpadCalibrationConfig const &) throw (yat::Exception);
	void on_set_ithl(XPADSetIthlConfig const &) throw (yat::Exception);
	void on_set_itune(XPADSetIthlConfig const &) throw (yat::Exception);
	void on_set_imfp(XPADSetIthlConfig const &) throw (yat::Exception);
	void on_read_dacl(void) throw (yat::Exception);
	void on_set_dacl(XPADSetDaclConfig const &) throw (yat::Exception);
	void on_load_flat_dacl(Tango::DevULong dacl) throw (yat::Exception);
	void on_load_default_config_g(void)  throw (yat::Exception);

private: //! members
	Tango::DevState _state;
	std::string _status;
	XPADType _type;

	XpadConfig _xpad;
	XRaysImage *_image;
	XRaysImage *_dacl;
	XRaysImage *_ithl;
	XRaysImage *_itune;
	XRaysImage *_imfp;

	//- the host device
	Tango::DeviceImpl *_host_dev;
};

#endif
