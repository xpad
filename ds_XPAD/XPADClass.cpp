static const char *ClassId    = "$Id: $";
static const char *CvsPath    = "$Source: $";
static const char *SvnPath    = "$HeadURL: $";
static const char *RcsId     = "$Header: /usr/local/CVS/DeviceServer/Specific/BeamLine/Cristal/XPAD/src/XPADClass.cpp,v 1.6 2009/03/25 14:12:33 picca Exp $";
static const char *TagName   = "$Name:  $";
static const char *HttpServer= "http://www.esrf.fr/computing/cs/tango/tango_doc/ds_doc/";
//+=============================================================================
//
// file :        XPADClass.cpp
//
// description : C++ source for the XPADClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the XPAD once per process.
//
// project :     TANGO Device Server
//
// $Author: picca $
//
// $Revision: 1.6 $
//
// $Log: XPADClass.cpp,v $
// Revision 1.6  2009/03/25 14:12:33  picca
// * add the exposure attribute
//
// Revision 1.5  2009/03/25 13:07:08  picca
// * rename the intensity -> intensities
// * add a new intensity scalar attribute #12131
//
// Revision 1.4  2008/12/11 14:52:05  picca
// * add the intensity attribute
//
// Revision 1.3  2008/02/22 15:43:16  picca
// * the DarkAcquisition method is working
//
// Need to add a convertion from UINT to USHORT in the xrays library. The Start command do not work now.
//
// Revision 1.2  2008/02/14 11:51:17  elattaoui
// Start and DarkAcquisition commands added
//
// Revision 1.1  2008/01/23 15:25:39  le
// initial import
//
//
// copyleft :   European Synchrotron Radiation Facility
//              BP 220, Grenoble 38043
//              FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <XPAD.h>
#include <XPADClass.h>


//+----------------------------------------------------------------------------
/**
 *	Create XPADClass singleton and return it in a C function for Python usage
 */
//+----------------------------------------------------------------------------
extern "C" {
#ifdef WIN32

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_XPAD_class(const char *name) {
		return XPAD_ns::XPADClass::init(name);
	}
}


namespace XPAD_ns
{
//+----------------------------------------------------------------------------
//
// method : 		LoadDefaultConfigGCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *LoadDefaultConfigGCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "LoadDefaultConfigGCmd::execute(): arrived" << endl;

	((static_cast<XPAD *>(device))->load_default_config_g());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		LoadFlatDaclCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *LoadFlatDaclCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "LoadFlatDaclCmd::execute(): arrived" << endl;

	Tango::DevULong	argin;
	extract(in_any, argin);

	((static_cast<XPAD *>(device))->load_flat_dacl(argin));
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		CalibrateCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *CalibrateCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "CalibrateCmd::execute(): arrived" << endl;

	const Tango::DevVarLongStringArray	*argin;
	extract(in_any, argin);

	((static_cast<XPAD *>(device))->calibrate(argin));
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		ResetCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *ResetCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "ResetCmd::execute(): arrived" << endl;

	((static_cast<XPAD *>(device))->reset());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		LoadCalibrationCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *LoadCalibrationCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "LoadCalibrationCmd::execute(): arrived" << endl;

	Tango::DevString	argin;
	extract(in_any, argin);

	((static_cast<XPAD *>(device))->load_calibration(argin));
	return new CORBA::Any();
}



//+----------------------------------------------------------------------------
//
// method : 		StartClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StartClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StartClass::execute(): arrived" << endl;

	((static_cast<XPAD *>(device))->start());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		StopClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StopClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StopClass::execute(): arrived" << endl;

	((static_cast<XPAD *>(device))->stop());
	return new CORBA::Any();
}



//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
XPADClass *XPADClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		XPADClass::XPADClass(string &s)
// 
// description : 	constructor for the XPADClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
XPADClass::XPADClass(string &s):DeviceClass(s)
{

	cout2 << "Entering XPADClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving XPADClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		XPADClass::~XPADClass()
// 
// description : 	destructor for the XPADClass
//
//-----------------------------------------------------------------------------
XPADClass::~XPADClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		XPADClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
XPADClass *XPADClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new XPADClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

XPADClass *XPADClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		XPADClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void XPADClass::command_factory()
{
	command_list.push_back(new StartClass("Start",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"no argin",
		"no argout",
		Tango::OPERATOR));
	command_list.push_back(new StopClass("Stop",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"no argin",
		"no argout",
		Tango::OPERATOR));
	command_list.push_back(new LoadCalibrationCmd("LoadCalibration",
		Tango::DEV_STRING, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new ResetCmd("Reset",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::OPERATOR));
	command_list.push_back(new CalibrateCmd("Calibrate",
		Tango::DEVVAR_LONGSTRINGARRAY, Tango::DEV_VOID,
		"parameters of the calibration\nstring path // useless for now\nlong: ith_min, ith_max, algo, noise, decalage\n",
		"",
		Tango::EXPERT));
	command_list.push_back(new LoadFlatDaclCmd("LoadFlatDacl",
		Tango::DEV_ULONG, Tango::DEV_VOID,
		"value to set",
		"",
		Tango::EXPERT));
	command_list.push_back(new LoadDefaultConfigGCmd("LoadDefaultConfigG",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::EXPERT));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		XPADClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum XPADClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		XPADClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum XPADClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		XPADClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum XPADClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		XPADClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void XPADClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new XPAD(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: XPADClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void XPADClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : exposure
	exposureAttrib	*exposure = new exposureAttrib();
	att_list.push_back(exposure);

	//	Attribute : gateNumber
	gateNumberAttrib	*gate_number = new gateNumberAttrib();
	att_list.push_back(gate_number);

	//	Attribute : gateMode
	gateModeAttrib	*gate_mode = new gateModeAttrib();
	Tango::UserDefaultAttrProp	gate_mode_prop;
	gate_mode_prop.set_description("the gate mode\n0: INTERNAL_GATE\n1: EXTERNAL_GATE");
	gate_mode->set_default_properties(gate_mode_prop);
	att_list.push_back(gate_mode);

	//	Attribute : image
	imageAttrib	*image = new imageAttrib();
	Tango::UserDefaultAttrProp	image_prop;
	image_prop.set_label("image");
	image_prop.set_description("the computed image");
	image->set_default_properties(image_prop);
	att_list.push_back(image);

	//	Attribute : dacl
	daclAttrib	*dacl = new daclAttrib();
	Tango::UserDefaultAttrProp	dacl_prop;
	dacl_prop.set_label("dacl");
	dacl_prop.set_description("The dacl xpad parameters");
	dacl->set_default_properties(dacl_prop);
	dacl->set_disp_level(Tango::EXPERT);
	att_list.push_back(dacl);

	//	Attribute : ithl
	ithlAttrib	*ithl = new ithlAttrib();
	Tango::UserDefaultAttrProp	ithl_prop;
	ithl_prop.set_label("ithl parameters");
	ithl_prop.set_description("The ithl xpad parameters");
	ithl->set_default_properties(ithl_prop);
	ithl->set_disp_level(Tango::EXPERT);
	att_list.push_back(ithl);

	//	Attribute : Itune
	ItuneAttrib	*itune = new ItuneAttrib();
	Tango::UserDefaultAttrProp	itune_prop;
	itune_prop.set_label("itune parameters");
	itune_prop.set_description("The itune xpad parameters");
	itune->set_default_properties(itune_prop);
	itune->set_disp_level(Tango::EXPERT);
	att_list.push_back(itune);

	//	Attribute : Imfp
	ImfpAttrib	*imfp = new ImfpAttrib();
	Tango::UserDefaultAttrProp	imfp_prop;
	imfp_prop.set_label("imfp parameters");
	imfp_prop.set_description("The imfp xpad parameters");
	imfp->set_default_properties(imfp_prop);
	imfp->set_disp_level(Tango::EXPERT);
	att_list.push_back(imfp);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		XPADClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void XPADClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	XPADClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void XPADClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "Type";
	prop_desc = "type of the XPAD connected to the device.\nvalues: \"500k\", \"single_module\", \"QuadCdTe\"";
	prop_def  = "500k";
	vect_data.clear();
	vect_data.push_back("500k");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		XPADClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void XPADClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Image treatment necessary to adjust a beamline during experiments coherences.");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("Class used to acquire a number of images and to apply a calculation on each.");
	str_desc.push_back("The returned result is the accumulation of these processed images.");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs or svn location
	string	filename(classname);
	filename += "Class.cpp";
	
	// Create a string with the class ID to
	// get the string into the binary
	string	class_id(ClassId);
	
	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}
	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
