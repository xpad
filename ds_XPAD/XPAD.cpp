//===================================================================
//
//	The following table gives the correspondence
//	between commands and method name.
//
//  Command name        |  Method name
//	----------------------------------------
//  State               |  dev_state()
//  Status              |  dev_status()
//  Start               |  start()
//  Stop                |  stop()
//  LoadCalibration     |  load_calibration()
//  Reset               |  reset()
//  Calibrate           |  calibrate()
//  LoadFlatDacl        |  load_flat_dacl()
//  LoadDefaultConfigG  |  load_default_config_g()
//
//===================================================================


#include <tango.h>
#include <XPAD.h>
#include <XPADClass.h>
#include <PogoHelper.h>

namespace XPAD_ns
{

	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::XPAD(string &s)
	// 
	// description : 	constructor for simulated XPAD
	//
	// in : - cl : Pointer to the DeviceClass object
	//      - s : Device name 
	//
	//-----------------------------------------------------------------------------
	XPAD::XPAD(Tango::DeviceClass *cl,string &s)
		:Tango::Device_4Impl(cl,s.c_str())
		{
			init_device();
		}

	XPAD::XPAD(Tango::DeviceClass *cl,const char *s)
		:Tango::Device_4Impl(cl,s)
		{
			init_device();
		}

	XPAD::XPAD(Tango::DeviceClass *cl,const char *s,const char *d)
		:Tango::Device_4Impl(cl,s,d)
		{
			init_device();
		}
	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::delete_device()
	// 
	// description : 	will be called at device destruction or at init command.
	//
	//-----------------------------------------------------------------------------
	void XPAD::delete_device()
	{
		//	Delete device's allocated object

		//- release the task
		if (_dev_task) {
			_dev_task->exit();
			_dev_task = 0;
		}
	}

	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::init_device()
	// 
	// description : 	will be called at device initialization.
	//
	//-----------------------------------------------------------------------------
	void XPAD::init_device()
	{
		INFO_STREAM << "XPAD::XPAD() create device " << device_name << endl;

		// Initialise variables to default values
		//--------------------------------------------
		_dev_task = 0;

		this->get_device_property();
		
		attr_exposure_write = 1000;
		attr_gateNumber_write = 1;
		attr_gateMode_write = 0;

		try
		{
			DEBUG_STREAM << "allocating the task" << std::endl;
			_dev_task = new XPADTask(this, type);
			if (!_dev_task)
				throw std::bad_alloc();
			//- start the task
			DEBUG_STREAM << "start the task" << std::endl;
			_dev_task->go();
			this->set_state(_dev_task->get_xpad_state());
			this->set_status(_dev_task->get_xpad_status());
		}
		catch (std::bad_alloc const &)
		{
			ERROR_STREAM << "allocation failed" << std::endl;
			this->set_status("initialization failed  - \
					task allocation failed");
			this->set_state(Tango::FAULT);
			this->delete_device();
			return;
		}
		catch (Tango::DevFailed const & df)
		{
			ERROR_STREAM << df << std::endl;
			this->set_status("initialization failed");
			this->set_state(Tango::FAULT);
			this->delete_device();
			return;
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - \
				unknown error" << std::endl;
			this->set_status("initialization failed [unknown error]");
			this->set_state(Tango::FAULT);
			this->delete_device();
			return;
		}

		DEBUG_STREAM << "device successfully initialized" << std::endl;
	}


	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::get_device_property()
	// 
	// description : 	Read the device properties from database.
	//
	//-----------------------------------------------------------------------------
	void XPAD::get_device_property()
	{
		//	Initialize your default values here (if not done with  POGO).
		//------------------------------------------------------------------

		//	Read device properties from database.(Automatic code generation)
		//------------------------------------------------------------------
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("Type"));

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_device()->get_property(dev_prop);
	Tango::DbDatum	def_prop, cl_prop;
	XPADClass	*ds_class =
		(static_cast<XPADClass *>(get_device_class()));
	int	i = -1;

	//	Try to initialize Type from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  type;
	else {
		//	Try to initialize Type from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  type;
	}
	//	And try to extract Type value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  type;



		//	End of Automatic code generation
		//------------------------------------------------------------------

		//-TODO create default Proxy !!

	}
	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::always_executed_hook()
	// 
	// description : 	method always executed before any command is executed
	//
	//-----------------------------------------------------------------------------
	void XPAD::always_executed_hook()
	{
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::read_attr_hardware
	// 
	// description : 	Hardware acquisition for attributes.
	//
	//-----------------------------------------------------------------------------
	void XPAD::read_attr_hardware(vector<long> &attr_list)
	{
		DEBUG_STREAM << "XPAD::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
		//	Add your own code here
	}
//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_Itune
// 
// description : 	Extract real attribute values for Itune acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_Itune(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_Itune(Tango::Attribute &attr) entering... "<< endl;

	XRaysImage *img = _dev_task->get_itune();
	if(img) {
		Tango::DevLong *data = (Tango::DevLong *)img->data;
		attr.set_value(data, img->width, img->height);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_Itune
// 
// description : 	Write Itune attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_Itune(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_Itune(Tango::WAttribute &attr) entering... "<< endl;

	const Tango::DevLong *itune;
	attr.get_write_value(itune);

	XPADSetIthlConfig config;

	config.img = itune;
	config.width = attr.get_w_dim_x();
	config.height = attr.get_w_dim_y();

	_dev_task->set_itune(config);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_Imfp
// 
// description : 	Extract real attribute values for Imfp acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_Imfp(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_Imfp(Tango::Attribute &attr) entering... "<< endl;

	XRaysImage *img = _dev_task->get_imfp();
	if(img) {
		Tango::DevLong *data = (Tango::DevLong *)img->data;
		attr.set_value(data, img->width, img->height);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_Imfp
// 
// description : 	Write Imfp attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_Imfp(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_Imfp(Tango::WAttribute &attr) entering... "<< endl;

	const Tango::DevLong *imfp;
	attr.get_write_value(imfp);

	XPADSetIthlConfig config;

	config.img = imfp;
	config.width = attr.get_w_dim_x();
	config.height = attr.get_w_dim_y();

	_dev_task->set_imfp(config);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_ithl
// 
// description : 	Extract real attribute values for ithl acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_ithl(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_ithl(Tango::Attribute &attr) entering... "<< endl;

	XRaysImage *img = _dev_task->get_ithl();
	if(img) {
		Tango::DevLong *data = (Tango::DevLong *)img->data;
		attr.set_value(data, img->width, img->height);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_ithl
// 
// description : 	Write ithl attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_ithl(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_ithl(Tango::WAttribute &attr) entering... "<< endl;
	const Tango::DevLong *ithl;
	attr.get_write_value(ithl);

	XPADSetIthlConfig config;

	config.img = ithl;
	config.width = attr.get_w_dim_x();
	config.height = attr.get_w_dim_y();

	_dev_task->set_ithl(config);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_dacl
// 
// description : 	Extract real attribute values for dacl acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_dacl(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_dacl(Tango::Attribute &attr) entering... "<< endl;

	XRaysImage *img = _dev_task->get_dacl();
	if(img) {
		Tango::DevLong *data = (Tango::DevLong *)img->data;
		attr.set_value(data, img->width, img->height);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_dacl
// 
// description : 	Write dacl attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_dacl(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_dacl(Tango::WAttribute &attr) entering... "<< endl;
	const Tango::DevLong *dacl;
	attr.get_write_value(dacl);

	XPADSetDaclConfig config;

	config.img = dacl;
	config.width = attr.get_w_dim_x();
	config.height = attr.get_w_dim_y();

	_dev_task->set_dacl(config);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_gateMode
// 
// description : 	Extract real attribute values for gateMode acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_gateMode(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_gateMode(Tango::Attribute &attr) entering... "<< endl;
	attr.set_value(&attr_gateMode_write);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_gateMode
// 
// description : 	Write gateMode attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_gateMode(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_gateMode(Tango::WAttribute &attr) entering... "<< endl;
	attr.get_write_value(attr_gateMode_write);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_gateNumber
// 
// description : 	Extract real attribute values for gateNumber acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_gateNumber(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_gateNumber(Tango::Attribute &attr) entering... "<< endl;
	attr.set_value(&attr_gateNumber_write);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_gateNumber
// 
// description : 	Write gateNumber attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_gateNumber(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_gateNumber(Tango::WAttribute &attr) entering... "<< endl;
	attr.get_write_value(attr_gateNumber_write);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::read_exposure
// 
// description : 	Extract real attribute values for exposure acquisition result.
//
//-----------------------------------------------------------------------------
void XPAD::read_exposure(Tango::Attribute &attr)
{
	DEBUG_STREAM << "XPAD::read_exposure(Tango::Attribute &attr) entering... "<< endl;
	attr.set_value(&attr_exposure_write);
}

//+----------------------------------------------------------------------------
//
// method : 		XPAD::write_exposure
// 
// description : 	Write exposure attribute values to hardware.
//
//-----------------------------------------------------------------------------
void XPAD::write_exposure(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "XPAD::write_exposure(Tango::WAttribute &attr) entering... "<< endl;
	attr.get_write_value(attr_exposure_write);
}

	//+----------------------------------------------------------------------------
	//
	// method : 		XPAD::read_image
	// 
	// description : 	Extract real attribute values for image acquisition result.
	//
	//-----------------------------------------------------------------------------
	void XPAD::read_image(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "XPAD::read_image(Tango::Attribute &attr) entering... "<< endl;
		XRaysImage *img = _dev_task->get_image();
		if(img) {
			Tango::DevLong *data = (Tango::DevLong *)img->data;
			attr.set_value(data, img->width, img->height);
		}
	}


	//+------------------------------------------------------------------
/**
 *	method:	XPAD::start
 *
 *	description:	method to execute "Start"
 *	Start the images treatment
 *
 *
 */
//+------------------------------------------------------------------
	void XPAD::start()
	{
		DEBUG_STREAM << "XPAD::start(): entering... !" << endl;

		//	Add your own code to control device here
		XPADStartConfig config;

		if(attr_exposure_write < 0
		   || attr_gateMode_write < 0
		   || attr_gateNumber_write < 0){
			ERROR_STREAM << "DATA_OUT_OF_RANGE must set all the values before starting an acquisition, exposure, gate mode and gate number." << endl;
			Tango::Except::throw_exception (
				static_cast<const char *>("DATA_OUT_OF_RANGE"),
   				static_cast<const char *>("must must set all the values before starting an acquisition, exposure, gate mode and gate number. "),
   				static_cast<const char *>("XPAD::start"));
		}

		config.exposure = attr_exposure_write;
		config.gate_number = attr_gateNumber_write;
		config.gate_mode = attr_gateMode_write;
		_dev_task->start(config);
	}

	//+------------------------------------------------------------------
/**
 *	method:	XPAD::stop
 *
 *	description:	method to execute "Stop"
 *	Stop the images treatment
 *
 *
 */
//+------------------------------------------------------------------
	void XPAD::stop()
	{
		DEBUG_STREAM << "XPAD::stop(): entering... !" << endl;

		//	Add your own code to control device here
		if(_dev_task)
			_dev_task->stop();
	}

//+------------------------------------------------------------------
/**
 *	method:	XPAD::load_calibration
 *
 *	description:	method to execute "LoadCalibration"
 *
 * @param	argin	
 *
 */
//+------------------------------------------------------------------
void XPAD::load_calibration(Tango::DevString argin)
{
	DEBUG_STREAM << "XPAD::load_calibration(): entering... !" << endl;

	//	Add your own code to control device here
	if(_dev_task)
		_dev_task->load_calibration(argin);
}


//+------------------------------------------------------------------
/**
 *	method:	XPAD::reset
 *
 *	description:	method to execute "Reset"
 *	Reset the hub and the modules of the xpad
 *
 *
 */
//+------------------------------------------------------------------
void XPAD::reset()
{
	DEBUG_STREAM << "XPAD::reset(): entering... !" << endl;

	//	Add your own code to control device here
	if(_dev_task)
		_dev_task->reset();
}




//+------------------------------------------------------------------
/**
 *	method:	XPAD::calibrate
 *
 *	description:	method to execute "Calibrate"
 *	do a calibration à la marseillaise :)
 *
 * @param	argin	parameters of the calibration\nstring path // useless for now\nlong: ith_min, ith_max, algo, noise, decalage\n
 *
 */
//+------------------------------------------------------------------
void XPAD::calibrate(const Tango::DevVarLongStringArray *argin)
{
	DEBUG_STREAM << "XPAD::calibrate(): entering... !" << endl;

	//	Add your own code to control device here
	// validate the argin
	if(!_dev_task || argin->lvalue.length() != 5 || argin->svalue.length() != 1)
		return;

	XpadCalibrationConfig config;
	config.ith_min = argin->lvalue[0];
	config.ith_max = argin->lvalue[1];
	config.walgo = (XpadDACLCalibrationAlgoType)argin->lvalue[2];
	config.noise_accepted = argin->lvalue[3];
	config.dec_dacl_val = argin->lvalue[4];

	config.path = argin->svalue[0];

	_dev_task->calibration(config);
}



//+------------------------------------------------------------------
/**
 *	method:	XPAD::dev_state
 *
 *	description:	method to execute "State"
 *	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *
 * @return	State Code
 *
 */
//+------------------------------------------------------------------
Tango::DevState XPAD::dev_state()
{
	Tango::DevState	argout = DeviceImpl::dev_state();
	DEBUG_STREAM << "XPAD::dev_state(): entering... !" << endl;

	//	Add your own code to control device here
	argout = _dev_task->get_xpad_state();
	set_state(argout);
	return argout;
}

//+------------------------------------------------------------------
/**
 *	method:	XPAD::dev_status
 *
 *	description:	method to execute "Status"
 *	This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *
 * @return	Status description
 *
 */
//+------------------------------------------------------------------
Tango::ConstDevString XPAD::dev_status()
{
	Tango::ConstDevString	argout = DeviceImpl::dev_status();
	DEBUG_STREAM << "XPAD::dev_status(): entering... !" << endl;

	//	Add your own code to control device here

	argout = _dev_task->get_xpad_status().c_str();
	set_status(argout);
	return argout;
}



//+------------------------------------------------------------------
/**
 *	method:	XPAD::load_flat_dacl
 *
 *	description:	method to execute "LoadFlatDacl"
 *	send to the xpad the default general configurations.
 *	ithl, ithune, imfp, ...
 *
 * @param	argin	value to set
 *
 */
//+------------------------------------------------------------------
void XPAD::load_flat_dacl(Tango::DevULong argin)
{
	DEBUG_STREAM << "XPAD::load_flat_dacl(): entering... !" << endl;

	//	Add your own code to control device here
	if(_dev_task)
		_dev_task->load_flat_dacl(argin);

}

//+------------------------------------------------------------------
/**
 *	method:	XPAD::load_default_config_g
 *
 *	description:	method to execute "LoadDefaultConfigG"
 *	send to the xpad the default general configurations.
 *	ithl, ithune, imfp, ...
 *
 *
 */
//+------------------------------------------------------------------
void XPAD::load_default_config_g()
{
	DEBUG_STREAM << "XPAD::load_default_config_g(): entering... !" << endl;

	//	Add your own code to control device here
	if(_dev_task)
		_dev_task->load_default_config_g();

}


}	//	namespace
