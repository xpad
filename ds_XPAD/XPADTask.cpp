#include <sstream>
#include <yat4tango/ExceptionHelper.h>
#include <Util.h>
#include <XPADTask.h>

#include "xpad.h"

#define COMMAND_TIMEOUT_MS 3000

XPADTask::XPADTask(Tango::DeviceImpl *device, const std::string &type) :
	Tango::LogAdapter(device)
{
	int width;
	int height;

	this->enable_timeout_msg(true);
	this->set_timeout_msg_period(5000000);
	this->enable_periodic_msg(false);

	_host_dev = device;

	/* compute the height and width dimensions */
	/* using the detector type */
	if(!strcasecmp(type.c_str(), "single_module")){
		width = 560;
		height = 120;
		this->_type = XPAD_TYPE_SINGLE_MODULE;
	}else if(!strcasecmp(type.c_str(), "QuadCdTe")){
		width = 160;
		height = 240;
		this->_type = XPAD_TYPE_QUAD_CDTE;
	}else{ /* default and 500k */
		width = 560;
		height = 960;
		this->_type = XPAD_TYPE_500K;
	}

	_image = xrays_image_new(XRAYS_IMAGE_INT, width, height, 1);
	_dacl = NULL;
	_ithl = NULL;
	_itune = NULL;
	_imfp = NULL;

	_xpad = xpadConfigDefault;
	_xpad.mode = MODE_32b;
}

void XPADTask::exit(void) throw (yat::Exception)
{
	yat::Task::exit();
}

XPADTask::~XPADTask(void)
{
	if(_image) {
		xrays_image_free(_image);
		_image = NULL;
	}
	if(_dacl){
		xrays_image_free(_dacl);
		_dacl = NULL;
	}
	if(_ithl){
		xrays_image_free(_ithl);
		_ithl = NULL;
	}
	if(_itune){
		xrays_image_free(_itune);
		_itune = NULL;
	}
	if(_imfp){
		xrays_image_free(_imfp);
		_imfp = NULL;
	}
}

//! start the droplet process
void XPADTask::start(XPADStartConfig const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "Acquisition request received";
	post_msg( *this, TASK_XPAD_START, COMMAND_TIMEOUT_MS, false, c );
}

//! start the droplet process
void XPADTask::stop(void)
{
	_state = Tango::RUNNING;
	_status = "Stop requeste received -- transformed into RESET";
	// for now the stop command == reset command.
	post_msg( *this, TASK_XPAD_RESET, COMMAND_TIMEOUT_MS, false);
}

//! load calibration process
void XPADTask::load_calibration(std::string const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "Load calibration requeste received";
	post_msg( *this, TASK_XPAD_LOAD_CALIBRATION, COMMAND_TIMEOUT_MS, false, c);
}

//! reset the xpad
void XPADTask::reset(void) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "Stop requeste received -- transformed into RESET";
	post_msg( *this, TASK_XPAD_RESET, COMMAND_TIMEOUT_MS, false);
}

//! calibration
void XPADTask::calibration(XpadCalibrationConfig const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "Calibration requeste received";
	post_msg(*this, TASK_XPAD_CALIBRATION, COMMAND_TIMEOUT_MS, false, c);
}

//- set ithl
void XPADTask::set_ithl(XPADSetIthlConfig const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "set ithl request received";

	// convert the data from long -> int
	memcpy(_xpad.ithl_val, c.img, sizeof(_xpad.ithl_val));
	post_msg( *this, TASK_XPAD_SET_ITHL, COMMAND_TIMEOUT_MS, false, c);
}

//- set itune
void XPADTask::set_itune(XPADSetIthlConfig const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "set itune request received";

	// convert the data from long -> int
	memcpy(_xpad.itune_val, c.img, sizeof(_xpad.itune_val));
	post_msg( *this, TASK_XPAD_SET_ITUNE, COMMAND_TIMEOUT_MS, false, c);
}

//- set imfp
void XPADTask::set_imfp(XPADSetIthlConfig const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "set imfp request received";

	// convert the data from long -> int
	memcpy(_xpad.imfp_val, c.img, sizeof(_xpad.imfp_val));
	post_msg( *this, TASK_XPAD_SET_IMFP, COMMAND_TIMEOUT_MS, false, c);
}

//- get dacl
void XPADTask::read_dacl(void) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "get dacl request received";	
	post_msg( *this, TASK_XPAD_GET_DACL, COMMAND_TIMEOUT_MS, false);
}

//- set dacl
void XPADTask::set_dacl(XPADSetDaclConfig const & c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "set dacl request received";
	memcpy(_xpad.dacl, c.img, sizeof(_xpad.dacl));

	post_msg( *this, TASK_XPAD_SET_DACL, COMMAND_TIMEOUT_MS, false, c);
}

//- upload a flat dacl to the xpad on all modules and chip.
void XPADTask::load_flat_dacl(Tango::DevULong c) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "load flat dacl request received";

	post_msg( *this, TASK_XPAD_LOAD_FLAT_DACL, COMMAND_TIMEOUT_MS, false, c);
}

//- send the default config g to the xpad
void XPADTask::load_default_config_g(void) throw (yat::Exception)
{
	_state = Tango::RUNNING;
	_status = "Load Default ConfigG requeste received";
	post_msg(*this, TASK_XPAD_LOAD_DEFAULT_CONFIG_G, COMMAND_TIMEOUT_MS, false);
}

void XPADTask::handle_message(yat::Message & msg) throw (yat::Exception)
{
	try
	{
		switch(msg.type()) {
		case yat::TASK_INIT:
			DEBUG_STREAM << "XPADTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			{ //- enter critical section
				yat::AutoMutex<> guard(this->m_lock);
				
				this->on_init();
				//this->on_read_dacl();
			} //- leave critical section
			break;
		case TASK_XPAD_START:
		{
			XPADStartConfig *config = NULL;
			msg.detach_data(config);
			if (config){
				this->on_start(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_LOAD_CALIBRATION:
		{
			std::string *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_load_calibration(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_RESET:
		{
			this->on_reset();
		}
		break;
		case TASK_XPAD_CALIBRATION:
		{
			XpadCalibrationConfig *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_calibration(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_SET_ITHL:
		{
			XPADSetIthlConfig *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_set_ithl(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_SET_ITUNE:
		{
			XPADSetIthlConfig *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_set_itune(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_SET_IMFP:
		{
			XPADSetIthlConfig *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_set_imfp(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_GET_DACL:
		{
			this->on_read_dacl();
		}
		break;
		case TASK_XPAD_SET_DACL:
		{
			XPADSetDaclConfig *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_set_dacl(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_LOAD_FLAT_DACL:
		{
			Tango::DevULong *config = NULL;
			msg.detach_data(config);
			if(config){
				this->on_load_flat_dacl(*config);
				delete config;
			}
		}
		break;
		case TASK_XPAD_LOAD_DEFAULT_CONFIG_G:
		{
			this->on_load_default_config_g();
		}
		break;

		case yat::TASK_EXIT:
		{
			DEBUG_STREAM << "XPAD::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
		}
		break;
		}
	}
	catch ( yat::Exception &ex)
	{
	}
}

void XPADTask::on_init(void) throw (yat::Exception)
{
	xpad_init();
	xpad_is_ready(&_xpad);
	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_start(XPADStartConfig const & config) throw (yat::Exception)
{
	_status = "Acquiring image";

	// process all images and populate droplet
	_xpad.acq.integration_time = config.exposure;
	_xpad.acq.Gate_Mode = config.gate_mode;
	_xpad.acq.nb_image = config.gate_number;
	xpad_acq(&_xpad);
  
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		/* copy the xpad data into the _image attribute */
		/* this copy depends on the detector type */
		switch (this->_type){
		case XPAD_TYPE_QUAD_CDTE:
			int i, j;
			int *dst;
			int *src;

			/* x x A B C D x -> B C */
			/*                  Ɐ D */

			/* chip  Ɐ D */
			dst = (int *)_image->data + XPAD_CHIP_HEIGHT * _image->width;
			src = (int *)&_xpad.image[0] + 2 * XPAD_CHIP_WIDTH;
			for(j=0; j<XPAD_CHIP_HEIGHT; ++j)
				for(i=0; i<XPAD_CHIP_WIDTH; ++i){
					int idx, idx2;
					/* compute the A transposition */
					idx = i + j * _image->width;
					idx2 = XPAD_WIDTH * (XPAD_CHIP_HEIGHT - 1 - j) + XPAD_CHIP_WIDTH - 1 - i;
					dst[idx] = src[idx2];

					/* compute the D transposition */
					dst[idx + XPAD_CHIP_WIDTH] = src[idx2 + 3*XPAD_CHIP_WIDTH];
				}
			
			/* B C */
			dst = (int *)_image->data;
			src = (int *)&_xpad.image + XPAD_CHIP_WIDTH; /* to fix */
			for(i=0; i<XPAD_CHIP_HEIGHT; ++i){
				memcpy(dst, src, sizeof(int) * _image->width);
				dst += _image->width;
				src += XPAD_WIDTH;
			}
			break;
		case XPAD_TYPE_SINGLE_MODULE:
		case XPAD_TYPE_500K:
		default:
			memcpy(_image->data, _xpad.image,
			       sizeof(int) * _image->width * _image->height);
			break;
		}
	} //- leave critical section
	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_load_calibration(std::string const & c) throw (yat::Exception)
{
	_status = "Loading Calibration";

	// process all images and populate droplet
	xpad_load_calibration_new(&_xpad, "calibrations", c.c_str());

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_reset(void) throw (yat::Exception)
{
	_status = "Reseting XPAD HUB and modules";

	xpad_reset_hub();
	xpad_reset_modules();
	xpad_is_ready(&_xpad);
	xpad_config_read_dacl(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_calibration(XpadCalibrationConfig const & config) throw (yat::Exception)
{
	_status = "Calibrating XPAD";

	xpad_config_calibrate(&_xpad, &config);
	xpad_config_read_dacl(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_set_ithl(XPADSetIthlConfig const & config) throw (yat::Exception)
{
	size_t i;
	size_t j;

	_status = "Setting the ithl";

	// now store the ithl at the right place.
	for(i=0; i< MAXMODULE; ++i)
		for(j=0; j<MAXCHIP; ++j)
			_xpad.ith_calib[i].val[j] = _xpad.ithl_val[i][j];
	
	xpad_config_send_ithl(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_set_itune(XPADSetIthlConfig const & config) throw (yat::Exception)
{
	size_t i;
	size_t j;

	_status = "Setting the itune";

	xpad_config_send_itune(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_set_imfp(XPADSetIthlConfig const & config) throw (yat::Exception)
{
	size_t i;
	size_t j;

	_status = "Setting the imfp";

	xpad_config_send_imfp(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_read_dacl(void) throw (yat::Exception)
{
	_status = "Reading the dacl";
	xpad_config_read_dacl(&_xpad);

/*
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		if(!_dacl)
			_dacl = xrays_image_new(XRAYS_IMAGE_INT,
						XPAD_WIDTH, XPAD_HEIGHT, 1);
		memcpy(_dacl->data, _xpad.dacl, sizeof(_xpad.dacl));
	} //- leave critical section
*/
	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_set_dacl(XPADSetDaclConfig const & config) throw (yat::Exception)
{
	_status = "Setting the dacl";

	xpad_config_send_dacl(&_xpad);
	//xpad_config_read_dacl(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_load_flat_dacl(Tango::DevULong dacl) throw (yat::Exception)
{
	_status = "Uploading flat dacl";

	xpad_config_send_dacl_flat(&_xpad, dacl);
	/* xpad_config_read_dacl(&_xpad); */ /* Not working for now */
	for(int i=0; i<XPAD_HEIGHT; ++i)
		for(int j=0;j<XPAD_WIDTH; ++j)
			_xpad.dacl[i][j] = dacl;

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

void XPADTask::on_load_default_config_g(void) throw (yat::Exception)
{
	_status = "Uploading Default ConfigG";

	xpad_config_init_config_g(&_xpad);
	xpad_config_send_config_g(&_xpad);

	_state = Tango::STANDBY;
	_status = "Device up and ready";
}

XRaysImage *XPADTask::get_image(void)
{
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		return this->get_image_i();
	} //- leave critical section
}

XRaysImage *XPADTask::get_image_i(void)
{
	return _image;
}

XRaysImage *XPADTask::get_ithl(void)
{
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		return this->get_ithl_i();
	} //- leave critical section
}

XRaysImage *XPADTask::get_ithl_i(void)
{
	if(!_ithl)
		_ithl = xrays_image_new(XRAYS_IMAGE_INT, 7, 8, 1);

	memcpy(_ithl->data, _xpad.ithl_val, sizeof(_xpad.ithl_val));

	return _ithl;
}
XRaysImage *XPADTask::get_itune(void)
{
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		return this->get_itune_i();
	} //- leave critical section
}

XRaysImage *XPADTask::get_itune_i(void)
{
	if(!_itune)
		_itune = xrays_image_new(XRAYS_IMAGE_INT, 7, 8, 1);

	memcpy(_itune->data, _xpad.itune_val, sizeof(_xpad.itune_val));

	return _itune;
}
XRaysImage *XPADTask::get_imfp(void)
{
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		return this->get_imfp_i();
	} //- leave critical section
}

XRaysImage *XPADTask::get_imfp_i(void)
{
	if(!_imfp)
		_imfp = xrays_image_new(XRAYS_IMAGE_INT, 7, 8, 1);

	memcpy(_imfp->data, _xpad.imfp_val, sizeof(_xpad.imfp_val));

	return _imfp;
}

XRaysImage *XPADTask::get_dacl(void)
{
	{ //- enter critical section
		yat::AutoMutex<> guard(this->m_lock);
		return this->get_dacl_i();
	} //- leave critical section
}

XRaysImage *XPADTask::get_dacl_i(void)
{
	if(!_dacl)
		_dacl = xrays_image_new(XRAYS_IMAGE_INT, XPAD_WIDTH, XPAD_HEIGHT, 1);

	memcpy(_dacl->data, _xpad.dacl, sizeof(_xpad.dacl));

	return _dacl;
}
